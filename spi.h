/**
 * @file spi.h
 * Low layer for a comunication with TCD50 using a spi interface.
 *
 * Copyright (C) 2008 UPEK Inc.
 *
 * @author Martin Krahulik
 */


#ifndef _SPI_H_
#define _SPI_H_

#ifndef DOXYGEN_PASS //tfmtypes.h makes problems to doxygen parser
    #include "types.h"
    #include "tfmtypes.h"
#endif

// ---- link packet sizes ------------------

#define SPI_PACKET_HEADER_SIZE       1 ///<Size of a miniframe packet header.
#define SPI_PACKET_DATA_SIZE         8 ///<Size of a miniframe packet data part.
#define SPI_PACKET_SIZE              (SPI_PACKET_HEADER_SIZE + SPI_PACKET_DATA_SIZE) ///<Size of a miniframe packet.

// ---- functions --------------------------
/** Function initializes an spi layer.
**/ 
PT_STATUS SpiInitialize(void);

/** Spi layer destructor.
**/ 
void SpiDestroy(void);

/** Function sends a miniframe(9-bytes) over a spi bus. It set a CS signal to high, starts a clock, transmits a miniframe, stops a clock, and sets a CS signal to low.
* @param pSendBuffer Buffer containing miniframe data sent on a SPI bus (input, size of SPI_PACKET_SIZE).
* @param pReceiveBuffer Buffer, where data comming on a SPI bus are stored (output, size of SPI_PACKET_SIZE).
* @return PT_STATUS_OK.
**/
PT_STATUS SpiSendAndReceiveMiniframe(IN void *pSendBuffer, OUT void *pReceiveBuffer);

/** Function tests, if an AWAKE signal is low. If so, a device is considered to be awake.
* @return TRUE, if awake; FALSE otherwise.
**/
bool8 SpiIsAwake(void);


/** Function produces a short (approx. 2ms) impulse on a CS line causing a hardware wake up of a TCD device.
**/
void SpiWakeUp(void);

#endif //_SPI_H_
