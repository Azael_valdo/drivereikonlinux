/**
 * @file uart.c
 *
 * The lowest-level interface to the UART.
 * This file has to be replaced by the real implementation for given target HW platform.
 * This one concrete implementation is for Windows.
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */


#include    "uart.h"
#include    <windows.h>


//========================================================================
//      Constants, macros and types
//========================================================================

extern char gCommPort[];

// COM port being used on PC
#define     COMM_PORT           gCommPort

// The default (initial) comm speed after reset
#define     DEFAULT_COMM_SPEED  9600

// The max. inter-byte delay in milliseconds (100 msec according to the specification)
#define     MAX_INTERBYTE_DELAY 100

// The length of the BREAK signal (20-100 msec according to the specification)
#define     BREAK_DELAY         20



//========================================================================
//      Global variables
//========================================================================

static HANDLE ghPort    = INVALID_HANDLE_VALUE;


//========================================================================
//      Internal functions
//========================================================================

/**
 * Clear Error State from previous UART function
 *
 * @param stat Pass-through status -- the same value will be returned as the result
 * @return Returns the value "stat", to make easier error-returning statements.
 */
static COMM_STATUS UartClearError (COMM_STATUS stat)
{
    DWORD Errors;
    COMSTAT ComStat;

    ClearCommError(ghPort, &Errors, &ComStat);

    return stat;
}


//========================================================================
//      Exported functions
//========================================================================

/**
 *	Open the UART
 */
COMM_STATUS PT_UartOpen (void)
{
    if (ghPort != INVALID_HANDLE_VALUE) return COMM_STATUS_ERROR;

    ghPort = CreateFileA (COMM_PORT, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    if (ghPort == NULL) ghPort=INVALID_HANDLE_VALUE;
    if (ghPort == INVALID_HANDLE_VALUE)
    {
        uint32 lastError = GetLastError();
        printf("LastError:%d", lastError); 
        return COMM_STATUS_ERROR;
    }

    return COMM_STATUS_OK;
}


/**
 *	Close the UART
 */
COMM_STATUS PT_UartClose (void)
{
    if (ghPort != INVALID_HANDLE_VALUE) {
        EscapeCommFunction (ghPort, CLRRTS);
        EscapeCommFunction (ghPort, CLRDTR);
        CloseHandle (ghPort);
        ghPort = INVALID_HANDLE_VALUE;
    }

    return COMM_STATUS_OK;    
}


/**
 *	Reset the UART to the default parameters
 */
COMM_STATUS PT_UartReset (void)
{
    DCB Dcb;
    COMMTIMEOUTS CommTimeouts;
    
    if (ghPort == INVALID_HANDLE_VALUE) return COMM_STATUS_ERROR;

    //
    // Setup the port
    //

    // set queues at first
    if (! SetupComm(ghPort, 8192, 8192)) return UartClearError (COMM_STATUS_ERROR);
    if (! GetCommState(ghPort, &Dcb)) return UartClearError (COMM_STATUS_ERROR);

    // set default values
    Dcb.BaudRate = DEFAULT_COMM_SPEED;
    Dcb.fBinary = TRUE;
    Dcb.fParity = FALSE;
    Dcb.fOutxCtsFlow = FALSE;
    Dcb.fOutxDsrFlow = FALSE;
    Dcb.fDtrControl = DTR_CONTROL_ENABLE;
    Dcb.fDsrSensitivity = FALSE;
    Dcb.fTXContinueOnXoff = FALSE;
    Dcb.fOutX = TRUE;
    Dcb.fInX = FALSE;                       // TRUE causes XON send when write buffer becomes empty. Hmm...
    Dcb.fErrorChar = FALSE;
    Dcb.fNull = FALSE;
    Dcb.fRtsControl = RTS_CONTROL_ENABLE;   // Or RTS_CONTROL_HANDSHAKE to enable the RTS handshake
                                            // However, on Windows this has no sense as the queue is large enough
    Dcb.fAbortOnError = TRUE;
    Dcb.XonLim = 4096;
    Dcb.XoffLim = 8192;
    Dcb.ByteSize = 8;
    Dcb.Parity = NOPARITY;
    Dcb.StopBits = ONESTOPBIT;
    Dcb.XonChar = 0x11;
    Dcb.XoffChar = 0x13;
    // Dcb.ErrorChar;
    // Dcb.EofChar;
    // Dcb.EvtChar;
    if (! SetCommState(ghPort, &Dcb)) return UartClearError (COMM_STATUS_ERROR);

    // set timeouts 
    if (! GetCommTimeouts(ghPort, &CommTimeouts)) return UartClearError (COMM_STATUS_ERROR);
    CommTimeouts.ReadIntervalTimeout = 0;
    CommTimeouts.ReadTotalTimeoutMultiplier = 0;
    CommTimeouts.ReadTotalTimeoutConstant = MAX_INTERBYTE_DELAY;     // 100 msec is the single byte timeout
    CommTimeouts.WriteTotalTimeoutMultiplier = 0;
    CommTimeouts.WriteTotalTimeoutConstant = 0;
    if (! SetCommTimeouts(ghPort, &CommTimeouts)) return UartClearError (COMM_STATUS_ERROR);

    // finally purge everything
    if (! PurgeComm(ghPort, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR)) return UartClearError (COMM_STATUS_ERROR);

    // Sucess
    return COMM_STATUS_OK;
}


/**
 *	Send the HW Break signal
 */
COMM_STATUS PT_UartBreak (void)
{
    if (! SetCommBreak (ghPort)) return UartClearError (COMM_STATUS_ERROR);
    PT_TimerDelayMilliseconds (BREAK_DELAY);
    if (! ClearCommBreak (ghPort)) return UartClearError (COMM_STATUS_ERROR);

    // Sucess
    return COMM_STATUS_OK;
}


/**
 *	Change the bit rate
 *
 *  @param dwBitRate The target bit rate. Has to be one of the LL_SIO_BAUDRATE_xxxx constants.
 */
COMM_STATUS PT_UartSetParams (uint32 dwBitRate)
{
    DCB Dcb;

    if (ghPort == INVALID_HANDLE_VALUE) return COMM_STATUS_ERROR;

    // Set the bit rate
    if (! GetCommState(ghPort, &Dcb)) return UartClearError (COMM_STATUS_ERROR);
    switch (dwBitRate) {
        case LL_SIO_BAUDRATE_9600:  Dcb.BaudRate = 9600;   break;
        case LL_SIO_BAUDRATE_19200: Dcb.BaudRate = 19200;  break;
        case LL_SIO_BAUDRATE_38400: Dcb.BaudRate = 38400;  break;
        case LL_SIO_BAUDRATE_57600: Dcb.BaudRate = 57600;  break;
        case LL_SIO_BAUDRATE_115200:Dcb.BaudRate = 115200; break;
        case LL_SIO_BAUDRATE_230400:Dcb.BaudRate = 230400; break;
        default:                    break;  // Do not change the speed
    }
    if (! SetCommState(ghPort, &Dcb)) return UartClearError (COMM_STATUS_ERROR);

    // Purge the input queue
//    if (! PurgeComm(ghPort, PURGE_RXABORT | PURGE_RXCLEAR)) return UartClearError (COMM_STATUS_ERROR);

    // Sucess
    return COMM_STATUS_OK;
}


/**
 *	Clear the receive queue
 */
COMM_STATUS PT_UartClearQueue (void)
{
    if (ghPort == INVALID_HANDLE_VALUE) return COMM_STATUS_ERROR;

    // Purge the input queue
    if (! PurgeComm(ghPort, PURGE_RXABORT | PURGE_RXCLEAR)) return UartClearError (COMM_STATUS_ERROR);

    // Sucess
    return COMM_STATUS_OK;
}


/**
 *	Read a single byte.
 *  If there is already a byte in the receive queue, return it.
 *  Otherwise wait for an incoming byte for up to 100 msec (as specified by the protocol).
 *
 *  @return Status code
 *  On success it returns COMM_STATUS_OK.
 *  Upon timeout it returns COMM_STATUS_TIMEOUT.
 *  In all the other cases (overrun, framing error etc.) it returns COMM_STATUS_ERROR.
 */
COMM_STATUS PT_UartReadByte (uint8 *byte)
{
    uint32   dwRead;

    if (ghPort == INVALID_HANDLE_VALUE) return COMM_STATUS_ERROR;

    if (! ReadFile (ghPort, byte, sizeof (*byte), &dwRead, NULL)) return UartClearError (COMM_STATUS_ERROR);
    if (! dwRead) return COMM_STATUS_TIMEOUT;

    // Sucess
    return COMM_STATUS_OK;
}


/**
 *	Send a single byte.
 *  This function may block, if the output queue is full.
 *
 *  @return Status code
 *  On success it returns COMM_STATUS_OK.
 *  In all the other cases (HW problem etc.) it returns COMM_STATUS_ERROR.
 */
COMM_STATUS PT_UartWriteByte (uint8 byte)
{
    uint32   dwWritten;

    if (ghPort == INVALID_HANDLE_VALUE) return COMM_STATUS_ERROR;

    if (! WriteFile (ghPort, &byte, sizeof (byte), &dwWritten, NULL) ||
        dwWritten != sizeof (byte)) return UartClearError (COMM_STATUS_ERROR);

    // Sucess
    return COMM_STATUS_OK;
}


/**
 *  Get the status of the AWAKE signal
 *
 *  @param pAwake Set to status of AWAKE: 0 or 1
 *  @return Status code
 */
COMM_STATUS PT_UartGetAwake (uint32 *pAwake)
{
    DWORD dwComStatus;

    if (ghPort == INVALID_HANDLE_VALUE) return COMM_STATUS_ERROR;
    if (! GetCommModemStatus (ghPort, &dwComStatus)) return UartClearError (COMM_STATUS_ERROR);
    *pAwake = ((dwComStatus & MS_DSR_ON) ? 1 : 0) ^ COMM_AWAKE_ACTIVE_STATE;

    return COMM_STATUS_OK;
}


/**
 *  Set the status of the WAKEUP signal
 *
 *  @param dwWakeup Desired status of WAKEUP signal
 *  @return Status code
 */
COMM_STATUS PT_UartSetWakeup (uint32 dwWakeup)
{
    if (ghPort == INVALID_HANDLE_VALUE) return COMM_STATUS_ERROR;
    if (! EscapeCommFunction (ghPort, dwWakeup ? SETRTS : CLRRTS)) return UartClearError (COMM_STATUS_ERROR);

    return COMM_STATUS_OK;
}


/**
 * Waits until all bytes in transmitting FIFO are sent.
 */
COMM_STATUS PT_UartWaitTransmissionEnd(void)
{
    BOOL boState = FlushFileBuffers (ghPort);
    return boState ? COMM_STATUS_OK : COMM_STATUS_ERROR;
}



/**
 *	Start/Stop functions have no effect on PC
 */
COMM_STATUS PT_UartStartTransmit(void)
{
    return COMM_STATUS_OK;
}

COMM_STATUS PT_UartStopTransmit(void)
{
    return COMM_STATUS_OK;
}

COMM_STATUS PT_UartStartReceive(void)
{
    return COMM_STATUS_OK;
}

COMM_STATUS PT_UartStopReceive(void)
{
    return COMM_STATUS_OK;
}

COMM_STATUS PT_UartStartClock(void)
{
    return COMM_STATUS_OK;
}

void PT_UartStopClock(void)
{

}