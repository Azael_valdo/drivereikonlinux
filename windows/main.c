#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <stddef.h>
#include "tfmapi.h"
#include "tfmerror.h"

int main(int argc, char* argv[])
{
    PT_STATUS   status = PT_STATUS_OK;
    int         retVal = 0;
    PT_BOOL		boQuit = PT_FALSE;

    setvbuf(stdout, 0, _IONBF, 0);

    // Initialize PT API with custom memory functions
    if (PT_STATUS_OK != (status = PTInitialize(0))) 
    {
        printf("Error: PTInitialize() failed! Error code %i\n", status);
        return -1;
    }
    else
    {
		PT_CONNECTION   conn;
        
        // Open connection to TFM
        if (PT_STATUS_OK != (status = PTOpen(0, &conn))) 
        {
            printf("Error: PTOpen() failed! Error code %i\n", status);
            retVal = -1;
            goto error;
        }

        while (!boQuit)
        {
			int ch;

            printf("\n");
            printf("e .. enroll finger\n");
            printf("v .. verify finger\n");
            printf("a .. verify all finger\n");
            printf("d .. delete sample\n");
            printf("l .. delete all samples\n");
            printf("q .. quit\n\n");
            printf("Enrolled samples:");

            // Print available fingers
			{
				PT_FINGER_LIST  *pFingerList = 0;

				// Get list of all enrolled fingers from the TFM
				PT_STATUS status = PTListAllFingers(conn, &pFingerList);
				if (PT_STATUS_OK == status) 
				{
					if (pFingerList->NumFingers) 
					{
						PT_DWORD i;
                        // Finger list is still in the packed form
                        // Traverse it correctly

                        typedef struct {
                            PT_LONG     SlotNr;
                            PT_DWORD    FingerDataLength;
                        } Finger;
                        
                        Finger *pFinger = (Finger *)&pFingerList->List[0];    
                        for (i = 0; i < pFingerList->NumFingers; ++i) 
                        {
                            printf(" %i", (PT_LONG)(pFinger->SlotNr));
                            pFinger = (Finger *)((PT_BYTE *)pFinger + sizeof(Finger) + pFinger->FingerDataLength);
                        }        
					}
					else
					{
						printf(" none");
					}
				}
			}
            printf("\n\n");
            
            ch = _getch();
            switch (ch) 
            {
                case 'q':
                    boQuit = PT_TRUE;
                    // close connection
                    PTClose(conn);
            	    break;
                case 'e':
                {
                    PT_LONG slotNr = -1;
                    // Enroll finger on the TFM
					printf("Enrolling finger ...\n");
                    if (PT_STATUS_OK != (status = PTEnroll(conn, PT_PURPOSE_ENROLL, 0, 0, &slotNr, 0, 10000, 0, 0, 0))) 
                    {
                        printf("Error: PTEnroll() failed! Error code %i\n", status);
                    }
                    else
                    {
                        printf("Sample index: %i\n", slotNr);
                    }
                    break;
                }
                case 'v':
                {
                    int sampleToVerify = 0;
					printf("Enter index of sample to verify against: ");
                    if (1 == _cscanf("%i", &sampleToVerify), printf("\r\n")) 
                    {
                        // Form input BIR structure with selected sample index
                        PT_INPUT_BIR    inputBir;
                        PT_BOOL boMatch = PT_FALSE;
                        PT_LONG maxFarRequested = 1;

						inputBir.byForm = PT_SLOT_INPUT;
						inputBir.InputBIR.lSlotNr = sampleToVerify;

                        // Verify the finger against selected sample
						printf("Verifying finger ...\n");
                        if (PT_STATUS_OK != (status = PTVerify(conn, &maxFarRequested, 0, PT_FALSE, &inputBir, 0, &boMatch, 
                            0, 0, 0, 10000, PT_TRUE, 0, 0, 0))) 
                        {
                            printf("Error: PTVerify() failed! Error code %i\n", status);
                        }
                        else
                        {
                            printf(boMatch ? "Finger matched sample\n" : "Finger DID NOT matched sample\n");
                        }
                    }
                    // Eat ENTER
                    _getch();
                    break;
                }
                case 'a':
                {
                    PT_LONG maxFarRequested = 1;
                    PT_LONG matchResult = -1;

					printf("Matching finger ...\n");
                    if (PT_STATUS_OK != (status = PTVerifyAll(conn, &maxFarRequested, 0, PT_FALSE, 0, &matchResult, 
                        0, 0, 0, 10000, PT_TRUE, 0, 0, 0))) 
                    {
                        printf("Error: PTVerifyAll() failed! Error code %i\n", status);
                    }
                    else
                    {
                        printf(matchResult != -1 
                            ? "Matched sample : %i\n" 
                            : "Finger DID NOT matched any of the enrolled sample\n", matchResult);
                    }
                    break;
                }
                case 'd':
                {
                    // Get index of sample to delete
                    int sampleToDelete = 0;
					printf("Enter index of sample to delete: ");
                    if (1 == _cscanf("%i", &sampleToDelete), printf("\r\n")) 
                    {
                        // Delete the sample from the TFM
                        if (PT_STATUS_OK != (status = PTDeleteFinger(conn, sampleToDelete))) 
                        {
                            printf("Error: PTDeleteFinger() failed! Error code %i\n", status);
                        }
                        else
                        {
                            printf("Sample deleted\n");
                        }
                    }
                    // Eat ENTER
                    _getch();
                    break;
                }
                case 'l':
                {
                    // Delete all samples on the TFM
                    if (PT_STATUS_OK != (status = PTDeleteAllFingers(conn))) 
                    {
                        printf("Error: PTDeleteAllFingers() failed! Error code %i\n", status);
                    }
                    else
                    {
                        printf("All samples deleted\n");
                    }
                    break;
                }
            }
        }

error:

        PTTerminate();
    }
    
	return retVal;
}

