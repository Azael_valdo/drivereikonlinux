/**
 * @file timer.c
 *
 * Timer-related support functions, needed by the ESS/TFM comm library.
 * This file has to be replaced by the real implementation for given target HW platform.
 * This one concrete implementation is for Windows.
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#include    "timer.h"
#include    <windows.h>


//========================================================================
//		Exported functions
//========================================================================

/**
 *	Number of milliseconds since startup
 */
uint32  PT_TimerGetMilliseconds (void)
{
    return GetTickCount ();
}

/**
*	Wait defined number of microseconds
*/

COMM_STATUS PT_TimerDelayMicroseconds(uint32 dwMicroseconds)
{	
    if (dwMicroseconds) Sleep (dwMicroseconds / 1000);
    return COMM_STATUS_OK;
}

/**
 *	Wait defined number of milliseconds
 */
COMM_STATUS PT_TimerDelayMilliseconds (uint32 dwMilliseconds)
{
    if (dwMilliseconds) Sleep (dwMilliseconds);
    return COMM_STATUS_OK;
}

/**
 * Check timer status
 */
COMM_STATUS PT_TimerGetStatus(void)
{
    return COMM_STATUS_OK;
}




