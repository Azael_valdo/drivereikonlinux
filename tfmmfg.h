/**
 * @file tfmmfg.h
 *
 * PTAPI manufacturing commands
 *
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#ifndef tfmmfg_h
#define tfmmfg_h


#include "tfmtypes.h"


//---------------------------------------------------------

/**
 * Diagnostic operations
 */
//@{
#define PT_DIAG_BASIC           1   //< Basic check
#define PT_DIAG_AWAKE_WAKEUP    2   //< Check of the AWAKE and WAKEUP signals
#define PT_DIAG_IMAGE_QUALITY   3   //< Image Quality Test
#define PT_DIAG_RAM             4   //< RAM test
//@}

/**
 * Test basic features
 */
//@{

/**
 * Input data block of basic features test
 */
typedef struct pt_diag_indata_basic {
    PT_DWORD	Operation;		///< = PT_DIAG_BASIC
    PT_DWORD	Features;		///< Bitmask of features to be tested, see PT_DIAG_FEATURE_xxxx
} PT_DIAG_INDATA_BASIC;


/**
 * Output data block of basic features test
 */
typedef struct pt_diag_outdata_basic {
    PT_DWORD	FeaturesOK;		///< Bitmask of features which were tested OK, see PT_DIAG_FEATURE_xxxx
} PT_DIAG_OUTDATA_BASIC;


/**
 * Features bits
 */
//@{
#define PT_DIAG_FEATURE_SENSOR_POWER        0x00000001  ///< Sensor power management
#define PT_DIAG_FEATURE_SENSOR_INTERFACE    0x00000002  ///< Sensor control / data channel
#define PT_DIAG_FEATURE_SENSOR_DATA         0x00000004  ///< Sensor data path
#define PT_DIAG_FEATURE_SENSOR_FDET         0x00000008  ///< Sensor finger detect - ESS2.1 only
#define PT_DIAG_FEATURE_SENSOR_BIAS         0x00000010  ///< Sensor biases/readout
#define PT_DIAG_FEATURE_IMAGE_QUAL          0x00000020  ///< Image quality
#define PT_DIAG_FEATURE_TCD_FINGER_DETECT   0x00000040  ///< TCD finger detect 
#define PT_DIAG_FEATURE_TURBO_CLK           0x00000080  ///< Turbo-mode Clock
#define PT_DIAG_FEATURE_AS_GRID             0x00000100  ///< AS grid                            [TCD50]
#define PT_DIAG_FEATURE_BLEEDING            0x00000200  ///< Sensor bleeding                    [SOnly, TCD50]
#define PT_DIAG_FEATURE_TCS_OVERCURR_DETECT 0x00000400  ///< TCS overcurrent detect             [TCD50]
#define PT_DIAG_FEATURE_CALIBRATION_CHECK   0x00000800  ///< Calibration results check          [TCD50]
#define PT_DIAG_FEATURE_TCD_FDET_CONNECTION 0x00001000  ///< TCD finger detect connection       [TCD50]
#define PT_DIAG_FEATURE_HW_NAV              0x00002000  ///< Hw navigation test [Only on platforms where HW navigation is supported]
#define PT_DIAG_FEATURE_TEST_DEVICE         0x00004000  ///< Device test [Area sensors only]
#define PT_DIAG_FEATURE_TEST_DAMAGE         0x00008000  ///< Damage test [Area sensors only]
#define PT_DIAG_FEATURE_QUERY_DEVICE        0x00010000  ///< Device query [Area sensors only]
#define PT_DIAG_FEATURE_SYSTEM_NOISE        0x00020000  ///< System noise test [Area sensors only]
#define PT_DIAG_FEATURE_BEZEL_CONNECTION    0x00080000  ///< Bezel connection test [Area sensors only]
//@}

//@}


/**
 * Test WAKEUP/AWAKE handshake
 */
//@{

/**
 * Input data block of AWAKE/WAKEUP test
 */
typedef struct pt_diag_indata_awake_wakeup {
    PT_DWORD	Operation;		///< = PT_DIAG_AWAKE_WAKEUP
    PT_DWORD	DiagTimeout;    ///< Timeout in milliseconds
    PT_DWORD    DiagLoops;      ///< Number of test cycles
} PT_DIAG_INDATA_AWAKE_WAKEUP;


/**
 * Output data block of AWAKE/WAKEUP test
 */
typedef struct pt_diag_outdata_awake_wakeup {
    PT_DWORD	ResultCode;		    ///< Test result code, 0 means success
} PT_DIAG_OUTDATA_AWAKE_WAKEUP;


/**
 * Result codes of AWAKE/WAKEUP diagnostic
 */
//@{
#define PT_DIAG_AW_RESULT_OK                0   ///< Test completed successfully
#define PT_DIAG_AW_RESULT_NOT_SUPPORTED     1   ///< AWAKE/WAKEUP features not supported 
#define PT_DIAG_AW_RESULT_TIMEOUT_NWAKEUP   2   ///< Timeout when waiting for WAKEUP signal is set to inactive state
#define PT_DIAG_AW_RESULT_TIMEOUT_WAKEUP    3   ///< Timeout when waiting for WAKEUP signal is set to active state
#define PT_DIAG_AW_RESULT_TIMEOUT_NAWAKE    4   ///< Timeout when waiting for AWAKE signal is set to inactive state
#define PT_DIAG_AW_RESULT_TIMEOUT_AWAKE     5   ///< Timeout when waiting for AWAKE signal is set to active state
//@}



#define PT_DIAG_MAX_BAD_PIXELS 15 ///< Length of the array of coord. of bad pixels

/**
 * Input data block of Image Quality Test
 */
typedef struct pt_diag_indata_image_quality {
    PT_DWORD	Operation;		///< = PT_DIAG_IMAGE_QUALITY
} PT_DIAG_INDATA_IMAGE_QUALITY;

/**
 * Output data block of Image Quality Test
 */
typedef struct pt_diag_outdata_image_quality {
    PT_DWORD  ResultCode;         ///< Test result code, 0 means success
    PT_WORD   NrBadPixels;        ///< number of detected bad pixels
    PT_WORD   BadPixels[PT_DIAG_MAX_BAD_PIXELS]; ///< Array of coordinates of bad pixels
                                                 ///< Low byte = X (0..253)
                                                 ///< High byte = Y (0..3)
} PT_DIAG_OUTDATA_IMAGE_QUALITY;

/**
 * Result codes of Image Quality Test
 */
//@{
#define PT_DIAG_IQ_RESULT_OK                   0   ///< Test completed successfully
#define PT_DIAG_IQ_RESULT_INIT_FAIL            1   ///< Initialization failed
#define PT_DIAG_IQ_RESULT_ROW_AVERAGE_MISMATCH 2   ///< Row averages are too dissimilar
#define PT_DIAG_IQ_RESULT_BAD_PIXELS           3   ///< Bad pixels
#define PT_DIAG_IQ_RESULT_HIST_FAIL            4   ///< Histogram out of spec.
#define PT_DIAG_IQ_RESULT_HW_FAIL              5   ///< Miscellaneous Hardware related problem
#define PT_DIAG_IQ_RESULT_DELTA_FAIL           6   ///< Delta as a result of charge step out of spec.
//@}

//@}


/**
 * RAM test
 */
//@{

/**
 * Input data block of RAM test
 */
typedef struct pt_diag_indata_ram {
    PT_DWORD	Operation;		///< = PT_DIAG_RAM
} PT_DIAG_INDATA_RAM;


/**
 * Output data block of RAM test
 */
typedef struct pt_diag_outdata_ram {
    PT_DWORD    ResultCode;     ///< Test result code, 0 means success
} PT_DIAG_OUTDATA_RAM;


/**
 * Result codes of RAM test
 */
//@{
#define PT_DIAG_RAM_RESULT_OK               0   ///< Test completed successfully
#define PT_DIAG_RAM_RESULT_FAULT_ONE_BIT    1   ///< Some RAM bit is permanent 1
#define PT_DIAG_RAM_RESULT_FAULT_ZERO_BIT   2   ///< Some RAM bit is permanent 0
//@}

//@}

//---------------------------------------------------------

/**
 * TFM configuration items
 */
//@{
#define PT_FWITEM_CFGFLAGS          (1)
#define PT_FWITEM_CFGSESSION        (2)
#define PT_FWITEM_AUTHENTIFY_ID     (3)
#define PT_FWITEM_AUTHENTIFY_KEY    (4)
#define PT_FWITEM_SETCFG_KEY        (5)
#define PT_FWITEM_UPDATE_KEY        (6)
#define PT_FWITEM_GPIO_CFG          (7)
#define PT_FWITEM_PSW_CFG           (8)
#define PT_FWITEM_COMM_CFG          (9)
#define PT_FWITEM_SECURITY_CFG      (10)
#define PT_FWITEM_SYSTEMINFO_CFG    (11)

#define PT_FWITEM_CFGSESSION_V1     0x10002
#define PT_FWITEM_CFGSESSION_V2     0x20002
#define PT_FWITEM_CFGSESSION_V3     0x30002
#define PT_FWITEM_CFGSESSION_V4     0x40002
#define PT_FWITEM_CFGSESSION_V5     0x50002
//@}


/**
 * Security FW cfg flags
 */
//@{
#define PT_FWCFG_SECURITYBIT_STRONG_CRYPTO      0x00000001      ///< Enable strong cryptography
#define PT_FWCFG_SECURITYBIT_ANTISPOOFING       0x00010000      ///< Enable antispoofing
#define PT_FWCFG_SECURITYBIT_ANTISPOOFING_LEVEL 0x00020000      ///< Enable change of antispoofing level
#define PT_FWCFG_SECURITYBIT_ANTISPOOFING_SCORE 0x00040000      ///< Enable acquring of antispoofing score
//@}

typedef enum
{
    SENSOR_POWER_SOURCE_SC_PWR_3V,      // sensor connected to SC_PWR. TCS3 CP used.
    SENSOR_POWER_SOURCE_SWITCH_3V,      // 3V sensor power switched by GPIO. TCS3 CP used.
    SENSOR_POWER_SOURCE_SWITCH_5V,      // 5V sensor power switched by GPIO. No CP.
    SENSOR_POWER_SOURCE_EXTERNAL,       // extern CP.
    SENSOR_POWER_SOURCE_MAX,
} SensorPowerSourceType;

/**
 * GPIO allocation configuration structure
 */
typedef struct pt_fwcfg_gpio {
    PT_BYTE EEPROM_sda;     ///< EEPROM serial data signal
    PT_BYTE EEPROM_scl;     ///< EEPROM serial clock signal
    PT_BYTE EEPROM_wc;      ///< EEPROM write control signal
    PT_BYTE DSR_data;       ///< DSR data signal
    PT_BYTE LED_red;        ///< Red LED signal
    PT_BYTE LED_green;      ///< Green LED signal
    PT_BYTE SC_voltage;     ///< SC power select signal
    PT_BYTE SSI_data;       ///< SSI data signal
    PT_BYTE SSI_clk;        ///< SSI clock signal
    PT_BYTE charge_pump;    ///< charge pump activate signal
    PT_BYTE TC_3V_power_switch;///< TouchChip power activate signal
    PT_BYTE TC_5V_power_switch;///< TouchChip power activate signal
    PT_BYTE reserved[4];
} PT_FWCFG_GPIO;

/**
 * PerfectSwipe configuration structure
 */
typedef struct pt_fwcfg_psw {
    PT_BYTE rotate180;          ///< Sensor orientation
    PT_BYTE startFromJoint;     ///< Swipe direction
    PT_BYTE leftColumnSkip;     ///< Number of skipped columns from left for image storing (it is recommended that this value should be multiple of 4)
    PT_BYTE rightColumnSkip;    ///< Number of skipped columns from right for image storing (it is recommended that this value should be multiple of 4)
    PT_BYTE navFlipX;           ///< Reverse X navigation direction
    PT_BYTE navFlipY;           ///< Reverse Y navigation direction
    PT_BYTE navExchXY;          ///< Reverse X with Y navigation
    PT_BYTE sensorpowersource;  ///< Power source for the sensor.

    PT_BYTE digitalGain1;       ///< Digital gain for standard mode (set 1)
    PT_BYTE digitalGainTM1;     ///< Digital gain for turbo mode (set 1)
    PT_BYTE digitalGainPTM1;    ///< Digital gain for pure turbo mode (set 1)
    PT_BYTE bgSeparation1;      ///< Threshold to distinguish finger from BG (standard mode, set 1)
    PT_BYTE bgSeparationTM1;    ///< Threshold to distinguish finger from BG (turbo mode, set 1)
    PT_BYTE bgSeparationPTM1;   ///< Threshold to distinguish finger from BG (pure turbo mode, set 1)
    PT_SHORT darkThMod1;        ///< Modifier of the darkTh (from 4*bgColor, standard mode, set 1)
    PT_SHORT brightThMod1;      ///< Modifier of the brightTh (from 4*bgColor, standard mode, set 1)
    PT_SHORT darkThModTM1;      ///< Modifier of the darkTh (from 4*bgColor, turbo mode, set 1)
    PT_SHORT brightThModTM1;    ///< Modifier of the brightTh (from 4*bgColor, turbo mode, set 1)
    PT_SHORT darkThModPTM1;     ///< Modifier of the darkTh (from 4*bgColor, pure turbo mode, set 1)
    PT_SHORT brightThModPTM1;   ///< Modifier of the brightTh (from 4*bgColor, pure turbo mode, set 1)

    PT_BYTE digitalGain2;       ///< Digital gain for standard mode (set 2)
    PT_BYTE digitalGainTM2;     ///< Digital gain for turbo mode (set 2)
    PT_BYTE digitalGainPTM2;    ///< Digital gain for pure turbo mode (set 2)
    PT_BYTE bgSeparation2;      ///< Threshold to distinguish finger from BG (standard mode, set 2)
    PT_BYTE bgSeparationTM2;    ///< Threshold to distinguish finger from BG (turbo mode, set 2)
    PT_BYTE bgSeparationPTM2;   ///< Threshold to distinguish finger from BG (pure turbo mode, set 2)
    PT_SHORT darkThMod2;        ///< Modifier of the darkTh (from 4*bgColor, standard mode, set 2)
    PT_SHORT brightThMod2;      ///< Modifier of the brightTh (from 4*bgColor, standard mode, set 2)
    PT_SHORT darkThModTM2;      ///< Modifier of the darkTh (from 4*bgColor, turbo mode, set 2)
    PT_SHORT brightThModTM2;    ///< Modifier of the brightTh (from 4*bgColor, turbo mode, set 2)
    PT_SHORT darkThModPTM2;     ///< Modifier of the darkTh (from 4*bgColor, pure turbo mode, set 2)
    PT_SHORT brightThModPTM2;   ///< Modifier of the brightTh (from 4*bgColor, pure turbo mode, set 2)

    PT_BYTE veryLowPowerMode;   ///< Flag whether very low power mode is active

    PT_BYTE reserved[19];
} PT_FWCFG_PSW;


#define PT_FWCFG_COMM_FLAG_ETX_MASK         0x3
#define PT_FWCFG_COMM_FLAG_ETX_DISABLED     0x0         // completely disabled, ETXes ignored
#define PT_FWCFG_COMM_FLAG_ETX_ENABLED      0x1         // enabled, receive ends before ETX
#define PT_FWCFG_COMM_FLAG_ETX_OPTIONAL     0x2         // handshake after reset
#define PT_FWCFG_COMM_FLAG_ETX_MANDATORY    0x3         // receive waits for ETX

/**
 * Communication module config structure
 */
typedef struct pt_fwcfg_comm {
    PT_DWORD    sio_initial_comm_speed; ///< Session-initial comm speed for UART (one of LL_SIO_BAUDRATE_XXXX)
    PT_DWORD    wtx_timeout;            ///< Session-initial length of WTX timeout in milliseconds
    PT_WORD     max_transfer_size;      ///< Session-initial maximal transfer size
    PT_WORD     flags;                  ///< Session-initial flags
    PT_BYTE     reserved[4];
} PT_FWCFG_COMM;

/**
 * Security features config strucure
 */
typedef struct pt_fwcfg_security {
    PT_DWORD    securityFlags;          ///< Security flags
    PT_BYTE     reserve[12];
} PT_FWCFG_SECURITY;

/**
 * System information structure
 */
typedef struct pt_fwcfg_systeminfo {
    PT_DWORD    SystemId;           ///< System identification bits
    PT_CHAR     SystemName[PT_SYSTEM_NAME_MAX_LEN]; ///< System identification name
    PT_DWORD    Usage;              ///< Type of the reader
    PT_BYTE     reserve[8];
} PT_FWCFG_SYSTEMINFO;

//---------------------------------------------------------

/**
 * Manufacturing operations
 */
//@{
#define PT_MFG_OPERATION_NOTURBOMODE    1   //< Full manufacturing process. The sensor calibration step is performed without enabling the sensor's turbo mode.
#define PT_MFG_OPERATION_TURBOMODE      2   //< Full manufacturing process. The sensor calibration step is performed with enabling the sensor's turbo mode.
//@}


/**
 * Calibration modes
 */
//@{
#define PT_CALIB_TYPE_REVERT_TO_MANUFACTURING       0
#define PT_CALIB_TYPE_MANUFACTURING_CALIBRATION     1
#define PT_CALIB_TYPE_TURBOMODE_CALIBRATION         2
#define PT_CALIB_TYPE_FULL_USER_CALIBRATION         3
#define PT_CALIB_TYPE_END_CALIBRATION               4
#define PT_CALIB_TYPE_TURBOMODE_NOAS_CALIBRATION    5
//@}

//---------------------------------------------------------

/**
 * Calibrate the fingerprint sensor to suite best to the given user. The calibration
 * data will be stored in NVM and used for all the following biometric operations, 
 * in this and future connections (communication sessions).
 *
 * PTCalibrate is currently used only for strip sensors (ESS).
 *
 * The standard calibration (PT_CALIB_TYPE_STANDARD) is an interactive operation. 
 * The user will be prompted to put, lift or swipe his finger. This feedback will
 * be communicated using the GUI callbacks. For the success of this operation is
 * therefore essential to enable and use the GUI callbacks. If the CallbackLevel
 * member of the session configuration is set to CALLBACKS_NONE, PTCalibrate will
 * directly fail.
 *
 * The GUI callbacks are also the only way how to interrupt this operation. If the
 * host fails to use callbacks, the only way how to regain control over FM is to
 * close and reopen the connection (communication session).
 *
 * @param Handle to the connection to TFM
 * @param dwType Type of calibration operation to be performed
 * @return Status code
 */
PTAPI_DLL PT_STATUS PTAPI PTCalibrate (
   IN PT_CONNECTION hConnection, 
   IN PT_DWORD dwType
);


/**
 * This function is primarily targeted for use in manufacturer's diagnostic programs.
 * Its documentation is restricted to ST and its close partners.
 *
 * Note: This function can be called directly after opening a communication session.
 * PTDiagnostics is also exempt from the need of host authentication (PTAuthentify),
 * which may be required in future API versions.
 *
 * @param hConnection Handle to the connection to TFM
 * @param pInData Input data block
 * @param ppOutData Pointer to a variable, which will receive the address of the output
 *   data block. The data has to be freed by host.
 * @return Status code
 */
PTAPI_DLL PT_STATUS PTAPI PTDiagnostics(
    IN PT_CONNECTION hConnection,
    IN PT_DATA *pInData,
    OUT PT_DATA **ppOutData
);


/**
 * Query the firmware configuration of a FM.
 *
 * @param hConnection Handle to the connection to FM
 * @param dwCfgItemId The firmware configuration item to query
 * @param ppData Address of pointer, which will be set to point to the resulting
 *   firmware configuration data. The data has to be discarded by a call to pfnFree()
 * @return Status code
 */
PTAPI_DLL PT_STATUS PTAPI PTFirmwareGetCfg (
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwCfgItemId,
    OUT PT_DATA **ppData
);


/**
 * Set the firmware configuration of a FM.
 * @hConnection Handle to the connection to FM
 * @dwCfgItemId The firmware configuration item to be set
 * @pData Address of the firmware configuration data to be used for given configuration item.
 * @return Status code
 */
PTAPI_DLL PT_STATUS PTAPI PTFirmwareSetCfg (
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwCfgItemId,
    IN PT_DATA *pData
);


/**
 * Set the firmware configuration of a FM using signed data block.
 * @hConnection Handle to the connection to FM.
 * @pData Address of the signed data block with firmware configuration.
 * @return Status code
 */
PTAPI_DLL PT_STATUS PTAPI PTFirmwareSetCfgEx (
    IN PT_CONNECTION hConnection,
    IN PT_DATA *pData
);


/**
 * Update TFM firmware.
 *
 * @param hConnection Connection handle to the TFM
 * @param pFirmwareData Pointer to firmware data to be loaded
 * @param boDeleteUserData Flag, whether user data should be deleted after
 *  the firmware update
 * @return Status code
 */
PTAPI_DLL PT_STATUS PTAPI PTFirmwareUpdate(
    IN PT_CONNECTION hConnection,
    IN PT_DATA *pUpdateData
);


/**
 * Requests TFM to reboot after the current session ends.
 *
 * @param hConnection Handle to the connection to TFM
 * @param boReboot Set or cancel request for reboot
 * @return Status code
 */
PTAPI_DLL PT_STATUS PTAPI PTFirmwareRequestReboot(
    IN PT_CONNECTION hConnection,
    IN PT_BOOL boReboot
);


/**
 * Performs the manufacturing initialization and tests
 *
 * @param hConnection Handle to the connection to FM
 * @param ppOutData Address of a pointer, which will be set to point to an
 *  output PT_DATA block. The output data block contains more detailed
 *  information about the results of the manufacturing tests. When no
 *  additional information is available, the block will be empty (zero length).
 * @return Status code
 */
PTAPI_DLL PT_STATUS PTAPI PTManufacturing(
    IN PT_CONNECTION hConnection,
    OUT PT_DATA **ppOutData
);


/**
 * Performs the manufacturing initialization and tests
 *
 * @param hConnection Handle to the connection to FM
 * @param pInData Input data block with parameters influencing manufacturing process
 * @param ppOutData Address of a pointer, which will be set to point to an
 *  output PT_DATA block. The output data block contains more detailed
 *  information about the results of the manufacturing tests. When no
 *  additional information is available, the block will be empty (zero length).
 * @return Status code
 */
PTAPI_DLL PT_STATUS PTAPI PTManufacturingEx(
    IN PT_CONNECTION hConnection,
    IN PT_DATA *pInData,
    OUT PT_DATA **ppOutData
);


#endif // #define tfmmfg_h
