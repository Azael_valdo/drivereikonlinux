 /**
 * @file comm.cpp
 *
 * UART Communication Layer (transport+link layer) interface for ESS/TFM communication protocol
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#include "comm.h"
#include "utils.h"
#if defined(ENABLE_PTSECURECHANNEL)
#include "secch.h"
#endif
#include "uart.h"



//========================================================================
//      Constants, macros and types
//========================================================================

// Special characters
#define DLE 	0x10
#define STX	    0x02
#define XON 	0x11
#define XOFF 	0x13

// Frame types
/// @name Frame types
//@{
/// normal data frame, to be delivered to the transport layer
#define LL_FRAME_TYPE_DATA              0
/// acknowledgment
#define LL_FRAME_TYPE_ACK               1
/// negative acknowledgment, request to resend
#define LL_FRAME_TYPE_NACK              2
/// answer to reset
#define LL_FRAME_TYPE_ATR               3
/// request to change parameters during connection setup
#define LL_FRAME_TYPE_SET_PARAMS        4
/// confirmation of parameters change during connection setup
#define LL_FRAME_TYPE_CONFIRM_PARAMS    5
/// reset frame when hw reset isn't supported
#define LL_FRAME_TYPE_RESET             6
/// end of connection
#define LL_FRAME_TYPE_CLOSE             7
/// wait time extension
#define LL_FRAME_TYPE_WTX               8
/// continue, response to WTX
#define LL_FRAME_TYPE_CONTINUE          9
/// abort, response to WTX
#define LL_FRAME_TYPE_ABORT             10
/// frame containing raw data used for sending stream of data from TFM to host
#define LL_FRAME_TYPE_RAW_DATA          11
/// frame used by host to request raw data abort
#define LL_FRAME_TYPE_ABORT_RAW_DATA    12
//@}

//========================================================================
//		CRC functions
//========================================================================

/**
 *	Update the CRC counter by given byte
 */
static void CrcUpdate (uint8 byte, uint16 *pCrc)
{
    register uint16 x = *pCrc;

    x  = (uint16)((uint8)(x >> 8) | (x << 8));
    x ^= (uint8) byte;
    x ^= (uint8)(x & 0xff) >> 4;
    x ^= (x << 8) << 4;
    x ^= ((x & 0xff) << 4) << 1;

    *pCrc = x;
}

//========================================================================
//		Sleep in session functions
//========================================================================

/**
 * Wait until AWAKE signal is set to desired state or timeout expires.
 *
 * @param boWaitForAwake If TRUE, wait until AWAKE signal is set, otherwise
 *   wait until it is cleared
 * @param dwTimeout Timeout in miliseconds
 * @return On success returns COMM_STATUS_OK, if timeout expired returns
 *   COMM_STATUS_TIMEOUT, if some other problem occurred returns COMM_STATUS_ERROR
 */
static COMM_STATUS WaitForAwake(BOOL boWaitForAwake, uint32 dwTimeout)
{
    COMM_STATUS status;
    uint32 awake = 0;
    uint32 startTime = PT_TimerGetMilliseconds();

    do {
        if (PT_TimerGetMilliseconds() - startTime > dwTimeout) 
            return COMM_STATUS_TIMEOUT; // timeout

        // Check for asynchronous power down
        status = PT_TimerGetStatus();
        if (COMM_STATUS_OK != status) { 
            // Asynchronous power down
            return status;
        }
        
        status = PT_UartGetAwake(&awake); // check AWAKE signal
        if (status != COMM_STATUS_OK) 
            return status; // some problem
    } while ((awake && !boWaitForAwake) || (!awake && boWaitForAwake));

    return COMM_STATUS_OK;
}

//========================================================================
//		Link layer functions
//========================================================================

/**
 *	Read a single byte with 100 msec timeout (like PT_UartReadByte).
 *  In addition performs unstuffing and calculates CRC.
 *
 *  @return Status code
 *  On success it returns COMM_STATUS_OK.
 *  Upon timeout it returns COMM_STATUS_TIMEOUT.
 *  In all the other cases (overrun, framing error etc.) it returns COMM_STATUS_ERROR.
 */
static COMM_STATUS LlReadByte (uint8 *byte, uint16 *pCrc)
{
    COMM_STATUS status = PT_UartReadByte (byte);

    if (status == COMM_STATUS_OK && *byte==DLE) {
        status = PT_UartReadByte (byte);
        if (*byte != DLE) (*byte)--;
    }

    if (status == COMM_STATUS_OK) 
        CrcUpdate (*byte, pCrc);
    return status;
}


/**
 *	Send a single byte (like PT_UartWriteByte).
 *  In addition performs stuffing and calculates CRC.
 *
 *  @return Status code
 *  On success it returns COMM_STATUS_OK.
 *  In all the other cases (HW problem etc.) it returns COMM_STATUS_ERROR.
 */
static COMM_STATUS LlWriteByte (uint8 byte, uint16 *pCrc)
{
    CrcUpdate (byte, pCrc);

    if (byte == STX || byte == XON || byte == XOFF || byte == DLE) {
        COMM_STATUS status = PT_UartWriteByte (DLE);
        if (status != COMM_STATUS_OK) 
            return status;
        if (byte != DLE) 
            byte++;
    }

    return PT_UartWriteByte (byte);
}


/**
 *	Read a sequence of bytes of specified length using the LlReadByte.
 *
 *  @param pBytes Target byte buffer
 *  @param dwLength Number of bytes to read
 *  @return Status code
 *  On success it returns COMM_STATUS_OK.
 *  Upon timeout it returns COMM_STATUS_TIMEOUT.
 *  In all the other cases (overrun, framing error etc.) it returns COMM_STATUS_ERROR.
 */
static COMM_STATUS LlReadBytes (uint8 *pBytes, uint32 dwLength, uint16 *pCrc)
{
    while (dwLength--) {
        COMM_STATUS status = LlReadByte (pBytes++, pCrc);
        if (status != COMM_STATUS_OK) 
            return status;
    }

    return COMM_STATUS_OK;
}


/**
 *	Write a sequence of bytes of specified length using the LlWriteByte.
 *
 *  @param pBytes Source byte buffer
 *  @param dwLength Number of bytes to write
 *  @return Status code
 *  On success it returns COMM_STATUS_OK.
 *  In all the other cases (HW problem etc.) it returns COMM_STATUS_ERROR.
 */
static COMM_STATUS LlWriteBytes (uint8 *pBytes, uint32 dwLength, uint16 *pCrc)
{
    while (dwLength--) {
        COMM_STATUS status = LlWriteByte (*pBytes++, pCrc);
        if (status != COMM_STATUS_OK) 
            return status;
    }

    return COMM_STATUS_OK;
}

/**
 *	Read and decode one Link Layer frame.
 *  The data part will be stored at gpConn.
 *
 *	@param pS Pointer to session block
 *  @param pbyFrameType Type of the frame (see LL_FRAME_TYPE_xxxx)
 *  @param pbyFrameSeq The frame sequence (used only for data frames)
 *  @param pdwLength Length of the data part of the frame
 *  @return Status code
 */
static COMM_STATUS LlReadFrame (PT_SESSION *pS, uint8 *pbyFrameType, uint8 *pbyFrameSeq, uint32 *pdwLength)
{
    COMM_STATUS status = COMM_STATUS_OK;
    uint8       byte;
    uint8       header[3];
    uint16      wExpectedCrc, wRecvCrc, dummy;
    uint32      dwStart = PT_TimerGetMilliseconds ();

    // Wait for STX
    for( ; ; ) 
    {
        status = PT_UartReadByte (&byte);
        if (status == COMM_STATUS_OK) 
        {
            if (byte == STX) 
                break;
        } 
        else if (status == COMM_STATUS_TIMEOUT) 
        {
            if (PT_TimerGetMilliseconds ()-dwStart > COMM_TIMEOUT)
            {
                status = COMM_STATUS_TIMEOUT;
                goto exit;
            }
        } 
        else 
        {
            // Any other error -> exit
            goto exit;
        }
    }

    // STX was received
    // Reset the CRC counter and receive the LL header
    wExpectedCrc = 0;
    status = LlReadBytes (header, sizeof (header), &wExpectedCrc);
    if (status == COMM_STATUS_TIMEOUT) status = COMM_STATUS_ERROR;  // Timeout for not leading byte is an error!
    if (status != COMM_STATUS_OK)
    {
        goto exit;
    }

    // Parse the header
    *pbyFrameType = (uint8) (header[0] & 0x0f);
    *pbyFrameSeq  = (uint8) (header[1] >> 4);
    *pdwLength    =         (header[2] + ((header[1] & 0x07) << 8));

    // Verify the length of the data part.
    // If acceptable, read the data
    if (*pdwLength > MAX_FRAME_DATA_SIZE) 
    {
        status = COMM_STATUS_ERROR; // This should never happen
        goto exit;
    }
                                                                    // The max. size is negotiated during connection setup
    status = LlReadBytes (pS->pBuf, *pdwLength, &wExpectedCrc);
    if (status == COMM_STATUS_TIMEOUT) status = COMM_STATUS_ERROR;  // Timeout for not leading byte is an error!
    if (status != COMM_STATUS_OK)
    {
        goto exit;
    }

    // Process the CRC
    status = LlReadBytes ((uint8 *)&wRecvCrc, sizeof (wRecvCrc), &dummy);
    if (status == COMM_STATUS_TIMEOUT) status = COMM_STATUS_ERROR;  // Timeout for not leading byte is an error!
    if (status != COMM_STATUS_OK) 
    {
        goto exit;
    }
    if (wExpectedCrc != wRecvCrc) 
    {
        status = COMM_STATUS_ERROR; // Wrong CRC
        goto exit;
    }

    status= COMM_STATUS_OK;


exit:   // Rather have the goto label here in case we have to something on leaving the function

    return status;
}


/**
 *	Encode and write one Link Layer frame.
 *  The data part is already prepared at gpConn.
 *
 *	@param pS Pointer to session block
 *  @param byFrameType Type of the frame (see LL_FRAME_TYPE_xxxx)
 *  @param byFrameSeq The frame sequence (used only for data frames)
 *  @param dwLength Length of the data part of the frame
 *  @return Status code
 */
static COMM_STATUS LlWriteFrame (PT_SESSION *pS, uint8 byFrameType, uint8 byFrameSeq, uint32 dwLength)
{
    COMM_STATUS status = COMM_STATUS_OK;
    uint8       header[3];
    uint16      wCrc, dummy;

    // Verify the length
    if (dwLength > MAX_FRAME_DATA_SIZE)
        return COMM_STATUS_ERROR; //:TODO: Improve error reporting

    PT_UartStartTransmit();

    // Send STX
    if ((status = PT_UartWriteByte (STX)) != COMM_STATUS_OK)
    {
        goto exit;
    }

    // Wait a little while to let TFM/ESS react
    // This is not really necessary (was marginally needed only for TFM 1.1)
    // PT_TimerDelayMilliseconds(5);
    //:TODO: Verify the need for the delay above

    // Reset the CRC counter and send the LL header
    wCrc = 0;
    header[0] = byFrameType;
    header[1] = (uint8)((byFrameSeq<<4) | ((dwLength >> 8) & 0x07));
    header[2] = (uint8)dwLength;
    status = LlWriteBytes (header, sizeof (header), &wCrc);
    if (status != COMM_STATUS_OK) 
    {
        goto exit;
    }

    // Write the data part.
    status = LlWriteBytes (pS->pBuf, dwLength, &wCrc);
    if (status != COMM_STATUS_OK) 
    {
        goto exit;
    }

    // Write the CRC
    status = LlWriteBytes ((uint8 *)&wCrc, sizeof (wCrc), &dummy);

exit:

    // Wait until all the characters are transmitted before we close
    // transmission operation
    PT_UartWaitTransmissionEnd();

    PT_UartStopTransmit();

    return status;
}


//========================================================================
//		High-level functions
//========================================================================

// 
// These are the same as the exported functions, but without closing the connection on error
//

/**
 *	As CommOpen
 */
static COMM_STATUS HLOpen (PT_SESSION *pS)
{
    COMM_STATUS status;
    uint8       byFrameType=0, byFrameSeq;
    uint32      dwLength=0;
    void        *p;
    uint32      i;

    // Update session structure
    pS->dwLastSentFragmentLength = 0;       // No last fragment is valid

    // Open the UART
    if ((status = PT_UartOpen()) != COMM_STATUS_OK) 
        return status;

    // Reset the COM port to a default state
    if ((status = PT_UartReset()) != COMM_STATUS_OK) 
        return status;
    
    // Start UART receiving
    PT_UartStartReceive();

    // And send the HW BREAK signal
    if ((status = PT_UartBreak ()) != COMM_STATUS_OK) 
    {
        goto error;
    }
    
    // Receive the ATR frame
    for (i=0; i < COMM_TRIES; i++)
    {
        status = LlReadFrame (pS, &byFrameType, &byFrameSeq, &dwLength);
        if (status == COMM_STATUS_TIMEOUT) 
        {
            // The BREAK didn't work. But maybe we've just only missed the ATR when sending break...?
            // Let's send a soft RESET frame, but at first wait a while
            // (in case the ATR was just sent, ESS/TFM needs some time for stabilization)
            status = PT_TimerDelayMilliseconds (50);
            // Handle potential asynchronous power down
            if (COMM_STATUS_OK != status) {
                goto error;
            }
            LlWriteFrame (pS, LL_FRAME_TYPE_RESET, 0, 0);
            status = LlReadFrame (pS, &byFrameType, &byFrameSeq, &dwLength);

            if (status == COMM_STATUS_TIMEOUT && pS->byCommSpeed != LL_SIO_BAUDRATE_9600) 
            {
                // Still no success -- let's try at alternate speed
                PT_UartSetParams (pS->byCommSpeed);
                LlWriteFrame (pS, LL_FRAME_TYPE_RESET, 0, 0);
                PT_UartSetParams (LL_SIO_BAUDRATE_9600);
                status = LlReadFrame (pS, &byFrameType, &byFrameSeq, &dwLength);
            }
        }

        if (status == COMM_STATUS_OK && byFrameType != LL_FRAME_TYPE_ATR) {
            status = COMM_STATUS_ERROR;
        }

        if (status == COMM_STATUS_OK) {
            break;
        }
    }

    if (status != COMM_STATUS_OK) 
    {
        goto error;
    }
    if (byFrameType != LL_FRAME_TYPE_ATR || dwLength != 5 || pS->pBuf[0] != LL_PROTOCOL_VERSION) 
    {
        status = COMM_STATUS_ERROR;
        goto error;
    }

    status = PT_TimerDelayMilliseconds (10);
    // Check for potential asynchronous power down
    if (COMM_STATUS_OK != status) {
        goto error;
    }

    // Prepare and send the SEND_PARAMS frame
    p = pS->pBuf;
    STORE_BYTE (p, LL_PROTOCOL_VERSION);		// Protocol version
    STORE_BYTE (p, 0);	                        // Flags
    STORE_DWORD(p, HTOL32(COMM_WTX_TIMEOUT));   // WTX timeout
    STORE_WORD (p, HTOL16(MAX_FRAME_DATA_SIZE));// Max size of the data part of LL frame
    STORE_BYTE (p, pS->byCommSpeed);            // Target communication speed

    // Try to send it COMM_TRIES times before giving up when getting NACK reply
    for (i = 0; i < COMM_TRIES; ++i)
    {
        // Request the params change
        if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_SET_PARAMS, 0, (uint8 *)p - pS->pBuf)) != COMM_STATUS_OK) 
        {
            // Try it again
            continue;
        }
        
        // Receive the CONFIRM_PARAMS frame
        if ((status = LlReadFrame (pS, &byFrameType, &byFrameSeq, &dwLength)) != COMM_STATUS_OK) 
        {
            // Try it again
            continue;
        }

        if (byFrameType != LL_FRAME_TYPE_CONFIRM_PARAMS || dwLength != 1 || pS->pBuf[0] == FALSE)
        {
            // Try it one more time
            status = COMM_STATUS_ERROR;
        }
        else
        {
            // Parameters confirmed
            break;
        }
    }
    if (COMM_STATUS_OK != status) 
    {
        goto error;
    }

    // If we are here, everything was successful. We can change the communication speed
    if ((status = PT_UartSetParams (pS->byCommSpeed)) != COMM_STATUS_OK) 
    {
        goto error;
    }

    // Wait for ESS/TFM to change parameters - 100 msec should be enough
    //:TODO: the PC driver uses 200 msec - isn't it better?
    status = PT_TimerDelayMilliseconds (100);
    // Handle potential asynchronous power down
    if (COMM_STATUS_OK != status) {
        goto error;
    }

    // And clear the sequence number counter
    pS->bySeqNr = 0;

    status = COMM_STATUS_OK;

error:

    // Stop UART receiving
    PT_UartStopReceive();

    // Stop the UART clock
    PT_UartStopClock();

    return status;
}


/**
 *	As CommClose
 */
static COMM_STATUS HLClose (PT_SESSION *pS)
{
    COMM_STATUS status;
    uint8       byFrameType, byFrameSeq;
    uint32      dwLength, i;

    // Start UART clock, must be closed again before leaving PTClose()
    status = PT_UartStartClock();
    if (COMM_STATUS_OK != status) {
        goto exit;
    }

    // Clear the queue -- no data should be in transit
    PT_UartClearQueue ();

    // Enable receiving
    PT_UartStartReceive();

    // Estimate failure unless we really succeed
    status = COMM_STATUS_ERROR;

    for (i=0; i<COMM_TRIES; i++) {
        // Send the LL_FRAME_TYPE_CLOSE frame
        pS->pBuf[0] = 0;                  // Byte reserved for future use
        if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_CLOSE, 0, 1)) != COMM_STATUS_OK) 
        {
            goto exit;
        }

        // Await the acknowledge
        if ((status = LlReadFrame (pS, &byFrameType, &byFrameSeq, &dwLength)) != COMM_STATUS_OK) {
            continue;       // Try again
        }

        if ((byFrameType != LL_FRAME_TYPE_ACK) || (dwLength != 0)) {
            continue;       // Try again
        }

        // If we are here -> acknowledged, we can exit
        status = COMM_STATUS_OK;
        break;
    }

exit:

    if (COMM_STATUS_OK != status) 
    {
        status = COMM_STATUS_ERROR;
    }

    // Stop receiving
    PT_UartStopReceive();

    // Stop UART clock
    PT_UartStopClock();

    return status;
}


/**
 *	As CommTransact
 */
static COMM_STATUS HLTransact (PT_SESSION *pS,
                        IN  uint32   dwSendFragmentSize, IN  uint32   dwSendTotalSize, IN  uint8   bySendFragmentType,
                        OUT uint32 *pdwRecvFragmentSize, OUT uint32 *pdwRecvTotalSize, OUT uint8 *pbyRecvFragmentType,
                        IN  uint32   dwFlags)
{
    COMM_STATUS status;
    BOOL        boNavigCanceled = FALSE;
    uint32      dwTriesLeft = COMM_TRIES;

    // Create the outbound packet
    // We have to move the data frame to make a space for the header
    //:TODO: Modify LlWriteFrame to be able to handle TL packets directly, without adding the header into the buffer
    switch (bySendFragmentType) {
        case TLTYPE_SINGLE:
        case TLTYPE_FIRST:
            // 3 byte TL header
            if (dwSendFragmentSize+3 > MAX_FRAME_DATA_SIZE || dwSendTotalSize > 0x3FFFF) 
                return COMM_STATUS_ERROR;
            if (dwSendFragmentSize) 
                PT_memmove (pS->pBuf+3, pS->pBuf, dwSendFragmentSize);
            dwSendFragmentSize += 3;
            pS->pBuf[0] = (uint8) (bySendFragmentType+(dwSendTotalSize>>16)+TL_PROTOCOL_VERSION);
            pS->pBuf[1] = (uint8) (dwSendTotalSize);
            pS->pBuf[2] = (uint8) (dwSendTotalSize >> 8);
            break;

        case TLTYPE_MIDDLE:
        case TLTYPE_LAST:
        case TLTYPE_CONTINUE:
            // 1 byte TL header
            if (dwSendFragmentSize+1 > MAX_FRAME_DATA_SIZE) 
                return COMM_STATUS_ERROR;
            if (bySendFragmentType == TLTYPE_CONTINUE && dwSendFragmentSize) 
                return COMM_STATUS_ERROR; // TLSF_CONTINUE packet must have no data!
            if (dwSendFragmentSize) 
                PT_memmove (pS->pBuf+1, pS->pBuf, dwSendFragmentSize);
            dwSendFragmentSize += 1;
            pS->pBuf[0] = (uint8) (bySendFragmentType+TL_PROTOCOL_VERSION);
            break;

        default:
            // Unknown type
            return COMM_STATUS_ERROR;
    }

    // Start UART clock, must be closed again before leaving PTClose()
    status = PT_UartStartClock();
    if (COMM_STATUS_OK != status) {
        goto error;
    }
    
    // Clear the queue -- no data should be in transit
    PT_UartClearQueue ();

    // Enable receiving
    PT_UartStartReceive();

    // And copy the packet for error recovery purposes
    PT_memcpy (pS->pLastSentFragmentBuf, pS->pBuf, dwSendFragmentSize);
    pS->dwLastSentFragmentLength = dwSendFragmentSize;

    // Send the packet
    if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_DATA, pS->bySeqNr, dwSendFragmentSize)) != COMM_STATUS_OK) 
    {
        goto error;
    }	    

    if (dwFlags & TRANSACT_FLAG_SLEEP)
    {
        // handle sleep in session
        
        
        // wait for AWAKE signal to be set
        status = WaitForAwake(FALSE, COMM_TIMEOUT);
        
        // if AWAKE wasn't set within timeout, device can report error - try to receive frame
        // else continue in sleep procedure
        if(status != COMM_STATUS_TIMEOUT)
        {
            if (status != COMM_STATUS_OK) 
            {
                goto error; // communication problem
            }

            PT_UartSetWakeup(0); // set WAKEUP=0 to confirm transition to sleep

            // ESS is sleeping now, we can turn off the UART clock to save some power

            // Disable receiving
            PT_UartStopReceive();

            // Stop UART clock
            PT_UartStopClock();

            for ( ; ; ) 
            {
                uint32 awake;
                // Check AWAKE state
                if ((status = PT_UartGetAwake(&awake)) != COMM_STATUS_OK) 
                {
                    // No need to enable UART clock again, because it is turned
                    // off at the end anyway

                    goto error;
                }
                if (awake != 0) 
                {
                    // FM woke up by itself (HW finger detect)

                    // Start UART clock
                    status = PT_UartStartClock();
                    if (COMM_STATUS_OK != status) {
                        goto error;
                    }

                    // Clear the queue -- no data should be in transit
                    PT_UartClearQueue ();

                    // Enable receiving
                    PT_UartStartReceive();

                    PT_UartSetWakeup(1); // set WAKEUP=1
                    break;
                } 
                else 
                {
                    if (pS->pfnIdleCallback != NULL)
                    {
                        uint8 response;
                        status = pS->pfnIdleCallback(pS->pIdleCallbackCtx, &response); // call idle callback
                        if (status != COMM_STATUS_OK || response == PT_SLEEP_STOP) 
                        {
                            // Start UART clock
                            status = PT_UartStartClock();
                            if (COMM_STATUS_OK != status) {
                                goto error;
                            }

                            // Clear the queue -- no data should be in transit
                            PT_UartClearQueue ();

                            // Enable receiving
                            PT_UartStartReceive();

                            // stop sleeping, wake-up
                            PT_UartSetWakeup(1); // set WAKEUP=1
                            // wait until FM really awakes
                            if ((status = WaitForAwake(TRUE, COMM_TIMEOUT)) != COMM_STATUS_OK) 
                            {
                                goto error; 
                            }

                            break;
                        }
                        // Handle potential asynchronous power down
                        status = PT_TimerGetStatus();
                        if (COMM_STATUS_OK != status) {
                            goto error;
                        }
                        if (!pS->boInSession) 
                        {
                            status = COMM_STATUS_ASYNC_POWERDOWN;
                            goto error;
                        }
                    } 
                    else 
                    {
                        status = PT_TimerDelayMilliseconds(50); // no callback, just save CPU power during polling
                        // Handle potential asynchronous power down
                        if (COMM_STATUS_OK != status) {
                            goto error;
                        }
                    }
                }
            }
        }
    }
    if (dwFlags & TRANSACT_FLAG_TEST)
    {
        uint32 i;
        // We should start with 500ms for the initial deassertion of the AWAKE signal 
        uint32 diagTimeout = (500 > pS->dwDiagTimeout) ? 500 : pS->dwDiagTimeout;

        pS->dwDiagResult = DIAG_RESULT_OK;

        // handle diagnostics of AWAKE/WAKEUP signals

        for (i = 0; i < pS->dwDiagLoops; i++) 
        {
            // wait until AWAKE==0
            if ((status = WaitForAwake(FALSE, diagTimeout)) != COMM_STATUS_OK) 
            { 
                if (status == COMM_STATUS_TIMEOUT) {
                    pS->dwDiagResult = DIAG_RESULT_TIMEOUT_NAWAKE;
                    break; // if timeout expired, still wait for response from FM
                }
                goto error;
            }

            // Continue with requested diag timeout
            diagTimeout = pS->dwDiagTimeout;

            PT_UartSetWakeup(0); // set WAKEUP=0
            status = WaitForAwake(TRUE, diagTimeout); // wait until AWAKE==1
            if (status != COMM_STATUS_OK) 
            {
                if (status == COMM_STATUS_TIMEOUT) 
                {
                    pS->dwDiagResult = DIAG_RESULT_TIMEOUT_AWAKE;
                    status = PT_TimerDelayMilliseconds(pS->dwDiagTimeout); // force timeout on ESS when waiting for WAKEUP to be set
                    // Handle potential asynchronous power down
                    if (COMM_STATUS_OK != status) {
                        goto error;
                    }
                    PT_UartSetWakeup(1); // set WAKEUP=1 
                    break; // if timeout expired, still wait for response from FM
                }
                goto error;
            }
            PT_UartSetWakeup(1); // set WAKEUP=1 before next step
        }
    }
     
    // Await the response
    for( ; ; )
    {
        uint8   byFrameType, byFrameSeq;
        uint32  dwLength;

        status = LlReadFrame (pS, &byFrameType, &byFrameSeq, &dwLength);
        if (status == COMM_STATUS_TIMEOUT) 
        {
            // Timeout - resend the last data fragment
            if (--dwTriesLeft == 0) 
            {
                goto error;  // Retries exhausted
            }

            PT_memcpy (pS->pBuf, pS->pLastSentFragmentBuf, pS->dwLastSentFragmentLength);
            if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_DATA, pS->bySeqNr, pS->dwLastSentFragmentLength)) != COMM_STATUS_OK) 
            {
                goto error;
            }
            continue;
        } 
        else if (status == COMM_STATUS_ERROR) 
        {
            // Problem receiving a frame. Most probably framing error or bad CRC. Send NACK and retry.
            //:TODO: Improve error codes to distinguish between soft and hard (non-recoverable) errors 
            void *p = pS->pBuf;
            STORE_WORD (p, HTOL16(0));		    // Error reason code - can be zero
            if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_NACK, 0, (uint8 *)p - pS->pBuf)) != COMM_STATUS_OK) 
            {
                goto error;
            }
            continue;
        } 
        else if (status != COMM_STATUS_OK) 
        {
            // Any other error - exit.
            goto error;
        }
        

        // Frame received OK - parse it!
        if (byFrameType == LL_FRAME_TYPE_DATA) 
        {
            // This is what we waited for
            if (byFrameSeq != pS->bySeqNr) 
            {
                status = COMM_STATUS_ERROR;
                goto error;
            }
            *pdwRecvFragmentSize = dwLength;
            break;
        } 
        else if (byFrameType == LL_FRAME_TYPE_RAW_DATA) 
        {
            // Process raw data (navigation data)
            PT_NAVIGATION_DATA navigData;
            uint8 response = PT_CANCEL;
            // get navigation data
            navigData.signalBits = ((uint16*)(pS->pBuf))[0]; // signal bits
            navigData.dx = ((sint16*)(pS->pBuf))[1]; // delta X
            navigData.dy = ((sint16*)(pS->pBuf))[2]; // delta Y
            if (pS->pfnNavigCallback != NULL && !boNavigCanceled) 
            {
                // call navigation callback
                status = pS->pfnNavigCallback(pS->pNavigCallbackCtx, &navigData, &response);
                if (status != COMM_STATUS_OK) 
                    response = PT_CANCEL;
                // Handle potential asynchronous power down
                status = PT_TimerGetStatus();
                if (COMM_STATUS_OK != status) {
                    goto error;
                }
                if (!pS->boInSession) 
                {
                    status = COMM_STATUS_ASYNC_POWERDOWN;
                    goto error;
                }
                if (response == PT_CANCEL) 
                    boNavigCanceled = TRUE;
            }
            // send response
            if ((dwFlags & TRANSACT_FLAG_NAV_BIDIR) || response == PT_CANCEL)
            {
                *(uint8*)(pS->pBuf) = response;
                if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_RAW_DATA, 0, sizeof(uint8))) != COMM_STATUS_OK) 
                {
                    goto error;
                }
            }
        } 
        else if (byFrameType == LL_FRAME_TYPE_WTX) 
        {
            // WTX - send continue
            // relax before send, TFM calls it from an interrupt and needs time to recover
            status = PT_TimerDelayMilliseconds(20);
            // Handle potential asynchronous power down
            if (COMM_STATUS_OK != status) {
                goto error;
            }
            if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_CONTINUE, 0, 0)) != COMM_STATUS_OK) 
            {
                goto error;
            }

            // If we are in the bidirectional navigation and received WTX, it means that a raw data packet was lost
            // Send a raw data to resume the raw data communication
            if (dwFlags & TRANSACT_FLAG_NAV_BIDIR)
            {
                void *p = pS->pBuf;
                STORE_BYTE (p, boNavigCanceled ? PT_CANCEL : PT_CONTINUE);
                if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_RAW_DATA, 0, (uint8 *)p - pS->pBuf)) != COMM_STATUS_OK) 
                {
                    goto error;
                }
            }
        } 
        else if (byFrameType == LL_FRAME_TYPE_NACK) 
        {
            // NACK - repeat the last data packet
            PT_memcpy (pS->pBuf, pS->pLastSentFragmentBuf, pS->dwLastSentFragmentLength);
            if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_DATA, pS->bySeqNr, pS->dwLastSentFragmentLength)) != COMM_STATUS_OK) 
            {
                goto error;
            }
        } 
        else 
        {
            // Nothing else is supposed to come -> error
            status = COMM_STATUS_ERROR;
            goto error;
        }
    }

    // We've received the data frame - parse it
    if ((pS->pBuf[0] & 0xE0) != TL_PROTOCOL_VERSION) 
    {
        status = COMM_STATUS_ERROR;
        goto error;
    }
    *pbyRecvFragmentType = (uint8) (pS->pBuf[0] & 0x1C);

    if (*pbyRecvFragmentType == TLTYPE_SINGLE || *pbyRecvFragmentType == TLTYPE_FIRST) 
    {
        // The TL header is 3 bytes
        if (*pdwRecvFragmentSize < 3) 
        {
            status = COMM_STATUS_ERROR;
            goto error;
        }
        *pdwRecvFragmentSize -= 3;
        *pdwRecvTotalSize = pS->pBuf[1] + (pS->pBuf[2] << 8) + ((pS->pBuf[0] & 0x03) << 16);
        if (*pdwRecvFragmentSize) 
            PT_memmove (pS->pBuf, pS->pBuf+3, *pdwRecvFragmentSize);
    } 
    else if (*pbyRecvFragmentType == TLTYPE_MIDDLE || *pbyRecvFragmentType == TLTYPE_LAST || *pbyRecvFragmentType == TLTYPE_CONTINUE) 
    {
        // The TL header is 1 byte
        if (*pdwRecvFragmentSize < 1 ||
            (*pbyRecvFragmentType == TLTYPE_CONTINUE && *pdwRecvFragmentSize != 1)) 
        {
            status = COMM_STATUS_ERROR;
            goto error;
        }
        *pdwRecvFragmentSize -= 1;
        *pdwRecvTotalSize = 0;
        if (*pdwRecvFragmentSize) 
        {
            PT_memmove (pS->pBuf, pS->pBuf+1, *pdwRecvFragmentSize);
        } 
    } 
    else 
    {
        // Other possible TL packets we currently do not recognize
        status = COMM_STATUS_ERROR;
        goto error;
    }

    status = COMM_STATUS_OK;
    
    // Success, increment the sequence counter
    pS->bySeqNr = (uint8)((pS->bySeqNr+1) & 0x0f);

error:

    return status;
}



//========================================================================
//		Exported functions
//========================================================================

/**
 * Open a communication session.
 * Must not be called if a session is already opened.
 *
 * @param pS Pointer to session block
 * @return Status code
 */
COMM_STATUS PT_CommOpen (IN PT_SESSION *pS)
{
    COMM_STATUS status;

    if (pS->boInSession) 
    {
        // Already in session
        status = COMM_STATUS_ERROR;
    } 
    else 
    {
        status = HLOpen (pS);
    }

    // Check the result
    if (status != COMM_STATUS_OK) 
    {
        // Close everything
        PT_CommClose(pS);
    } 
    else 
    {
        // The session is open
        pS->boInSession = TRUE;
    }
    return status;
}


/**
 * Close a communication session.
 * If a session is already closed, does nothing.
 *
 * @param pS Pointer to session block
 * @return Status code
 */
COMM_STATUS PT_CommClose (IN PT_SESSION *pS)
{
    COMM_STATUS status = COMM_STATUS_OK;

    if (pS->boInSession) 
    {
        // Session is not yet closed - let's try to close it
        status = HLClose (pS);
    }

    // Stop receiving
    PT_UartStopReceive();

    // Close UART
    PT_UartClose ();

    // Drive XRTS1 (WAKEUP) low to let the ESS sleep
    PT_UartSetWakeup(0);
    
    pS->boInSession = FALSE;
    return status;
}


/**
 * Perform a transport-layer transaction (send and/or receive a data fragment).
 * A session must be already opened.
 * If CommTransact returns with any other code than COMM_STATUS_OK, a non-recoverable problem
 * has ocurred. The current session is automatically terminated. CommOpen must be called
 * before any communication can continue.
 *
 * The outbound fragment had to be prepared in advance in the pS->pBuff buffer.
 * The inbound fragment will be after the transaction available in the pS->pBuff buffer.
 *
 * @param pS				Pointer to session block
 * @param dwSendFragmentSize    Length of the fragment to be sent
 * @param dwSendTotalSize       Total length of all the outbound fragments. 
 *                              Used only for fragment types TLTYPE_SINGLE and TLTYPE_FIRST.
 * @param bySendFragmentType    Type of the fragment to be sent, see TLTYPE_xxxx
 *
 * @param pdwRecvFragmentSize   Length of the received fragment
 * @param pdwRecvTotalSize      Total length of all the inbound fragments. 
 *                              Valid only when recv. fragment type is TLTYPE_SINGLE or TLTYPE_FIRST.
 * @param pbyRecvFragmentType   Type of the received fragment, see TLTYPE_xxxx
 * @param dwFlags               Flags controlling special behavior (sleep, WAKEUP/AWAKE behavior etc.)
 *                              See TRANSACT_FLAG_xxxx.
 *
 * @return Status code.
 */
COMM_STATUS PT_CommTransact (IN  PT_SESSION *pS,
                             IN  uint32   dwSendFragmentSize, IN  uint32   dwSendTotalSize, IN  uint8   bySendFragmentType,
                             OUT uint32 *pdwRecvFragmentSize, OUT uint32 *pdwRecvTotalSize, OUT uint8 *pbyRecvFragmentType,
                             IN  uint32   dwFlags)
{
    COMM_STATUS status;

    if (! pS->boInSession) 
    {
        // Not in session
        status = COMM_STATUS_SESSION_TERMINATED;
    } 
    else 
    {
#if defined(ENABLE_PTSECURECHANNEL)
        // process data for secure channel
        status = ProcessSendData(pS, bySendFragmentType, dwSendFragmentSize, dwSendTotalSize, &dwSendFragmentSize, &dwSendTotalSize);
        if (status != PT_STATUS_OK) goto end;
#endif // #if defined(ENABLE_PTSECURECHANNEL)

        status = HLTransact (pS,
                             dwSendFragmentSize, dwSendTotalSize, bySendFragmentType,
                             pdwRecvFragmentSize, pdwRecvTotalSize, &pS->byLastRecvFragmentType,
                             dwFlags);

        *pbyRecvFragmentType = pS->byLastRecvFragmentType;

#if defined(ENABLE_PTSECURECHANNEL)
        if (status == PT_STATUS_OK)
        {
            // process received data from secure channel
            status = ProcessReceiveData(pS, *pbyRecvFragmentType, *pdwRecvFragmentSize, pdwRecvFragmentSize);
        }
#endif // #if defined(ENABLE_PTSECURECHANNEL)

    }
    
#if defined(ENABLE_PTSECURECHANNEL)
end:; 
#endif // #if defined(ENABLE_PTSECURECHANNEL)

    // Check the result
    if (status != COMM_STATUS_OK) 
    {
        // Close UART
        PT_UartClose ();
        pS->boInSession = FALSE;
    }
    return status;
}



