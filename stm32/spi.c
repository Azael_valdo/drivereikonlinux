/**
 * @file spi.c
 * Low layer for a comunication with TCD50 using a spi interface.
 *
 * Copyright (C) 2008 UPEK Inc.
 *
 * @author Martin Krahulik
 */

#include "stm32f10x_lib.h"      // Definitions of the STM32 library
#include "configuration.h"    // Definition of a platform
#include "spi.h"                // Spi functions header
#include "tfmerror.h"           // Error codes
#include "timer.h"
#include "defines.h"

#define DMA_CHANNEL_ENABLE 0x00000001
#define DMA_CHANNEL_DISABLE 0xFFFFFFFE


/* Function initializes an Spi layer.
*/ 
PT_STATUS SpiInitialize(void){

    //set an SS (CS) pin to 0 
    GPIO_ResetBits(SPI_CS_SIGNAL_GPIO, SPI_CS_SIGNAL_PIN);
    
    return PT_STATUS_OK;
}

/* Spi layer destructor.
*/ 
void SpiDestroy(void)
{

}

/** Function tests, if an AWAKE signal is low. If so, a device is considered to be awake.
* @return TRUE, if awake; FALSE otherwise.
**/
bool8 SpiIsAwake(void)
{
    bool8 isWake = FALSE;
    
    isWake = (GPIO_ReadInputDataBit(SPI_AWAKE_SIGNAL_GPIO, SPI_AWAKE_SIGNAL_PIN) == COMM_AWAKE_ACTIVE_STATE);
    return isWake;   
}

/** Function produces a short (approx. 3ms) impulse on a CS line causing a hardware wake up of a TCD device.
**/
void SpiWakeUp()
{
#if (COMM_AWAKE_ACTIVE_STATE == 0)
    GPIO_SetBits(SPI_CS_SIGNAL_GPIO, SPI_CS_SIGNAL_PIN);
    PT_TimerDelayMilliseconds (3);
    GPIO_ResetBits(SPI_CS_SIGNAL_GPIO, SPI_CS_SIGNAL_PIN);  
#else
    GPIO_ResetBits(SPI_CS_SIGNAL_GPIO, SPI_CS_SIGNAL_PIN);
    PT_TimerDelayMilliseconds (3);
    GPIO_SetBits(SPI_CS_SIGNAL_GPIO, SPI_CS_SIGNAL_PIN);
#endif  //#if (COMM_AWAKE_ACTIVE_STATE == 0) 
}


/** Function sends a miniframe(9-bytes) over a spi bus. It set a CS signal to high, starts a clock, transmits a miniframe, stops a clock, and sets a CS signal to low.
* @param pSendBuffer Buffer containing miniframe data sent on a SPI bus (input, size of SPI_PACKET_SIZE).
* @param pReceiveBuffer Buffer, where data comming on a SPI bus are stored (output, size of SPI_PACKET_SIZE).
* @return PT_STATUS_OK.
**/
PT_STATUS SpiSendAndReceiveMiniframe(IN void *pSendBuffer, OUT void *pReceiveBuffer)
{
    PT_STATUS status = PT_STATUS_OK;
        
    //set an SS (CS) pin to COMM_AWAKE_ACTIVE_STATE
#if (COMM_AWAKE_ACTIVE_STATE == 0)
    GPIO_SetBits(SPI_CS_SIGNAL_GPIO, SPI_CS_SIGNAL_PIN); 
#else
    GPIO_ResetBits(SPI_CS_SIGNAL_GPIO, SPI_CS_SIGNAL_PIN);
#endif  //#if (COMM_AWAKE_ACTIVE_STATE == 0) 

    
#if !defined(TCD50D_TCS1_TCS2)
    //wait approx. 10us
    PT_TimerDelayMicroseconds(10);
#endif //if !defined(TCD50D_TCS1_TCS2)

    //start DMA transfer (Rx direction)
    //set memory dst address
    DMA1_Channel4->CMAR = (uint32)pReceiveBuffer;

    //set buffer size
    DMA1_Channel4->CNDTR = SPI_PACKET_SIZE;

    //enable DMA1 channel4
    DMA1_Channel4->CCR |= DMA_CHANNEL_ENABLE;

    //start DMA transfer (Tx direction)
    //set memory dst address
    DMA1_Channel5->CMAR = (uint32)pSendBuffer;

    //set buffer size
    DMA1_Channel5->CNDTR = SPI_PACKET_SIZE;

    //enable DMA1 channel4
    DMA1_Channel5->CCR |= DMA_CHANNEL_ENABLE;

    //wait till whole miniframe is received
    while(DMA1_Channel4->CNDTR != 0)
    {
        ;
    }

    
    // Reset a chip select signal
#if (COMM_AWAKE_ACTIVE_STATE == 0)
    GPIO_ResetBits(SPI_CS_SIGNAL_GPIO, SPI_CS_SIGNAL_PIN); 
#else
    GPIO_SetBits(SPI_CS_SIGNAL_GPIO, SPI_CS_SIGNAL_PIN);
#endif  //#if (COMM_AWAKE_ACTIVE_STATE == 0) 
  

    // Clear pending DMA RX transfer
    DMA1_Channel4->CCR &= DMA_CHANNEL_DISABLE;
      
    // Clear pending DMA TX transfer
    DMA1_Channel5->CCR &= DMA_CHANNEL_DISABLE;
       
    return status;
}


