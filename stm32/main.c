/**
 * @file main.c
 *
 * Example showing an enrollment on device.
 *
 * Copyright (C) 2008 UPEK Inc.
 *
 * @author Martin Krahulik
 */

/* Includes platform dependent ------------------------------------------*/
#include "configuration.h"  // Configuration of the platform
#include "ui.h"             // User interface input/output functions 

/* Includes platform independent ----------------------------------------*/
#include "tfmapi.h"
#include "tfmerror.h"
#include "types.h"
#include "tfmtypes.h"



int main(void)
{ 
    PT_STATUS status;
    PT_CONNECTION hConnection;
 
    //Configure platform interfaces (i.e. USART, SPI). See configuration.c file.
    ConfigureInterfaces();
    
    //Example code
    status = PTInitialize(NULL);
    if(status!=PT_STATUS_OK){
        UiPrint("Unable to initilialize PTApi: %d\n\r",status);
        goto lbExit;
    }
    
    status = PTOpen("",&hConnection);
    if(status!=PT_STATUS_OK){
        UiPrint("Unable to open a device: %d\n\r",status);
        goto lbExit;
    }
    
    
    // Place here your code
    
    
    status = PTClose(hConnection);
    if(status!=PT_STATUS_OK){
        UiPrint("Unable to close a device: %d\n\r",status);
        goto lbExit;
    }
    
    PTTerminate();
    if(status!=PT_STATUS_OK){
        UiPrint("Unable to terminate PTApi: %d\n\r",status);
        goto lbExit;
    }

lbExit:;
    
    while (1)
    {
    }
}

