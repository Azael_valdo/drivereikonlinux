/**
 * @file ui.h
 * User interface functions.
 *
 * Copyright (C) 2008 UPEK Inc.
 *
 * @author Martin Krahulik
 */
 
#ifndef _UI_H_
#define _UI_H_

#include "types.h"
#include "stdio.h"

/** Macro defines a printf-like function.
*/
#define UiPrint \
            printf

#ifdef __GNUC__
    /** Function used by a Keil library printf implementation.
    * @param ch Character to be printed.
    * @return Character to be printed.
    */
    int __io_putchar(int ch);
#else
    /** Function used by a Keil library printf implementation.
    * @param ch Character to be printed.
    * @param f Output file.
    * @return Character to be printed.
    */
    int fputc(int ch, FILE *f);
#endif /* __GNUC__ */


/** Function gets an input character (in STM32 demo implementation from USART interface).
* @return Character.
*/
uint8 UiGetc(void);            
            
#endif //_UI_H_ 
