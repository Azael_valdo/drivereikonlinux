/**
 * @file timer.c
 *
 * Timer-related support functions, needed by the ESS/TFM comm library.
 * 
 */

#include "stm32f10x_lib.h"
#include "timer.h"


//========================================================================
//		Exported functions
//========================================================================

/**
 *	Number of milliseconds since startup
 */
uint32  PT_TimerGetMilliseconds (void)
{
    return RTC_GetCounter();
}


/**
 *	Wait defined number of milliseconds
 */
COMM_STATUS PT_TimerDelayMilliseconds(uint32 dwMilliseconds)
{
    //PT_TimerDelayMicroseconds has capacity 4294967ms, timer2 counter 65536ms
    return PT_TimerDelayMicroseconds(dwMilliseconds * 1000);
}

/**
 *	Wait defined number of microseconds
 */

 COMM_STATUS PT_TimerDelayMicroseconds(uint32 dwMicroseconds)
{	
    uint32 stop = TIM3->CNT + dwMicroseconds;

    //wait till remaining time is smaller than 100
    //at 1000 timer3 overruns and resets to zero, sets update flag
    //(20ms interval since TIM3 value 995 = 5ms to 1000 + 15ms to TIM3 value 15, stop at beginning = 995 + 20 = 115)
    while(stop >= 1000)
    {
        //clear flags
        TIM3->SR = 0;
        while((TIM3->SR & TIM_FLAG_Update) != TIM_FLAG_Update) ; //at update timer3 has reset to zero
        stop -= 1000;
    }

    //wait for the remaining time
    while (TIM3->CNT <= stop) ; 

    return COMM_STATUS_OK;
}

/**
 * Check timer status
 */
COMM_STATUS PT_TimerGetStatus(void)
{
    return COMM_STATUS_OK;
}




