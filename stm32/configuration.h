/**
 * @file configuration.h
 * Configuration of the STM32 platform for the Tfmsimple library.
 *
 * Copyright (C) 2008 UPEK Inc.
 *
 * @author Martin Krahulik
 */

#ifndef _CONFIGURATION_H_
#define _CONFIGURATION_H_

#include "defines.h"
#include "stm32f10x_lib.h"      // Definitions of the STM32 library

//---Defines-------------------------------------------------------

/**Selection of the used hardware platform.
*/
#if !defined (USE_STM3210B_EVAL) &&  !defined (USE_STM3210E_EVAL)
 #define USE_STM3210B_EVAL
 //#define USE_STM3210E_EVAL
#endif


// Flash page size
#ifdef USE_STM3210B_EVAL
  #define FLASH_PAGE_SIZE    ((u16)0x400)
#elif defined USE_STM3210E_EVAL
  #define FLASH_PAGE_SIZE    ((u16)0x800)
#endif 

// PLL multiplication factor
#if defined(TCD50D_TCS1_TCS2)
    #define PLL_MUL 7  //8MHz * 7 = 56 MHz, needed for reaching SPI communication speed 7MHz 
#else
    #define PLL_MUL 9  //8MHz * 9 = 72 MHz 
#endif //#if defined(TCD50D_TCS1_TCS2)

#define USE_USART1
//#define USE_USART2
//#define USE_USART3
//#define USE_UART4
//#define USE_UART5

#ifdef USE_USART1
  #define  USARTx                     USART1
  #define  GPIOx                      GPIOA
  #define  RCC_APB2Periph_GPIOx       RCC_APB2Periph_GPIOA
  #define  GPIO_RxPin                 GPIO_Pin_10
  #define  GPIO_TxPin                 GPIO_Pin_9
#elif defined USE_USART2 && defined USE_STM3210B_EVAL
  #define  USARTx                     USART2
  #define  RCC_APB1Periph_USARTx      RCC_APB1Periph_USART2
  #define  GPIOx                      GPIOD
  #define  RCC_APB2Periph_GPIOx       RCC_APB2Periph_GPIOD
  #define  GPIO_TxPin                 GPIO_Pin_5
  #define  GPIO_RxPin                 GPIO_Pin_6
#elif defined USE_USART2 && defined   USE_STM3210E_EVAL  
  #define  USARTx                     USART2
  #define  RCC_APB1Periph_USARTx      RCC_APB1Periph_USART2
  #define  GPIOx                      GPIOA
  #define  RCC_APB2Periph_GPIOx       RCC_APB2Periph_GPIOA
  #define  GPIO_TxPin                 GPIO_Pin_2
  #define  GPIO_RxPin                 GPIO_Pin_3
#elif defined USE_USART3
  #define  USARTx                     USART3
  #define  GPIOx                      GPIOB
  #define  RCC_APB2Periph_GPIOx       RCC_APB2Periph_GPIOB
  #define  RCC_APB1Periph_USARTx      RCC_APB1Periph_USART3
  #define  GPIO_RxPin                 GPIO_Pin_11
  #define  GPIO_TxPin                 GPIO_Pin_10
#elif defined USE_UART4
  #define  USARTx                     UART4
  #define  GPIOx                      GPIOC
  #define  RCC_APB2Periph_GPIOx       RCC_APB2Periph_GPIOC
  #define  RCC_APB1Periph_USARTx      RCC_APB1Periph_UART4
  #define  GPIO_RxPin                 GPIO_Pin_11
  #define  GPIO_TxPin                 GPIO_Pin_10
#elif defined USE_UART5
  #define  USARTx                     UART5
  #define  GPIOx                      GPIOC
  #define  RCC_APB2Periph_GPIOx       RCC_APB2Periph_GPIOC
  #define  RCC_APB1Periph_USARTx      RCC_APB1Periph_UART5
  #define  GPIO_RxPin                 GPIO_Pin_2
  #define  GPIO_TxPin                 GPIO_Pin_12
#endif

//--- SPI Configuration ------------------------------------------------------
#define SPI_INTERFACE SPI2
#if defined(TCD50D_TCS1_TCS2)
    #define SPI_BUS_PRESCALER SPI_BaudRatePrescaler_4// TCD supports clock rates 250kHz - 1MHz, 36MHz / 128 = 281250 Hz
#else
    #define SPI_BUS_PRESCALER SPI_BaudRatePrescaler_128// TCD supports clock rates 250kHz - 1MHz, 36MHz / 128 = 281250 Hz
#endif //#if defined(TCD50D_TCS1_TCS2)
    
#define SPI_INTERFACE_GPIO GPIOB

#define SPI_CLK_SIGNAL_PIN GPIO_Pin_13    // clock signal
#define SPI_MISO_SIGNAL_PIN GPIO_Pin_14   // MISO signal
#define SPI_MOSI_SIGNAL_PIN GPIO_Pin_15   // MOSI signal
    
#define SPI_CS_SIGNAL_GPIO GPIOB
#define SPI_CS_SIGNAL_PIN  GPIO_Pin_12    // chip select (NSS)GPIO_Pin_12

#define SPI_AWAKE_SIGNAL_GPIO GPIOB
#define SPI_AWAKE_SIGNAL_PIN GPIO_Pin_0   //Data 0 on Mantisse board

#define SPI_DR_Address    0x4000380C 	  // SPI DR register address


//---Functions----------------------------------------------------------
/**
* Function configures interrupts and clock, USART, and SPI interfaces.
**/
void ConfigureInterfaces(void);

#endif //_CONFIGURATION_H_
