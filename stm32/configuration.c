/**
 * @file configuration.c
 *
 * Configuration of the STM32 platform for the Tfmsimple library.
 *
 * Copyright (C) 2008 UPEK Inc.
 *
 * @author Martin Krahulik
 */

#include "stm32f10x_lib.h"
#include "configuration.h"

/**
* Configures the different system clocks.
**/
void RccConfiguration(void)
{
  ErrorStatus HSEStartUpStatus;

  /* RCC system reset(for debug purpose) */
  RCC_DeInit();

  /* Enable HSE */
  RCC_HSEConfig(RCC_HSE_ON);

  /* Wait till HSE is ready */
  HSEStartUpStatus = RCC_WaitForHSEStartUp();

  if(HSEStartUpStatus == SUCCESS)
  {
    /* Enable Prefetch Buffer */
    FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);

    /* Flash 2 wait state */
    FLASH_SetLatency(FLASH_Latency_2);
 
    /* HCLK = SYSCLK */
    RCC_HCLKConfig(RCC_SYSCLK_Div1); 
  
    /* PCLK2 = HCLK */
    RCC_PCLK2Config(RCC_HCLK_Div1); 

    /* PCLK1 = HCLK/2 */
    RCC_PCLK1Config(RCC_HCLK_Div2);

    /* PLLCLK = 8MHz * PLL_MUL*/
#define MULT1(mult) RCC_PLLMul_##mult
#define MULT2(mult) MULT1(mult)
    RCC_PLLConfig(RCC_PLLSource_HSE_Div1, MULT2(PLL_MUL));
#undef MULT2
#undef MULT1

    /* Enable PLL */ 
    RCC_PLLCmd(ENABLE);

    /* Wait till PLL is ready */
    while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)
    {
    }

    /* Select PLL as system clock source */
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);

    /* Wait till PLL is used as system clock source */
    while(RCC_GetSYSCLKSource() != 0x08)
    {
    }
  }
    
  /* Enable GPIOx clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOx, ENABLE);

  // Enable DMA1 clock 
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
  
  //enable SPI_GPIO clock   
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOC|RCC_APB2Periph_AFIO, ENABLE); 
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2 | RCC_APB1Periph_TIM2 | RCC_APB1Periph_TIM3, ENABLE);        

/* Enable USARTx clocks */
#ifdef USE_USART1  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
#else
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USARTx, ENABLE);
#endif
}

/**
* Configures the different GPIO ports.
**/
void GpioConfiguration(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

#if defined USE_USART2	&& defined USE_STM3210B_EVAL
  /* Enable AFIO clock */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

  /* Enable the USART2 Pins Software Remapping */
  GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);
#endif

  /* Configure USARTx_Tx as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = GPIO_TxPin;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOx, &GPIO_InitStructure);

  /* Configure USARTx_Rx as input floating */
  GPIO_InitStructure.GPIO_Pin = GPIO_RxPin;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOx, &GPIO_InitStructure);
  
#if defined SPI_INTERFACE
  // Configure SPI pins: SCK, MISO and MOSI ---------------------------------
  GPIO_InitStructure.GPIO_Pin = SPI_CLK_SIGNAL_PIN | SPI_MISO_SIGNAL_PIN | SPI_MOSI_SIGNAL_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; //GPIO_Mode_AF_PP;
  GPIO_Init(SPI_INTERFACE_GPIO, &GPIO_InitStructure);
  
  // Configure CS pin to be manually controlled by the application
  GPIO_InitStructure.GPIO_Pin = SPI_CS_SIGNAL_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(SPI_CS_SIGNAL_GPIO, &GPIO_InitStructure);
  
  // Configure SPI awake port
  GPIO_InitStructure.GPIO_Pin = SPI_AWAKE_SIGNAL_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(SPI_AWAKE_SIGNAL_GPIO, &GPIO_InitStructure);

#endif //defined SPI_INTERFACE 
}

/**
* Configures Vector Table base location.
**/
void NvicConfiguration(void)
{ 
#ifdef  VECT_TAB_RAM  
  /* Set the Vector Table base location at 0x20000000 */ 
  NVIC_SetVectorTable(NVIC_VectTab_RAM, 0x0); 
#else  /* VECT_TAB_FLASH  */
  /* Set the Vector Table base location at 0x08000000 */ 
  NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);   
#endif

}

/**
* Configures USART.
**/
void UsartConfiguration(void)
{ 
  USART_InitTypeDef USART_InitStructure;

    /* USARTx configured as follow:
        - BaudRate = 115200 baud  
        - Word Length = 8 Bits
        - One Stop Bit
        - No parity
        - Hardware flow control disabled (RTS and CTS signals)
        - Receive and transmit enabled
  */
  USART_InitStructure.USART_BaudRate = 115200;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  
  /* Configure the USARTx */ 
  USART_Init(USARTx, &USART_InitStructure);
  /* Enable the USARTx */
  USART_Cmd(USARTx, ENABLE);
}

/**
* Cleans previous DMA transfer and configures a new one.
**/
void DMAConfiguration(void)
{
    //---RX direction---
    // Reset DMA1 Channel4 memory address register 
    DMA1_Channel4->CMAR = 0;
     
    // Reset control register
    DMA1_Channel4->CCR =	DMA_DIR_PeripheralSRC | DMA_Mode_Normal |
                            DMA_PeripheralInc_Disable | DMA_MemoryInc_Enable |
                            DMA_PeripheralDataSize_Byte | DMA_MemoryDataSize_Byte |
                            DMA_Priority_VeryHigh | DMA_M2M_Disable; 

    // Reset a peripheral base address
    DMA1_Channel4->CPAR = SPI_DR_Address;
         
    // Enable SPI2 DMA Rx request
    SPI2->CR2 |= SPI_I2S_DMAReq_Rx;

    //---TX direction---

    // Reset DMA1 Channel5 memory address register 
    DMA1_Channel5->CMAR = 0;
     
    // Reset control register
    DMA1_Channel5->CCR =	DMA_DIR_PeripheralDST | DMA_Mode_Normal |
                            DMA_PeripheralInc_Disable | DMA_MemoryInc_Enable |
                            DMA_PeripheralDataSize_Byte | DMA_MemoryDataSize_Byte |
                            DMA_Priority_High | DMA_M2M_Disable; 

    // Reset a peripheral base address
    DMA1_Channel5->CPAR = SPI_DR_Address;
         
    // Enable SPI2 DMA Tx request
    SPI2->CR2 |= SPI_I2S_DMAReq_Tx;
}

/**
* Configures SPI.
**/
void SpiConfiguration(void)
{
    SPI_InitTypeDef  spiInitStructure;
        
    spiInitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    spiInitStructure.SPI_Mode = SPI_Mode_Master;
    spiInitStructure.SPI_DataSize = SPI_DataSize_8b;
    spiInitStructure.SPI_CPOL = SPI_CPOL_Low;                          // Clock idle low
    spiInitStructure.SPI_CPHA = SPI_CPHA_1Edge;                        // Data is captured on the first edge
    spiInitStructure.SPI_NSS = SPI_NSS_Soft;                           // Chip select activated by a software, so NSS bit remains free for TCD control
    spiInitStructure.SPI_BaudRatePrescaler = SPI_BUS_PRESCALER;  
    spiInitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    spiInitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(SPI_INTERFACE, &spiInitStructure);
        
    /* Disable SPI CRC calculation */
    SPI_CalculateCRC(SPI_INTERFACE, DISABLE);
    
    /* Enable SPI */
    SPI_Cmd(SPI_INTERFACE, ENABLE);
    
    SPI_SSOutputCmd(SPI_INTERFACE, ENABLE);
    SPI_NSSInternalSoftwareConfig(SPI_INTERFACE, SPI_NSSInternalSoft_Set);
       
}

/**
* Configures the RTC.
**/
void RTC_Configuration(void)
{

  /* Enable PWR and BKP clocks */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);

  /* Allow access to BKP Domain */
  PWR_BackupAccessCmd(ENABLE);

  /* Reset Backup Domain */
  BKP_DeInit();
  
  /* Enable the LSI OSC */
  RCC_LSICmd(ENABLE);
  
  /* Wait till LSI is ready */
  while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
    ;
    
  /* Select the RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

  /* Enable RTC Clock */
  RCC_RTCCLKCmd(ENABLE);

  /* Wait for RTC registers synchronization */
  RTC_WaitForSynchro();
  
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();

  /* Enable the RTC Second */
  RTC_ITConfig(RTC_IT_SEC, ENABLE);

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();

  /* Set RTC prescaler: set RTC period to 1sec */
  RTC_SetPrescaler(40); /* RTC frequency = RTCCLK/RTC_PR = (40.000 KHz)/(39999+1) */

  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
  
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
  
  /* Reset a time */
  RTC_SetCounter(0);
  /* Wait until last write operation on RTC registers has finished */
  RTC_WaitForLastTask();
}

/**
* Configures timers for 100us and us.
*/
void TimerConfiguration(void)
{
    // We will use two counters chained in master/slave configuration. TIM3 will be configured to
    // count microseconds and to be master to TIM2. TIM2 will be slave of TIM3 and will count 
    // milliseconds.
    
    // Update the counter each 8*PLL_MUL CK_CNT ticks (i.e. each 1us)
    // PSC (Prescaler Value)
    TIM3->PSC = 8*PLL_MUL;
    
    //reset value
    TIM3->ARR = 1000;			   
      
    // Set counter to zero
    TIM3->CNT = 0;
        
    TIM3->EGR = 0x0000;

    // Enable timer3
    // CEN (Counter ENable)
    TIM3->CR1 = 0x0001;		    
}

/**
*  Configures interfaces.
**/

void ConfigureInterfaces(void){      
    /* System Clocks Configuration */
    RccConfiguration();
       
    /* NVIC configuration */
    NvicConfiguration();

    /* Configure the GPIO ports */
    GpioConfiguration();

    /* USARTx configuration */
    UsartConfiguration();

    /* DMA configuration for SPI communication */
    DMAConfiguration();
    
    /* SPIx configuration */
    SpiConfiguration();
    
    /* RTC configuration */
    RTC_Configuration();

    /* Timer configuration */
    TimerConfiguration();	
}
