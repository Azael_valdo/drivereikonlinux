/**
 * @file utils.c
 *
 * Utility functions, e.g. things from <string.h>, which are not available in EPOC
 * 
 * @author Vladimir Lieberzeit (vladimir.lieberzeit@st.com)
 */

#include "utils.h"
#include <string.h>


//========================================================================
//      Exported frunctions
//========================================================================

/**
 *	Copy data block
 */
void*	PT_memcpy (void *dst, const void *src, uint32 size)
{
    memcpy(	dst, src, size);	
    return dst;
}


/**
 *	Move data block - source and destination may overlap
 */
void*	PT_memmove (void *dst, const void *src, uint32 size)
{
    //this implementation seems to be faster than STM32 native implementation
    if (src > dst) {
        // Bottom-up
        memcpy(dst,src, size);
    } else {
        // Top-down
        uint8 *pDst = (uint8*)dst+size;
        uint8 *pSrc = (uint8*)src+size;
        uint8 *pEnd = (uint8*)src;
        while (pSrc > pEnd) *--pDst = *--pSrc;
    }  

    return dst;
}

/**
 *	Fill in data block
 */
void*	PT_memset (void *dst, uint8 fillByte, uint32 size)
{
    uint8 *itDst = (uint8*)dst;
    uint32 idx;
    
    for(idx=0; idx<size; idx++, itDst++)
       *itDst = fillByte; 
        
    return dst;
}

/**
 * Compare block of memory
 */
sint32 PT_memcmp(const void *first, const void  *second, uint32 size)
{
   
    uint8 *c1,*c2;
    for (c1 = (uint8*)first, c2 = (uint8*)second; size--; )
        if (*c1++ != *c2++) return((c1[-1] < c2[-1]) ? -1 : 1);
    return(0);	   
}
