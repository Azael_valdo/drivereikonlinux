/**
 * @file stm32f10x_conf.h
 *
 * Defines specifying peripherials to be supported by a STM library.
 *
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __STM32F10x_CONF_H
#define __STM32F10x_CONF_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_type.h"

#define _BKP
#define _DMA
#define _DMA1_Channel4	//Store data from SPI2
#define _DMA1_Channel5	//Transmit data through SPI2 
#define _FLASH
#define _FLASH_PROG //Flash memory programming functions
#define _GPIO
#define _GPIOA
#define _GPIOB
#define _GPIOC
#define _GPIOD
#define _AFIO
#define _NVIC
#define _PWR
#define _RCC
#define _RTC
#define _SPI
#define _SPI1
#define _SPI2
#define _TIM
#define _TIM3
#define _USART
#define _USART1
#define HSE_Value    ((u32)8000000) // External High Speed oscillator frequency
#define assert_param(expr) ((void)0)

#endif /* __STM32F10x_CONF_H */
