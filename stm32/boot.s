; @file boot.s
;
; Device boot sequence. It sets interrupt vectors, SP and PC, and calls a main()
;

; Amount of memory (in bytes) allocated for Stack
Stack_Size       EQU     0x00000400

                 AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem        SPACE   Stack_Size

__initial_sp


; Amount of memory (in bytes) allocated for Heap
Heap_Size        EQU     0x00000400

                 AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem         SPACE   Heap_Size
__heap_limit
		

                 THUMB
                 PRESERVE8

; Import interrupt handlers
                 IMPORT  DefaultHandler
                 IMPORT  DefaultCriticalHandler
                 
                 
; Fill an interrupt vector table
                 AREA    RESET, DATA, READONLY
                 EXPORT  __Vectors
                      
__Vectors        DCD  __initial_sp              ; Top of Stack
                 DCD  Reset_Handler
                 DCD  DefaultHandler			; NMI exception handler
                 DCD  DefaultCriticalHandler	; Hard fault exception handler
                 DCD  DefaultCriticalHandler	; Memory management exception handler
                 DCD  DefaultCriticalHandler	; Bus fault exception handler
                 DCD  DefaultCriticalHandler	; Usage fault exception handler
                 DCD  0							; Reserved
                 DCD  0							; Reserved
                 DCD  0							; Reserved
                 DCD  0							; Reserved
                 DCD  DefaultHandler			; SV call exception handler
                 DCD  DefaultHandler			; Debug monitor handler
                 DCD  0							; Reserved
                 DCD  DefaultHandler			; Pend SVC handler
                 DCD  DefaultHandler			; Sys tick handler
                 DCD  DefaultHandler			; WWDG IRQ handler
                 DCD  DefaultHandler			; PVD IRQ handler
                 DCD  DefaultHandler			; Tamper IRQ handler
                 DCD  DefaultHandler			; RTC IRQ handler
                 DCD  DefaultHandler			; Flash IRQ handler
                 DCD  DefaultHandler			; RCC IRQ handler
                 DCD  DefaultHandler			; EXTI0 IRQ handler
                 DCD  DefaultHandler			; EXTI1 IRQ handler
                 DCD  DefaultHandler			; EXTI2 IRQ handler
                 DCD  DefaultHandler			; EXTI3 IRQ handler
                 DCD  DefaultHandler			; EXTI4 IRQ handler
                 DCD  DefaultHandler			; DMA 1 Channel 1 IRQ handler
                 DCD  DefaultHandler			; DMA 1 Channel 2 IRQ handler
                 DCD  DefaultHandler			; DMA 1 Channel 3 IRQ handler
                 DCD  DefaultHandler			; DMA 1 Channel 4 IRQ handler
                 DCD  DefaultHandler			; DMA 1 Channel 5 IRQ handler
                 DCD  DefaultHandler			; DMA 1 Channel 6 IRQ handler
                 DCD  DefaultHandler			; DMA 1 Channel 7 IRQ handler
                 DCD  DefaultHandler			; ADC1 and ADC2 IRQ handler
                 DCD  DefaultHandler			; USB high priority handler
                 DCD  DefaultHandler			; USB low priority handler
                 DCD  DefaultHandler			; CAN RX1 IRQ handler
                 DCD  DefaultHandler			; CAN SCE IRQ Handler
                 DCD  DefaultHandler			; External line 9 to 5 handler
                 DCD  DefaultHandler			; TIM1 break IRQ handler
                 DCD  DefaultHandler			; TIM1 overflow and update IRQ handler
                 DCD  DefaultHandler			; TIM1 trigger and commutation IRQ handler
                 DCD  DefaultHandler			; TIM1 capture compare IRQ handler
                 DCD  DefaultHandler			; TIM2 IRQ handler
                 DCD  DefaultHandler			; TIM3 IRQ handler
                 DCD  DefaultHandler			; TIM4 IRQ handler
                 DCD  DefaultHandler			; I2C1 EV IRQ handler
                 DCD  DefaultHandler			; I2C1 ER IRQ handler
                 DCD  DefaultHandler			; I2C2 EV IRQ handler
                 DCD  DefaultHandler			; I2C2 ER IRQ handler
                 DCD  DefaultHandler			; SPI1 IRQ handler
                 DCD  DefaultHandler			; SPI2 IRQ handler
                 DCD  DefaultHandler			; USART1 IRQ handler
                 DCD  DefaultHandler			; USART2 IRQ handler
                 DCD  DefaultHandler			; USART3 IRQ handler
                 DCD  DefaultHandler			; External line 15 to 10 handler
                 DCD  DefaultHandler			; RTC Alarm IRQ Handler
                 DCD  DefaultHandler			; USB wake up IRQ handler
                 DCD  DefaultHandler			; TIM8 break IRQ handler
                 DCD  DefaultHandler			; TIM8 overflow and update IRQ handler
                 DCD  DefaultHandler			; TIM8 trigger and commutation IRQ handler
                 DCD  DefaultHandler			; TIM8 capture compare IRQ handler
                 DCD  DefaultHandler			; ADC3 IRQ handler
                 DCD  DefaultHandler			; FSMC IRQ handler
                 DCD  DefaultHandler			; SDIO IRQ handler
                 DCD  DefaultHandler			; TIM5 IRQ handler
                 DCD  DefaultHandler			; SPI3 IRQ handler
                 DCD  DefaultHandler			; UART4 IRQ handler
                 DCD  DefaultHandler			; UART5 IRQ handler
                 DCD  DefaultHandler			; TIM6 IRQ handler
                 DCD  DefaultHandler			; TIM7 IRQ handler
                 DCD  DefaultHandler			; DMA 2 Channel 1 IRQ handler
                 DCD  DefaultHandler			; DMA 2 Channel 2 IRQ handler
                 DCD  DefaultHandler			; DMA 2 Channel 3 IRQ handler
                 DCD  DefaultHandler			; DMA 2 Channel 4 IRQ handler
                 
                 AREA    |.text|, CODE, READONLY

; Reset handler
Reset_Handler    PROC
                 EXPORT  Reset_Handler		        
				        
				 IMPORT  __main
                 LDR     R0, =__main
                 BX      R0
                 ENDP

                 ALIGN

				; User Stack and Heap initialization
                 IF      :DEF:__MICROLIB           
                
                 EXPORT  __initial_sp
                 EXPORT  __heap_base
                 EXPORT  __heap_limit
                
                 ELSE
                
                 IMPORT  __use_two_region_memory
                 EXPORT  __user_initial_stackheap
                 
__user_initial_stackheap

                 LDR     R0, =  Heap_Mem
                 LDR     R1, =(Stack_Mem + Stack_Size)
                 LDR     R2, = (Heap_Mem +  Heap_Size)
                 LDR     R3, = Stack_Mem
                 BX      LR

                 ALIGN

                 ENDIF

                 END

