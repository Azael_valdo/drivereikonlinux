/**
 * @file ui.c
 * User interface functions.
 *
 * Copyright (C) 2008 UPEK Inc.
 *
 * @author Martin Krahulik
 */

#include "stm32f10x_lib.h"
#include "ui.h"
#include "configuration.h"


#ifdef __GNUC__
int __io_putchar(int ch)
#else
int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
{
  // Write a character to the USART
  USART_SendData(USARTx, (u8) ch);

  // Loop until the end of transmission 
  while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET)
  {
  }

  return ch;
}

/** Function gets an input character.
* @return Character.
*/
uint8 UiGetc(void)
{
    uint16 c = 0;
    
    while(c == 0){
        while(USART_GetFlagStatus(USARTx, USART_FLAG_RXNE) == RESET)
            ;
        
        /* Read one byte from the receive data register */
        c = USART_ReceiveData(USARTx);  //& 0x7F   
    }

    return (uint8)c; 
}
