/**
 * @file it_handlers.h
 *
 * Interrupt handlers.
 *
 */

#include "stm32f10x_it.h"

/** Default handler for non-critical errors.
*/
void DefaultHandler(void)
{
}

/** Default handler for critical errors.
*/
void DefaultCriticalHandler(void)
{
    while(TRUE)
    {
    }
}
