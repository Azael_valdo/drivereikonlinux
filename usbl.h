/**
* @file usbl.h
*
* USB layer functions
*
*/

#ifndef USBL_H
#define USBL_H
#include "types.h"
#include "tfmtypes.h"
// header types
typedef void* USB_HANDLE;
typedef short USB_COMMAND;

// other constants
#define USB_MAX_BULK_PACKET_SIZE 64

//functions
PT_STATUS UsbOpenDevice(OUT USB_HANDLE *pDevice);

PT_STATUS UsbCloseDevice(USB_HANDLE device);

PT_STATUS UsbReadData(USB_HANDLE device, char *buffer, int length);

PT_STATUS UsbWriteData(USB_HANDLE device, char *buffer, int length);

PT_STATUS UsbReadStatusReg(USB_HANDLE device,uint32 *pData);

PT_STATUS UsbSendCommand(USB_HANDLE device, USB_COMMAND command);

#endif // USBL_H
