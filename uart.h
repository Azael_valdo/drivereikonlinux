/**
 * @file uart.h
 *
 * The lowest-level interface to the UART.
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#ifndef __UART_H__
#define __UART_H__

#include    "comm.h"
#include    "timer.h"


//========================================================================
//      Constants, macros and types
//========================================================================


//========================================================================
//      Exported functions
//========================================================================

/**
 *	Open the UART
 */
COMM_STATUS PT_UartOpen (void);


/**
 *	Close the UART
 */
COMM_STATUS PT_UartClose (void);


/**
 *	Reset the UART to the default parameters
 *  and send the "RESET" signal.
 */
COMM_STATUS PT_UartReset (void);

/**
 *	Send the HW Break signal
 */
COMM_STATUS PT_UartBreak (void);


/**
 *	Change the bit rate
 *
 *  @param dwBitRate The target bit rate. Has to be one of the LL_SIO_BAUDRATE_xxxx constants.
 */
COMM_STATUS PT_UartSetParams (uint32 dwBitRate);


/**
 *	Clear the receive queue
 */
COMM_STATUS PT_UartClearQueue (void);

COMM_STATUS PT_UartStartTransmit(void);
COMM_STATUS PT_UartStopTransmit(void);

COMM_STATUS PT_UartStartReceive(void);
COMM_STATUS PT_UartStopReceive(void);

COMM_STATUS PT_UartStartClock(void);
void PT_UartStopClock(void);

/**
 * Enables UART auto hard flow control. RTS is driven by
 * UART itself, we cannot manipulate it directly.
 */
void PT_UartEnableAHFC(void);

/**
 * Disables UART auto hard flow control. We can manipulate RTS state directly.
 */
void PT_UartDisableAHFC(void);

/**
 *	Read a single byte.
 *  If there is already a byte in the receive queue, return it.
 *  Otherwise wait for an incoming byte for up to 100 msec (as specified by the protocol).
 *
 *  @return Status code
 *  On success it returns COMM_STATUS_OK.
 *  Upon timeout it returns COMM_STATUS_TIMEOUT.
 *  In all the other cases (overrun, framing error etc.) it returns COMM_STATUS_ERROR.
 */
COMM_STATUS PT_UartReadByte (uint8 *byte);


/**
 *	Send a single byte.
 *  This function may block, if the output queue is full.
 *
 *  @return Status code
 *  On success it returns COMM_STATUS_OK.
 *  In all the other cases (HW problem etc.) it returns COMM_STATUS_ERROR.
 */
COMM_STATUS PT_UartWriteByte (uint8 byte);

/**
 * Waits until all bytes in transmitting FIFO are sent.
 */
COMM_STATUS PT_UartWaitTransmissionEnd(void);

/**
 *  Get the status of the AWAKE signal
 *
 *  @param pAwake Set to status of AWAKE: 0 or 1
 *  @return Status code
 */
COMM_STATUS PT_UartGetAwake (uint32 *pAwake);


/**
 *  Set the status of the WAKEUP signal
 *
 *  @param dwWakeup Desired status of WAKEUP signal
 *  @return Status code
 */
COMM_STATUS PT_UartSetWakeup (uint32 dwWakeup);


#endif  // __UART_H__
