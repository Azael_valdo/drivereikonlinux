/**
 * @file errcodes.h
 *
 * Internal PerfectTrust API and TFM error codes.
 *
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#ifndef ERRCODES_H
#define ERRCODES_H

#if defined(_MSC_VER) && _MSC_VER > 1000
#pragma once
#endif

#include "tfmerror.h"

/**
@page err_codes Error codes

@section err_codes_overview Overview

We intend to use error code that fit into signed 16-bit value. Negative values indicate an error,
positive ones can indicate an warning. Zero is reserved for success code.

Error codes names must be in following form:
- @b PT_STATUS_XXXXXX Public error codes
- @b PT_STATUS_MODUL_XXXXXX Internal error codes

@section err_codes_ranges Error code ranges

It is recommended to reserve ranges for specific modules by hundreds. The same ranges are
reserved for specific module at the possitive side. We describe here only negative range values
for simplicity.

It is also recommended to define error codes as direct values (e.g. -2001) and not as relative value
to some constant (like PTSTATUS_TOKEN_ERR - 1). This eases error code lookup.

- 0..-999 Reserved for use of Berkeley team
- -1000..-1999 & -3100..-3199 Public PerfectTrust error codes
- -2000..-2199 TFM Token functions internal error codes
- -2200..-2299 TFM CL error codes
- -2300..-2399 Transport Layer for PC error codes
- -2400..-2499 Link Layer error codes
- -2500..-2599 Application Communication Layer for TFM error codes
- -2600..-2699 NVM error codes
- -2700..-2799 Crypto error codes
- -2800..-2899 Bio error codes
- -2900..-2999 Emulated biomodule error codes
- -3000..-3099 Transport layer (internal) error codes
- -3200..-3299 Serial interface (SIO) error codes
- -3300..-3399 Internal communication testing error codes
- -3400..-32000 Currently available. Please add your range above and decrease available range at this line.
*/

/**
 * @name OS HAL internal error codes. Range 0..-999
 */
/*@{*/

/// TCI return code indicating no error
#define STERR_OK                                             (0)
/// TCI return code indicating general error
#define STERR_ERROR                                          (-1)
/// TCI return code indicating that no response from device was detected.
#define STERR_NODEVICE                                       (-3)
/// TCI return code indicating bad input parameters
#define STERR_BADPARAMETER                                   (-5)
/// TCI return code indicating sensor power problem
#define STERR_TCPWR                                          (-6)
/// TCI return code indicating that supplied buffer is too small
#define STERR_BUFFER_TOO_SMALL                               (-20)
// definition of errors for calibration step1 and step2
#define STERR_CALIBRATION_STEP1                              (-25)
#define STERR_CALIBRATION_STEP2                              (-26)
// calibratio for FDET failed
#define STERR_CALIBRATION_FDET                               (-27)
/// TCI return code indicating that too many bad lines were detected on sensor.
#define STERR_TOO_MANY_BADLINES                              (-32)
/// Sensor is not calibrated
#define STERR_SENSOR_NOT_CALIBRATED                          (-33)
/// Error reading Flash
#define STERR_FLASH_READ_ERROR                               (-34)
/// Library moved
#define STERR_LIBRARY_MOVED                                  (-35)

//  -400 to -499 used for Biometric errors
/// Returned by fpProcess3 when window size does not make sense or is too small.
#define STERR_BAD_WINDOW                                     (-499)
/// TCI return code indicating that a timeout has occurred.
#define STERR_TIMEOUT                                        (-500)
/// TCI return code indicating that the function was cancelation has occured.
#define STERR_CANCELED                                       (-501)
/// TCI return code indicating that the memory allocation failed.
#define STERR_MEMORY_ALLOCATION                              (-502)
/// TCI return code indicating that the functionality is not supported.
#define STERR_NOT_SUPPORTED                                  (-503)
/// TCI return code indicating that the swipe was too fast.
#define STERR_SWIPE_TOO_FAST                                 (-504)
/// TCI return code indicating that the sensor is not properly powered
#define STERR_SENSOR_INACTIVE                                (-505)
/// TCI return code indicating that the DMA failed
#define STERR_DMA_FAILURE                                    (-506)
/// TCI return code indicating that the Clock failed
#define STERR_CLOCK_FAILURE                                  (-507)
/// TCI return code indicating that the average skew was too high
#define STERR_SWIPE_TOO_SKEWED                               (-508)
/// TCI return codes for navigation callback
#define STERR_NAV_CONTINUE                                   (-509)
#define STERR_NAV_STOP                                       (-510)
/// TCI return code indicating that a file was not found
#define STERR_FILE_NOT_FOUND                                 (-511)
/// TCI return codes for calibration callback
#define STERR_CALIB_CONTINUE                                 (-512)
#define STERR_CALIB_STOP                                     (-513)
/// TCI return code indicating that the vendor code read was wrong
#define STERR_BAD_VENDORCODE                                 (-514)
/// TCI return code indicating that the transfer interface is not supported
#define STERR_INTERFACE_NOT_SUPPORTED			     (-515)
/// TCI return code indicating that the turbo grid is not supported
#define STERR_TMODE_NOT_SUPPORTED                            (-516)
/// There are no additional frames to be acquired.
#define STERR_DMA_NO_ADDITIONAL_FRAME                        (-517)
/// DMA is not active
#define STERR_DMA_NOT_READY                                  (-518)
/// HW finger detect failure: finger is on the sensor or damage
#define STERR_HW_FDET_FAILURE								 (-519)
/// HW finger detect not supported
#define STERR_HW_FDET_NOT_SUPPORTED                          (-520)
/// The sensor interface does not match the desired companion interface
#define STERR_INTERFACE_NOT_MATCHING                         (-521)
/// QualityPlus3 image too small
#define STERR_IMAGE_TOO_SMALL                                (-522)
/// Signals a backwards movement during the swipe
#define STERR_BACKWARDS_MOVEMENT                             (-523)
/// Signals a joint presence in the image
#define STERR_JOINT_DETECTED                                 (-524)
/// Signals a short reconstructed image
#define STERR_TOO_SHORT                                      (-525)


/*@}*/

/**
 * @name Token functions internal error codes. Range -2100..-2199
 */
/*@{*/

/// General token error
#define PT_STATUS_TOKEN_ERROR                               (-2001)
/// Failed to init token functionality during TFM boot
#define PT_STATUS_TOKEN_INTERNAL_INIT_FAILED                (-2002)
/// Found invalid internal data in NVM store
#define PT_STATUS_TOKEN_INVALID_INTERNAL_DATA               (-2003)

/*@}*/

// ---- control logic error codes ----------

/**
    @name Control logic interface error codes. Range -2200..-2299
    These errors can be returned by FM
*/
//@{
/// general error; use only if there isn't more precise error code
#define PT_STATUS_CL_ERROR                                  (-2201)
/// invalid packet, can't process
#define PT_STATUS_CL_INVALID_PACKET                         (-2202)
/// other side reset connection
#define PT_STATUS_CL_RESET                                  (-2203)
/// receive buffer was too short for received packet
#define PT_STATUS_CL_SHORT_BUFFER                           (-2204)
/// invalid parameter to function call
#define PT_STATUS_CL_INVALID_PARAMETER                      (-2205)
/// encryption is already used, can't set
#define PT_STATUS_CL_ENCRYPTION_IN_USE                      (-2206)
/// encryption isn't used, nothing to clear
#define PT_STATUS_CL_NO_ENCRYPTION                          (-2207)
/// unknown command received
#define PT_STATUS_CL_UNKNOWN_COMMAND                        (-2220)
/// received too short packet to process
#define PT_STATUS_CL_SHORT_PACKET                           (-2221)
/// invalid tag in received packet
#define PT_STATUS_CL_INVALID_TAG                            (-2222)
/// power off attempt failed
#define PT_STATUS_CL_GOING_TO_SLEEP                         (-2223)
//@}

// ---- TlPc error codes -------------------

/**
    @name Transport Layer for PC error codes. Range -2300..-2339

    @warn The range ends on -2339. Below LL codes mistakenly use -2340 and next numbers. Mea culpa (MV).
*/
//@{
/// general error; use only if there isn't more precise error code
#define PT_STATUS_TLPC_ERROR                                (-2301)
/// invalid connection handle
#define PT_STATUS_TLPC_INVALID_HANDLE                       (-2302)
/// send data buffer is too long
#define PT_STATUS_TLPC_TOO_BIG                              (-2303)
/// connection of this type is already opened
#define PT_STATUS_TLPC_ALREADY_OPENED                       (-2304)
/// not implemented, yet
#define PT_STATUS_TLPC_NOT_IMPLEMENTED                      (-2305)
/// invalid parameter
#define PT_STATUS_TLPC_INVALID_PARAMETER                    (-2306)
/// data too long (?)
#define PT_STATUS_TLPC_OVERFLOW                             (-2307)
/// continue waiting for TFM wakeup
#define PT_STATUS_TLPC_CONTINUE                             (-2308)
/// wakeup TFM immediatelly
#define PT_STATUS_TLPC_WAKEUP                               (-2309)
/// abort waiting, let TFM sleep and return
#define PT_STATUS_TLPC_ABORT                                (-2310)
// WARNING: don't use -2340 and next numbers. See above.
//@}

// ---- Link Layer error codes -------------

/**
    @name Link Layer error codes. Range -2400..-2499
    These errors can be returned by both FM and host
*/
//@{
/// general error; use only if there isn't more precise error code
#define PT_STATUS_LL_ERROR                                  (-2401)
/// invalid connection handle
#define PT_STATUS_LL_INVALID_HANDLE                         (-2402)
/// can't send packet
#define PT_STATUS_LL_CANT_SEND                              (-2403)
/// invalid packet, can't process
#define PT_STATUS_LL_INVALID_PACKET                         (-2404)
/// packet data too big, can't send
#define PT_STATUS_LL_TOO_BIG                                (-2405)
/// packet too short for received data
#define PT_STATUS_LL_OVERFLOW                               (-2406)
/// connection wasn't initialized, yet
#define PT_STATUS_LL_NOT_INIT                               (-2407)
/// connection was reset by other side
#define PT_STATUS_LL_RESET                                  (-2408)
/// can't connect to TFM
#define PT_STATUS_LL_CANT_CONNECT                           (-2409)
/// connection timeout
#define PT_STATUS_LL_TIMEOUT                                (-2410)
/// invalid parameter suplied
#define PT_STATUS_LL_INVALID_PARAMETER                      (-2411)
/// not implemented function
#define PT_STATUS_LL_NOT_IMPLEMENTED                        (-2412)
/// already opened
#define PT_STATUS_LL_ALREADY_OPENED                         (-2413)
/// mismatch between computed CRC and CRC in packet
#define PT_STATUS_LL_BAD_CRC                                (-2414)
/// operation is pending
#define PT_STATUS_LL_PENDING                                (-2415)
/// operation is is process and isn't available until finishes
#define PT_STATUS_LL_OCCUPIED                               (-2416)
/// this link layer module is currently disabled
#define PT_STATUS_LL_DISABLED                               (-2417)
/// this link layer module is already enabled (no need to enable)
#define PT_STATUS_LL_ALREADY_ENABLED                        (-2418)
/// Packet does not have sequence number which was expected
#define PT_STATUS_LL_PACKET_OUT_OF_SEQUENCE                 (-2419)
/// host issued suspend request
#define PT_STATUS_LL_SUSPEND                                (-2420)
/// connection was closed
#define PT_STATUS_LL_CLOSE                                  (-2421)
/// unexpected packet start is detected during receive
#define PT_STATUS_LL_NEW_PACKET_START                       (-2422)
/// unsupported protocol version used
#define PT_STATUS_LL_UNKNOWN_PROTOCOL                       (-2423)
/// received data size is insufficient
#define PT_STATUS_LL_FRAME_TRUNCATED                        (-2424)
/// unknown frame type received
#define PT_STATUS_LL_UNKNOWN_FRAME_TYPE                     (-2425)
/// frame refused by other side
#define PT_STATUS_LL_FRAME_REFUSED                          (-2426)
/// unexpected frame type received
#define PT_STATUS_LL_UNEXPECTED_FRAME_TYPE                  (-2427)
/// attempt to acquire packet which is already acquired
#define PT_STATUS_LL_ALREADY_ACQUIRED                       (-2428)
/// attempt to release packet which wasn't acquired
#define PT_STATUS_LL_NOT_ACQUIRED                           (-2429)
/// data aren't available just now
#define PT_STATUS_LL_NOT_AVAILABLE                          (-2430)
/// OS error occured during requested operation (host only)
#define PT_STATUS_LL_OS_ERROR                               (-2431)
/// known frame type with unknown format received
#define PT_STATUS_LL_UNKNOWN_FRAME_FORMAT                   (-2432)
/// not enough memory for requested operation
#define PT_STATUS_LL_NO_MEMORY                              (-2433)
/// communication error occured during frame receive
#define PT_STATUS_LL_COMM_ERROR                             (-2434)
/// host requested to abort current command processing
#define PT_STATUS_LL_ABORT_REQUESTED                        (-2435)
/// host requested to abort current raw data stream
#define PT_STATUS_LL_RAW_ABORT_REQUESTED                    (-2436)
/// no data available for current operation
#define PT_STATUS_LL_NO_DATA_AVAILABLE                      (-2437)
/// data truncated because of short buffer
#define PT_STATUS_LL_DATA_TRUNCATED                         (-2438)
/// driver in use, can't change parameters
#define PT_STATUS_LL_IN_USE                                 (-2439)
/// called out of expected execution context
#define PT_STATUS_LL_INVALID_CONTEXT                        (-2340)
/// transfer was broken unexpectedly
#define PT_STATUS_LL_TRANSFER_BROKEN                        (-2341)
/// selected mode is already active
#define PT_STATUS_LL_ALREADY_ACTIVE                         (-2342)
/// selected mode isn't active
#define PT_STATUS_LL_NOT_ACTIVE                             (-2343)
/// TFM is prepared to sleep or already sleeping
#define PT_STATUS_LL_SLEEP_ENABLED                          (-2344)
/// TFM isn't prepared to sleep
#define PT_STATUS_LL_SLEEP_DISABLED                         (-2345)
/// waiting for (TFM wakeup) was aborted
#define PT_STATUS_LL_WAIT_ABORTED                           (-2346)
/// wakeup diagnostics failed
#define PT_STATUS_LL_DIAG_FAILED                            (-2347)
/// there is no appropriate device
#define PT_STATUS_LL_DEVICE_NOT_FOUND                       (-2348)
/// requested functionality not supported
#define PT_STATUS_LL_NOT_SUPPORTED                          (-2349)
/// device present but doesn't work as expected
#define PT_STATUS_LL_DEVICE_SICK                            (-2450)
//@}

// ---- Acl for TFM  error codes -----------

/**
    @name Application Communication Layer for TFM error codes. Range -2500..-2599
    These errors can be returned by FM
*/
//@{
/// general error; use only if there isn't more precise error code
#define PT_STATUS_ACL_ERROR                                 (-2501)
/// connection was reset at PC side
#define PT_STATUS_ACL_RESET                                 (-2502)
/// connection wan't initialized
#define PT_STATUS_ACL_NOT_INIT                              (-2503)
/// can't init connection which was already initialized
#define PT_STATUS_ACL_ALREADY_INIT                          (-2504)
/// an attempt to send invalid packet
#define PT_STATUS_ACL_INVALID_PACKET                        (-2505)
/// current receive was aborted by other side
#define PT_STATUS_ACL_ABORTED                               (-2506)
/// error during decryption of received data
#define PT_STATUS_ACL_DECRYPTION_ERROR                      (-2507)
/// too short data for decryption
#define PT_STATUS_ACL_DECR_ERROR_TOO_SHORT                  (-2508)
/// invalid data size for decryption (i.e. not divisible by 8)
#define PT_STATUS_ACL_DECR_ERROR_INVALID_SIZE               (-2509)
/// command received, encrypted packet expected
#define PT_STATUS_ACL_DECR_ERROR_COMMAND_RECEIVED           (-2510)
/// encrypted packet expected, something else received
#define PT_STATUS_ACL_DECR_ERROR_INVALID_TAG                (-2511)
/// invalid padding size in decrypted data
#define PT_STATUS_ACL_DECR_ERROR_INVALID_PADDING            (-2512)
/// decrypted data broken
#define PT_STATUS_ACL_DECR_ERROR_DATA_BROKEN                (-2513)
//@}


/// Supplied buffer is too small
#define PT_STATUS_CRYPTO_BUFFER_TOO_SMALL                   (-2705)

// Bio error codes. Range -2800..-2899
/// General bio error
#define PT_STATUS_BIO_ERROR                                 (-2801)
/// Bad template data
#define PT_STATUS_BIO_BAD_TEMPLATE                          (-2802)
/// Bad data: error in decoding request paket
#define PT_STATUS_BIO_BAD_PARAMETER                         (-2803)
/// Requested slot was not found
#define PT_STATUS_BIO_SLOT_NOT_FOUND                        (-2804)
/// Attempt to export antispoofing info from TFM
#define PT_STATUS_BIO_ANTISPOOFING_EXPORT                   (-2805)
/// Attempt to import antispoofing info to TFM
#define PT_STATUS_BIO_ANTISPOOFING_IMPORT                   (-2806)

// ---- transport layer error codes --------

/**
    @name Transport Layer internal error codes. Range -3000..-3099
    These errors can be returned by both FM and host
*/
//@{
/// general error; use only if there isn't more precise error code
#define PT_STATUS_TL_ERROR                                  (-3001)
/// invalid packet received, can't process
#define PT_STATUS_TL_INVALID_PACKET                         (-3002)
/// bad protocol version
#define PT_STATUS_TL_BAD_PROTOCOL_VERSION                   (-3003)
/// received frame is out of order
#define PT_STATUS_TL_OUT_OF_ORDER                           (-3004)
/// not enough memory to process data
#define PT_STATUS_TL_NO_MEMORY                              (-3005)
/// received less data than indicated in headers
#define PT_STATUS_TL_DATA_TRUNCATED                         (-3006)
/// too much data received
#define PT_STATUS_TL_DATA_OVERFLOW                          (-3007)
/// transfer aborted by other side
#define PT_STATUS_TL_TRANSFER_ABORTED                       (-3008)
//@}

// ---- serial interface (SIO) error codes --------


//@}

#endif /* #ifndef ERRCODES_H */
