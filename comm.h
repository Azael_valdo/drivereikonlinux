/**
 * @file comm.h
 *
 * General Communication Layer (transport+link layer) interface for ESS/TFM communication protocol
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#ifndef __COMM_H__
#define __COMM_H__

#include    "types.h"
#include    "defines.h"
#include    "tfmtypes.h"
#include    "endian.h"
#include    "tfmerror.h"
#if defined(ENABLE_PTSECURECHANNEL)
#include    "secstrct.h"
#define     ENABLE_PTAUTHENTIFY
#endif // #if defined(ENABLE_PTSECURECHANNEL)

//========================================================================
//      Constants, macros and types
//========================================================================

// Comm error codes
#define COMM_STATUS_OK                  ((COMM_STATUS)PT_STATUS_OK)                 // 0
#define COMM_STATUS_TIMEOUT             ((COMM_STATUS)PT_STATUS_TIMEOUT)            // -1041
#define COMM_STATUS_ERROR               ((COMM_STATUS)PT_STATUS_COMM_ERROR)         // -1057
#define COMM_STATUS_SESSION_TERMINATED  ((COMM_STATUS)PT_STATUS_SESSION_TERMINATED) // -1058
#define COMM_STATUS_ASYNC_POWERDOWN     ((COMM_STATUS)PT_STATUS_POWER_SHUTOFF)      // -1074


// Transport layer fragment types
#define TLTYPE_SINGLE           0x08        // Only one fragment = first&last
#define TLTYPE_FIRST            0x0C        // First fragment of multi-fragment sequence
#define TLTYPE_MIDDLE           0x04        // Middle fragment of multi-fragment sequence
#define TLTYPE_LAST             0x00        // Last fragment of multi-fragment sequence
#define TLTYPE_CONTINUE         0x10        // No data, only signal to the other side
                                            // to continue sending its data

// Transact flags
#define TRANSACT_FLAG_SLEEP     0x0001      // ESS/TFM sleep is expected during transaction
#define TRANSACT_FLAG_TEST      0x0002      // AWAKE/WAKEUP signal test is performed during transaction
#define TRANSACT_FLAG_NAV_BIDIR 0x0004      // Bi-directional mode of navigation (default is uni-directional)


// UART baudrates
#define LL_SIO_BAUDRATE_9600    1
#define LL_SIO_BAUDRATE_19200   2
#define LL_SIO_BAUDRATE_38400   3
#define LL_SIO_BAUDRATE_57600   4
#define LL_SIO_BAUDRATE_115200  5
#define LL_SIO_BAUDRATE_230400  7

/// current protocol version
#define LL_PROTOCOL_VERSION     1
#define TL_PROTOCOL_VERSION     (1<<5)


// Max. length of the data part of one communication frame usable for transport layer. 
// If secure channel is enabled, the data part of last message fragment can have maximally size
// (MAX_TL_DATA_SIZE - SECURE_CHANNEL_ADDITION).
#define MAX_TL_DATA_SIZE        (MAX_FRAME_DATA_SIZE-8)


// Results of AWAKE/WAKEUP diagnostics
#define DIAG_RESULT_OK              0   // No problems found
#define DIAG_RESULT_TIMEOUT_AWAKE   1   // Timeout when waiting for AWAKE signal to be set to active state
#define DIAG_RESULT_TIMEOUT_NAWAKE  2   // Timeout when waiting for AWAKE signal to be set to inactive state

// USB defines
#if !defined(USE_USB)
    #define USB_ADDITION 0
#else
    #define USB_HEADER_MAGIC        0x6F616943
    #define USB_HEADER_MAGIC_SIZE   4
    #define USB_ADDITION            (USB_HEADER_MAGIC_SIZE + 3 + sizeof(uint16))
#endif



// General result type
typedef uint32  COMM_STATUS;


//========================================================================
//      Macros for processing the packet structures
//========================================================================

#if defined(__arm)
#define UNALIGNED __packed
#else
#ifndef UNALIGNED
    #define UNALIGNED  
#endif //#ifndef UNALIGNED
#endif

// Store given element and advance pointer
#define	STORE_BYTE(p,x)		{ *(UNALIGNED uint8 *) p=(uint8) (x); p=(uint8  *)p+1; }
#define	STORE_WORD(p,x)		{ *(UNALIGNED uint16 *)p=(uint16)(x); p=(uint16 *)p+1; }
#define	STORE_DWORD(p,x)	{ *(UNALIGNED uint32 *)p=(uint32)(x); p=(uint32 *)p+1; }


// Load given element and advance pointer
#define	LOAD_BYTE(p,x)		{ x=*(UNALIGNED uint8 *) p; p=(uint8  *)p+1; }
#define	LOAD_WORD(p,x)		{ x=*(UNALIGNED uint16 *)p; p=(uint16 *)p+1; }
#define	LOAD_DWORD(p,x)		{ x=*(UNALIGNED uint32 *)p; p=(uint32 *)p+1; }

// Advance pointer
#define	SKIP_BYTE(p)		{ p=(uint8  *)p+1; }
#define	SKIP_WORD(p)		{ p=(uint16 *)p+1; }
#define	SKIP_DWORD(p)		{ p=(uint32 *)p+1; }

// Advance pointer to given boundary
#define	ALIGN_BYTE(p)		// No operation
#define	ALIGN_WORD(p)		{ p=(uint16 *)(((uint32)p+1) & ~2); }
#define	ALIGN_DWORD(p)		{ p=(uint32 *)(((uint32)p+3) & ~4); }


//========================================================================
//      Session Block (pointed to by handle)
//========================================================================

typedef struct pt_session {
    /**
     *	Flag that a valid session is open.
     *  Automatically clears, if a session is closed due to any error.
     */
    uint32	boInSession;                // Actually a BOOL value

    /**
     * Address of the fragment buffer. The buffer has to be filled before the transaction
     * with the outbound fragment data, and after transaction it will contain the inbound
     * fragment data. Size of the buffer is at least MAX_FRAME_DATA_SIZE bytes.
     * The buffer is guaranteed to be DWORD-aligned.
     */
    uint8   *pBuf;

    /**
     * Address of the copy of the last sent data fragment. 
     * The last fragment is stored for error recovery purposes.
     * The buffer is guaranteed to be DWORD-aligned.
     */
    uint8   *pLastSentFragmentBuf;

    /// Last sent data fragment length (0 means the last sent data fragment is not valid)
    uint32  dwLastSentFragmentLength;

    /// Pointer to GUI state callback
    PT_GUI_STATE_CALLBACK pfnGuiStateCallback;

    /// Pointer to GUI state callback context
    void	*pGuiStateCallbackCtx;

    /// Pointer to navigation callback
    PT_NAVIGATION_CALLBACK pfnNavigCallback;

    /// Pointer to navigation callback context
    void    *pNavigCallbackCtx;

    /// Pointer to PTSleep idle callback
    PT_IDLE_CALLBACK pfnIdleCallback;

    /// Pointer to idle callback context
    void    *pIdleCallbackCtx;
    
    /// Pointer to GUI async callback
    PT_GUI_ASYNC_CALLBACK pfnGuiAsyncCallback;
    
    /// Pointer to GUI async callback context
    void	*pGuiAsyncCallbackCtx;
    
    /// Size of up to now received data through asynchronous callbacks; restarted each time it reaches totalSize.    
    uint32 dwGuiAsyncCallbackReceivedSize;

    /// Timeout for AWAKE/WAKEUP signals test
    uint32  dwDiagTimeout;

    /// Number of loops of AWAKE/WAKEUP signals test
    uint32  dwDiagLoops;
    
    /// Result of AWAKE/WAKEUP signals test from view of host
    uint32  dwDiagResult;

    /// Type of last received fragment (see TLTYPE_xxxx)
    uint8   byLastRecvFragmentType;

    /// Sequence number of the next data frame (0..15)
    uint8	bySeqNr;

    /// Ignores AWAKE during boot, can be set via DSN parameter of PTOpen()
    PT_BOOL boIgnoreAwake;

    /// Communication speed, one of LL_SIO_BAUDRATE_xxx constants
    uint8   byCommSpeed; 

#if defined(ENABLE_PTSECURECHANNEL)
    // Secure channel context
    PT_SECURE_CHANNEL_CONTEXT SecureChannelContext;
#endif // #if defined(ENABLE_PTSECURECHANNEL)

#if defined(USE_USB)

    //Usb device handle
    void *pDevice;

    //Cached wake state
    PT_BOOL boLastAwake;
#endif // #if defined(USE_USB)
} PT_SESSION;


//========================================================================
//		Exported functions
//========================================================================


/**
 * Open a communication session.
 * Must not be called if a session is already opened.
 *
 * @param pS Pointer to session block
 * @return Status code
 */
COMM_STATUS PT_CommOpen (IN PT_SESSION *pS);


/**
 * Close a communication session.
 * If a session is already closed, does nothing.
 *
 * @param pS Pointer to session block
 * @return Status code
 */
COMM_STATUS PT_CommClose (IN PT_SESSION *pS);


/**
 * Perform a transport-layer transaction (send and/or receive a data fragment).
 * A session must be already opened.
 * If CommTransact returns with any other code than COMM_STATUS_OK, a non-recoverable problem
 * has ocurred. The current session is automatically terminated. CommOpen must be called
 * before any communication can continue.
 *
 * The outbound fragment had to be prepared in advance in the gPT buffer.
 * The inbound fragment will be after the transaction available in the gPT buffer.
 *
 * @param pS				Pointer to session block
 * @param dwSendFragmentSize    Length of the fragment to be sent
 * @param dwSendTotalSize       Total length of all the outbound fragments. 
 *                              Used only for fragment types TLTYPE_SINGLE and TLTYPE_FIRST.
 * @param bySendFragmentType    Type of the fragment to be sent, see TLTYPE_xxxx
 *
 * @param pdwRecvFragmentSize   Length of the received fragment
 * @param pdwRecvTotalSize      Total length of all the inbound fragments. 
 *                              Valid only when recv. fragment type is TLTYPE_SINGLE or TLTYPE_FIRST.
 * @param pbyRecvFragmentType   Type of the received fragment, see TLTYPE_xxxx
 * @param dwFlags               Flags controlling special behavior (sleep, WAKEUP/AWAKE behavior etc.)
 *                              See TRANSACT_FLAG_xxxx.
 *
 * @return Status code.
 */
COMM_STATUS PT_CommTransact (IN  PT_SESSION *pS,
                             IN  uint32   dwSendFragmentSize, IN  uint32   dwSendTotalSize, IN  uint8   bySendFragmentType,
                             OUT uint32 *pdwRecvFragmentSize, OUT uint32 *pdwRecvTotalSize, OUT uint8 *pbyRecvFragmentType,
                             IN  uint32   dwFlags);
  


#endif // __COMM_H__



