// ----------------------------------------------------------------------------
//
//  ClCodes.H
//
//  Control login handler codes.
//
//  Copyright (C) 2001-2006 UPEK Inc.
//
// ----------------------------------------------------------------------------

#ifndef __CLCODES_H__
#define __CLCODES_H__

// ---- description ------------------------

/**
    @file clcodes.h
    Control login handler codes.

    @author Michal Vodicka <michal.vodicka@st.com>

    All control codes used for control logic handler are here. They should be at
    one place because shall be unique.

    @warning Ensure a new code is unique before adding.
*/

#include "types.h"

// ---- types ------------------------------

/// Control code type
typedef uint16 CL_CONTROL_CODE;

/// Status code type (for responses)
typedef sint16 CL_STATUS_CODE;

/// Tag used at application packet start.
typedef uint32 CL_TAG;

// ---- tag types --------------------------

/**
    @name Tag types

    The highest 4 bits of every tag contain tag type. The rest of tag value
    is dependent on this type.
*/
//@{

/// the request from Host to TFM to perform an action
#define CL_COMMAND              0
/// the response to a command from TFM to Host
#define CL_RESPONSE             0x1
/// the GUI callback packet
#define CL_GUI_CALLBACK         0x2
/// the Host's response to the GUI callback packet
#define CL_GUI_RESPONSE         0x3
/**
    the encrypted packet. This value is never used on the application layer.
    Instead it is used by the Presentation Layer to label an encrypted packet.
    All packets with given tag will be considered encrypted and will be decrypted on receival.
*/
#define CL_ENCRYPTED            0x8

/// bitmask used for tag type
#define CL_TAG_TYPE_MASK        0xf
/// number used for shifting tag type value to get tag type
#define CL_TAG_TYPE_SHIFT       28
//@}

/**
    @name Tag contents for CL_COMMAND and CL_RESPONSE tag types.

    Masks and shifts define code (#CL_CONTROL_CODE) and status (#CL_STATUS_CODE) placement.
    They shouldn't be used directly, use @ref clcodes_macros "macros" instead.
*/
//@{
// bitmask used for control code
#define CL_CONTROL_CODE_MASK    0xfff
/// number used for shifting control code value inside tags
#define CL_CONTROL_CODE_SHIFT   16
/// bitmask used for status code
#define CL_STATUS_CODE_MASK     0xffff
/// for completeness only (not used)
#define CL_STATUS_CODE_SHIFT    0
//@}

// ---- encryption flags -------------------

#define CL_ENCRYPT_GUI_CALLBACKS    0x1

// ---- macros -----------------------------

/**
    @name Tag access macros
    @anchor clcodes_macros

    Macros for creation or access #CL_TAG values. They should be used instead of
    direct tags values access. It is possible to add more macros when necessary.

    Notes for macro creators:
    - every macro name should have CL_ prefix
    - only symbolic constants should be used instead of hardcoded values
    - tag creation macros should cast result to expected type (as CL_TAG)
    - tag access macros @b shouldn't cast input values to expected types (as CL_TAG).
    It is caller's job and if made, it could mask errors in caller's code.
*/
//@{

/// Translates TagType value to empty CL_TAG with this type. Used only by other macros.
#define CL_TAG_TYPE_TO_TAG(TagType)   \
    ((CL_TAG)(((uint32)TagType) << CL_TAG_TYPE_SHIFT))

/// Translates ControlCode value to empty CL_TAG with this code. Used only by other macros.
#define CL_CONTROL_CODE_TO_TAG(ControlCode) \
    ((CL_TAG)(((ControlCode) & CL_CONTROL_CODE_MASK) << CL_CONTROL_CODE_SHIFT))

/// Translates StatusCode value to empty CL_TAG with this status. Used only by other macros.
#define CL_STATUS_CODE_TO_TAG(StatusCode)   \
    ((CL_TAG)((StatusCode) & CL_STATUS_CODE_MASK))

/// Makes CL_TAG, which representes command of given control code
#define CL_MAKE_COMMAND(ControlCode)    \
    (CL_TAG_TYPE_TO_TAG(CL_COMMAND) | CL_CONTROL_CODE_TO_TAG(ControlCode))

/// Makes CL_TAG, which representes response of given control and status codes
#define CL_MAKE_RESPONSE(ControlCode, StatusCode) \
    (CL_TAG_TYPE_TO_TAG(CL_RESPONSE) | CL_CONTROL_CODE_TO_TAG(ControlCode) | CL_STATUS_CODE_TO_TAG(StatusCode))

/// Makes CL_TAG, which representes tag for GUI callback packet
#define CL_MAKE_GUI_CALLBACK (CL_TAG_TYPE_TO_TAG(CL_GUI_CALLBACK))

/// Makes CL_TAG, which representes tag for response to GUI callback packet
#define CL_MAKE_GUI_RESPONSE (CL_TAG_TYPE_TO_TAG(CL_GUI_RESPONSE))

/// Makes CL_TAG which represents tag encrypted packet
#define CL_MAKE_ENCRYPTED (CL_TAG_TYPE_TO_TAG(CL_ENCRYPTED))

/// Get tag type from tag value.
#define CL_GET_TAG_TYPE(Tag) \
    (((Tag) >> CL_TAG_TYPE_SHIFT) & CL_TAG_TYPE_MASK)

/// Gets control code from given tag
#define CL_GET_CONTROL_CODE(Tag)    \
    ((CL_CONTROL_CODE)(((Tag) >> CL_CONTROL_CODE_SHIFT) & CL_CONTROL_CODE_MASK))

/// Gets status code from given tag
#define CL_GET_STATUS_CODE(Tag)     \
    ((CL_STATUS_CODE)((Tag) & CL_STATUS_CODE_MASK))

/// Checks whether given control code tag represents command
#define CL_IS_COMMAND(Tag)  \
    (CL_GET_TAG_TYPE(Tag) == CL_COMMAND)

/// Checks whether given control code tag represents response
#define CL_IS_RESPONSE(Tag)  \
    (CL_GET_TAG_TYPE(Tag) == CL_RESPONSE)

/// Checks whether given control code tag represents GUI callback packet
#define CL_IS_GUI_CALLBACK(Tag) \
    (CL_GET_TAG_TYPE(Tag) == CL_GUI_CALLBACK)

/// Checks whether given control code tag represents response to GUI callback
#define CL_IS_GUI_RESPONSE(Tag) \
    (CL_GET_TAG_TYPE(Tag) == CL_GUI_RESPONSE)

/// Checks whether given tag represents encrypted packet
#define CL_IS_ENCRYPTED(Tag) \
    (CL_GET_TAG_TYPE(Tag) == CL_ENCRYPTED)

//@}

// ---- command codes ----------------------

/**
    @defgroup clcodes_commands Application communication protocol codes
*/
//@{

/**
    @name Internal CL commands

    Range 0xff0 - 0xfff is reserved for internal CL use.
*/
//@{
/// TFM detected error during command processing. Status (#CL_STATUS_CODE) contains the problem code
#define CL_COMMAND_ERROR_RESPONSE                   0x0ff0
//@}

/**
 * @name Token commands
 */
/*@{*/

/// TokenGetInfo()
#define CL_COMMAND_TOKEN_GET_INFO                   0x0101

/// TokenGetMechanismList()
#define CL_COMMAND_TOKEN_GET_MECHANISM_LIST         0x0102

/// TokenGetMechanismInfo()
#define CL_COMMAND_TOKEN_GET_MECHANISM_INFO         0x0103

/// TokenInit()
#define CL_COMMAND_TOKEN_INIT                       0x0104

/// TokenEnroll()
#define CL_COMMAND_TOKEN_ENROLL                     0x0105

/// TokenLogin()
#define CL_COMMAND_TOKEN_LOGIN                      0x0106

/// TokenLogout()
#define CL_COMMAND_TOKEN_LOGOUT                     0x0107

/// TokenCreateObject()
#define CL_COMMAND_TOKEN_CREATE_OBJECT              0x0108

/// TokenCopyObject()
#define CL_COMMAND_TOKEN_COPY_OBJECT                0x0109

/// TokenDestroyObject()
#define CL_COMMAND_TOKEN_DESTROY_OBJECT             0x010a

/// TokenGetObjectSize()
#define CL_COMMAND_TOKEN_GET_OBJECT_SIZE            0x010b

/// TokenGetAttributeValue()
#define CL_COMMAND_TOKEN_GET_ATTRIBUTE_VALUE        0x010c

/// TokenSetAttributeValue()
#define CL_COMMAND_TOKEN_SET_ATTRIBUTE_VALUE        0x010d

/// TokenFindObjects()
#define CL_COMMAND_TOKEN_FIND_OBJECTS               0x010e

/// TokenEncrypt()
#define CL_COMMAND_TOKEN_ENCRYPT                    0x010f

/// TokenDecrypt()
#define CL_COMMAND_TOKEN_DECRYPT                    0x0110

/// TokenGenerateKeyPair()
#define CL_COMMAND_TOKEN_GENERATE_KEY_PAIR          0x0111


/*@}*/

/**
 * @name Bio commands
 */

//@{

/// BioDetectFinger()
#define CL_COMMAND_BIO_DETECT_FINGER                0x0201

/// BioEnroll()
#define CL_COMMAND_BIO_ENROLL                       0x0202

/// BioVerify()
#define CL_COMMAND_BIO_VERIFY                       0x0203

/// BioVerifyEx()
#define CL_COMMAND_BIO_VERIFYEX                     0x0204

/// BioStoreFinger()
#define CL_COMMAND_BIO_STORE_FINGER                 0x0205

/// BioDeleteFinger()
#define CL_COMMAND_BIO_DELETE_FINGER                0x0206

/// BioEraseAllFingers()
#define CL_COMMAND_BIO_DELETE_ALL_FINGERS           0x0207

/// BioCapture()
#define CL_COMMAND_BIO_CAPTURE                      0x0208

/// BioVerifyMatch()
#define CL_COMMAND_BIO_VERIFY_MATCH                 0x0209

/// BioVerifyAll()
#define CL_COMMAND_BIO_VERIFY_ALL                   0x020a

/// BioSetFingerData()
#define CL_COMMAND_BIO_SET_FINGER_DATA              0x020b

/// BioGetFingerData()
#define CL_COMMAND_BIO_GET_FINGER_DATA              0x020c

/// BioListAllFingers()
#define CL_COMMAND_BIO_LIST_ALL_FINGERS             0x020d

/// BioGrab()
#define CL_COMMAND_BIO_GRAB                         0x020e

/// BioGrabWindow()
#define CL_COMMAND_BIO_GRAB_WINDOW                  0x020f

/// BioDetectFingerEx()
#define CL_COMMAND_BIO_DETECT_FINGEREX              0x0210

/// BioCalibrate()
#define CL_COMMAND_BIO_CALIBRATE                    0x0211

/// BioNavigate()
#define CL_COMMAND_BIO_NAVIGATE                     0x0212

/// BioClickCalibrate()
#define CL_COMMAND_BIO_CLICK_CALIBRATE              0x0213

/// BioAntispoofCapture()
#define CL_COMMAND_BIO_ANTISPOOF_CAPTURE            0x0214

/// BioScanQuality()
#define CL_COMMAND_BIO_SCAN_QUALITY                 0x0215

/// BioSleepThenGrab()
#define CL_COMMAND_BIO_SLEEP_THEN_GRAB              0x0216

/// BioGetSwipeInfo()
#define CL_COMMAND_BIO_GET_SWIPE_INFO               0x0217

/// BioSleepThenCapture()
#define CL_COMMAND_BIO_SLEEP_THEN_CAPTURE           0x0218

/// BioSetFingerPayload()
#define CL_COMMAND_BIO_SET_FINGER_PAYLOAD           0x0219

/// BioUpdateFingerPayload()
#define CL_COMMAND_BIO_UPDATE_FINGER_PAYLOAD        0x021a

/// BioSetFingerDataEx()
#define CL_COMMAND_BIO_SET_FINGER_DATAEX            0x021b

/// BioGetFingerDataEx()
#define CL_COMMAND_BIO_GET_FINGER_DATAEX            0x021c

/// BioCopyFingerData()
#define CL_COMMAND_BIO_COPY_FINGER_DATA             0x021d

/// BioUseFingerData()
#define CL_COMMAND_BIO_USE_FINGER_DATA              0x021e

/// BioControlLastMatchData()
#define CL_COMMAND_BIO_CONTROL_LAST_MATCH_DATA      0x021f

/// BioQueryFingerData()
#define CL_COMMAND_BIO_QUERY_FINGER_DATA            0x0220

/// BioStoreFingerWrapped()
#define CL_COMMAND_BIO_STORE_FINGER_WRAPPED         0x0221

/// BioLoadFingerEx()
#define CL_COMMAND_BIO_LOAD_FINGEREX                0x0222

/// BioConvertTemplateEx()
#define CL_COMMAND_BIO_CONVERT_TEMPLATE_EX          0x022a

/// BioGetAntispoofingInfo()
#define CL_COMMAND_BIO_GET_ANTISPOOFING_INFO        0x0250

//@}


/**
 * @name Access control commands
 */

//@{

/// AccessSetAuthKey()
#define CL_COMMAND_ACC_SET_AUTH_KEY                 0x0301

/// AccessSetAccessRights()
#define CL_COMMAND_ACC_SET_ACCESS_RIGHTS            0x0302

/// AccessGetAccessRights()
#define CL_COMMAND_ACC_GET_ACCESS_RIGHTS            0x0303

/// AccCreateProfileWrapped()
#define CL_COMMAND_ACC_CREATE_PROFILE_WRAPPED       0x0304

/// AccSetProfileWrapped()
#define CL_COMMAND_ACC_SET_PROFILE_WRAPPED          0x0305

/// AccAuthentifyAdv()
#define CL_COMMAND_ACC_AUTHENTIFY_ADV               0x0308

//@}


/**
 * @name Miscellaneous commands
 */

//@{

/// MiscSetAppData()
#define CL_COMMAND_MISC_SET_APP_DATA                0x0401

/// MiscGetAppData()
#define CL_COMMAND_MISC_GET_APP_DATA                0x0402

/// MiscGetAvailableMemory()
#define CL_COMMAND_MISC_GET_AVAILABLE_MEMORY        0x0403

/// MiscSetSessionCfg()
#define CL_COMMAND_MISC_SET_SESSION_CFG             0x0404

/// MiscGetSessionCfg()
#define CL_COMMAND_MISC_GET_SESSION_CFG             0x0405

/// MiscGetInfo()
#define CL_COMMAND_MISC_GET_INFO                    0x0406

/// MiscGetChallenge()
#define CL_COMMAND_MISC_GET_CHALLENGE               0x0407

/// MiscAuthentify()
#define CL_COMMAND_MISC_AUTHENTIFY                  0x0408

/// MiscSetLED()
#define CL_COMMAND_MISC_SET_LED                     0x0409

/// MiscGetLED()
#define CL_COMMAND_MISC_GET_LED                     0x040a

/// MiscSetSessionCfgEx()
#define CL_COMMAND_MISC_SET_SESSION_CFGEX           0x040b

/// MiscGetSessionCfgEx()
#define CL_COMMAND_MISC_GET_SESSION_CFGEX           0x040c

/// MiscSleep()
#define CL_COMMAND_MISC_SLEEP                       0x040d

/// MiscDiagnostics()
#define CL_COMMAND_MISC_DIAGNOSTICS                 0x040e

/// MiscAuthentifyEx()
#define CL_COMMAND_MISC_AUTHENTIFY_EX               0x040f

/// MiscSecureChannel()
#define CL_COMMAND_MISC_SECURE_CHANNEL              0x0410

/// MiscGetExtendedInfo()
#define CL_COMMAND_MISC_GET_EXTENDED_INFO           0x0411

/// MiscOtpSetSecretKey()
#define CL_COMMAND_MISC_OTP_SET_SECRET_KEY          0x0412

/// MiscOtpSetUserKey()
#define CL_COMMAND_MISC_OTP_SET_USER_KEY            0x0413

/// MiscOtpSetSequenceNumber()
#define CL_COMMAND_MISC_OTP_SET_SEQUENCE_NUMBER     0x0414

/// MiscOtpGenerate()
#define CL_COMMAND_MISC_OTP_GENERATE                0x0415

/// MiscSecureChannelEx()
#define CL_COMMAND_MISC_SECURE_CHANNEL_EX           0x0416

/// MiscCreateSab()
#define CL_COMMAND_MISC_CREATE_SAB                  0x0417

/// MiscCreateSabWrapped()
#define CL_COMMAND_MISC_CREATE_SAB_WRAPPED          0x0418

/// MiscDeleteSab()
#define CL_COMMAND_MISC_DELETE_SAB                  0x0419

/// MiscDeleteAllSabs()
#define CL_COMMAND_MISC_DELETE_ALL_SABS             0x041a

/// MiscListAllSabsSab()
#define CL_COMMAND_MISC_LIST_ALL_SABS               0x041b

/// MiscChangeSabSecret()
#define CL_COMMAND_MISC_CHANGE_SAB_SECRET           0x041c

/// MiscSetSabData()
#define CL_COMMAND_MISC_SET_SAB_DATA                0x041d

/// MiscGetSabData()
#define CL_COMMAND_MISC_GET_SAB_DATA                0x041e

/// MiscQuerySabData()
#define CL_COMMAND_MISC_QUERY_SAB_DATA              0x041f

/// MiscUseSabData()
#define CL_COMMAND_MISC_USE_SAB_DATA                0x0420

/// MiscUseUpdateSabData()
#define CL_COMMAND_MISC_USE_UPDATE_SAB_DATA         0x0421

/// MiscUseSetSabData()
#define CL_COMMAND_MISC_USE_SET_SAB_DATA            0x0422

/// FirmwareSetCfg()
#define CL_COMMAND_FIRMWARE_SET_CFG_AUTH            0x0450

/// FirmwareGetCfg()
#define CL_COMMAND_FIRMWARE_GET_CFG                 0x0451

/// FirmwareUpdate()
#define CL_COMMAND_FIRMWARE_UPDATE                  0x0452

/// FirmwareManufacturing()
#define CL_COMMAND_FIRMWARE_MANUFACTURING           0x0453

/// BioLoadFinger()
#define CL_COMMAND_BIO_LOAD_FINGER                  0x0454

/// FirmwareRequestReboot()
#define CL_COMMAND_FIRMWARE_REQUEST_REBOOT          0x0455

/// FirmwareSetCfg()
#define CL_COMMAND_FIRMWARE_SET_CFG                 0x0456

/// FirmwareManufacturingEx()
#define CL_COMMAND_FIRMWARE_MANUFACTURINGEX         0x0457

/// FirmwareSetCfgEx()
#define CL_COMMAND_FIRMWARE_SET_CFGEX               0x0458

/// FormatExternalEEPROM()
#define CL_COMMAND_FORMAT_EXTERNAL_EEPROM           0x0460

//@}

/**
 * @name Smart Card reader control commands
 */

//@{

/// ScardIsPresent()
#define CL_COMMAND_SCARD_IS_PRESENT                 0x0500

/// ScardGetStatus()
#define CL_COMMAND_SCARD_GET_STATUS                 0x0501

/// ScardPower()
#define CL_COMMAND_SCARD_POWER                      0x0502

/// ScardSetProtocol()
#define CL_COMMAND_SCARD_SET_PROTOCOL               0x0503

/// ScardTransmit()
#define CL_COMMAND_SCARD_TRANSMIT                   0x0504

//@}


#endif  // __CLCODES_H__


