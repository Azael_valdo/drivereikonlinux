/**
 * @file tfmapi.c
 *
 * Implementation of simplified ESS/TFM API library
 *
 * Copyright (C) 2001-2011 AuthenTec Inc.
 */

#include "types.h"
#include "tfmtypes.h"
#include "/usr/include/endian.h"
#include "tfmapi.h"
#include "tfmerror.h"
#include "tfmmfg.h"
#include "comm.h"
#include "clcodes.h"
#include "clbio.h"
#include "utils.h"
#include "errcodes.h"
#include "timer.h"
#include "defines.h"

//#include <stdio.h>
//#include <string.h>
//#include <ctype.h>

#if defined (TARGET_STANDARD_API)
#ifndef _DOS
//#include <malloc.h>
#include <stdlib.h>
#endif
#endif

#if defined(ENABLE_PTSECURECHANNEL)
#include "secch.h"
#endif // #if defined(ENABLE_PTSECURECHANNEL)

// Current API version
#define PT_CURRENT_API_VERSION  (0x03000000)

//========================================================================
//      Constants, macros and types
//========================================================================

// Macro for aligning value to next nearest DWORD boundary
#define DW_ALIGN(value) ((uint32)(((value) + 3) & 0xfffffffc))

// Turbo mode used in COMPRESS1 format
#define COMPRESS1_TURBOMODE     0x80000000

// Mask for getting background color in COMPRESS1 format
#define COMPRESS1_BGCOLOR_MASK  0x000000ff

//========================================================================
//      Global static variables
//========================================================================

#if !defined(ENABLE_PTSECURECHANNEL)
#define SECURE_CHANNEL_ADDITION 0 // if secure channel is not used no buffer addition is needed
#endif // #if !defined(ENABLE_PTSECURECHANNEL)

/**
 * The fragment buffer. The buffer has to be filled before the transaction
 * with the outbound fragment data, and after transaction it will contain the inbound
 * fragment data. Size of the buffer is at least MAX_FRAME_DATA_SIZE.
 * The buffer is guaranteed to be DWORD-aligned.
 */
static uint32  gBuf[((FRAME_BUFFER_SIZE+SECURE_CHANNEL_ADDITION+USB_ADDITION)>>2) + 1];   // uint32 to guarantee DWORD alignment

/**
 * The last sent data fragment buffer. 
 * Contains a copy of the last sent data fragment for error recovery purposes.
 * The buffer is guaranteed to be DWORD-aligned.
 */
static uint32  gLastSentFragmentBuf[((MAX_FRAME_DATA_SIZE+SECURE_CHANNEL_ADDITION+USB_ADDITION)>>2) + 1];   // uint32 to guarantee DWORD alignment

/**
 * The session context block.
 * In this implementation we allow only one session, so we can allocate it statically
 */
PT_SESSION gSession = {FALSE, (uint8 *)&gBuf, (uint8 *)&gLastSentFragmentBuf, 0};

// We need to communicate somehow which COM port should be used
// and there is no time for nice solutions
// Default to COM1
char gCommPort[] = "COM1";


#if defined (TARGET_STANDARD_API)

// Memory allocation function
PT_MALLOC gpfnMalloc;

// Memory deallocation function
PT_FREE gpfnFree;

// Indication of PTInitialize() call
PT_BOOL apiInitialized = PT_FALSE;

#endif


//========================================================================
//      Default memory functions
//========================================================================

#if defined (TARGET_STANDARD_API)

#ifndef _DOS
/**
 * Default memory allocating function
 */
void* PTAPI Malloc(PT_DWORD dwSize)
{
    return malloc(dwSize);
}

/**
 * Default memory freeing function
 */
void PTAPI Free(void *pMemblock)
{
    if (pMemblock)
    {
        free(pMemblock);
    }
}
#endif

/**
 * Allocate memory buffer, and copy data into it
 */
void* MallocCopy(PT_DWORD dwSize, void *pSrcData)
{
    void *p;

    if ((p = gpfnMalloc(dwSize)) != NULL)
    {
        PT_memcpy(p, pSrcData, dwSize);
    }

    return p;
}

#endif

#if !defined(TBX)
//========================================================================
//      Image decompressing tools
//========================================================================

typedef struct IUParams
{
    uint32 enhancedBgColor;       // background color after enhancement
    uint32 dGainFactor;           // multiplication factor for digital gain    
    uint32 turboMode;             // flag for turbo mode
    uint32 dGainTurboMode;        // digital gain for turbo mode
} IUParamsType, *PIUParamsType;


// the global parameter structure
IUParamsType gIUParams = { 
    232,  //enhancedBgColor 
    48,  //dGainFactor << 4
    0,   //turboMode
    24   //dGainTurboMode << 4
};

void IUUncompressRowD5bit(
    IN  uint8  *in, 
    OUT uint8  *out, 
    IN  sint32 out_line_len) 
{
    uint8  *out_pos;
    uint8  *in_pos;
    sint32 last_stored;
    sint32 col;
    sint32 act_diff;
    sint32 bit_shift;

    in_pos  = in;
    out_pos = out;

    last_stored = (sint32)*in_pos++;
    *out_pos++ = (uint8)last_stored;
    bit_shift = 0;
    for(col=1;col<out_line_len;col++) 
    {
        if (bit_shift > 3)
        {
            act_diff = (sint32)(*in_pos++);
            act_diff += (sint32)((*in_pos) << 8);
        }
        else
        {
            act_diff = (sint32)(*in_pos);
        }
        act_diff >>= bit_shift;

        if ((act_diff & 0x0010) == 0x0010) 
        {
            act_diff &= 0x000F;
            act_diff <<= 2;
            act_diff = -act_diff;
        }
        else 
        {
            act_diff &= 0x000F;
            act_diff <<= 2;
        }

        last_stored += act_diff;
        if (last_stored > 255) last_stored = 255;
        if (last_stored < 0) last_stored = 0;
        *out_pos++ = (uint8)last_stored;

        bit_shift += 5;
        if (bit_shift > 7) 
        {
            bit_shift -= 8;
            if (bit_shift == 0)
            {
                in_pos++;
            }
        }
    }
}

void IUEnhanceRow(
    INOUT uint8 *in,
    IN    sint32 line_len, 
    IN    sint32 bgColor)
{
    sint32 i;
    sint32 temp_pixel;
    sint32 gain;
    uint32 act_dgain;

    if (gIUParams.turboMode == 1)
    {
        act_dgain = gIUParams.dGainTurboMode;
    }
    else
    {
        act_dgain = gIUParams.dGainFactor;
    }

    for(i=0;i<line_len; i++)
    {
        temp_pixel=(sint32)*in - (sint32)bgColor;
        gain=temp_pixel<0 ? act_dgain : (1 << 4);
        temp_pixel=(sint32)gIUParams.enhancedBgColor + ((temp_pixel * gain) >> 4);

        if (temp_pixel < 0) temp_pixel = 0;
        if (temp_pixel > 255) temp_pixel = 255;

        *in++ = (uint8)temp_pixel;
    }
}

void IUSetTurboMode(IN uint32 active)
{
    gIUParams.turboMode = active;
}

void IUResetDGain(void)
{
    //force defaults
    if(gIUParams.turboMode)
    {    
        gIUParams.dGainTurboMode = 24; //1.5 << 4
    }
    else
    {    
        gIUParams.dGainFactor = 3 << 4;
    }
}

void IUSetDGain(IN uint32 dGain)
{
    //forcing defaults?
    if(dGain == 0)
    {
        IUResetDGain();
        return;
    }

    if(gIUParams.turboMode)
    {    
        gIUParams.dGainTurboMode = dGain;
    }
    else
    {    
        gIUParams.dGainFactor = dGain;
    }
}

void IUUncompressAndCenterRowD5bit(
    IN  uint8  *in, 
    OUT uint8  *out,     
    IN  sint32 decompressed_len,
    IN  sint32 out_line_len,
    IN  sint32 bgColor)
{
    sint32 shift;

    shift = (out_line_len - decompressed_len) >> 1;
    if(shift < 0)
    {
        return;
    }

    //erase the row
    PT_memset(out, bgColor, (uint32)out_line_len);

    //uncompress as usual
    IUUncompressRowD5bit(in, out + shift, decompressed_len);
}
#endif //#if !defined(TBX)

//========================================================================
//      Helper functions
//========================================================================

/*------- Status code translation ---------------*/

// Test if error code is public
#define IS_PUBLIC_ERROR(errCode) (((errCode) == PT_STATUS_OK) || ((errCode) >= -1999 && (errCode) <= -1000))

// Test if error code is communicatin error code
#define IS_COMM_ERROR(errCode) (((errCode) >= -2599 && (errCode) <= -2200) || \
                                ((errCode) >= -3099 && (errCode) <= -3000) || \
                                ((errCode) >= -3299 && (errCode) <= -3200))

/**
 * Translates TFM error codes and other non-public codes to public PerfectTrust codes.
 */
static PT_STATUS TranslateStatusCode(PT_STATUS inStatus) {
    PT_STATUS outStatus;
    
    // return codes, which don't need to be translated
    if (IS_PUBLIC_ERROR(inStatus))
    {
        outStatus = inStatus;
    }
    else
    {
        
#ifdef _DEBUG
        outStatus = inStatus; // for debuging return orginal error code
#else
        // TODO: translate more error codes
        switch (inStatus) {
        case PT_STATUS_NOT_ENOUGH_MEMORY:   outStatus = PT_STATUS_NOT_ENOUGH_TFM_MEMORY; break;
            
        case PT_STATUS_CL_UNKNOWN_COMMAND:  outStatus = PT_STATUS_UNKNOWN_COMMAND; break;
        case PT_STATUS_CL_GOING_TO_SLEEP:   outStatus = PT_STATUS_GOING_TO_SLEEP; break;
            
        case PT_STATUS_TLPC_INVALID_HANDLE: outStatus = PT_STATUS_SESSION_TERMINATED; break;
        case PT_STATUS_TLPC_ALREADY_OPENED: outStatus = PT_STATUS_ALREADY_OPENED; break;
        case PT_STATUS_TLPC_INVALID_PARAMETER: outStatus = PT_STATUS_INVALID_PARAMETER; break;
        case PT_STATUS_TLPC_NOT_IMPLEMENTED:outStatus = PT_STATUS_NOT_IMPLEMENTED; break;
            
        case PT_STATUS_LL_CANT_CONNECT:     outStatus = PT_STATUS_CANNOT_CONNECT; break;
        case PT_STATUS_LL_INVALID_HANDLE:   outStatus = PT_STATUS_INVALID_HANDLE; break;
        case PT_STATUS_LL_TIMEOUT:          outStatus = PT_STATUS_TIMEOUT; break;
            
        case PT_STATUS_BIO_BAD_TEMPLATE:    outStatus = PT_STATUS_BAD_BIO_TEMPLATE; break;
        case PT_STATUS_BIO_BAD_PARAMETER:   outStatus = PT_STATUS_INVALID_PARAMETER; break;
        case PT_STATUS_BIO_SLOT_NOT_FOUND:  outStatus = PT_STATUS_SLOT_NOT_FOUND; break;
        case PT_STATUS_BIO_ANTISPOOFING_EXPORT: outStatus = PT_STATUS_ANTISPOOFING_EXPORT; break;
        case PT_STATUS_BIO_ANTISPOOFING_IMPORT: outStatus = PT_STATUS_ANTISPOOFING_IMPORT; break;
            
        case STERR_BADPARAMETER:            outStatus = PT_STATUS_INVALID_PARAMETER; break;
        case STERR_TCPWR:                   outStatus = PT_STATUS_TOUCH_CHIP_ERROR; break;
        case STERR_TIMEOUT:                 outStatus = PT_STATUS_BIOMETRIC_TIMEOUT; break;
        case STERR_CANCELED:                outStatus = PT_STATUS_BIO_OPERATION_CANCELED; break;
        case STERR_MEMORY_ALLOCATION:       outStatus = PT_STATUS_NOT_ENOUGH_TFM_MEMORY; break;
        case STERR_NOT_SUPPORTED:           outStatus = PT_STATUS_NOT_SUPPORTED; break;
        case STERR_CALIBRATION_STEP1:
        case STERR_CALIBRATION_STEP2:
        case STERR_CALIBRATION_FDET:        outStatus = PT_STATUS_CALIBRATION_FAILED; break;
        case STERR_SENSOR_NOT_CALIBRATED:   outStatus = PT_STATUS_SENSOR_NOT_CALIBRATED; break;
            
        default:
            // translate remaining communication errors to general communication error
            if (IS_COMM_ERROR(inStatus))
            {
                outStatus = PT_STATUS_COMM_ERROR;
            }
            else
            {
                outStatus = PT_STATUS_GENERAL_ERROR; // all remaining error codes translate to general error
            }
        }
#endif // _DEBUG
    }
    
    return outStatus;
}

#ifdef ENABLE_ASYNC_IMG_TRANSFER
static PT_STATUS ProcessAsyncGui(
    IN PT_SESSION *pS,
    IN PT_DWORD guiDataLen,
    IN PT_BYTE *pData)
{
#include "strpack1.h"
    typedef PACKED_PRE struct {
        uint32  type;
        uint32  flags;
        uint16  imageWidth;
        uint16  imageHeight;        
    } PACKED_POST ImageHeader;
#include "stoppack.h"  

    ImageHeader *pHeader = (ImageHeader*)pData; 
    PT_STATUS status = PT_STATUS_OK;
    PT_DWORD dwPartSize;
    PT_DWORD dwTotalSize;
    uint32 receivedSize;
     
    //if data are too short or hae bad type, return error
    if((guiDataLen < sizeof(ImageHeader)) || (LTOH32(pHeader->type) != PT_INTERNAL_DATA_TYPE_IMAGE_CHUNCK))
    {
        
        status = PT_STATUS_WRONG_RESPONSE;
        goto endfunc;
    }

    //calculate image sizes
    dwPartSize  = guiDataLen - sizeof(ImageHeader);
    dwTotalSize = LTOH16(pHeader->imageWidth) * LTOH16(pHeader->imageHeight);
    
    //handle situation when whole image has been transfered to host and it haven't passed through final quality check =>
    //grabbed the whole image once again
    receivedSize = pS->dwGuiAsyncCallbackReceivedSize + dwPartSize;
    
    //if it's not the first async callback of a grab operation
    if(receivedSize != dwPartSize)
    {
        if(receivedSize > dwTotalSize)
        {
            receivedSize = dwPartSize;  //reset counter, call callback handler with total size to signalize that data should
                                        //be discarded
        } else {
            dwTotalSize = 0;            //asynchronous callback in the middle of transfer 
        }
    }
    pS->dwGuiAsyncCallbackReceivedSize = receivedSize;
        
    if(pS->pfnGuiAsyncCallback != NULL) 
    {
        status = pS->pfnGuiAsyncCallback(pS->pGuiAsyncCallbackCtx, pData + sizeof(ImageHeader),
                                         dwPartSize, dwTotalSize, 0);
    }
    
endfunc:; 
    return status;   
}
#endif //#ifdef ENABLE_ASYNC_IMG_TRANSFER

/**
 * Test, if sent data fits to fragment of given type.
 * @param dwSendSize Size of sent data.
 * @param byFragmentType Type of fragment.
 * @param pdwMaxTransferSize If not NULL, it is filled with min(dwSendSize, max transfer size for given fragment type).
 * @return Fits or not.
 */
bool8 PT_FitsSentDataToFragment(
    IN uint32 dwSendSize,
    IN uint8 byFragmentType,
    OUT uint32 *pdwMaxTransferSize
){
    uint32 maxSize = MAX_TL_DATA_SIZE;

#if defined(ENABLE_PTSECURECHANNEL)   
    // First fragment carries encrypted CL_TAG
    if(byFragmentType == TLTYPE_SINGLE || byFragmentType == TLTYPE_FIRST)
    {
        maxSize -= sizeof(CL_TAG);
    } else {
        maxSize -= MAX_BLOCKSIZE;   // Encryption can add a part of previous fragment that wasn't aligned to encryption block
    }

    // Last fragment must carry secure channel data prolonging frame
    if(byFragmentType == TLTYPE_SINGLE || byFragmentType == TLTYPE_LAST)
    {
        maxSize -= SECURE_CHANNEL_ADDITION;
    }
#endif //#if defined(ENABLE_PTSECURECHANNEL) 

    // Fill maximal size of next transfer
    if(pdwMaxTransferSize != NULL)
    {
        if(dwSendSize > maxSize)
        {
            *pdwMaxTransferSize = maxSize;
        } else {
            *pdwMaxTransferSize = dwSendSize;
        }
    }
    
    return (bool8)(dwSendSize <= maxSize);
}

/**
 * Process received GUI packet from previous transaction, call GUI callback
 * and send back response to TFM
 */
static PT_STATUS GuiTransact(
    PT_SESSION *pS,
    uint32 *pRecvFragmentSize, 
    uint32 *pRecvTotalSize, 
    uint8 *pRecvFragmentType, 
    uint32 dwFlags)
{
    typedef struct gui_request_tag {
        CL_TAG  tag;
        uint32  guiState;               // Gui state (dwGuiState)
        uint32  message;                // Number of the message (dwMessage)
        uint32  dataLen;                // Data length (pData->Length)
        uint8   progress;               // Sample progress (byProgress)
        uint8   dataStart;              // Start of data
    } GuiRequest;

    typedef struct gui_response_tag {
        CL_TAG tag;
        uint8   response;               // Response from the application (pbyResponse)
    } GuiResponse;

    PT_STATUS status = PT_STATUS_OK;
    PT_BYTE response = PT_CONTINUE;
    uint32 sendSize;
    GuiRequest *pGuiRequest = (GuiRequest*)(pS->pBuf);
    PT_DWORD guiState = LTOH32(pGuiRequest->guiState);
    PT_DWORD message = LTOH32(pGuiRequest->message);
    PT_BYTE progress = pGuiRequest->progress;
    uint32 guiDataLen = LTOH32(pGuiRequest->dataLen);
    PT_GUI_SAMPLE_IMAGE *pGuiSample = NULL;
    PT_DATA *pGuiData = NULL;
    
#if defined(ENABLE_PTSECURECHANNEL) && defined(ENABLE_ASYNC_IMG_TRANSFER)
    pS->SecureChannelContext.ResponseToAsyncData = ((guiState & PT_INTERNAL_DATA_PROVIDED) != 0);
#endif //#if defined(ENABLE_PTSECURECHANNEL) && defined(ENABLE_ASYNC_IMG_TRANSFER)    

    if (guiState & PT_INTERNAL_DATA_PROVIDED)
    {
#ifdef ENABLE_ASYNC_IMG_TRANSFER    
        status = ProcessAsyncGui(pS, guiDataLen, (PT_BYTE*)(pS->pBuf + offsetof(GuiRequest, dataStart)));
#else
        // async GUI callbacks cannot be processed if ENABLE_ASYNC_IMG_TRANSFER is not defined
        status = PT_STATUS_WRONG_RESPONSE;
#endif //#ifdef ENABLE_ASYNC_IMG_TRANSFER 
    } 
    else 
    {
        if (guiState & PT_SAMPLE_IMAGE_PROVIDED)
        {
            // process sample data
#ifdef TARGET_STANDARD_API
            uint32 dataLen, partLen;
            uint8 *pPos;
            if (guiDataLen < offsetof(PT_GUI_SAMPLE_IMAGE, ImageData.Data))
            {
                // data to short
                status = PT_STATUS_WRONG_RESPONSE;
                goto lbError;
            }
            dataLen = ((PT_GUI_SAMPLE_IMAGE*)&(pGuiRequest->dataStart))->ImageData.Length;
            if ((guiDataLen - offsetof(PT_GUI_SAMPLE_IMAGE, ImageData.Data)) < dataLen)
            {
                // data too short
                status = PT_STATUS_WRONG_RESPONSE;
                goto lbError;
            }
            dataLen += offsetof(PT_GUI_SAMPLE_IMAGE, ImageData.Data);

            // allocate pGuiSample structure
            if ((pGuiSample = (PT_GUI_SAMPLE_IMAGE*)gpfnMalloc(dataLen)) == NULL)
            {
                status = PT_STATUS_MALLOC_FAILED;
                goto lbError;
            }
            pPos = (uint8*)pGuiSample;

            // copy first part of data
            partLen = min(*pRecvFragmentSize - offsetof(GuiRequest, dataStart), dataLen);
            PT_memcpy(pPos, &pGuiRequest->dataStart, partLen);
            pPos += partLen;
            dataLen -= partLen;

            while (dataLen > 0)
            {
                if (*pRecvFragmentType != TLTYPE_FIRST && *pRecvFragmentType != TLTYPE_MIDDLE)
                {
                    // unexpected end of data transfer
                    status = PT_STATUS_WRONG_RESPONSE;
                    goto lbError;
                }

                // read next fragment
                status = PT_CommTransact(pS, 0, 0, TLTYPE_CONTINUE, pRecvFragmentSize, pRecvTotalSize, pRecvFragmentType, 0);
                if (status != PT_STATUS_OK)
                {
                    goto lbError;
                }

                // copy next part of data
                partLen = min(*pRecvFragmentSize, dataLen);
                PT_memcpy(pPos, pS->pBuf, partLen);
                pPos += partLen;
                dataLen -= partLen;
            }
#else
            // cannot be processed if TARGET_STANDARD_API is not defined
            status = PT_STATUS_WRONG_RESPONSE;
            goto lbError;
#endif //#ifdef TARGET_STANDARD_API
        }

        if (guiState & PT_DATA_PROVIDED)
        {
            // process additional data
            // TODO:
        }

        if (pS->pfnGuiStateCallback != NULL)
        {
            status = pS->pfnGuiStateCallback(
                pS->pGuiStateCallbackCtx,
                guiState,
                &response,
                message,
                progress,
                pGuiSample,
                pGuiData);
            if (status != PT_STATUS_OK)
            {
                goto lbError;
            }
        }
lbError:;
#ifdef TARGET_STANDARD_API
        if (pGuiSample != NULL)
        {
            gpfnFree(pGuiSample);
        }
#endif //#ifdef TARGET_STANDARD_API
    }
    
    if (status != PT_STATUS_OK)
    {
        response = PT_CANCEL;
    }

    ((GuiResponse*)pS->pBuf)->response = response;
        
    sendSize = sizeof(CL_TAG) + sizeof(uint8);

    // check size of sent data
    if(!PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }

    // set command control code
    ((GuiResponse*)pS->pBuf)->tag = HTOL32(CL_MAKE_GUI_RESPONSE);

    //remove flag, when we are in PTSleepThenGrab command
    dwFlags &= ~TRANSACT_FLAG_SLEEP;

    // commit transaction
    status = PT_CommTransact(pS, 
        sendSize, sendSize, TLTYPE_SINGLE,
        pRecvFragmentSize, pRecvTotalSize, pRecvFragmentType,
        dwFlags);
    
    return TranslateStatusCode(status);
}


/**
 * Perform simple communication transaction. Size of sent data is guaranteed
 * to fit into one fragment, received data may be split into several fragments
 * (when *pdwRecvTotoalSize > *pdwRecvFrameSize). To receive next fragments of
 * split data use GetNextFragment(). GUI packets are processed. 
 *
 * @param pS Pointer to session block
 * @param dwControlCode Command control code
 * @param dwSendSize Size of data to be sent including the command code
 * @param pdwRecvTotalSize Size of whole data to be received
 * @param pdwRecvFragmentSize Size of data received in last fragment
 * @param dwFlags Transaction flags
 * @return Status code
 */
PT_STATUS PT_SimpleTransactEx(
    IN PT_SESSION *pS,
    IN PT_DWORD dwControlCode,
    IN PT_DWORD dwSendSize,
    OUT PT_DWORD *pdwRecvTotalSize,
    OUT PT_DWORD *pdwRecvFragmentSize,
    IN PT_DWORD dwFlags)
{
    PT_STATUS status;
    uint32 recvFragmentSize, recvTotalSize;
    uint8 recvFragmentType;
    
    // check size of sent data
    if(!PT_FitsSentDataToFragment(dwSendSize, TLTYPE_SINGLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }
    
    // set command control code
    *(CL_TAG*)pS->pBuf = HTOL32(CL_MAKE_COMMAND(dwControlCode));
    
    // commit transaction
    status = PT_CommTransact(pS,
        dwSendSize, dwSendSize, TLTYPE_SINGLE,
        &recvFragmentSize, &recvTotalSize, &recvFragmentType,
        dwFlags);  

    while (status == COMM_STATUS_OK)
    {
        if (recvFragmentType != TLTYPE_SINGLE && recvFragmentType != TLTYPE_FIRST)
        {
            // unexpected fragment type
            return PT_STATUS_WRONG_RESPONSE;
        }
#ifdef _DEBUG
        // check length of received data
        if (recvFragmentSize < sizeof(CL_TAG))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
    
        // process GUI callbacks
        if (CL_IS_GUI_CALLBACK(LTOH32(*(CL_TAG*)pS->pBuf)))
        {
            status = GuiTransact(pS, &recvFragmentSize, &recvTotalSize, &recvFragmentType, dwFlags);
        }
        else
        {
            // wasn't GUI packet, process received data
            break;
        }
    }
    
    if (status == COMM_STATUS_OK)
    {
        
#ifdef _DEBUG
        // check length of received data
        if (recvFragmentSize < sizeof(CL_TAG))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get status code of command
        status = CL_GET_STATUS_CODE(LTOH32(*(CL_TAG*)pS->pBuf));

        if (pdwRecvTotalSize != NULL)
        {
            *pdwRecvTotalSize = recvTotalSize;
        }

        if (pdwRecvFragmentSize != NULL)
        {
            *pdwRecvFragmentSize = recvFragmentSize;
        }
    }
    
    return TranslateStatusCode(status);
}    

/**
 * Perform simple communication transaction. Size of sent and size of received
 * data is guaranteed to fit into one fragment. GUI packets are processed. 
 *
 * @param pS Pointer to session block
 * @param dwControlCode Command control code
 * @param dwSendSize Size of data to be sent including the command code
 * @param pdwRecvSize Size of received data
 * @param dwFlags Transaction flags
 * @return Status code
 */
PT_STATUS PT_SimpleTransact(
    IN PT_SESSION *pS,
    IN PT_DWORD dwControlCode,
    IN PT_DWORD dwSendSize,
    OUT PT_DWORD *pdwRecvSize,
    IN PT_DWORD dwFlags)
{
    PT_STATUS status;
    uint32 recvFragmentSize, recvTotalSize;
    
    status = PT_SimpleTransactEx(pS, dwControlCode, dwSendSize, &recvTotalSize, &recvFragmentSize, dwFlags);

    if (status == PT_STATUS_OK)
    {
        if (pdwRecvSize != NULL)
        {
            *pdwRecvSize = recvTotalSize;
        }
    }

    return status;
}


/**
 * Perform sending of first data fragment. Receiving of data is not 
 * expected. GUI packets are NOT processed. This function may be
 * followed by several calls of MiddleTransact() or must be followed
 * by one call of LastTransact().
 *
 * @param pS Pointer to session block
 * @param dwControlCode Command control code
 * @param dwSendFragmentSize Size of fragment to be sent
 * @param dwSendTotalSize Size of all data to be sent
 * @param dwFlags Transaction flags
 * @return Status code
 */
PT_STATUS PT_FirstTransact(
    IN PT_SESSION *pS,
    IN PT_DWORD dwControlCode,
    IN PT_DWORD dwSendFragmentSize,
    IN PT_DWORD dwSendTotalSize,
    IN PT_DWORD dwFlags)
{
    PT_STATUS status;
    uint32 recvFragmentSize, recvTotalSize;
    uint8 recvFragmentType;
    
    // check size of sent data
    if(!PT_FitsSentDataToFragment(dwSendFragmentSize, TLTYPE_FIRST, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }
    
    // set command control code
    *(CL_TAG*)pS->pBuf = HTOL32(CL_MAKE_COMMAND(dwControlCode));
    
    // commit transaction
    status = PT_CommTransact(pS,
        dwSendFragmentSize, dwSendTotalSize, TLTYPE_FIRST,
        &recvFragmentSize, &recvTotalSize, &recvFragmentType,
        dwFlags);  
    
    return TranslateStatusCode(status);
}


/**
 * Perform sending of middle data fragment. Receiving of data is not 
 * expected. GUI packets are NOT processed. This function may be
 * followed by several calls of MiddleTransact() or must be followed
 * by one call of LastTransact().
 *
 * @param pS Pointer to session block
 * @param dwSendFragmentSize Size of fragment to be sent
 * @param dwFlags Transaction flags
 * @return Status code
 */
PT_STATUS PT_MiddleTransact(
    IN PT_SESSION *pS,
    IN PT_DWORD dwSendFragmentSize,
    IN PT_DWORD dwFlags)
{
    PT_STATUS status;
    uint32 recvFragmentSize, recvTotalSize;
    uint8 recvFragmentType;
   
    // check size of sent data
    if(!PT_FitsSentDataToFragment(dwSendFragmentSize, TLTYPE_MIDDLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }
    
    // commit transaction
    status = PT_CommTransact(pS,
        dwSendFragmentSize, 0, TLTYPE_MIDDLE,
        &recvFragmentSize, &recvTotalSize, &recvFragmentType,
        dwFlags);  
    
    return TranslateStatusCode(status);
}


/**
 * Perform sending of last data fragment and receive response from
 * TFM. GUI packets are processed.
 *
 * @param pS Pointer to session block
 * @param dwSendFragmentSize Size of fragment to be sent
 * @param pdwRecvSize Size of received data
 * @param dwFlags Transaction flags
 * @return Status code
 */
PT_STATUS PT_LastTransact(
    IN PT_SESSION *pS,
    IN PT_DWORD dwSendFragmentSize,
    OUT PT_DWORD *pdwRecvSize,
    IN PT_DWORD dwFlags)
{
    PT_STATUS status;
    uint32 recvFragmentSize, recvTotalSize;
    uint8 recvFragmentType;
   
    // check size of sent data
    if(!PT_FitsSentDataToFragment(dwSendFragmentSize, TLTYPE_LAST, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }
    
    // commit transaction
    status = PT_CommTransact(pS,
        dwSendFragmentSize, 0, TLTYPE_LAST,
        &recvFragmentSize, &recvTotalSize, &recvFragmentType,
        dwFlags);  

#ifdef _DEBUG
    // check length of received data
    if (recvFragmentSize < sizeof(CL_TAG))
    {
        return PT_STATUS_WRONG_RESPONSE;
    }
#endif
    
    // process GUI callbacks
    while (status == COMM_STATUS_OK && CL_IS_GUI_CALLBACK(LTOH32(*(CL_TAG*)pS->pBuf)))
    {
        status = GuiTransact(pS, &recvFragmentSize, &recvTotalSize, &recvFragmentType, dwFlags);
    }
    
    if (status == COMM_STATUS_OK)
    {

#ifdef _DEBUG
        // check length of received data
        if (recvFragmentSize < sizeof(CL_TAG))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get status code of command
        status = CL_GET_STATUS_CODE(LTOH32(*(CL_TAG*)pS->pBuf));

        if (pdwRecvSize != NULL)
        {
            *pdwRecvSize = recvFragmentSize;
        }
    }
    
    return TranslateStatusCode(status);
}


/**
 * Receive next fragment of multi-fragment sequence
 *
 * @param pS Pointer to session block
 * @param pdwRecvFragmentSize Size of data received in last fragment
 * @return Status code
 */
PT_STATUS PT_GetNextFragment(
    IN PT_SESSION *pS,
    OUT PT_DWORD *pdwRecvFragmentSize)
{
    PT_STATUS status;
    uint32 recvFragmentSize, recvTotalSize;
    uint8 recvFragmentType;

    if (pS->byLastRecvFragmentType != TLTYPE_FIRST && pS->byLastRecvFragmentType != TLTYPE_MIDDLE)
    {
        // previous fragment was neither FIRST nor MIDDLE, so return zero length
        if (pdwRecvFragmentSize != NULL)
        {
            *pdwRecvFragmentSize = 0;
        }

        return PT_STATUS_OK;
    }

    // request for next fragment
    status = PT_CommTransact(pS, 0, 0, TLTYPE_CONTINUE, &recvFragmentSize, &recvTotalSize, &recvFragmentType, 0);

    if (status == COMM_STATUS_OK)
    {
        if (recvFragmentType != TLTYPE_MIDDLE && recvFragmentType != TLTYPE_LAST)
        {
            // wrong frame type
            return PT_STATUS_WRONG_RESPONSE;
        }

        if (pdwRecvFragmentSize != NULL)
        {
            *pdwRecvFragmentSize = recvFragmentSize;
        }
    }

    return TranslateStatusCode(status);
}

/**
* Receive next fragments of multi-fragment sequence. Merge all fragments in communication buffer.
*
* @param pS Pointer to session block
* @param pRecvSize Size of data returned by PT_SimpleTransactEx on input, size of all received data on output.
* @return Status code
*/
PT_STATUS PT_GetNextFragmentCombined(
    IN PT_SESSION *pS,
    INOUT PT_DWORD *pRecvSize)
{
    PT_STATUS status = PT_STATUS_OK;
    PT_DWORD recvFragmentSize = *pRecvSize;
    bool8 dataToLarge = PT_FALSE;

    // address of the frame buffer. The buffer is indexed by pBuf and fragment by fragment transfered by link layer.
    uint8 *pBufBegin = pS->pBuf;

    // if there are more data to receive
    while(pS->byLastRecvFragmentType != TLTYPE_SINGLE && pS->byLastRecvFragmentType != TLTYPE_LAST)
    {
        // move frame buffer pointer
        pS->pBuf += recvFragmentSize;

        // check that there is enough memory in frame buffer
        if(FRAME_BUFFER_SIZE - MAX_FRAME_DATA_SIZE < pS->pBuf - pBufBegin)
        {
            pS->pBuf -= recvFragmentSize; // get all remaining data not to break communication protocol
            dataToLarge = PT_TRUE;
        }

        // get next data fragment
        status = PT_GetNextFragment(pS, &recvFragmentSize);
        if(status != PT_STATUS_OK)
        {
            break;
        }

        *pRecvSize += recvFragmentSize;
    }

    // reset frame buffer pointer
    pS->pBuf = pBufBegin;
    
    if((status == PT_STATUS_OK) && dataToLarge)
    {
        // data are invalid
        status = PT_STATUS_DATA_TOO_LARGE;
    }
    
    return status;
}

/**
* Transfer array of templates to device within started operation, templates are expected to be last communication parameter. 
*
* @param pS Pointer to session block
* @param pStoredTemplates Pointer to transfered templates
* @param byNrTemplates Number of transfered templates.
* @param pRecvSize Size of last fragment answer.
* @return Status code
*/
PT_STATUS PT_TransferTemplates(
                               PT_SESSION *pS,
                               IN PT_INPUT_BIR *pStoredTemplates,
                               IN PT_BYTE byNrTemplates,
                               OUT PT_DWORD *pRecvSize)
{
    PT_STATUS status = PT_STATUS_OK;
    uint32 sendSize;
    sint32 i;

    // send all templates 
    for (i = 0; i < byNrTemplates; i++)
    {
        uint32 birFormLength = sizeof(uint32); // length of BIR form
        sendSize = birFormLength;

        // copy BIR form to comm buffer
        *(uint32*)(pS->pBuf) = HTOL32(pStoredTemplates[i].byForm);

        if (pStoredTemplates[i].byForm == PT_FULLBIR_INPUT)
        {
            PT_BYTE *pData = (PT_BYTE *)(pStoredTemplates[i].InputBIR.pBIR);
            sendSize += LTOH32(pStoredTemplates[i].InputBIR.pBIR->Header.Length);

            // if template is too large to fit within one/last frame, split it to more fragments
            while(!PT_FitsSentDataToFragment(sendSize, TLTYPE_LAST, NULL))
            {
                uint32 nextTransferSize;
                
                // get next transfer size
                PT_FitsSentDataToFragment(sendSize, TLTYPE_MIDDLE, &nextTransferSize);
                
                // copy BIR data to comm buffer
                PT_memmove(pS->pBuf + birFormLength, pData, nextTransferSize - birFormLength);

                // transfer data (MAX_TL_DATA_SIZE is always DWORD aligned)
                status = PT_MiddleTransact(pS, nextTransferSize, 0);

                // move to next fragment
                pData += nextTransferSize - birFormLength;

                // in next fragment BIR_FORM is not sent any more
                birFormLength = 0;

                // Decrease amount of data to be sent next time
                sendSize -= nextTransferSize;
            }

            // copy BIR data to comm buffer
            PT_memmove(pS->pBuf + birFormLength, pData, sendSize);
        }
        else
        {
            sendSize += sizeof(sint32);

            // copy slot number of template to comm buffer
            *(sint32*)(pS->pBuf + sizeof(uint32)) = HTOL32(pStoredTemplates[i].InputBIR.lSlotNr);
        }

        if (i < byNrTemplates-1)
        {
            // all templates except the last

            sendSize = DW_ALIGN(sendSize);

            status = PT_MiddleTransact(pS, sendSize, 0);
        }
        else
        {
            // last template
            status = PT_LastTransact(pS, sendSize, pRecvSize, 0);
        }

        if (status != PT_STATUS_OK)
        {
            break;
        }
    }

    return status;
}

/**
* Go through all supplied templates and check their BIR forms and lengths and also compute total data length. 
*
* @param pStoredTemplates Pointer to transfered templates
* @param byNrTemplates Number of transfered templates.
* @return Transfer size.
*/
PT_STATUS PT_CalculateTemplateTransferSize(
    IN PT_INPUT_BIR *pStoredTemplates,
    IN PT_DWORD byNrTemplates,
    OUT PT_DWORD *pTemplateTransferSize)
{
    uint32 i, sendSize;

    *pTemplateTransferSize = 0;

    for (i = 0; i < byNrTemplates; i++)
    {
        sendSize = sizeof(uint32);  // BIR_FORM

        switch (pStoredTemplates[i].byForm)
        {
        case PT_FULLBIR_INPUT:
            sendSize += LTOH32(pStoredTemplates[i].InputBIR.pBIR->Header.Length);
            break;

        case PT_SLOT_INPUT:
            sendSize += sizeof(sint32);
            break;

        default:
            return PT_STATUS_INVALID_PARAMETER;
        }

        if (i < byNrTemplates-1)
        {
            sendSize = DW_ALIGN(sendSize); // align size
        }

        // add to total size
        (*pTemplateTransferSize) += sendSize;
    }

    return PT_STATUS_OK;
}


//========================================================================
//      Application general functions
//========================================================================

PTAPI_DLL PT_STATUS PTAPI PTInitialize(IN PT_MEMORY_FUNCS *pMemoryFuncs) 
{
    UNREFERENCED_PARAMETER (pMemoryFuncs);

#if defined (TARGET_STANDARD_API)
    // check whether API hasn't been already initialized
    if (apiInitialized)
        return PT_STATUS_API_ALREADY_INITIALIZED;
    
    // set global memory allocation/freeing routines
    if (pMemoryFuncs == NULL) 
    {
#ifndef _DOS
        // Set default memory functions
        gpfnMalloc = Malloc;
        gpfnFree = Free;
#else
        return PT_STATUS_INVALID_PARAMETER;
#endif
    }
    else
    {
        // Set user specified memory functions
        if (((gpfnMalloc = pMemoryFuncs->pfnMalloc) == NULL) || 
            ((gpfnFree = pMemoryFuncs->pfnFree) == NULL)) 
        {
            return PT_STATUS_INVALID_PARAMETER;
        }
    }
    
    apiInitialized = PT_TRUE;

#else

    PT_memset(&gSession, 0, sizeof(PT_SESSION));

#if defined(USE_USB)
    gSession.dwDevice = 0xFFFFFFFF;
#endif

    gSession.boInSession = FALSE;

    gSession.pBuf = (uint8 *)&gBuf;

    gSession.pLastSentFragmentBuf = (uint8 *)&gLastSentFragmentBuf;

#endif

    return PT_STATUS_OK;
}


PTAPI_DLL PT_STATUS PTAPI PTTerminate(void) 
{

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;

    PTClose((PT_CONNECTION)&gSession); // close session

    apiInitialized = PT_FALSE;
    
    // invalidate memory functions
    gpfnMalloc = NULL;
    gpfnFree = NULL;
    
#endif

    return PT_STATUS_OK;
}
 

PTAPI_DLL PT_STATUS PTAPI PTOpen(
    IN PT_CHAR *pszDsn,
    OUT PT_CONNECTION *phConnection) 
{
    PT_STATUS status;
    PT_SESSION *pS;
//    UNREFERENCED_PARAMETER (pszDsn);

#if defined (TARGET_STANDARD_API)
    // check initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif

    // check validity of input parameters
    if (phConnection == NULL) return PT_STATUS_INVALID_PARAMETER;

    // Allocate/Initialize/Return the session block
    pS = &gSession;
    if (pS->boInSession) return PT_STATUS_ALREADY_OPENED;

    // Preset default values
    pS->boIgnoreAwake = PT_FALSE;
    pS->byCommSpeed = LL_SIO_BAUDRATE_115200;
    
    pS->pfnGuiStateCallback = NULL;
    pS->pGuiStateCallbackCtx = NULL;

#if defined(ENABLE_PTSECURECHANNEL)
    ResetSecureChannel(pS); // reset secure channel
#endif //#if defined(ENABLE_PTSECURECHANNEL)

    *phConnection = 1;

    status = PT_CommOpen(pS);

    return TranslateStatusCode(status);
}


PTAPI_DLL PT_STATUS PTAPI PTClose(IN PT_CONNECTION hConnection) 
{
    PT_STATUS status;
    PT_SESSION *pS = &gSession;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif
    
    // check validity of input parameters
    if (!hConnection) return PT_STATUS_INVALID_PARAMETER;
    status = PT_CommClose(pS);

    return TranslateStatusCode(status);
}


PTAPI_DLL PT_STATUS PTAPI PTSetGUICallbacks(
    IN PT_CONNECTION hConnection,
    IN PT_GUI_STREAMING_CALLBACK pfnGuiStreamingCallback,
    IN void *pGuiStreamingCallbackCtx,
    IN PT_GUI_STATE_CALLBACK pfnGuiStateCallback,
    IN void *pGuiStateCallbackCtx) 
{
    PT_SESSION *pS = &gSession;
    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (pfnGuiStreamingCallback);
    UNREFERENCED_PARAMETER (pGuiStreamingCallbackCtx);

#if defined (TARGET_STANDARD_API)
    // check initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif
    
    // check validity of input parameters
    if (!hConnection) return PT_STATUS_INVALID_PARAMETER;

    pS->pfnGuiStateCallback = pfnGuiStateCallback;
    pS->pGuiStateCallbackCtx = pGuiStateCallbackCtx;

    return PT_STATUS_OK;
}


#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTGlobalInfo(OUT PT_GLOBAL_INFO **ppGlobalInfo)
{
    PT_STATUS status = PT_STATUS_NOT_IMPLEMENTED;
    
#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
    
    if (ppGlobalInfo == NULL) 
        return PT_STATUS_INVALID_PARAMETER;
    
    if ((*ppGlobalInfo = (PT_GLOBAL_INFO*)gpfnMalloc(sizeof(PT_GLOBAL_INFO))) == NULL)
        return PT_STATUS_MALLOC_FAILED;
    
    PT_memset(*ppGlobalInfo, 0, sizeof(**ppGlobalInfo));
    
    // set API version
    (*ppGlobalInfo)->ApiVersion = PT_CURRENT_API_VERSION;
    
    // set stndard functionality as default
    (*ppGlobalInfo)->Functionality = PT_GIFUNC_STANDARD;
    
    status = PT_STATUS_OK;
#endif
    
    return status;
}
#endif //#if !defined(TBX)

PTAPI_DLL void PTAPI PTFree(void *Memblock)
{
#if defined (TARGET_STANDARD_API)
    if (gpfnFree != NULL)
    {
        gpfnFree(Memblock);
    }
#endif
}

//========================================================================
//      Miscellaneous functions
//========================================================================

PTAPI_DLL PT_STATUS PTAPI PTSetAppData(
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwArea,
    IN PT_DATA *pAppData)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 area;            // Target area
        uint32 dataLen;         // Length of data
    } Request;

    uint32 sendSize;
    PT_SESSION *pS = &gSession;
    PT_STATUS status = PT_STATUS_OK;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || pAppData == NULL)
        return PT_STATUS_INVALID_PARAMETER;
            
    // check length of input data
    sendSize = sizeof(Request) + pAppData->Length;
    
    ((Request*)pS->pBuf)->area = HTOL32(dwArea);
    ((Request*)pS->pBuf)->dataLen = HTOL32(pAppData->Length);
    
    if(PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        // copy input data into communication buffer
        PT_memmove(pS->pBuf + sizeof(Request), pAppData->Data, pAppData->Length);
    
        // transfer data in one chunk
        status = PT_SimpleTransact(pS, CL_COMMAND_MISC_SET_APP_DATA, sendSize, (uint32*)NULL, 0);
    } else {
        uint8 *pData = pAppData->Data;
        uint32 nextTransferSize;
        
        // Get next transfer size
        PT_FitsSentDataToFragment(sendSize, TLTYPE_FIRST, &nextTransferSize);
    
        // copy input data into communication buffer
        PT_memmove(pS->pBuf + sizeof(Request), pData, nextTransferSize - sizeof(Request));

        // transfer header and first data part
        status = PT_FirstTransact(pS, CL_COMMAND_MISC_SET_APP_DATA, nextTransferSize, sendSize, 0);
        sendSize -= nextTransferSize;
        pData += nextTransferSize - sizeof(Request);

        while(status == PT_STATUS_OK)
        {
            if(!PT_FitsSentDataToFragment(sendSize, TLTYPE_LAST, NULL))
            {
                // Get next transfer size
                PT_FitsSentDataToFragment(sendSize, TLTYPE_MIDDLE, &nextTransferSize);
            
                // copy input data into communication buffer
                PT_memmove(pS->pBuf, pData, nextTransferSize);

                // transfer
                status = PT_MiddleTransact(pS, nextTransferSize, 0);
                sendSize -= nextTransferSize;
                pData += nextTransferSize;
            } else {
                // copy input data into communication buffer
                PT_memmove(pS->pBuf, pData, sendSize);

                // transfer
                status = PT_LastTransact(pS, sendSize, (uint32 *)NULL, 0);
                break;
            }
        }
    }    
    return status;
}


PTAPI_DLL PT_STATUS PTAPI PTGetAppData(
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwArea,
    OUT PT_DATA **ppAppData)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 area;            // Source area
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        uint32 dataLen;         // size of returned data
    } Response;
    
    PT_STATUS status;
    uint32 recvSize, recvTotalSize;
    PT_SESSION *pS = &gSession; 
    
    UNREFERENCED_PARAMETER (hConnection);   

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || ppAppData == NULL)
        return PT_STATUS_INVALID_PARAMETER;
    
    // copy input data into communication buffer
    ((Request*)pS->pBuf)->area = HTOL32(dwArea);
    
    // transact data
    status = PT_SimpleTransactEx(pS, CL_COMMAND_MISC_GET_APP_DATA, sizeof(Request), &recvTotalSize, &recvSize, 0);

    // get remaining fragments, if needed
    if (status == PT_STATUS_OK)
    {
        status = PT_GetNextFragmentCombined(pS, &recvSize);
    }

    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response) ||
            recvSize < sizeof(Response) + LTOH32(((Response*)pS->pBuf)->dataLen))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        *ppAppData = (PT_DATA*)(pS->pBuf + offsetof(Response, dataLen));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
        (*ppAppData)->Length = LTOH32((*ppAppData)->Length);
#endif

#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        if ((*ppAppData = (PT_DATA*)MallocCopy((*ppAppData)->Length + sizeof(PT_DWORD), *ppAppData)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
#endif
    }

    return status;
}

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTGetAvailableMemory(
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwType,
    OUT PT_DWORD *pdwAvailableMemory)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 type;        // memory type
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        uint32 availableMem; // available memory
    } Response;

    PT_STATUS status;
    uint32 recvSize;
    PT_SESSION *pS = &gSession;
    
    UNREFERENCED_PARAMETER (hConnection);    

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pdwAvailableMemory == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->type = dwType;
    
    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_MISC_GET_AVAILABLE_MEMORY, sizeof(Request), &recvSize, 0);
    
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        *pdwAvailableMemory = LTOH32(((Response*)pS->pBuf)->availableMem);
    }
    
    return status;
}
#endif //#if !defined(TBX)

#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
// WARNING:
// ==========================================================
// This is list of all versions of PT_SESSION_CFG structure,
// we are able to handle regarding endianness of the host.
// Whenever you add new item into the list you _MUST_ implement
// handling of endians of the new version of the structure in
// TfmConvertSessionCfg().
static const struct {
    PT_WORD wCfgVersion;
    PT_WORD wCfgLength;
} sessionCfgVersionMap[] = {
    { 1, sizeof(PT_SESSION_CFG_V1) }, 
    { 2, sizeof(PT_SESSION_CFG_V2) },
    { 3, sizeof(PT_SESSION_CFG_V3) },
    { 4, sizeof(PT_SESSION_CFG_V4) },
    { 5, sizeof(PT_SESSION_CFG_V5) }
};

// Returns PT_STATUS_OK, if we know about the version of PT_SESSION_CFG
// and the expected size matches to the size of appropriate structure.
// If wCfgLength is zero, this extra check is omitted.
static PT_STATUS TfmCheckSessionCfgVersion(PT_WORD wCfgVersion, PT_WORD wCfgLength)
{
    PT_STATUS status = PT_STATUS_ENDIAN_ERROR;
    int i;

    for(i = 0; i < sizeof(sessionCfgVersionMap) / sizeof(sessionCfgVersionMap[0]); i++)
    {
        if(wCfgVersion == sessionCfgVersionMap[i].wCfgVersion)
        // We found the version.
        {
            // Check that the structure has the expected size.
            if(wCfgLength == 0  ||  wCfgLength == sessionCfgVersionMap[i].wCfgLength)
                status = PT_STATUS_OK;
            break;
        }
    }
    
    return status;
}


// Constants for parameter direction of TfmConvertSessionCfg().
#define TFM_HOST_TO_LITTLE               1
#define TFM_LITTLE_TO_HOST               2

// Function wrappers for the endianess conversion macros. 
// Makes TfmConvertSessionCfg() much easier (we can use function pointers).
static uint32 TfmHostToLittle32(uint32 value)   { return HTOL32(value); }
static uint16 TfmHostToLittle16(uint16 value)   { return HTOL16(value); }
static uint32 TfmLittleToHost32(uint32 value)   { return LTOH32(value); }
static uint16 TfmLittleToHost16(uint16 value)   { return LTOH16(value); }

// Converts the PT_SESSION_CFG form host endian to little endian or vice versa.
// You should call "TfmCheckSessionCfgVersion() prior to this function.
// If "TfmCheckSessionCfgVersion() succeeds it's guaranteed the conversion will be ok.
// Note that the conversion is done in place.
static void TfmConvertSessionCfg(PT_SESSION_CFG* pCfg, PT_WORD wCfgVersion, PT_BYTE direction)
{
    uint32 (*fnConv32)(uint32);
    uint16 (*fnConv16)(uint16);
    
    // Decide what converion we perform.
    if(direction == TFM_HOST_TO_LITTLE) {
        fnConv32 = TfmHostToLittle32;
        fnConv16 = TfmHostToLittle16;
    } else {
        fnConv32 = TfmLittleToHost32;
        fnConv16 = TfmLittleToHost16;
    }

// Helper temporary macro used for preventing stupid bugs caused by human :-)
// This way we don't need to check manualy sizes of PT_SESSION_CFG member
// sizes in the following boring code. Vivat preprocessror.
#define FN_CONV(v)                                \
    switch(sizeof(v)) {                           \
        case 1:   break;                          \
        case 2:   v = fnConv16(v); break;         \
        case 4:   v = fnConv32(v); break;         \
        default:  break;              \
    }

    if(wCfgVersion == 1) 
    {
        // Convert PT_SESSION_CFG_V1:
        PT_SESSION_CFG_V1* pCfgV1 = (PT_SESSION_CFG_V1*) pCfg;
        FN_CONV(pCfgV1->SecuritySetting);
        FN_CONV(pCfgV1->AntispoofingLevel);
        FN_CONV(pCfgV1->MultipleEnroll);
        FN_CONV(pCfgV1->LatentDetect);
        FN_CONV(pCfgV1->SubSample);
        FN_CONV(pCfgV1->SensorDisabling);
        FN_CONV(pCfgV1->CallbackLevel);
    } 
    else if(wCfgVersion == 2) 
    {
        // Convert PT_SESSION_CFG_V2:
        PT_SESSION_CFG_V2* pCfgV2 = (PT_SESSION_CFG_V2*) pCfg;
        FN_CONV(pCfgV2->SecuritySetting);
        FN_CONV(pCfgV2->AntispoofingLevel);
        FN_CONV(pCfgV2->MultipleEnroll);
        FN_CONV(pCfgV2->LatentDetect);
        FN_CONV(pCfgV2->SubSample);
        FN_CONV(pCfgV2->SensorDisabling);
        FN_CONV(pCfgV2->CallbackLevel);
        FN_CONV(pCfgV2->WakeUpByFinger);
        FN_CONV(pCfgV2->SubWindowArea);
        FN_CONV(pCfgV2->WffUseHwDetection);
        FN_CONV(pCfgV2->WffFingerDownDelay);
        FN_CONV(pCfgV2->WffFingerUpDelay);
        FN_CONV(pCfgV2->RecUseHwDetection);
        FN_CONV(pCfgV2->RecFingerDownDelay);
        FN_CONV(pCfgV2->RecFingerUpDelay);
        FN_CONV(pCfgV2->RecRemoveTopdown);
        FN_CONV(pCfgV2->RecRemoveBottomup);
        FN_CONV(pCfgV2->NavUseHwDetection);
        FN_CONV(pCfgV2->NavFingerDownDelay);
        FN_CONV(pCfgV2->NavFingerUpDelay);
        FN_CONV(pCfgV2->NavClickTimeMin);
        FN_CONV(pCfgV2->NavClickTimeMax);
        FN_CONV(pCfgV2->NavMovementDelay);
        FN_CONV(pCfgV2->NavClickAllowedMovement);
    } 
    else if(wCfgVersion == 3)
    {
        // Convert PT_SESSION_CFG_V3:
        PT_SESSION_CFG_V3* pCfgV3 = (PT_SESSION_CFG_V3*) pCfg;
        FN_CONV(pCfgV3->SecuritySetting);
        FN_CONV(pCfgV3->AntispoofingLevel);
        FN_CONV(pCfgV3->MultipleEnroll);
        FN_CONV(pCfgV3->LatentDetect);
        FN_CONV(pCfgV3->SubSample);
        FN_CONV(pCfgV3->SensorDisabling);
        FN_CONV(pCfgV3->CallbackLevel);
        FN_CONV(pCfgV3->WakeUpByFinger);
        FN_CONV(pCfgV3->WakeUpByFingerTimeout);
        FN_CONV(pCfgV3->SubWindowArea);
        FN_CONV(pCfgV3->ConsolidationType);
        FN_CONV(pCfgV3->WffUseHwDetection);
        FN_CONV(pCfgV3->WffFingerDownDelay);
        FN_CONV(pCfgV3->WffFingerUpDelay);
        FN_CONV(pCfgV3->RecUseHwDetection);
        FN_CONV(pCfgV3->RecFingerDownDelay);
        FN_CONV(pCfgV3->RecFingerUpDelay);
        FN_CONV(pCfgV3->RecTerminationPolicy);
        FN_CONV(pCfgV3->RecRetuning);
        FN_CONV(pCfgV3->RecDigitalGain);
        FN_CONV(pCfgV3->RecRemoveTopdown);
        FN_CONV(pCfgV3->RecRemoveBottomup);
        FN_CONV(pCfgV3->NavUseHwDetection);
        FN_CONV(pCfgV3->NavFingerDownDelay);
        FN_CONV(pCfgV3->NavFingerUpDelay);
        FN_CONV(pCfgV3->NavClickTimeMin);
        FN_CONV(pCfgV3->NavClickTimeMax);
        FN_CONV(pCfgV3->NavMovementDelay);
        FN_CONV(pCfgV3->NavClickAllowedMovement);
        FN_CONV(pCfgV3->NavNavigationType);
        FN_CONV(pCfgV3->BioEnrollInputType);
        FN_CONV(pCfgV3->BioVerifyInputType);
        FN_CONV(pCfgV3->EnableScanQualityQuery);
    }
    else if(wCfgVersion == 4)
    {
        // Convert PT_SESSION_CFG_V4:
        PT_SESSION_CFG_V4* pCfgV4 = (PT_SESSION_CFG_V4*) pCfg;
        FN_CONV(pCfgV4->SecuritySetting);
        FN_CONV(pCfgV4->AntispoofingEnable);
        FN_CONV(pCfgV4->AntispoofingSecurityLevel);
        FN_CONV(pCfgV4->MultipleEnroll);
        FN_CONV(pCfgV4->LatentDetect);
        FN_CONV(pCfgV4->SubSample);
        FN_CONV(pCfgV4->SensorDisabling);
        FN_CONV(pCfgV4->CallbackLevel);
        FN_CONV(pCfgV4->WakeUpByFinger);
        FN_CONV(pCfgV4->WakeUpByFingerTimeout);
        FN_CONV(pCfgV4->SubWindowArea);
        FN_CONV(pCfgV4->ConsolidationType);
        FN_CONV(pCfgV4->CreateWrappedBIRs);
        FN_CONV(pCfgV4->SignatureType);
        FN_CONV(pCfgV4->RestartBioTimeout);
        FN_CONV(pCfgV4->IdentificationThreshold);
        FN_CONV(pCfgV4->ExtractionMaxMinu);
        FN_CONV(pCfgV4->EnrollMaxMinu);
        FN_CONV(pCfgV4->TemplateType);
        FN_CONV(pCfgV4->WffUseHwDetection);
        FN_CONV(pCfgV4->WffFingerDownDelay);
        FN_CONV(pCfgV4->WffFingerUpDelay);
        FN_CONV(pCfgV4->RecUseHwDetection);
        FN_CONV(pCfgV4->RecSwipeDirection)
        FN_CONV(pCfgV4->RecNoiseRobustness);
        FN_CONV(pCfgV4->RecSwipeTimeout);
        FN_CONV(pCfgV4->RecNoMovementTimeout);
        FN_CONV(pCfgV4->RecNoMovementResetTimeout);
        FN_CONV(pCfgV4->RecTerminationPolicy);
        FN_CONV(pCfgV4->RecRemoveTopdown);
        FN_CONV(pCfgV4->RecRemoveBottomup);
        FN_CONV(pCfgV4->NavUseHwDetection);
        FN_CONV(pCfgV4->NavFingerDownDelay);
        FN_CONV(pCfgV4->NavFingerUpDelay);
        FN_CONV(pCfgV4->NavClickTimeMin);
        FN_CONV(pCfgV4->NavClickTimeMax);
        FN_CONV(pCfgV4->NavMovementDelay);
        FN_CONV(pCfgV4->NavFlags);
        FN_CONV(pCfgV4->NavClickAllowedMovement);
        FN_CONV(pCfgV4->NavNavigationType);
        FN_CONV(pCfgV4->NavOrientation);
        FN_CONV(pCfgV4->NavSubPixelPrecision);
        FN_CONV(pCfgV4->BioEnrollInputType);
        FN_CONV(pCfgV4->BioVerifyInputType);
        FN_CONV(pCfgV4->BioFingerDetectInputType);
        FN_CONV(pCfgV4->EnableScanQualityQuery);
        FN_CONV(pCfgV4->UseHwFingerDetection);
        FN_CONV(pCfgV4->Retuning);
        FN_CONV(pCfgV4->SuspendInSleep);
        FN_CONV(pCfgV4->SensorSecurityMode);
    } 
    else if(wCfgVersion == 5) 
    {
        // Convert PT_SESSION_CFG_V5:
        PT_SESSION_CFG_V5* pCfgV5 = (PT_SESSION_CFG_V5*) pCfg;
        FN_CONV(pCfgV5->SecuritySetting);
        FN_CONV(pCfgV5->AntispoofingEnable);
        FN_CONV(pCfgV5->AntispoofingSecurityLevel);
        FN_CONV(pCfgV5->AntispoofingRejectsEnrollment);
        FN_CONV(pCfgV5->MultipleEnroll);
        FN_CONV(pCfgV5->LatentDetect);
        FN_CONV(pCfgV5->SensorDisabling);
        FN_CONV(pCfgV5->TemplateType);
        FN_CONV(pCfgV5->CallbackLevel);
        FN_CONV(pCfgV5->ConsolidationType);
        FN_CONV(pCfgV5->ConsolidationSecurityLevel);
        FN_CONV(pCfgV5->ConsolidationNumTemplates);
        FN_CONV(pCfgV5->CreateWrappedBIRs);
        FN_CONV(pCfgV5->RestartBioTimeout);
        FN_CONV(pCfgV5->IdentificationThreshold);
        FN_CONV(pCfgV5->ExtractionMaxMinu);
        FN_CONV(pCfgV5->EnrollMaxMinu);
        FN_CONV(pCfgV5->ExtractionMaxSize);
        FN_CONV(pCfgV5->EnrollMaxSize);
        FN_CONV(pCfgV5->WakeUpByFingerTimeout);
        FN_CONV(pCfgV5->WakeUpByFinger);
        FN_CONV(pCfgV5->SignatureType);
        //FN_CONV(pCfgV5->reserved1[10]);
        FN_CONV(pCfgV5->WffFingerDownDelay);
        FN_CONV(pCfgV5->WffFingerUpDelay);
        FN_CONV(pCfgV5->RecSwipeDirection);
        FN_CONV(pCfgV5->RecNoiseRobustness);
        FN_CONV(pCfgV5->RecNoiseRobustnessTrigger);
        FN_CONV(pCfgV5->RecTerminationPolicy);
        FN_CONV(pCfgV5->RecSwipeTimeout);
        FN_CONV(pCfgV5->RecNoMovementTimeout);
        FN_CONV(pCfgV5->RecNoMovementResetTimeout);
        FN_CONV(pCfgV5->RecFlags);
        //FN_CONV(pCfgV5->reserved2[8]);
        FN_CONV(pCfgV5->NavFingerDownDelay);
        FN_CONV(pCfgV5->NavFingerUpDelay);
        FN_CONV(pCfgV5->NavClickTimeMin);
        FN_CONV(pCfgV5->NavClickTimeMax);
        FN_CONV(pCfgV5->NavMovementDelay);
        FN_CONV(pCfgV5->NavFlags);
        FN_CONV(pCfgV5->NavClickAllowedMovement);
        FN_CONV(pCfgV5->NavNavigationType);
        FN_CONV(pCfgV5->NavOrientation);
        FN_CONV(pCfgV5->NavClickSensitivity);
        //FN_CONV(pCfgV5->reserved3[9]);
        FN_CONV(pCfgV5->BioEnrollInputType);
        FN_CONV(pCfgV5->BioVerifyInputType);
        FN_CONV(pCfgV5->BioFingerDetectInputType);
        FN_CONV(pCfgV5->UseHwFingerDetection);
        FN_CONV(pCfgV5->EnableScanQualityQuery);
        FN_CONV(pCfgV5->Retuning);
        FN_CONV(pCfgV5->SuspendInSleep);
        FN_CONV(pCfgV5->SensorSecurityMode);
        //FN_CONV(pCfgV5->reserved4[19]);
    } 
    else 
    {
        // Unsupported version should be filtered out by TfmCheckSessionCfgVersion()
        // prior to calling this function.
        //TASSERTF();
    }

#undef FN_CONV  // not needed anymore
    
}
#endif  // TFM_ENDIAN != TFM_ENDIAN_LITTLE

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTSetSessionCfgEx(
    IN PT_CONNECTION hConnection,
    IN PT_WORD wCfgVersion,
    IN PT_WORD wCfgLength,
    IN PT_SESSION_CFG *pSessionCfg)
{

    typedef struct request_tag {
        CL_TAG tag;
        uint32 cfgVersion;         // Version of configuration data
        uint32 cfgLength;          // Length of configuration data
    } Request;

    uint32 sendSize;
    PT_SESSION *pS = &gSession;
    PT_SESSION_CFG *pBufSessionCfg;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pSessionCfg == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }

    // check length of input data
    sendSize = sizeof(Request) + wCfgLength;

    if (!PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }

    // copy input data into communication buffer
    PT_memmove(pS->pBuf + sizeof(Request), pSessionCfg, wCfgLength);
    //convert the multiple bytes fields of PT_SESSION_CFG into little endian if needed.
    pBufSessionCfg = (PT_SESSION_CFG *)(pS->pBuf + sizeof(Request));

#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
    if (wCfgLength > 0)
    {
        TfmConvertSessionCfg((PT_SESSION_CFG*)pBufSessionCfg, wCfgVersion, TFM_HOST_TO_LITTLE);
    }
#endif

    ((Request*)pS->pBuf)->cfgVersion = HTOL32(wCfgVersion);
    ((Request*)pS->pBuf)->cfgLength = HTOL32(wCfgLength);

    // transact data
    return PT_SimpleTransact(pS, CL_COMMAND_MISC_SET_SESSION_CFGEX, sendSize, (uint32 *)NULL, 0);
}
#endif //#if !defined(TBX)    

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTGetSessionCfgEx(
    IN PT_CONNECTION hConnection,
    IN PT_WORD wCfgVersion,
    OUT PT_WORD *pwCfgLength,
    OUT PT_SESSION_CFG **ppSessionCfg)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 cfgVersion;         // Version of configuration data
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        uint32 cfgLength;          // Length of configuration data
    } Response;
    
    PT_STATUS status;
    uint32 recvSize;
    PT_SESSION *pS = &gSession;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pwCfgLength == NULL || ppSessionCfg == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }
   
    // copy input data into communication buffer
    ((Request*)pS->pBuf)->cfgVersion = HTOL32(wCfgVersion);
    
    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_MISC_GET_SESSION_CFGEX, sizeof(Request), &recvSize, 0);
    
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response) ||
            recvSize < sizeof(Response) + LTOH32(((Response*)pS->pBuf)->cfgLength))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get returned data
        *pwCfgLength = (PT_SHORT)(LTOH32(((Response*)pS->pBuf)->cfgLength));
        *ppSessionCfg = (PT_SESSION_CFG*)(pS->pBuf + sizeof(Response));
        
//convert the multiple bytes fields of PT_SESSION_CFG into native endian if needed. 
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
    if (*pwCfgLength > 0)
    {
        TfmConvertSessionCfg((PT_SESSION_CFG*)*ppSessionCfg, wCfgVersion, TFM_LITTLE_TO_HOST);
    }
#endif

#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        if ((*ppSessionCfg = (PT_SESSION_CFG*)MallocCopy(*pwCfgLength, *ppSessionCfg)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
#endif
        
    }
    
    return status;
}
#endif //#if !defined(TBX)

PTAPI_DLL PT_STATUS PTAPI PTInfo (
    IN PT_CONNECTION hConnection, 
    OUT PT_INFO **ppInfo
)
{
    typedef struct reponse_tag {
        CL_TAG tag;
        PT_INFO info;
    } Response;

    PT_STATUS status;
    PT_SESSION *pS = &gSession;
    uint32 recvSize;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || ppInfo == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }
   
    // There is no request block - simply do transact
    status = PT_SimpleTransact(pS, CL_COMMAND_MISC_GET_INFO, sizeof(CL_TAG), &recvSize, 0);
    
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get returned data
        *ppInfo = &((Response*)pS->pBuf)->info;

#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
        //convert the multiple bytes fields of PT_INFO into native endian if needed.  
        (*ppInfo)->FwVersion = LTOH32((*ppInfo)->FwVersion);
        (*ppInfo)->FwMinNextVersion = LTOH32((*ppInfo)->FwMinNextVersion);
        (*ppInfo)->FwVariant = LTOH32((*ppInfo)->FwVariant);
        (*ppInfo)->FwFunctionality = LTOH32((*ppInfo)->FwFunctionality);
        (*ppInfo)->FwConfig = LTOH32((*ppInfo)->FwConfig);
        (*ppInfo)->Id = LTOH32((*ppInfo)->Id);
        (*ppInfo)->AuthentifyId = LTOH32((*ppInfo)->AuthentifyId);
        (*ppInfo)->Usage = LTOH32((*ppInfo)->Usage);
        (*ppInfo)->SensorType = LTOH32((*ppInfo)->SensorType);
        (*ppInfo)->ImageWidth = LTOH16((*ppInfo)->ImageWidth);
        (*ppInfo)->ImageHeight = LTOH16((*ppInfo)->ImageHeight);
        (*ppInfo)->MaxGrabWindow = LTOH32((*ppInfo)->MaxGrabWindow);
        (*ppInfo)->CompanionVendorCode = LTOH32((*ppInfo)->CompanionVendorCode);
#endif        

#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        if ((*ppInfo = (PT_INFO*)MallocCopy(sizeof(**ppInfo), *ppInfo)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
#endif
    }
    
    return status;
}


PTAPI_DLL PT_STATUS PTAPI PTExtendedInfo (
    IN PT_CONNECTION hConnection, 
    OUT PT_EXTENDED_INFO **ppExtInfo
)
{
    typedef struct reponse_tag {
        CL_TAG tag;
        PT_EXTENDED_INFO info;
    } Response;

    PT_STATUS status;
    PT_SESSION *pS = &gSession;
    uint32 recvSize;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || ppExtInfo == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }

    // There is no request block - simply do transact
    status = PT_SimpleTransact(pS, CL_COMMAND_MISC_GET_EXTENDED_INFO, sizeof(CL_TAG), &recvSize, 0);

    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif

        // get returned data
        *ppExtInfo = &((Response*)pS->pBuf)->info;

#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
        //convert the multiple bytes fields of PT_EXTENDED_INFO into native endian if needed.  
        (*ppExtInfo)->Version = LTOH32((*ppExtInfo)->Version);
        (*ppExtInfo)->SystemId = LTOH32((*ppExtInfo)->SystemId);
        if ((*ppExtInfo)->Version >= 2)
        {
            PT_EXTENDED_INFO_V2 *pExtInfo= (PT_EXTENDED_INFO_V2 *)(*ppExtInfo);
            pExtInfo->FwFunctionality2 = LTOH32(pExtInfo->FwFunctionality2);
            pExtInfo->BioLibVersion = LTOH32(pExtInfo->BioLibVersion);
            pExtInfo->TemplateTypeFlags = LTOH32(pExtInfo->TemplateTypeFlags);
            pExtInfo->FwUpdateCount = LTOH16(pExtInfo->FwUpdateCount);
            pExtInfo->SensorVariant = LTOH16(pExtInfo->SensorVariant);
            pExtInfo->ReaderVersion = LTOH32(pExtInfo->ReaderVersion);
        }
#endif
#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        {
            uint32 copySize = ((*ppExtInfo)->Version < 2) ? sizeof(PT_EXTENDED_INFO_V1) : sizeof(PT_EXTENDED_INFO_V2);
            if ((*ppExtInfo = (PT_EXTENDED_INFO*)MallocCopy(copySize, *ppExtInfo)) == NULL)
                status = PT_STATUS_MALLOC_FAILED;
        }        
#endif
    }

    return status;
}

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTSetLED(
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwMode,
    IN PT_DWORD dwLED1,
    IN PT_DWORD dwLED2)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 mode;               // Mode of LEDs
        uint32 led1;               // Parameter defining behavior of LED 1
        uint32 led2;               // Parameter defining behavior of LED 2
    } Request;
    
    PT_SESSION *pS = &gSession;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection) return PT_STATUS_INVALID_PARAMETER;
    
    // copy input data into communication buffer
    ((Request*)pS->pBuf)->mode = HTOL32(dwMode);
    ((Request*)pS->pBuf)->led1 = HTOL32(dwLED1);
    ((Request*)pS->pBuf)->led2 = HTOL32(dwLED2);
    
    // transact data
    return PT_SimpleTransact(pS, CL_COMMAND_MISC_SET_LED, sizeof(Request), (uint32 *)NULL, 0);
}
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTGetLED(
    IN PT_CONNECTION hConnection, 
    OUT PT_DWORD *pdwMode,
    OUT PT_DWORD *pdwLED1,
    OUT PT_DWORD *pdwLED2)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 mode;               // Mode of LEDs
        uint32 led1;               // Parameter defining behavior of LED 1
        uint32 led2;               // Parameter defining behavior of LED 2
    } Response;
    
    PT_STATUS status;
    uint32 recvSize;
    PT_SESSION *pS = &gSession;
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pdwMode == NULL || pdwLED1 == NULL || pdwLED2 == NULL)
        return PT_STATUS_INVALID_PARAMETER;
    
    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_MISC_GET_LED, sizeof(CL_TAG), &recvSize, 0);
    
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        *pdwMode = LTOH32(((Response*)pS->pBuf)->mode);
        *pdwLED1 = LTOH32(((Response*)pS->pBuf)->led1);
        *pdwLED2 = LTOH32(((Response*)pS->pBuf)->led2);
    } 
    
    return status;
}
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTSleep(
    IN PT_CONNECTION hConnection, 
    IN PT_DWORD dwSleepMode,
    IN PT_IDLE_CALLBACK pfnCallback,
    IN void *pIdleCallbackCtx,
    OUT PT_DWORD *pdwWakeupCause)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 mode;    // sleep mode
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        uint32 wakeUpCause;     // cause of wakeup
    } Response;

    PT_STATUS status;
    uint32 recvSize;
    PT_SESSION *pS = &gSession;
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pfnCallback == NULL || pdwWakeupCause == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    // set idle callback
    pS->pfnIdleCallback = pfnCallback;
    pS->pIdleCallbackCtx = pIdleCallbackCtx;

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->mode = HTOL32(dwSleepMode);
    
    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_MISC_SLEEP, sizeof(Request), &recvSize, TRANSACT_FLAG_SLEEP);

    pS->pfnIdleCallback = NULL;
    pS->pIdleCallbackCtx = NULL;
    
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        *pdwWakeupCause = LTOH32(((Response*)pS->pBuf)->wakeUpCause);
    } 
    
    return status;
}
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTGetAccessRights(
    IN PT_CONNECTION    hConnection,
    IN PT_DWORD         dwAccountId,
    OUT PT_DATA         **ppAccessRights)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 account;     // Account ID
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        uint32 dataLen;     // size of returned data
    } Response;

    PT_STATUS status;
    uint32 recvSize;
    PT_SESSION *pS = &gSession;    
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || ppAccessRights == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->account = HTOL32(dwAccountId);

    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_ACC_GET_ACCESS_RIGHTS, sizeof(Request), &recvSize, 0);

    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response) ||
            recvSize < sizeof(Response) + LTOH32(((Response*)pS->pBuf)->dataLen))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        *ppAccessRights = (PT_DATA*)(pS->pBuf + offsetof(Response, dataLen));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
        (*ppAccessRights)->Length = LTOH32((*ppAccessRights)->Length);
#endif

#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        if ((*ppAccessRights = (PT_DATA*)MallocCopy((*ppAccessRights)->Length + sizeof(PT_DWORD), *ppAccessRights)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
#endif
    }

    return status;
}
#endif //#if !defined(TBX)

//========================================================================
//      Biometric functions
//========================================================================

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTGrabPart(
    IN PT_CONNECTION hConnection, 
    IN PT_BYTE byType,
    IN PT_LONG lTimeout,
    IN PT_BOOL boWaitForAcceptableFinger,
    IN PT_BOOL boStartGrab,
    OUT PT_BYTE **ppGrabbedData, 
    OUT PT_DWORD *pdwPartSize,
    OUT PT_DWORD *pdwTotalSize,
    IN PT_DATA* pSignData,
    OUT PT_DATA **ppSignature)
{
    typedef struct request_tag {
        CL_TAG tag;
        sint32 timeout;     // timeout
        uint8  type;        // grab type
        uint8  flagsLo;     // Low 8 bits of flags
        uint8  flagsHi;     // High 8 bits of flags
    } Request;
    
    typedef struct response_tag {
        CL_TAG tag;
        uint32 dataLen;     // size of returned data
    } Response;
    
    PT_STATUS status;
    uint32 sendSize, recvTotalSize, recvFragmentSize, flags = 0;
    PT_SESSION *pS = &gSession;

    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || ppGrabbedData == NULL || pdwPartSize == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    if (boStartGrab)
    {
        // start grab, and receive first response fragment

        if (pdwTotalSize == NULL)
            return PT_STATUS_INVALID_PARAMETER;

        if (boWaitForAcceptableFinger) // wait for acceptable quality ?
        {
            flags |= BG_WAIT_GOOD_FINGER;
        }

        // copy input data into communication buffer
        ((Request*)pS->pBuf)->timeout = HTOL32(lTimeout);
        ((Request*)pS->pBuf)->type = byType;
        ((Request*)pS->pBuf)->flagsLo = (uint8)flags;
        ((Request*)pS->pBuf)->flagsHi = (uint8)(flags >> 8);

        sendSize = sizeof(CL_TAG) + sizeof(sint32) + 3*sizeof(uint8);

        status = PT_SimpleTransactEx(pS, CL_COMMAND_BIO_GRAB, sendSize, &recvTotalSize, &recvFragmentSize, 0);

        if (status == PT_STATUS_OK)
        {
#ifdef _DEBUG
            if (recvFragmentSize < sizeof(Response) || LTOH32(((Response*)((PT_SESSION*)pS)->pBuf)->dataLen) > recvTotalSize)
            {
                return PT_STATUS_WRONG_RESPONSE;
            }
#endif
            *ppGrabbedData = (PT_BYTE*)(((PT_SESSION*)pS)->pBuf + sizeof(Response));
            *pdwPartSize = recvFragmentSize - sizeof(Response);
            *pdwTotalSize = LTOH32(((Response*)((PT_SESSION*)pS)->pBuf)->dataLen);
        }
    }    
    else
    {
        // get next data fragments
        status = PT_GetNextFragment(pS, &recvFragmentSize);
        if (status == PT_STATUS_OK)
        {
            *ppGrabbedData = (PT_BYTE*)((PT_SESSION*)pS)->pBuf;
            *pdwPartSize = recvFragmentSize;
        }
    }

#if defined (TARGET_STANDARD_API)
    if (status == PT_STATUS_OK)
    {
        // allocate buffer for output parameter and copy data into it
        if ((*ppGrabbedData = (PT_BYTE*)MallocCopy(*pdwPartSize, *ppGrabbedData)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
    }
#endif
        
    return status;    
}
#endif //#if !defined(TBX)

#if !defined(TBX)
#if defined(ENABLE_ASYNC_IMG_TRANSFER)
PTAPI_DLL PT_STATUS PTAPI PTGrabAsync(
    IN PT_CONNECTION hConnection, 
    IN PT_BYTE byType,
    IN PT_LONG lTimeout,
    IN PT_BOOL boWaitForAcceptableFinger,
    IN PT_GUI_ASYNC_CALLBACK pGuiAsyncCallback,
    IN void	*pGuiAsyncCallbackCtx, 
    IN PT_DATA* pSignData,
    OUT PT_DATA **ppSignature)
{

    typedef struct request_tag {
        CL_TAG tag;
        sint32 timeout;     // timeout
        uint8  type;        // grab type
        uint8  flagsLo;     // Low 8 bits of flags
        uint8  flagsHi;     // High 8 bits of flags
    } Request;
    
    typedef struct response_tag {
        CL_TAG tag;
        uint32 dataLen;     // size of returned data
    } Response;
    
    PT_STATUS status;
    uint32 sendSize, recvTotalSize, recvFragmentSize, flags = 0;
    PT_SESSION *pS = &gSession;

    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pGuiAsyncCallback == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    if (boWaitForAcceptableFinger) // wait for acceptable quality ?
    {
        flags |= BG_WAIT_GOOD_FINGER;
    }

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->timeout = HTOL32(lTimeout);
    ((Request*)pS->pBuf)->type = byType;
    ((Request*)pS->pBuf)->flagsLo = (uint8)flags;
    ((Request*)pS->pBuf)->flagsHi = (uint8)(flags >> 8);
    
    pS->pfnGuiAsyncCallback = pGuiAsyncCallback;
    pS->pGuiAsyncCallbackCtx = pGuiAsyncCallbackCtx;
    pS->dwGuiAsyncCallbackReceivedSize = 0;

    sendSize = sizeof(CL_TAG) + sizeof(sint32) + 3*sizeof(uint8);

    status = PT_SimpleTransactEx(pS, CL_COMMAND_BIO_GRAB, sendSize, &recvTotalSize, &recvFragmentSize, 0); 
    
    pS->pfnGuiAsyncCallback = NULL;  
    pS->pGuiAsyncCallbackCtx = NULL;
            
    return status;    
}
#endif //#if defined(ENABLE_ASYNC_IMG_TRANSFER)
#endif //#if !defined(TBX)

#if !defined(TBX)
#if defined(ENABLE_ASYNC_IMG_TRANSFER)
#if defined (TARGET_STANDARD_API)
PT_STATUS PTAPI GrabAsyncDefaultCallback(
    IN void *pGuiAsyncCallbackCtx,
    IN PT_BYTE *pImageData,
    IN PT_DWORD dwPartSize,
    IN PT_DWORD dwTotalSize,
    IN PT_DWORD dwFlags)
{
    PT_DATA **ppGrabbedData = (PT_DATA **)pGuiAsyncCallbackCtx;
    PT_DATA *pImage = *ppGrabbedData;
    PT_STATUS status = PT_STATUS_OK;

    //if this is the first asynchronous GUI callback of a new image transfer
    if(dwTotalSize != 0)
    {
        //discard all previously received data
        if(pImage != NULL)
        {
            gpfnFree(pImage);
        }
        
        //allocate buffer for output image
        pImage = (PT_DATA*)gpfnMalloc(dwTotalSize + sizeof(PT_DWORD));
        *ppGrabbedData = pImage;
        if (pImage == NULL)
        {
            status = PT_STATUS_MALLOC_FAILED;
            goto endfunc;
        }  
        
        pImage->Length = 0;      
    }

    PT_memcpy(pImage->Data + pImage->Length, pImageData, dwPartSize);
    pImage->Length += dwPartSize;   
        
endfunc:;       
    return status;  
}
#endif //#if defined (TARGET_STANDARD_API)
#endif //#if defined(ENABLE_ASYNC_IMG_TRANSFER)
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTGrab (
    IN PT_CONNECTION hConnection,
    IN PT_BYTE byType,
    IN PT_LONG lTimeout,
    IN PT_BOOL boWaitForAcceptableFinger,
    OUT PT_DATA **ppGrabbedData,
    IN PT_DATA *pSignData,
    OUT PT_DATA **ppSignature
)
{
    PT_STATUS status = PT_STATUS_ACCESS_DENIED;

    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (byType);
    UNREFERENCED_PARAMETER (lTimeout);
    UNREFERENCED_PARAMETER (boWaitForAcceptableFinger);
    UNREFERENCED_PARAMETER (ppGrabbedData);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature);

#if defined (TARGET_STANDARD_API)
    {
        PT_DWORD totalSize, partSize, decompSize;
        PT_DWORD currentSize = 0;
        PT_BYTE *pBytes;
        PT_DATA *pImage;

        // check for initialization
        if (!apiInitialized)
            return PT_STATUS_API_NOT_INIT;

        // check validity of input parameters
        if (!hConnection || ppGrabbedData == NULL)
            return PT_STATUS_INVALID_PARAMETER;
       
        //if image data should be provided through asynchronous callbacks, handle them separately
        if (byType == PT_GRAB_TYPE_508_508_8_SCAN508_508_8)
        {
#if defined(ENABLE_ASYNC_IMG_TRANSFER)         
            *ppGrabbedData = NULL;           
            status = PTGrabAsync(hConnection, byType, lTimeout, boWaitForAcceptableFinger, GrabAsyncDefaultCallback,
                                 (void*)ppGrabbedData, pSignData, ppSignature);
#else
            status = PT_STATUS_NOT_SUPPORTED;
#endif //#if defined(ENABLE_ASYNC_IMG_TRANSFER)
            goto endfunc;
        }

        // grab start
        status = PTGrabPart(
            hConnection, 
            byType, 
            lTimeout, 
            boWaitForAcceptableFinger, 
            PT_TRUE, 
            &pBytes,
            &partSize,
            &totalSize,
            pSignData,
            ppSignature);

        if (status != PT_STATUS_OK)
            return status;

        // alocate buffer for output image
        if ((pImage = (PT_DATA*)gpfnMalloc(totalSize + sizeof(PT_DWORD))) == NULL)
        {
            gpfnFree(pBytes);
            return PT_STATUS_MALLOC_FAILED;
        }

        pImage->Length = totalSize;
        PT_memcpy(pImage->Data, pBytes, partSize);
        currentSize = partSize;
        gpfnFree(pBytes);

        while (currentSize < totalSize)
        {
            // get next part of the image
            status = PTGrabPart(
                hConnection, 
                byType, 
                lTimeout, 
                boWaitForAcceptableFinger, 
                PT_FALSE, 
                &pBytes,
                &partSize,
                &totalSize,
                pSignData,
                ppSignature);
            
            if (status != PT_STATUS_OK)
                return status;

            PT_memcpy(pImage->Data + currentSize, pBytes, partSize);
            currentSize += partSize;
            gpfnFree(pBytes);
        }

        // is image compressed ?
        if (PTDecompressImage(byType, pImage->Length, pImage->Data, &decompSize, NULL) == PT_STATUS_OK)
        {
            PT_DATA *pDecompImage;

            // alocate buffer for decompressed image
            if ((pDecompImage = (PT_DATA*)gpfnMalloc(decompSize + sizeof(PT_DWORD))) == NULL)
            {
                gpfnFree(pImage);
                return PT_STATUS_MALLOC_FAILED;
            }

            pDecompImage->Length = decompSize;

            // decompress image
            PTDecompressImage(byType, pImage->Length, pImage->Data, &decompSize, pDecompImage->Data);

            gpfnFree(pImage);
            pImage = pDecompImage;
        }
            
        *ppGrabbedData = pImage;        

        status = PT_STATUS_OK;
    }

endfunc:;
#endif    
    
    return status;
}
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTSleepThenGrabPart(
    IN PT_CONNECTION hConnection,
    IN PT_IDLE_CALLBACK pfnCallback,
    IN void *pIdleCallbackCtx,
    IN PT_BYTE byGrabType,
    IN PT_LONG lTimeout,
    IN PT_BOOL boAssessImageQuality,
    IN PT_BOOL boStartGrab,
    OUT PT_DWORD *pdwWakeupCause,
    OUT PT_DWORD *pdwGrabGuiMessage,
    OUT PT_BYTE **ppGrabbedData, 
    OUT PT_DWORD *pdwPartSize,
    OUT PT_DWORD *pdwTotalSize,
    IN PT_DATA* pSignData,
    OUT PT_DATA **ppSignature)
{
    typedef struct request_tag {
        CL_TAG tag;
        sint32 timeout;     // timeout
        uint8  type;        // grab type
        uint8  flagsLo;     // Low 8 bits of flags
        uint8  flagsHi;     // High 8 bits of flags
        uint8  padding;
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        uint32 wakeupCause;
        uint32 guiMessage;
        uint32 dataLen;
    } Response;

    PT_STATUS status;
    uint32 recvTotalSize, recvFragmentSize, flags = 0;
    PT_SESSION *pS = &gSession;

    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || pdwWakeupCause == NULL || pdwGrabGuiMessage == NULL || ppGrabbedData == NULL || pdwPartSize == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    if (boStartGrab)
    {
        // start grab, and receive first response fragment

        if (pdwTotalSize == NULL)
            return PT_STATUS_INVALID_PARAMETER;

        if (boAssessImageQuality) // wait for acceptable quality ?
        {
            flags |= BG_WAIT_GOOD_FINGER;
        }

        // set idle callback
        pS->pfnIdleCallback = pfnCallback;
        pS->pIdleCallbackCtx = pIdleCallbackCtx;

        // copy input data into communication buffer
        ((Request*)pS->pBuf)->timeout = HTOL32(lTimeout);
        ((Request*)pS->pBuf)->type = byGrabType;
        ((Request*)pS->pBuf)->flagsLo = (uint8)flags;
        ((Request*)pS->pBuf)->flagsHi = (uint8)(flags >> 8);

        status = PT_SimpleTransactEx(pS, CL_COMMAND_BIO_SLEEP_THEN_GRAB, offsetof(Request, padding), &recvTotalSize, &recvFragmentSize, TRANSACT_FLAG_SLEEP);

        pS->pfnIdleCallback = NULL;
        pS->pIdleCallbackCtx = NULL;

        if (status == PT_STATUS_OK)
        {
#ifdef _DEBUG
            if (recvFragmentSize < offsetof(Response, dataLen) || 
                LTOH32(((Response*)(PT_SESSION*)pS->pBuf)->guiMessage) == PT_GUIMSG_GOOD_IMAGE && 
                (recvFragmentSize < sizeof(Response) || 
                LTOH32(((Response*)(PT_SESSION*)pS->pBuf)->dataLen) > recvTotalSize))
            {
                return PT_STATUS_WRONG_RESPONSE;
            }
#endif
            *pdwWakeupCause = LTOH32(((Response*)(PT_SESSION*)pS->pBuf)->wakeupCause);
            *pdwGrabGuiMessage = LTOH32(((Response*)(PT_SESSION*)pS->pBuf)->guiMessage);
            if (LTOH32(((Response*)(PT_SESSION*)pS->pBuf)->guiMessage) == PT_GUIMSG_GOOD_IMAGE)
            {
                *ppGrabbedData = (PT_BYTE*)(((PT_SESSION*)pS)->pBuf + sizeof(Response));
                *pdwPartSize = recvFragmentSize - sizeof(Response);
                *pdwTotalSize = LTOH32(((Response*)(PT_SESSION*)pS->pBuf)->dataLen);
            }
            else
            {
                *ppGrabbedData = NULL;
                *pdwPartSize = 0;
                *pdwTotalSize = 0;
            }
        }
    }    
    else
    {
        // get next data fragments
        status = PT_GetNextFragment(pS, &recvFragmentSize);
        if (status == PT_STATUS_OK)
        {
            *ppGrabbedData = (PT_BYTE*)((PT_SESSION*)pS)->pBuf;
            *pdwPartSize = recvFragmentSize;
        }
    }

#if defined (TARGET_STANDARD_API)
    if (status == PT_STATUS_OK)
    {
        // allocate buffer for output parameter and copy data into it
        if ((*ppGrabbedData = (PT_BYTE*)MallocCopy(*pdwPartSize, *ppGrabbedData)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
    }
#endif

    return status;    
}
#endif //#if !defined(TBX)

#if !defined(TBX)
#if defined(ENABLE_ASYNC_IMG_TRANSFER)
PTAPI_DLL PT_STATUS PTAPI PTSleepThenGrabAsync(
    IN PT_CONNECTION hConnection,
    IN PT_IDLE_CALLBACK pfnCallback,
    IN void *pIdleCallbackCtx,
    IN PT_BYTE byGrabType,
    IN PT_LONG lTimeout,
    IN PT_BOOL boAssessImageQuality,
    OUT PT_DWORD *pdwWakeupCause,
    OUT PT_DWORD *pdwGrabGuiMessage,
    IN PT_GUI_ASYNC_CALLBACK pGuiAsyncCallback,
    IN void	*pGuiAsyncCallbackCtx,
    IN PT_DATA *pSignData,
    OUT PT_DATA **ppSignature
)
{
    typedef struct request_tag {
        CL_TAG tag;
        sint32 timeout;     // timeout
        uint8  type;        // grab type
        uint8  flagsLo;     // Low 8 bits of flags
        uint8  flagsHi;     // High 8 bits of flags
        uint8  padding;
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        uint32 wakeupCause;
        uint32 guiMessage;
        uint32 dataLen;
    } Response;

    PT_STATUS status;
    uint32 recvTotalSize, recvFragmentSize, flags = 0;
    PT_SESSION *pS = &gSession;

    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || pdwWakeupCause == NULL || pdwGrabGuiMessage == NULL || pGuiAsyncCallback == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    if (boAssessImageQuality) // wait for acceptable quality ?
    {
        flags |= BG_WAIT_GOOD_FINGER;
    }

    // set idle callback
    pS->pfnIdleCallback = pfnCallback;
    pS->pIdleCallbackCtx = pIdleCallbackCtx;
    
    //set async callback
    pS->pfnGuiAsyncCallback = pGuiAsyncCallback;
    pS->pGuiAsyncCallbackCtx = pGuiAsyncCallbackCtx;
    pS->dwGuiAsyncCallbackReceivedSize = 0;

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->timeout = HTOL32(lTimeout);
    ((Request*)pS->pBuf)->type = byGrabType;
    ((Request*)pS->pBuf)->flagsLo = (uint8)flags;
    ((Request*)pS->pBuf)->flagsHi = (uint8)(flags >> 8);

    status = PT_SimpleTransactEx(pS, CL_COMMAND_BIO_SLEEP_THEN_GRAB, offsetof(Request, padding), &recvTotalSize, &recvFragmentSize, TRANSACT_FLAG_SLEEP);

    pS->pfnIdleCallback = NULL;
    pS->pIdleCallbackCtx = NULL;
    pS->pfnGuiAsyncCallback = NULL;  
    pS->pGuiAsyncCallbackCtx = NULL;

    if (status == PT_STATUS_OK)
    {
        *pdwWakeupCause = LTOH32(((Response*)(PT_SESSION*)pS->pBuf)->wakeupCause);
        *pdwGrabGuiMessage = LTOH32(((Response*)(PT_SESSION*)pS->pBuf)->guiMessage);
    }    

    return status;    
}
#endif //#if defined(ENABLE_ASYNC_IMG_TRANSFER)
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTSleepThenGrab(
    IN PT_CONNECTION hConnection,
    IN PT_IDLE_CALLBACK pfnCallback,
    IN void *pIdleCallbackCtx,
    IN PT_BYTE byGrabType,
    IN PT_LONG lTimeout,
    IN PT_BOOL boAssessImageQuality,
    OUT PT_DWORD *pdwWakeupCause,
    OUT PT_DWORD *pdwGrabGuiMessage,
    OUT PT_DATA **ppGrabbedData,
    IN PT_DATA *pSignData,
    OUT PT_DATA **ppSignature)
{
    PT_STATUS status = PT_STATUS_ACCESS_DENIED;

    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (pfnCallback);
    UNREFERENCED_PARAMETER (pIdleCallbackCtx);
    UNREFERENCED_PARAMETER (byGrabType);
    UNREFERENCED_PARAMETER (lTimeout);
    UNREFERENCED_PARAMETER (boAssessImageQuality);
    UNREFERENCED_PARAMETER (pdwWakeupCause);
    UNREFERENCED_PARAMETER (pdwGrabGuiMessage);
    UNREFERENCED_PARAMETER (ppGrabbedData);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature);

#if defined (TARGET_STANDARD_API)
    {
        PT_DWORD totalSize, partSize, currentSize, decompSize, dw;
        PT_BYTE *pBytes;
        PT_DATA *pImage;

        // check for initialization
        if (!apiInitialized)
            return PT_STATUS_API_NOT_INIT;

        // check validity of input parameters
        if (!hConnection || ppGrabbedData == NULL)
            return PT_STATUS_INVALID_PARAMETER;
            
        //if image data should be provided through asynchronous callbacks, handle them separately
        if (byGrabType == PT_GRAB_TYPE_508_508_8_SCAN508_508_8)
        {
#if defined(ENABLE_ASYNC_IMG_TRANSFER)         
            *ppGrabbedData = NULL;
                                 
            status = PTSleepThenGrabAsync(
            hConnection, 
            pfnCallback,
            pIdleCallbackCtx,
            byGrabType, 
            lTimeout, 
            boAssessImageQuality, 
            pdwWakeupCause,
            pdwGrabGuiMessage,
            GrabAsyncDefaultCallback,
            (void*)ppGrabbedData,
            pSignData,
            ppSignature);
#endif //#if defined(ENABLE_ASYNC_IMG_TRANSFER)
            goto endfunc;
        }

        // grab start
        status = PTSleepThenGrabPart(
            hConnection, 
            pfnCallback,
            pIdleCallbackCtx,
            byGrabType, 
            lTimeout, 
            boAssessImageQuality, 
            PT_TRUE,
            pdwWakeupCause,
            pdwGrabGuiMessage,
            &pBytes,
            &partSize,
            &totalSize,
            pSignData,
            ppSignature);

        if (status != PT_STATUS_OK)
            return status;

        if (*pdwGrabGuiMessage == PT_GUIMSG_GOOD_IMAGE)
        {
            // allocate buffer for output image
            if ((pImage = (PT_DATA*)gpfnMalloc(totalSize + sizeof(PT_DWORD))) == NULL)
            {
                gpfnFree(pBytes);
                return PT_STATUS_MALLOC_FAILED;
            }

            pImage->Length = totalSize;
            PT_memcpy(pImage->Data, pBytes, partSize);
            currentSize = partSize;
            gpfnFree(pBytes);

            while (currentSize < totalSize)
            {
                // get next part of the image
                status = PTSleepThenGrabPart(
                    hConnection, 
                    pfnCallback,
                    pIdleCallbackCtx,
                    byGrabType, 
                    lTimeout, 
                    boAssessImageQuality, 
                    PT_FALSE, 
                    &dw,
                    &dw,
                    &pBytes,
                    &partSize,
                    &totalSize,
                    pSignData,
                    ppSignature);

                if (status != PT_STATUS_OK)
                    return status;

                PT_memcpy(pImage->Data + currentSize, pBytes, partSize);
                currentSize += partSize;
                gpfnFree(pBytes);
            }

            // is image compressed ?
            if (PTDecompressImage(byGrabType, pImage->Length, pImage->Data, &decompSize, NULL) == PT_STATUS_OK)
            {
                PT_DATA *pDecompImage;

                // allocate buffer for decompressed image
                if ((pDecompImage = (PT_DATA*)gpfnMalloc(decompSize + sizeof(PT_DWORD))) == NULL)
                {
                    gpfnFree(pImage);
                    return PT_STATUS_MALLOC_FAILED;
                }

                pDecompImage->Length = decompSize;

                // decompress image
                PTDecompressImage(byGrabType, pImage->Length, pImage->Data, &decompSize, pDecompImage->Data);

                gpfnFree(pImage);
                pImage = pDecompImage;
            }

            *ppGrabbedData = pImage;        
        }
    }
endfunc:;
#endif    

    return status;
}
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTGrabWindowPart(
    IN PT_CONNECTION hConnection, 
    IN PT_BYTE byCompressionType,
    IN PT_LONG lTimeout,
    IN PT_BOOL boWaitForAcceptableFinger,
    IN PT_LONG lRows,
    IN PT_LONG lRowOffset,
    IN PT_LONG lRowDelta,
    IN PT_LONG lCols,
    IN PT_LONG lColOffset,
    IN PT_LONG lColDelta,
    IN PT_BOOL boStartGrab,
    OUT PT_BYTE **ppGrabbedData,
    OUT PT_DWORD *pdwPartSize,
    OUT PT_DWORD *pdwTotalSize,
    IN PT_DATA *pSignData,
    OUT PT_DATA **ppSignature)
{
    typedef struct request1_tag {
        CL_TAG tag;
        sint32 timeout;     // timeout
        uint8  type;        // grab type
        uint8  dummy;
    } Request1;

    typedef struct request2_tag {
        sint32 lRows;       // Number of rows
        sint32 lRowOffset;  // Row offset
        sint32 lRowDelta;   // Row subsampling factor
        sint32 lCols;       // Number of columns
        sint32 lColOffset;  // Column offset
        sint32 lColDelta;   // Column subsampling factor
        uint16 flags;       // Flags
        uint8  dummy;
    } Request2;
    
    typedef struct response_tag {
        CL_TAG tag;
        uint32 dataLen;     // size of returned data
    } Response;
    
    PT_STATUS status;
    uint32 sendSize, recvTotalSize, recvFragmentSize;
    Request2 request2;
    PT_SESSION *pS = &gSession;
    
    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature);
    
#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || ppGrabbedData == NULL || pdwPartSize == NULL)
        return PT_STATUS_INVALID_PARAMETER;
    
    if (boStartGrab)
    {
        // start grab, and receive first response fragment
        
        if (pdwTotalSize == NULL)
            return PT_STATUS_INVALID_PARAMETER;
        
        request2.lRows = HTOL32(lRows);
        request2.lRowOffset = HTOL32(lRowOffset);
        request2.lRowDelta = HTOL32(lRowDelta);
        request2.lCols = HTOL32(lCols);
        request2.lColOffset = HTOL32(lColOffset);
        request2.lColDelta = HTOL32(lColDelta);
        request2.flags = HTOL16(0);

        if (boWaitForAcceptableFinger) // wait for acceptable quality ?
        {
            request2.flags |= BG_WAIT_GOOD_FINGER;
        }
        
        // copy input data into communication buffer
        ((Request1*)pS->pBuf)->timeout = HTOL32(lTimeout);
        ((Request1*)pS->pBuf)->type = byCompressionType;
        PT_memcpy(pS->pBuf + offsetof(Request1, dummy), &request2, offsetof(Request2, dummy)); // add second part of request structure
        
        sendSize = offsetof(Request1, dummy) + offsetof(Request2, dummy);
        
        status = PT_SimpleTransactEx(pS, CL_COMMAND_BIO_GRAB_WINDOW, sendSize, &recvTotalSize, &recvFragmentSize, 0);
        
        if (status == PT_STATUS_OK)
        {
#ifdef _DEBUG
            if (recvFragmentSize < sizeof(Response) || recvFragmentSize > ((Response*)((PT_SESSION*)pS)->pBuf)->dataLen)
            {
                return PT_STATUS_WRONG_RESPONSE;
            }
#endif
            *ppGrabbedData = (PT_BYTE*)(((PT_SESSION*)pS)->pBuf + sizeof(Response));
            *pdwPartSize = recvFragmentSize - sizeof(Response);
            *pdwTotalSize = LTOH32(((Response*)((PT_SESSION*)pS)->pBuf)->dataLen);
        }
    }    
    else
    {
        // get next data fragments
        status = PT_GetNextFragment(pS, &recvFragmentSize);
        if (status == PT_STATUS_OK)
        {
            *ppGrabbedData = (PT_BYTE*)((PT_SESSION*)pS)->pBuf;
            *pdwPartSize = recvFragmentSize;
        }
    }
    
#if defined (TARGET_STANDARD_API)
    if (status == PT_STATUS_OK)
    {
        // allocate buffer for output parameter and copy data into it
        if ((*ppGrabbedData = (PT_BYTE*)MallocCopy(*pdwPartSize, *ppGrabbedData)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
    }
#endif
    
    return status;    
}
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTGrabWindow(
    IN PT_CONNECTION hConnection, 
    IN PT_BYTE byCompressionType,
    IN PT_LONG lTimeout,
    IN PT_BOOL boWaitForAcceptableFinger,
    IN PT_LONG lRows,
    IN PT_LONG lRowOffset,
    IN PT_LONG lRowDelta,
    IN PT_LONG lCols,
    IN PT_LONG lColOffset,
    IN PT_LONG lColDelta,
    OUT PT_DATA **ppGrabbedData, 
    IN PT_DATA *pSignData,
    OUT PT_DATA **ppSignature)
{
    PT_STATUS status = PT_STATUS_ACCESS_DENIED;

    UNREFERENCED_PARAMETER(hConnection);
    UNREFERENCED_PARAMETER(byCompressionType);
    UNREFERENCED_PARAMETER(lTimeout);
    UNREFERENCED_PARAMETER(boWaitForAcceptableFinger);
    UNREFERENCED_PARAMETER(lRows);
    UNREFERENCED_PARAMETER(lRowOffset);
    UNREFERENCED_PARAMETER(lRowDelta);
    UNREFERENCED_PARAMETER(lCols);
    UNREFERENCED_PARAMETER(lColOffset);
    UNREFERENCED_PARAMETER(lColDelta);
    UNREFERENCED_PARAMETER(ppGrabbedData);
    UNREFERENCED_PARAMETER(pSignData);
    UNREFERENCED_PARAMETER(ppSignature);
    
#if defined (TARGET_STANDARD_API)
    {
        PT_DWORD totalSize, partSize, currentSize;
        PT_BYTE *pBytes;
        PT_DATA *pImage;

        // check for initialization
        if (!apiInitialized)
            return PT_STATUS_API_NOT_INIT;

        // check validity of input parameters
        if (!hConnection || ppGrabbedData == NULL)
            return PT_STATUS_INVALID_PARAMETER;

        // grab start
        status = PTGrabWindowPart(
            hConnection, 
            byCompressionType, 
            lTimeout, 
            boWaitForAcceptableFinger, 
            lRows,
            lRowOffset,
            lRowDelta,
            lCols,
            lColOffset,
            lColDelta,
            PT_TRUE,
            &pBytes,
            &partSize,
            &totalSize,
            pSignData,
            ppSignature);

        if (status != PT_STATUS_OK)
            return status;

        // alocate buffer for output image
        if ((pImage = (PT_DATA*)gpfnMalloc(totalSize + sizeof(PT_DWORD))) == NULL)
        {
            gpfnFree(pBytes);
            return PT_STATUS_MALLOC_FAILED;
        }

        pImage->Length = totalSize;
        PT_memcpy(pImage->Data, pBytes, partSize);
        currentSize = partSize;
        gpfnFree(pBytes);

        while (currentSize < totalSize)
        {
            // get next part of the image
            status = PTGrabWindowPart(
                hConnection, 
                byCompressionType, 
                lTimeout, 
                boWaitForAcceptableFinger, 
                lRows,
                lRowOffset,
                lRowDelta,
                lCols,
                lColOffset,
                lColDelta,
                PT_FALSE, 
                &pBytes,
                &partSize,
                &totalSize,
                pSignData,
                ppSignature);
            
            if (status != PT_STATUS_OK)
                return status;

            PT_memcpy(pImage->Data + currentSize, pBytes, partSize);
            currentSize += partSize;
            gpfnFree(pBytes);
        }

        *ppGrabbedData = pImage;        

        status = PT_STATUS_OK;
    }

#endif    
    
    return status;
}
#endif //#if !defined(TBX)

PTAPI_DLL PT_STATUS PTAPI PTEnroll(
    IN PT_CONNECTION hConnection, 
    IN PT_BYTE byPurpose, 
    IN PT_INPUT_BIR *pStoredTemplate, 
    OUT PT_BIR **ppNewTemplate, 
    OUT PT_LONG *plSlotNr,
    IN PT_DATA *pPayload, 
    IN PT_LONG lTimeout, 
    OUT PT_BIR **ppAuditData, 
    IN PT_DATA *pSignData,
    OUT PT_DATA **ppSignature)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 timeout;             // Timeout for enroll operation
        uint8 purpose;              // Required purpose of created template  
        uint8 flagsLo;              // Low 8 bits of flags
        uint8 flagsHi;              // High 8 bits of flags
        uint8 padding;
        uint32 dataLen;             // OPTIONAL: length of payload data
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        sint32 slotNr;              // Slot number where a new template was stored
    } Response;
    
    PT_STATUS status;
    uint32 sendSize, recvSize, recvTotalSize;
    uint16 flags = 0;
    PT_SESSION *pS = &gSession;

    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (pStoredTemplate);
    UNREFERENCED_PARAMETER (ppAuditData);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }

    sendSize = offsetof(Request, padding);
    
    if (pPayload != NULL) // is payload supplied ?
    {
        flags |= BE_HAS_PAYLOAD;

        sendSize = sizeof(Request) + pPayload->Length;

        // check length of input data
        if (!PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
        {
            return PT_STATUS_DATA_TOO_LARGE;
        }

        // copy payload data into comm buffer
        ((Request*)pS->pBuf)->dataLen = HTOL32(pPayload->Length);
        PT_memmove(pS->pBuf + sizeof(Request), pPayload->Data, pPayload->Length);
    }

    if (plSlotNr != NULL) // store template into NVM ?
    {
        flags |= BE_STORE_TEMPLATE;
    }
    
    if (ppNewTemplate != NULL) // should be the new template returned ?
    {
        flags |= BE_RETURN_TEMPLATE;
    }        

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->timeout = HTOL32(lTimeout);
    ((Request*)pS->pBuf)->purpose = byPurpose;
    ((Request*)pS->pBuf)->flagsLo = (uint8)flags;
    ((Request*)pS->pBuf)->flagsHi = (uint8)(flags >> 8);

    // transact data
    status = PT_SimpleTransactEx(pS, CL_COMMAND_BIO_ENROLL, sendSize, &recvTotalSize, &recvSize, 0);
    
    // get remaining fragments, if needed
    if (status == PT_STATUS_OK)
    {
        status = PT_GetNextFragmentCombined(pS, &recvSize);
    }
        
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response) ||
            (ppNewTemplate != NULL && 
                recvSize < sizeof(Response) + 
                LTOH32(((PT_BIR*)(pS->pBuf + sizeof(Response)))->Header.Length)))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get returned data
        if (plSlotNr != NULL)
        {
            *plSlotNr = LTOH32(((Response*)pS->pBuf)->slotNr);
        }

        if (ppNewTemplate != NULL)
        {
            *ppNewTemplate = (PT_BIR*)(pS->pBuf + sizeof(Response));

#if defined (TARGET_STANDARD_API)
            // allocate buffer for output parameter and copy data into it
            if ((*ppNewTemplate = (PT_BIR*)MallocCopy(LTOH32((*ppNewTemplate)->Header.Length), *ppNewTemplate)) == NULL)
                status = PT_STATUS_MALLOC_FAILED;
#endif
           
        }
    }

    return status;
}    

PTAPI_DLL PT_STATUS PTAPI PTCapture(
    IN PT_CONNECTION hConnection, 
    IN PT_BYTE byPurpose, 
    OUT PT_BIR **ppCapturedTemplate, 
    IN PT_LONG lTimeout, 
    OUT PT_BIR **ppAuditData, 
    IN PT_DATA *pSignData,
    OUT PT_DATA **ppSignature)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 timeout;             // Timeout for enroll operation
        uint8 purpose;              // Required purpose of created template  
        uint8 flagsLo;              // Low 8 bits of flags
        uint8 flagsHi;              // High 8 bits of flags
        uint8 padding;
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
    } Response;        
    
    PT_STATUS status;
    uint32 sendSize, recvSize, recvTotalSize;
    uint16 flags = 0;
    PT_SESSION *pS = &gSession;

    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (ppAuditData);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection)
        return PT_STATUS_INVALID_PARAMETER;
    
    // check whether resulting template should be returned
    if (ppCapturedTemplate != NULL) 
    {
        flags |= BE_RETURN_TEMPLATE;
    }

    // check length of input data
    sendSize = offsetof(Request, padding);
    if (!PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->timeout = HTOL32(lTimeout);
    ((Request*)pS->pBuf)->purpose = byPurpose;
    ((Request*)pS->pBuf)->flagsLo = (uint8)flags;
    ((Request*)pS->pBuf)->flagsHi = (uint8)(flags >> 8);
    
    // transact data
    status = PT_SimpleTransactEx(pS, CL_COMMAND_BIO_CAPTURE, sendSize, &recvTotalSize, &recvSize, 0);

    // get remaining fragments, if needed
    if (status == PT_STATUS_OK)
    {
        status = PT_GetNextFragmentCombined(pS, &recvSize);
    }    
    
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (ppCapturedTemplate != NULL && recvSize < sizeof(Response) + LTOH32(((PT_BIR*)(pS->pBuf + sizeof(Response)))->Header.Length))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get returned data
        if (ppCapturedTemplate != NULL)
        {
            *ppCapturedTemplate = (PT_BIR*)(pS->pBuf + sizeof(Response));

#if defined (TARGET_STANDARD_API)
            // allocate buffer for output parameter and copy data into it
            if ((*ppCapturedTemplate = (PT_BIR*)MallocCopy(LTOH32((*ppCapturedTemplate)->Header.Length), *ppCapturedTemplate)) == NULL)
                status = PT_STATUS_MALLOC_FAILED;
#endif
            
        }
    }
    
    return status;
}

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTSleepThenCapture(
    IN PT_CONNECTION hConnection,
    IN PT_IDLE_CALLBACK pfnCallback,
    IN void *pIdleCallbackCtx,
    IN PT_BYTE byPurpose,
    IN PT_LONG lTimeout,
    OUT PT_DWORD *pdwWakeupCause,
    OUT PT_DWORD *pdwCaptureGuiMessage,
    OUT PT_BIR **ppCapturedTemplate, 
    OUT PT_BIR **ppAuditData,
    IN PT_DATA *pSignData,
    OUT PT_DATA **ppSignature)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 timeout;             // Timeout for enroll operation
        uint8 purpose;              // Required purpose of created template  
        uint8 flagsLo;              // Low 8 bits of flags
        uint8 flagsHi;              // High 8 bits of flags
        uint8 padding;
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        uint32 wakeupCause;
        uint32 guiMessage;
    } Response;        

    PT_STATUS status;
    uint32 sendSize, recvSize, recvTotalSize;
    uint16 flags = 0;
    PT_SESSION *pS = &gSession;

    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (ppAuditData);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection)
        return PT_STATUS_INVALID_PARAMETER;

    // check whether resulting template should be returned
    if (ppCapturedTemplate != NULL) 
    {
        flags |= BE_RETURN_TEMPLATE;
    }

    // check length of input data
    sendSize = offsetof(Request, padding);
    if (!PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }

    // set idle callback
    pS->pfnIdleCallback = pfnCallback;
    pS->pIdleCallbackCtx = pIdleCallbackCtx;

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->timeout = LTOH32(lTimeout);
    ((Request*)pS->pBuf)->purpose = byPurpose;
    ((Request*)pS->pBuf)->flagsLo = (uint8)flags;
    ((Request*)pS->pBuf)->flagsHi = (uint8)(flags >> 8);

    // transact data
    status = PT_SimpleTransactEx(pS, CL_COMMAND_BIO_SLEEP_THEN_CAPTURE, offsetof(Request, padding), &recvTotalSize, &recvSize, TRANSACT_FLAG_SLEEP);

    // get remaining fragments, if needed
    if (status == PT_STATUS_OK)
    {
        status = PT_GetNextFragmentCombined(pS, &recvSize);
    }

    pS->pfnIdleCallback = NULL;
    pS->pIdleCallbackCtx = NULL;

    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response) ||
            (ppCapturedTemplate != NULL) && 
            (LTOH32(((Response*)pS->pBuf)->wakeupCause) == PT_WAKEUP_CAUSE_FINGER) &&
            (LTOH32(((Response*)pS->pBuf)->guiMessage) == PT_GUIMSG_GOOD_IMAGE) &&
            (recvSize < sizeof(Response) + LTOH32(((PT_BIR*)(pS->pBuf + sizeof(Response)))->Header.Length)))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif

        *pdwWakeupCause = LTOH32(((Response*)pS->pBuf)->wakeupCause);
        *pdwCaptureGuiMessage = LTOH32(((Response*)pS->pBuf)->guiMessage);

        // get returned template
        if (ppCapturedTemplate != NULL && LTOH32(((Response*)pS->pBuf)->guiMessage) == PT_GUIMSG_GOOD_IMAGE)
        {
            *ppCapturedTemplate = (PT_BIR*)(pS->pBuf + sizeof(Response));

#if defined (TARGET_STANDARD_API)
            // allocate buffer for output parameter and copy data into it
            if ((*ppCapturedTemplate = (PT_BIR*)MallocCopy(LTOH32((*ppCapturedTemplate)->Header.Length), *ppCapturedTemplate)) == NULL)
                status = PT_STATUS_MALLOC_FAILED;
#endif
        }
    } 

    return status;
}
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTVerifyMatch(
    IN PT_CONNECTION hConnection, 
    IN PT_LONG *plMaxFARRequested, 
    IN PT_LONG *plMaxFRRRequested, 
    IN PT_BOOL *pboFARPrecedence, 
    IN PT_INPUT_BIR *pCapturedTemplate,
    IN PT_INPUT_BIR *pStoredTemplate, 
    OUT PT_BIR **ppAdaptedTemplate,
    OUT PT_BOOL *pboResult, 
    OUT PT_LONG *plFARAchieved, 
    OUT PT_LONG *plFRRAchieved, 
    OUT PT_DATA **ppPayload)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 maxFAR;              // Maximum FAR requested
        uint32 maxFRR;              // Maximum FRR requested
        uint32 precedenceFAR;       // Non-zero FAR has higher priority
        uint16 flags;               // Flags
        uint16 padding;
        //uint32 birForm;           // BIR form of stored template (aligned to DWORD) - transfered with a template
    } Request;
    
    typedef struct response_tag {
        CL_TAG tag;
        PT_BOOL result;             // Result of verification
        PT_LONG achievedFAR;        // Achieved FAR. 
        PT_LONG achievedFRR;        // Achieved FRR.
    } Response;
    
    PT_STATUS status;
    uint32 recvSize=0, templateTransferSize;
    uint8 numberOfTemplates=1;
    uint16 flags = 0;
    PT_SESSION *pS = &gSession;
    PT_INPUT_BIR pBirs[2] = {0};

    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (plMaxFARRequested);
    UNREFERENCED_PARAMETER (plMaxFRRRequested);
    UNREFERENCED_PARAMETER (pboFARPrecedence);
    UNREFERENCED_PARAMETER (ppAdaptedTemplate);
    UNREFERENCED_PARAMETER (plFARAchieved);
    UNREFERENCED_PARAMETER (plFRRAchieved);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pStoredTemplate == NULL || pboResult == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }
        
    // copy pStoredTemplate to pBirs[0] (BIR_FORM and pointer to data)
    PT_memcpy(&(pBirs[0]), pStoredTemplate, sizeof(PT_INPUT_BIR));

    // compute size of the second transfered template and its BIR_FORM
    if (pCapturedTemplate != NULL) // is captured template supplied ?
    {    
        flags |= BV_HAS_CAPTURED_TEMPLATE;
        
        numberOfTemplates = 2;
       
        // copy pCapturedTemplate to pBirs[1]
        PT_memcpy(&(pBirs[1]), pCapturedTemplate, sizeof(PT_INPUT_BIR));
    }        
    
    // check whether payload should be returned
    if (ppPayload != NULL) {
        flags |= BV_RETURN_PAYLOAD;
        *ppPayload = (PT_DATA *)NULL;
    }

    // clear header of the first packet in communication buffer
    PT_memset(pS->pBuf, 0, sizeof(Request));

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->flags = HTOL16(flags);
    
    // calculate size of transfered templates
    status = PT_CalculateTemplateTransferSize(pBirs, numberOfTemplates, &templateTransferSize);

    // transact data
    if(status == PT_STATUS_OK)
    {
        status = PT_FirstTransact(pS, CL_COMMAND_BIO_VERIFY_MATCH, sizeof(Request), sizeof(Request) + templateTransferSize, 0);
    }

    if (status == PT_STATUS_OK)
    {
        // send all templates 
        status = PT_TransferTemplates(pS, pBirs, numberOfTemplates, &recvSize);
    }
     
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < offsetof(Response, achievedFAR) ||
            LTOH32(((Response*)(pS->pBuf))->result) && recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get returned data
        *pboResult = LTOH32(((Response*)pS->pBuf)->result);

        if (ppPayload != NULL && *pboResult)
        {
            *ppPayload = (PT_DATA*)(pS->pBuf + sizeof(Response));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE            
            (*ppPayload)->Length = LTOH32((*ppPayload)->Length);
#endif

#if defined (TARGET_STANDARD_API)
            // allocate buffer for output parameter and copy data into it
            if ((*ppPayload = (PT_DATA*)MallocCopy((*ppPayload)->Length + sizeof(PT_DATA), *ppPayload)) == NULL)
                status = PT_STATUS_MALLOC_FAILED;
#endif
            
        }
    }
    
    return status;
}
#endif //#if !defined(TBX)

PTAPI_DLL PT_STATUS PTAPI PTVerify(
    IN PT_CONNECTION hConnection, 
    IN PT_LONG *plMaxFARRequested, 
    IN PT_LONG *plMaxFRRRequested, 
    IN PT_BOOL *pboFARPrecedence, 
    IN PT_INPUT_BIR *pStoredTemplate, 
    OUT PT_BIR **ppAdaptedTemplate,
    OUT PT_BOOL *pboResult, 
    OUT PT_LONG *plFARAchieved, 
    OUT PT_LONG *plFRRAchieved, 
    OUT PT_DATA **ppPayload, 
    IN PT_LONG lTimeout, 
    IN PT_BOOL boCapture,
    OUT PT_BIR **ppAuditData, 
    IN PT_DATA *pSignData,
    OUT PT_DATA **ppSignature) 
{
    typedef struct request_tag {
        CL_TAG  tag;
        uint32 maxFAR;              // Maximum FAR requested
        uint32 maxFRR;              // Maximum FRR requested
        uint32 precedenceFAR;       // Non-zero FAR has higher priority
        uint32 timeout;             // Timeout for the operation
        uint16 flags;               // Flags
        uint16 padding;
        //uint32 birForm;           // Form of BIR - part of template transfer
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        PT_BOOL result;             // Result of verification
        PT_LONG achievedFAR;        // Achieved FAR. 
        PT_LONG achievedFRR;        // Achieved FRR.
    } Response;

    PT_STATUS status;
    uint32 recvSize = 0, templateTransferSize;
    uint16 flags = 0;
    PT_SESSION *pS = &gSession;

    UNREFERENCED_PARAMETER (hConnection);    
    UNREFERENCED_PARAMETER (plMaxFARRequested);
    UNREFERENCED_PARAMETER (plMaxFRRRequested);
    UNREFERENCED_PARAMETER (pboFARPrecedence);
    UNREFERENCED_PARAMETER (ppAdaptedTemplate);
    UNREFERENCED_PARAMETER (plFARAchieved);
    UNREFERENCED_PARAMETER (plFRRAchieved);
    UNREFERENCED_PARAMETER (ppAuditData);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature); 

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pStoredTemplate == NULL || pboResult == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }
    
    // check whether payload should be returned
    if (ppPayload != NULL) {
        flags |= BV_RETURN_PAYLOAD;
        *ppPayload = (PT_DATA *)NULL;
    }

    // check whether template has to be captured
    if (boCapture)
    {
        flags |= BV_CAPTURE_FINGERPRINT;
    }
  
    // clear header of packet in communication buffer
    PT_memset(pS->pBuf, 0, sizeof(Request));
    
    // copy input data into communication buffer
    ((Request*)pS->pBuf)->timeout = HTOL32(lTimeout);
    ((Request*)pS->pBuf)->flags = HTOL16(flags);
    
    // calculate size of transfered templates
    status = PT_CalculateTemplateTransferSize(pStoredTemplate, 1, &templateTransferSize);

    // transact data
    if(status == PT_STATUS_OK)
    {
        status = PT_FirstTransact(pS, CL_COMMAND_BIO_VERIFY, sizeof(Request), sizeof(Request) + templateTransferSize, 0);
    }

    if (status == PT_STATUS_OK)
    {
        // send template
        status = PT_TransferTemplates(pS, pStoredTemplate, 1, &recvSize);
    }

    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < offsetof(Response, achievedFAR) ||
            LTOH32(((Response*)(pS->pBuf))->result) && recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get returned data
        *pboResult = LTOH32(((Response*)pS->pBuf)->result);
        
        if (ppPayload != NULL && *pboResult)
        {
            *ppPayload = (PT_DATA*)(pS->pBuf + sizeof(Response));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE            
            (*ppPayload)->Length = LTOH32((*ppPayload)->Length);
#endif

#if defined (TARGET_STANDARD_API)
            // allocate buffer for output parameter and copy data into it
            if ((*ppPayload = (PT_DATA*)MallocCopy((*ppPayload)->Length + sizeof(PT_DATA), *ppPayload)) == NULL)
                status = PT_STATUS_MALLOC_FAILED;
#endif
            
        }
    }
    
    return status;
}

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTVerifyEx(
    IN PT_CONNECTION hConnection, 
    IN PT_LONG *plMaxFARRequested, 
    IN PT_LONG *plMaxFRRRequested, 
    IN PT_BOOL *pboFARPrecedence, 
    IN PT_INPUT_BIR *pStoredTemplates, 
    IN PT_BYTE byNrTemplates, 
    OUT PT_BIR **ppAdaptedTemplate,
    OUT PT_SHORT *pshResult, 
    OUT PT_LONG *plFARAchieved, 
    OUT PT_LONG *plFRRAchieved, 
    OUT PT_DATA **ppPayload, 
    IN PT_LONG lTimeout, 
    IN PT_BOOL boCapture,
    OUT PT_BIR **ppAuditData, 
    IN PT_DATA *pSignData,
    OUT PT_DATA **ppSignature) 
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 maxFAR;              // Maximum FAR requested
        uint32 maxFRR;              // Maximum FRR requested
        uint32 precedenceFAR;       // Non-zero FAR has higher priority
        uint32 timeout;             // Timeout for the operation
        uint8  nrTemplates;         // Number of supplied templates
        uint8  flagsLo;             // Low 8 bits of flags
        uint8  flagsHi;             // High 8 bits of flags
        uint8  padding;
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        PT_LONG resultIndex;        // Result of verification
        PT_LONG achievedFAR;        // Achieved FAR. 
        PT_LONG achievedFRR;        // Achieved FRR.
    } Response;

    PT_STATUS status;
    uint32 recvSize=0, templateTransferSize;
    uint16 flags = 0;
    PT_SESSION *pS = &gSession;
 
    UNREFERENCED_PARAMETER (hConnection);   
    UNREFERENCED_PARAMETER (plMaxFARRequested);
    UNREFERENCED_PARAMETER (plMaxFRRRequested);
    UNREFERENCED_PARAMETER (pboFARPrecedence);
    UNREFERENCED_PARAMETER (ppAdaptedTemplate);
    UNREFERENCED_PARAMETER (plFARAchieved);
    UNREFERENCED_PARAMETER (plFRRAchieved);
    UNREFERENCED_PARAMETER (ppAuditData);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature); 

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pStoredTemplates == NULL || pshResult == NULL || byNrTemplates == 0)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }
    
    // check whether payload should be returned
    if (ppPayload != NULL) {
        flags |= BV_RETURN_PAYLOAD;
        *ppPayload = (PT_DATA *)NULL;
    }
    
    // check whether template has to be captured
    if (boCapture)
    {
        flags |= BV_CAPTURE_FINGERPRINT;
    }
    
    // clear header of packet in communication buffer
    PT_memset(pS->pBuf, 0, sizeof(Request));
    
    // copy input data into communication buffer
    ((Request*)pS->pBuf)->timeout = HTOL32(lTimeout);
    ((Request*)pS->pBuf)->nrTemplates = byNrTemplates;
    ((Request*)pS->pBuf)->flagsLo = (uint8)flags;
    ((Request*)pS->pBuf)->flagsHi = (uint8)(flags >> 8);
    
    // calculate size of transfered templates
    status = PT_CalculateTemplateTransferSize(pStoredTemplates, byNrTemplates, &templateTransferSize);

    // transact data
    if(status == PT_STATUS_OK)
    {
        status = PT_FirstTransact(pS, CL_COMMAND_BIO_VERIFYEX, sizeof(Request), sizeof(Request) + templateTransferSize, 0);
    }

    if (status == PT_STATUS_OK)
    {
        // send all templates 
        status = PT_TransferTemplates(pS, pStoredTemplates, byNrTemplates, &recvSize);
    }

    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < offsetof(Response, achievedFAR) ||
            (LTOH32(((Response*)pS->pBuf)->resultIndex)) != -1 && recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get returned data
        *pshResult = (PT_SHORT)LTOH32(((Response*)pS->pBuf)->resultIndex);
        
        if (ppPayload != NULL && *pshResult != -1)
        {
            *ppPayload = (PT_DATA*)(pS->pBuf + sizeof(Response));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
            (*ppPayload)->Length = LTOH32((*ppPayload)->Length);
#endif

#if defined (TARGET_STANDARD_API)
            // allocate buffer for output parameter and copy data into it
            if ((*ppPayload = (PT_DATA*)MallocCopy((*ppPayload)->Length + sizeof(PT_DATA), *ppPayload)) == NULL)
                status = PT_STATUS_MALLOC_FAILED;
#endif
            
        }
    }
    
    return status;
}
#endif //#if !defined(TBX)

PTAPI_DLL PT_STATUS PTAPI PTVerifyAll(
    IN PT_CONNECTION hConnection, 
    IN PT_LONG *plMaxFARRequested, 
    IN PT_LONG *plMaxFRRRequested, 
    IN PT_BOOL *pboFARPrecedence, 
    OUT PT_BIR **ppAdaptedTemplate,
    OUT PT_LONG *plResult, 
    OUT PT_LONG *plFARAchieved, 
    OUT PT_LONG *plFRRAchieved, 
    OUT PT_DATA **ppPayload, 
    IN PT_LONG lTimeout, 
    IN PT_BOOL boCapture,
    OUT PT_BIR **ppAuditData, 
    IN PT_DATA *pSignData,
    OUT PT_DATA **ppSignature) 
{
    typedef struct request_tag {
        CL_TAG  tag;
        uint32 maxFAR;              // Maximum FAR requested
        uint32 maxFRR;              // Maximum FRR requested
        uint32 precedenceFAR;       // Non-zero FAR has higher priority
        uint32 timeout;             // Timeout for the operation
        uint16 flags;               // Flags
    } Request;
    
    typedef struct response_tag {
        CL_TAG tag;
        PT_LONG resultSlot;         // Number of slot with matching template 
        PT_LONG achievedFAR;        // Achieved FAR. 
        PT_LONG achievedFRR;        // Achieved FRR.
    } Response;
    
    PT_STATUS status;
    uint32 sendSize, recvSize;
    uint16 flags = 0;
    PT_SESSION *pS = &gSession;

    UNREFERENCED_PARAMETER (hConnection);
    UNREFERENCED_PARAMETER (plMaxFARRequested);
    UNREFERENCED_PARAMETER (plMaxFRRRequested);
    UNREFERENCED_PARAMETER (pboFARPrecedence);
    UNREFERENCED_PARAMETER (ppAdaptedTemplate);
    UNREFERENCED_PARAMETER (plFARAchieved);
    UNREFERENCED_PARAMETER (plFRRAchieved);
    UNREFERENCED_PARAMETER (ppAuditData);
    UNREFERENCED_PARAMETER (pSignData);
    UNREFERENCED_PARAMETER (ppSignature);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || plResult == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }

    sendSize = sizeof(CL_TAG) + 4*sizeof(uint32) + sizeof(uint16);
    
    // check whether payload shoud be returned
    if (ppPayload != NULL) {
        flags |= BV_RETURN_PAYLOAD;
        *ppPayload = (PT_DATA *)NULL;
    }
    
    // check whether template has to be captured
    if (boCapture)
    {
        flags |= BV_CAPTURE_FINGERPRINT;
    }
   
    // clear header of packet in communication buffer
    PT_memset(pS->pBuf, 0, sizeof(Request));
    
    // copy input data into communication buffer
    ((Request*)pS->pBuf)->timeout = HTOL32(lTimeout);
    ((Request*)pS->pBuf)->flags = HTOL16(flags);
    
    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_BIO_VERIFY_ALL, sendSize, &recvSize, 0);
    
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < offsetof(Response, achievedFAR) ||
            LTOH32(((Response*)(pS->pBuf))->resultSlot) != -1 && recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get returned data
        *plResult = LTOH32(((Response*)pS->pBuf)->resultSlot);
        
        if (ppPayload != NULL && *plResult != -1)
        {
            *ppPayload = (PT_DATA*)(pS->pBuf + sizeof(Response));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
            (*ppPayload)->Length = LTOH32((*ppPayload)->Length);
#endif

#if defined (TARGET_STANDARD_API)
            // allocate buffer for output parameter and copy data into it
            if ((*ppPayload = (PT_DATA*)MallocCopy((*ppPayload)->Length + sizeof(PT_DATA), *ppPayload)) == NULL)
                status = PT_STATUS_MALLOC_FAILED;
#endif
            
        }
    }
    
    return status;
}
        
#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTDetectFingerEx(
    IN PT_CONNECTION hConnection, 
    IN PT_LONG lTimeout,
    IN PT_DWORD dwFlags)
{
    typedef struct request_tag {
        CL_TAG  tag;
        sint32  timeout;            // Timeout for the operation
        uint32  flags;              // Flags
    } Request;
    
    PT_SESSION *pS = &gSession;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->timeout = HTOL32(lTimeout);
    ((Request*)pS->pBuf)->flags = HTOL32(dwFlags);
    
    // transact data
    return PT_SimpleTransact(pS, CL_COMMAND_BIO_DETECT_FINGEREX, sizeof(Request), (uint32 *)NULL, 0);
}    
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTStoreFinger(
    IN PT_CONNECTION hConnection,
    IN PT_INPUT_BIR *pTemplate, 
    IN PT_LONG *plSlotNr) 
{
    typedef struct request_tag {
        CL_TAG tag;
        uint16 flags;       // Flags
        uint16 padding;
        //uint32 birForm;     // BIR form (alligned to DWORD) - transfered with template
    } Request;
    
    typedef struct response_tag {
        CL_TAG tag;
        sint32 slotNr;      // Slot number where template was stored
    } Response;
    
    PT_STATUS status;
    uint32 sendSize, recvSize = 0, templateTransferSize;
    PT_SESSION *pS = &gSession;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || plSlotNr == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }

    // check whether template is supplied
    if (pTemplate == NULL) 
    {
        // no template is supplied, send only flags
        sendSize = offsetof(Request, padding);
        ((Request*)pS->pBuf)->flags = HTOL16(0);
    }
    else
    {
        // template is supplied
        ((Request*)pS->pBuf)->flags = HTOL16(BS_HAS_TEMPLATE);
        
        sendSize = sizeof(Request);
    }
    
    // calculate size of transfered templates
    status = PT_CalculateTemplateTransferSize(pTemplate, 1, &templateTransferSize);

    // transact data
    if(status == PT_STATUS_OK)
    {
        status = PT_FirstTransact(pS, CL_COMMAND_BIO_STORE_FINGER, sendSize, sendSize + templateTransferSize, 0);
    }
        
    if (status == PT_STATUS_OK)
    {
        // send template
        status = PT_TransferTemplates(pS, pTemplate, 1, &recvSize);
    }
    
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get returned data
        *plSlotNr = LTOH32(((Response*)pS->pBuf)->slotNr);
    }
    
    return status;
}
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTDeleteFinger(
    IN PT_CONNECTION hConnection,
    IN PT_LONG lSlotNr)
{
    typedef struct request_tag {
        CL_TAG tag;
        sint32 slotNr;              // Slot number
        uint16 flags;               // Flags 
    } Request;
    
    PT_SESSION *pS = &gSession;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection) return PT_STATUS_INVALID_PARAMETER;

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->slotNr = HTOL32(lSlotNr);
    ((Request*)pS->pBuf)->flags = HTOL16(0);

    // transact data
    return PT_SimpleTransact(pS, CL_COMMAND_BIO_DELETE_FINGER, sizeof(CL_TAG) + sizeof(sint32) + sizeof(uint16), (uint32 *)NULL, 0);
}    
#endif //#if !defined(TBX)

PTAPI_DLL PT_STATUS PTAPI PTDeleteAllFingers(IN PT_CONNECTION hConnection)
{
    PT_SESSION *pS = &gSession;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection) return PT_STATUS_INVALID_PARAMETER;

    // transact data
    return PT_SimpleTransact(pS, CL_COMMAND_BIO_DELETE_ALL_FINGERS, sizeof(CL_TAG), (uint32 *)NULL, 0);
}

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTSetFingerData(
    IN PT_CONNECTION hConnection,
    IN PT_LONG lSlotNr,
    OUT PT_DATA *pFingerData)
{
    typedef struct request_tag {
        CL_TAG tag;
        sint32 slotNr;      // slot number
        uint32 dataLen;     // size of data
    } Request;

    uint32 sendSize;
    PT_SESSION *pS = &gSession;    
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pFingerData == NULL)
        return PT_STATUS_INVALID_PARAMETER;
    
    // check length of input data
    sendSize = sizeof(Request) + pFingerData->Length;
    if (!PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }
    
    // copy input data into communication buffer
    PT_memmove(pS->pBuf + sizeof(Request), pFingerData->Data, pFingerData->Length);
    ((Request*)pS->pBuf)->slotNr = HTOL32(lSlotNr);
    ((Request*)pS->pBuf)->dataLen = HTOL32(pFingerData->Length);
    
    // transact data
    return PT_SimpleTransact(pS, CL_COMMAND_BIO_SET_FINGER_DATA, sendSize, (uint32 *)NULL, 0);
}
#endif //#if !defined(TBX)

PTAPI_DLL PT_STATUS PTAPI PTGetFingerData(
    IN PT_CONNECTION hConnection,
    IN PT_LONG lSlotNr,
    OUT PT_DATA **ppFingerData)
{
    typedef struct request_tag {
        CL_TAG tag;
        sint32 slotNr;      // slot number
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        uint32 dataLen;     // size of returned data
    } Response;

    PT_STATUS status;
    uint32 recvSize;
    PT_SESSION *pS = &gSession;    
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || ppFingerData == NULL)
        return PT_STATUS_INVALID_PARAMETER;
    
    // copy input data into communication buffer
    ((Request*)pS->pBuf)->slotNr = HTOL32(lSlotNr);
    
    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_BIO_GET_FINGER_DATA, sizeof(Request), &recvSize, 0);

    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response) ||
            recvSize < sizeof(Response) + LTOH32(((Response*)pS->pBuf)->dataLen))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif

        // get returned data
        *ppFingerData = (PT_DATA*)(pS->pBuf + offsetof(Response, dataLen));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
        (*ppFingerData)->Length = LTOH32((*ppFingerData)->Length);
#endif

#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        if ((*ppFingerData = (PT_DATA*)MallocCopy((*ppFingerData)->Length + sizeof(PT_DATA), *ppFingerData)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
#endif
        
    }

    return status;
}


PTAPI_DLL PT_STATUS PTAPI PTListAllFingers(
    IN PT_CONNECTION hConnection,
    OUT PT_FINGER_LIST **ppFingerList)
{
#ifdef _DEBUG
    typedef struct response_tag {
        CL_TAG tag;
        uint32 numFingers;
    } Response;
#endif

    PT_STATUS status;
    uint32 recvSize, recvTotalSize;
    PT_SESSION *pS = &gSession;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || ppFingerList == NULL)
        return PT_STATUS_INVALID_PARAMETER;
    
    // transact data
    status = PT_SimpleTransactEx(pS, CL_COMMAND_BIO_LIST_ALL_FINGERS, sizeof(CL_TAG), &recvTotalSize, &recvSize, 0);

    // get remaining fragments, if needed
    // NOTE that frame buffer isn't most likely sufficient to hold all data, so they will be cropped.
    if (status == PT_STATUS_OK)
    {
        status = PT_GetNextFragmentCombined(pS, &recvSize);
    }
    
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get returned data
        *ppFingerList = (PT_FINGER_LIST*)(pS->pBuf + sizeof(CL_TAG));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
        // convert multiple bytes fields to native endian
        (*ppFingerList)->NumFingers = LTOH32((*ppFingerList)->NumFingers);
#endif            

#if defined (TARGET_STANDARD_API)
        {
            typedef struct list_item_tag {
                PT_LONG     SlotNr;                                // Number of slot, where is the finger stored
                PT_DWORD    FingerDataLength;                      // Length of data associated with the finger
                PT_BYTE     FingerData[PT_MAX_FINGER_DATA_LENGTH]; // Data associated with the finger
            } ListItem;

            PT_FINGER_LIST *pFL;
            uint32 size, i, dataLen;
            uint8 *pPos;

            // compute size of PT_FINGER_LIST structure
            size = sizeof(PT_DWORD) + (*ppFingerList)->NumFingers*(sizeof(ListItem));
            if ((pFL = (PT_FINGER_LIST*)gpfnMalloc(size)) == NULL)
            {
                status = PT_STATUS_MALLOC_FAILED;
            }
            else
            {
                PT_memset(pFL, 0, size);

                // copy finger data into list
                pFL->NumFingers = (*ppFingerList)->NumFingers;
                
                pPos = (uint8*)(*ppFingerList)->List;
                for (i = 0; i < (*ppFingerList)->NumFingers; i++)
                {
                    PT_memcpy(&dataLen, pPos + offsetof(ListItem, FingerDataLength), sizeof(dataLen)); // load finger data length
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
                    dataLen = LTOH32(dataLen);
#endif

                    size = offsetof(ListItem, FingerData) + dataLen; // length of one item

                    PT_memcpy(&pFL->List[i], pPos, size);

#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
                    pFL->List[i].FingerDataLength = LTOH32(pFL->List[i].FingerDataLength);
                    pFL->List[i].SlotNr = LTOH32(pFL->List[i].SlotNr);
#endif
                    pPos += size;
                }

                *ppFingerList = pFL;
            }
        }
#endif

    }
    
    return status;
}
    

PTAPI_DLL PT_STATUS PTAPI PTLoadFinger(
    IN PT_CONNECTION hConnection,
    IN PT_LONG lSlotNr,
    IN PT_BOOL boReturnPayload,
    OUT PT_BIR **ppStoredTemplate)
{
    typedef struct request {
        CL_TAG tag;
        sint32 slotNr;  // slot number
        uint16 flags;   // flags
        uint8 padding;
    } Request;

    typedef struct response {
        CL_TAG tag;
        PT_BIR_HEADER header;
    } Response;

    PT_STATUS status;
    PT_SESSION *pS = &gSession;
    uint32 recvSize, recvTotalSize;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || ppStoredTemplate == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }

    ((Request*)pS->pBuf)->slotNr = HTOL32(lSlotNr);
    ((Request*)pS->pBuf)->flags = HTOL16(boReturnPayload ? BLF_RETURN_PAYLOAD : 0);

    // transact data
    status = PT_SimpleTransactEx(pS, CL_COMMAND_BIO_LOAD_FINGER, offsetof(Request, padding), &recvTotalSize, &recvSize, 0);

    // get remaining fragments, if needed
    if (status == PT_STATUS_OK)
    {
        status = PT_GetNextFragmentCombined(pS, &recvSize);
    } 

    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response) || recvSize < LTOH32(((Response*)pS->pBuf)->header.Length))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif

        // get returned data
        *ppStoredTemplate = (PT_BIR*)&((Response*)pS->pBuf)->header;

#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        if ((*ppStoredTemplate = (PT_BIR*)MallocCopy(LTOH32((*ppStoredTemplate)->Header.Length), *ppStoredTemplate)) == NULL)
            return PT_STATUS_MALLOC_FAILED;
#endif
    }

    return status;
}

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTSetFingerPayload(
    IN PT_CONNECTION hConnection,
    IN PT_LONG lSlotNr,
    IN PT_DATA *pPayload)
{
    typedef struct request {
        CL_TAG tag;
        sint32 slotNr;  // slot number
    } Request;

    PT_SESSION *pS = &gSession;
    uint32 sendSize, recvSize;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || pPayload == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }

    // check length of input data
    sendSize = sizeof(Request) + sizeof(PT_DWORD) + pPayload->Length;

    if (!PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->slotNr = HTOL32(lSlotNr);
    *(PT_DWORD *)(pS->pBuf + sizeof(Request)) = HTOL32(pPayload->Length);
    PT_memmove(pS->pBuf + sizeof(Request) + sizeof(PT_DWORD), pPayload->Data, pPayload->Length);

    // transact data
    return PT_SimpleTransact(pS, CL_COMMAND_BIO_SET_FINGER_PAYLOAD, sendSize, &recvSize, 0);
}
#endif //#if !defined(TBX)

PTAPI_DLL PT_STATUS PTAPI PTUpdateFingerPayload(
    IN PT_CONNECTION hConnection,
    IN PT_LONG lSlotNr,
    IN PT_LONG lOffset,
    IN PT_DATA *pPayloadData)
{
    typedef struct request {
        CL_TAG tag;
        sint32 slotNr;  // slot number
        sint32 offset;  // offset
    } Request;

    PT_SESSION *pS = &gSession;
    uint32 sendSize, recvSize;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || pPayloadData == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }

    // check length of input data
    sendSize = sizeof(Request) + sizeof(PT_DWORD) + pPayloadData->Length;

    if (!PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->slotNr = HTOL32(lSlotNr);
    ((Request*)pS->pBuf)->offset = HTOL32(lOffset);
    *(PT_DWORD *)(pS->pBuf + sizeof(Request)) = HTOL32(pPayloadData->Length);
    PT_memmove(pS->pBuf + sizeof(Request) + sizeof(PT_DWORD), pPayloadData->Data, pPayloadData->Length);    

    // transact data
    return PT_SimpleTransact(pS, CL_COMMAND_BIO_UPDATE_FINGER_PAYLOAD, sendSize, &recvSize, 0);
}

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTScanQuality (
    IN PT_CONNECTION hConnection, 
    OUT PT_DWORD *pdwScanQuality)
{
    typedef struct response_tag {
        CL_TAG tag;
        uint32 scanQuality;     // scan quality of last finger swipe
    } Response;

    PT_STATUS status;
    uint32 recvSize;
    PT_SESSION *pS = &gSession;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pdwScanQuality == NULL)
        return PT_STATUS_INVALID_PARAMETER;
    
    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_BIO_SCAN_QUALITY, sizeof(CL_TAG), &recvSize, 0);
    
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif

        // get returned data
        *pdwScanQuality = LTOH32(((Response*)pS->pBuf)->scanQuality);
    }
    
    return status;
}
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTGetSwipeInfo(
    IN PT_CONNECTION hConnection,
    OUT PT_SWIPE_INFO **ppSwipeInfo,
    OUT PT_DATA **ppSkewInfo,
    OUT PT_DATA **ppSpeedInfo
)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 flags;
    } Request;

    typedef struct reponse_tag {
        CL_TAG tag;
        PT_SWIPE_INFO info;
    } Response;

    PT_STATUS status;
    PT_SESSION *pS = &gSession;
    uint32 recvSize, remaining, length;
    uint32 flags = 0;
    uint8 *pPos;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || ppSwipeInfo == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }

    if (ppSkewInfo != NULL) // return skew info ?
    {
        flags |= BSQ_RETURN_SKEW_INFO;
    }

    if (ppSpeedInfo != NULL) // return speed info ?
    {
        flags |= BSQ_RETURN_SPEED_INFO;
    }

    ((Request*)pS->pBuf)->flags = HTOL32(flags);

    // There is no request block - simply do transact
    status = PT_SimpleTransact(pS, CL_COMMAND_BIO_GET_SWIPE_INFO, sizeof(Request), &recvSize, 0);
    
    // get remaining fragments, if needed (SwipeInfo and SkewInfo are together bigger that SPI communication frame)
    if (status == PT_STATUS_OK)
    {
        status = PT_GetNextFragmentCombined(pS, &recvSize);
    }

    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif

        // get returned data
        *ppSwipeInfo = &((Response*)pS->pBuf)->info;
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
        //covert multipe bytes fields to native endian
        (*ppSwipeInfo)->RealHeight = LTOH16((*ppSwipeInfo)->RealHeight);
        (*ppSwipeInfo)->BackgroundColor = LTOH16((*ppSwipeInfo)->BackgroundColor);
        (*ppSwipeInfo)->ROIPos = LTOH16((*ppSwipeInfo)->ROIPos);
        (*ppSwipeInfo)->ROILength = LTOH16((*ppSwipeInfo)->ROILength);
        (*ppSwipeInfo)->RecErrorStates = LTOH16((*ppSwipeInfo)->RecErrorStates);
        (*ppSwipeInfo)->QualityMessage = LTOH16((*ppSwipeInfo)->QualityMessage);
#endif

#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        if ((*ppSwipeInfo = (PT_SWIPE_INFO*)MallocCopy(sizeof(**ppSwipeInfo), *ppSwipeInfo)) == NULL)
            return PT_STATUS_MALLOC_FAILED;
#endif

        pPos = pS->pBuf + sizeof(Response);
        remaining = recvSize - sizeof(Response);
        if (ppSkewInfo != NULL) // return skew info ?
        {
            length = LTOH32(*(PT_DWORD*)pPos) + sizeof(PT_DWORD);
#ifdef _DEBUG
            if (remaining < sizeof(PT_DWORD) || remaining < length)
            {
                return PT_STATUS_WRONG_RESPONSE;
            }
#endif
            *ppSkewInfo = (PT_DATA*)pPos;
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
            (*ppSkewInfo)->Length = LTOH32((*ppSkewInfo)->Length);
#endif

#if defined (TARGET_STANDARD_API)
            // allocate buffer for output parameter and copy data into it
            if ((*ppSkewInfo = (PT_DATA*)MallocCopy(length, *ppSkewInfo)) == NULL)
                return PT_STATUS_MALLOC_FAILED;
#endif
            pPos = pPos + length;
            remaining = remaining - length;
        }

        if (ppSpeedInfo != NULL) // return speed info ?
        {
            length = LTOH32(*(PT_DWORD*)pPos) + sizeof(PT_DWORD);
#ifdef _DEBUG
            if (remaining < sizeof(PT_DWORD) || remaining < LTOH32(*(PT_DWORD*)pPos) + sizeof(PT_DWORD))
            {
                return PT_STATUS_WRONG_RESPONSE;
            }
#endif
            *ppSpeedInfo = (PT_DATA*)pPos;
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
            (*ppSpeedInfo)->Length = LTOH32((*ppSpeedInfo)->Length);
#endif

#if defined (TARGET_STANDARD_API)
            // allocate buffer for output parameter and copy data into it
            if ((*ppSpeedInfo = (PT_DATA*)MallocCopy(length, *ppSpeedInfo)) == NULL)
                return PT_STATUS_MALLOC_FAILED;
#endif
        }
    }

    return status;
}
#endif //#if !defined(TBX)

#if !defined(TBX)
PTAPI_DLL PT_STATUS PTAPI PTNavigate(
    IN PT_CONNECTION hConnection, 
    IN PT_LONG lEventPeriod,
    IN PT_NAVIGATION_CALLBACK pfnCallback,
    IN void *pNavigationCallbackCtx)
{
    typedef struct request_tag {
        CL_TAG tag;
        sint32 eventPeriod;     // requested event period
    } Request;
    
    PT_STATUS status;
    PT_DWORD flags = 0;
    PT_SESSION *pS = &gSession;
    
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pfnCallback == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    // set navigation callback
    pS->pfnNavigCallback = pfnCallback;
    pS->pNavigCallbackCtx = pNavigationCallbackCtx;

    // prepare flags for transaction
    flags |= (lEventPeriod == -1) ? TRANSACT_FLAG_NAV_BIDIR : 0; // bi-directional mode ?

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->eventPeriod = HTOL32(lEventPeriod);

    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_BIO_NAVIGATE, sizeof(Request), (uint32*)NULL, flags);

    pS->pfnNavigCallback = NULL;
    pS->pNavigCallbackCtx = NULL;
    
    return status;
}
#endif //#if !defined(TBX)

#if !defined(TBX)
PT_STATUS PTDecompressImage(
    IN PT_BYTE  byType,
    IN PT_DWORD dwInSize,
    IN PT_BYTE  *pbyInData,
    OUT PT_DWORD *pdwOutSize,
    OUT PT_BYTE *pbyOutData)
{
    uint32 size, i;
    PT_BYTE *pEnd = pbyInData + dwInSize;
    
#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    if (pbyInData == 0 || pdwOutSize == 0)
        return PT_STATUS_INVALID_PARAMETER;
    
    if (byType == PT_GRAB_TYPE_508_508_8_COMPRESS1)
    {
        // COMPRESS1 format

        uint16 imageWidth, imageHeight, imageByteWidth;
        sint32 backgroundColor;

        // check length of input data
        if (dwInSize < (2*sizeof(uint16) + sizeof(sint32)))
            return PT_STATUS_INVALID_PARAMETER;
        
        imageWidth = *(uint16*)pbyInData; // get image width (aligned in the buffer)
        pbyInData += sizeof(uint16);
        
        imageHeight = *(uint16*)pbyInData; // get image height (aligned in the buffer)
        pbyInData += sizeof(uint16);
        
        backgroundColor = *(sint32*)pbyInData; // get background color (aligned in the buffer)
        pbyInData += sizeof(sint32);
                
        // mask background color bits only
        backgroundColor &= COMPRESS1_BGCOLOR_MASK;
        
        imageByteWidth = (uint16)(((imageWidth - 1) * 5 + 7) / 8 + 1); // length of one compressed line in bytes
        size = imageWidth * imageHeight; // size of decompressed image

        // set size of decompressed image
        *pdwOutSize = size;

        if (pbyOutData == NULL)
        {
            // no output buffer so return only size of decompressed image
            return PT_STATUS_OK;
        }
        
        for (i = 0; i < imageHeight; i++)
        {
            // decompress line by line
            
            if (pbyInData > pEnd) // check whether pointer isn't out of buffer
            {
                return PT_STATUS_INVALID_PARAMETER;
            }
            
            IUUncompressRowD5bit(pbyInData, pbyOutData, imageWidth);
            IUEnhanceRow(pbyOutData, imageWidth, backgroundColor);
            
            pbyInData += imageByteWidth;
            pbyOutData += imageWidth;
        }
    }
    else if (byType == PT_GRAB_TYPE_508_508_8_COMPRESS2)
    {
        // COMPRESS2 format

        uint16 imageWidth, imageByteWidth, fullImageWidth, imageHeight;
        uint8 bgColor, dGain, flags;

        // check length of input data
        if (dwInSize < (4*sizeof(uint8) + 3*sizeof(uint16)))
            return PT_STATUS_INVALID_PARAMETER;

        if (*(uint8*)pbyInData != 1)  // check header version
            return PT_STATUS_NOT_SUPPORTED;

        pbyInData += sizeof(uint8);

        flags = *(uint8*)pbyInData; // get flags
        pbyInData += sizeof(uint8);

        bgColor = *(uint8*)pbyInData; // get background color
        pbyInData += sizeof(uint8);

        dGain = *(uint8*)pbyInData; // get digital gain
        pbyInData += sizeof(uint8);

        imageWidth = *(uint16*)pbyInData; // get image width (aligned in the buffer)
        pbyInData += sizeof(uint16);

        fullImageWidth = *(uint16*)pbyInData; // get full image width (aligned in the buffer)
        pbyInData += sizeof(uint16);

        imageHeight = *(uint16*)pbyInData; // get image height (aligned in the buffer)
        pbyInData += sizeof(uint16);

        imageByteWidth = (uint16)(((imageWidth - 1) * 5 + 7) / 8 + 1); // length of one compressed line in bytes
        size = fullImageWidth * imageHeight; // size of decompressed image

        // set size of decompressed image
        *pdwOutSize = size;

        if (pbyOutData == NULL)
        {
            // no output buffer so return only size of decompressed image
            return PT_STATUS_OK;
        }

        IUSetTurboMode(flags & 1); // turbomode flag
        IUSetDGain(dGain); // digital gain

        for (i = 0; i < imageHeight; i++)
        {
            // decompress line by line

            if (pbyInData > pEnd) // check whether pointer isn't out of buffer
            {
                return PT_STATUS_INVALID_PARAMETER;
            }

            IUUncompressAndCenterRowD5bit(pbyInData, pbyOutData, imageWidth, fullImageWidth, bgColor);
            IUEnhanceRow(pbyOutData, fullImageWidth, bgColor);

            pbyInData += imageByteWidth;
            pbyOutData += fullImageWidth;
        }
    }
    else if (byType == PT_GRAB_TYPE_508_508_8_COMPRESS3)
    {
        sint32 inWidth  = 144, outWidth = 192;
        sint32 P[4][3] = { { 19, -5, 1 }, { 3, 15, -3 }, { -3, 15, 3 }, { 1, -5, 19 } };   // pseudoinverse times 15
        sint32 R[4][3] = { { 3, 0, 0 }, { 1, 2, 0 }, { 0, 2, 1 }, { 0, 0, 3 } };    // upsample matrix times 3

        sint32 inHeight, outHeight;
        sint32 tempData[3][4];       // the temporary 3x4 matrix (3 rows, 4 columns)
        sint32 i,j,k,inx,iny,outx,outy;
        sint32 result, tmp;
        uint8 bgColor, flags;

        // COMPRESS3 format
        
        if (dwInSize <= sizeof(uint8))
        {
            return PT_STATUS_INVALID_PARAMETER;
        }

        // check header version
        if (*(uint8*)pbyInData != 1)
        {
            // unsupported version
            return PT_STATUS_NOT_SUPPORTED;
        }

        // version 1

        if (dwInSize <= 10*sizeof(uint8)) // check length of input data
        {
            // data too short
            return PT_STATUS_INVALID_PARAMETER;
        }

        pbyInData += sizeof(uint8);

        flags = *(uint8*)pbyInData; // get flags
        pbyInData += sizeof(uint8);

        bgColor = *(uint8*)pbyInData; // get background color
        pbyInData += sizeof(uint8);

        pbyInData += 7*sizeof(uint8); // skip reserved bytes

        inHeight  = (sint32)((dwInSize-10*sizeof(uint8))/inWidth);
        outHeight = inHeight*4/3;   // this works as it should even if inHeight % 3 != 0

        // set size of decompressed image
        *pdwOutSize = outWidth * outHeight;

        if (pbyOutData == NULL)
        {
            // no output buffer so return only size of decompressed image
            return PT_STATUS_OK;
        }

        // indices follow the matrix indexing convention. Resample squares 3x3 => 4x4.
        for (inx=0, outx=0; inx<inHeight; inx+=3, outx+=4) {
            for (iny=0, outy=0; iny<inWidth; iny+=3, outy+=4) {
                // resample row-wise, i.e. tempData = input * P^T
                for (i=0; i<min(3,inHeight - inx); i++) {
                    for (j=0; j<4; j++) {
                        result = 0;
                        for (k=0; k<3; k++) {
                            result += pbyInData[(inx + i) * inWidth + (iny + k)] * P[j][k];
                        }
                        tempData[i][j] = result;
                    }
                }

                // in case we are on the bottom edge of the image, use bgColor instead of the missing part
                for (; i<3; i++) {
                    for (j=0; j<4; j++) {
                        tempData[i][j] = 15*(sint32)bgColor;
                    }
                }

                // resample column-wise, i.e. output = P * tempData
                for (i=0; i<min(3,inHeight - inx)*4/3; i++) {
                    for (j=0; j<4; j++) {
                        result = 0;
                        for (k=0; k<3; k++) {
                            result += P[i][k] * tempData[k][j];
                        }
                        pbyOutData[(outx + i) * outWidth + (outy + j)] = (uint8)min(max((result/(15*15)),1),255);  // normalize (the pseudoinverse matrix P is not normalized)
                    }
                }

                // now resample the background mask row-wise, i.e. tempData = (input != bgColor) * R^T
                for (i=0; i<min(3,inHeight - inx); i++) {
                    for (j=0; j<4; j++) {
                        result = 0;
                        for (k=0; k<3; k++) {
                            result += (pbyInData[(inx + i) * inWidth + (iny + k)] != bgColor) * R[j][k];
                        }
                        tempData[i][j] = result;
                    }
                }

                // in case we are on the bottom edge of the image, cut the backround mask appropriately
                // note that in the current version this is unnecessary, but it is better to have it here
                for ( ; i<3; i++) {
                    for (j=0; j<4; j++) {
                        tempData[i][j] = 0;
                    }
                }

                // resample the background mask column-wise, i.e. output = output && (R * tempData)
                for (i=0; i<min(3,inHeight - inx)*4/3; i++) {
                    for (j=0; j<4; j++) {
                        result = 0;
                        for (k=0; k<3; k++) {
                            result += R[i][k] * tempData[k][j];
                        }

                        tmp = (outx + i) * outWidth + (outy + j);
                        if (result <= 0) {      // all pixels containing at least a bit of a non-bg pixel are considered non-bg
                            pbyOutData[tmp] = (PT_BYTE)bgColor;
                        }
                        else if (pbyOutData[tmp] == bgColor) {
                            pbyOutData[tmp] = (PT_BYTE)(bgColor - 1);
                        }
                    }
                }
            }
        }
    }
    else
    {
        return PT_STATUS_NOT_SUPPORTED;
    }

    return PT_STATUS_OK;
}

#endif //#if !defined(TBX)

//========================================================================
//      Not implemented functions
//========================================================================

#if !defined(TBX)

#ifndef ENABLE_PTAUTHENTIFY
PTAPI_DLL PT_STATUS PTAPI PTAuthentify (
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwAuthentifyId,
    IN PT_DATA *pKey)
{
    UNREFERENCED_PARAMETER(hConnection);
    UNREFERENCED_PARAMETER(dwAuthentifyId);
    UNREFERENCED_PARAMETER(pKey);

    return PT_STATUS_NOT_IMPLEMENTED;
}

PTAPI_DLL PT_STATUS PTAPI PTAuthentifyEx (
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwAccountId,
    IN PT_DATA *pKey)
{
    UNREFERENCED_PARAMETER(hConnection);
    UNREFERENCED_PARAMETER(dwAccountId);
    UNREFERENCED_PARAMETER(pKey);

    return PT_STATUS_NOT_IMPLEMENTED;
}

PTAPI_DLL PT_STATUS PTAPI PTAuthentifyAdv(
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwAccountId,
    IN PT_DATA *pWrappedProfile,
    IN PT_DATA *pAuthKey,
    IN PT_DATA *pPublicKey,
    IN PT_DWORD dwMechanism,
    IN PT_DWORD dwKeyLength,
    IN PT_DWORD dwFlags,
    IN PT_DWORD dwVersion)
{
    UNREFERENCED_PARAMETER(hConnection);
    UNREFERENCED_PARAMETER(dwAccountId);
    UNREFERENCED_PARAMETER(pWrappedProfile);
    UNREFERENCED_PARAMETER(pAuthKey);
    UNREFERENCED_PARAMETER(pPublicKey);
    UNREFERENCED_PARAMETER(dwMechanism);
    UNREFERENCED_PARAMETER(dwKeyLength);
    UNREFERENCED_PARAMETER(dwFlags);
    UNREFERENCED_PARAMETER(dwVersion);

    return PT_STATUS_NOT_IMPLEMENTED;
}
#endif // #ifndef ENABLE_PTAUTHENTIFY

#ifndef ENABLE_PTSECURECHANNEL
PTAPI_DLL PT_STATUS PTAPI PTSecureChannel(
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwMechanism,
    IN PT_DWORD dwKeyLength,
    IN PT_DWORD dwFlags)
{
    UNREFERENCED_PARAMETER(hConnection);
    UNREFERENCED_PARAMETER(dwMechanism);
    UNREFERENCED_PARAMETER(dwKeyLength);
    UNREFERENCED_PARAMETER(dwFlags);

    return PT_STATUS_NOT_IMPLEMENTED;
}
PTAPI_DLL PT_STATUS PTAPI PTSecureChannelEx(
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwMechanism,
    IN PT_DWORD dwKeyLength,
    IN PT_DWORD dwFlags,
    IN PT_DWORD dwVersion)
{
    UNREFERENCED_PARAMETER(hConnection);
    UNREFERENCED_PARAMETER(dwMechanism);
    UNREFERENCED_PARAMETER(dwKeyLength);
    UNREFERENCED_PARAMETER(dwFlags);
    UNREFERENCED_PARAMETER(dwVersion);

    return PT_STATUS_NOT_IMPLEMENTED;
}
#endif // #ifndef ENABLE_PTSECURECHANNEL
#endif //#if !defined(TBX)


