// ----------------------------------------------------------------------------
//
//  ClBio.h
//
//  Control logic biometric definitions.
//
//  Copyright (C) 2001-2006 UPEK Inc.
//
// ----------------------------------------------------------------------------

#ifndef __CLBIO_H__
#define __CLBIO_H__

//-----------------------------------------------------------------------------

/**
 * Flags for finger detect
 */

#define BD_WAIT_GOOD_FINGER              0x1

//-----------------------------------------------------------------------------

/**
 * Flags for enroll and capture
 */

#define BE_HAS_STORED_TEMPLATE           0x1
#define BE_HAS_PAYLOAD                   0x2
#define BE_HAS_SIGN_DATA                 0x4
#define BE_STORE_TEMPLATE                0x8
#define BE_RETURN_AUDIT_DATA           0x100
#define BE_RETURN_SIGNATURE            0x200
#define BE_RETURN_TEMPLATE             0x800

//-----------------------------------------------------------------------------

/**
 * Flags for verification 
 */

#define BV_HAS_PAYLOAD                   0x2
#define BV_HAS_SIGN_DATA                 0x4
#define BV_USE_FRR_MAX                   0x8
#define BV_HAS_CAPTURED_TEMPLATE        0x10
#define BV_CAPTURE_FINGERPRINT          0x20
#define BV_RETURN_AUDIT_DATA           0x100
#define BV_RETURN_SIGNATURE            0x200
#define BV_RETURN_PAYLOAD              0x400
#define BV_RETURN_ADAPTED_TEMPLATE     0x800
#define BV_RETURN_FRR_ACHIEVED        0x1000
#define BV_RETURN_FAR_ACHIEVED        0x2000

//-----------------------------------------------------------------------------

/**
 * Flags for store template
 */

#define BS_HAS_TEMPLATE                  0x1

//-----------------------------------------------------------------------------

/**
 * Flags for grabbing finger image
 */

#define BG_WAIT_GOOD_FINGER              0x1
#define BG_HAS_SIGN_DATA                 0x4
#define BG_RETURN_SIGNATURE            0x200

//-----------------------------------------------------------------------------

/**
 * Flags for ScanQualityEx
 */

#define BSQ_RETURN_SKEW_INFO             0x1
#define BSQ_RETURN_SPEED_INFO            0x2

//-----------------------------------------------------------------------------

/**
 * Flags for LoadFinger
 */

#define BLF_HAS_TEMPLATE                   0x1
#define BLF_RETURN_PAYLOAD               0x400

//-----------------------------------------------------------------------------

#endif
