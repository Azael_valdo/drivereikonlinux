/**
 * Sample application. Demonstrates core functions of PTAPI.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdio_ext.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
#include <usb.h>
#include <tfmapi.h>
#include <tfmerror.h>
#include "convert/convert.h" //for PTConvertTemplateEx
#include "maxt.h"       //large template
#include "tcs4c_temp.h" //normal size template

#define DSN "usb"

static PT_CONNECTION conn;
static quit = 0;
static int err;
static const int timeout = -1;

//=== Tracing functions ===

const char* err_msg(PT_STATUS err);  // implemented in err.c

/** Tests, if last command returned error. If so, prints error message.
 * @param err PTAPI error code.
 * @param func Function name.
 * @return Err value.
 */
static int check_err(PT_STATUS err, const char* func)
{
    if(err == PT_STATUS_OK)
    {
        return 0;
    }
    printf("%s failed (status == %ld): %s\n", func,(long) err, err_msg(err));
    return (int)err;
}


/** Dumps memory block.
 * @param mem Memory address.
 * @param len Length.
 */
static void dump_bytes_np(void* mem, unsigned int len)
{
    unsigned char* buffer = (unsigned char*) mem;
    int i;

    for(i = 0; i < len; i++)
        printf("%02x ", buffer[i]);
}

/** Dumps memory block with a size limitation.
 * @param mem Memory address.
 * @param len Length. If length is bigger than 16B, only buffer begin and end are printed.
 */
static void dump_bytes(void* mem, unsigned int len)
{
    if(len > 16)
    {
        unsigned char* buffer = (unsigned char*) mem;
        dump_bytes_np(buffer, 8);
        printf("... ");
        dump_bytes_np(buffer + len - 8, 8);
        return;
    }
    else
    {
        dump_bytes_np(mem, len);
    }
    printf("[%ld bytes]", (long)len);
}

/** Dumps memory block, 16B per line.
 * @param mem Memory address.
 * @param len Length.
 */
static void dump_block_np(void* mem, unsigned len)
{
    unsigned char* buffer = (unsigned char*) mem;
    unsigned char* ptr = buffer;

    while(ptr < buffer + len)
    {
        printf("  ");
        dump_bytes_np(ptr, (len < 16 ? len : 16));
        printf("\n");
        ptr += 16;
    }
}

/** Dumps memory block with a size limitation, 16B per line.
 * @param mem Memory address.
 * @param len Length.
 */
static void dump_block(void* mem, unsigned int len)
{
    unsigned char* buffer = (unsigned char*) mem;

    if(len > 8 * 16)
    {
        dump_block_np(buffer, 4 * 16);
        printf("  ...\n");
        dump_block_np(buffer + len - (4 * 16), 4 * 16);
    }
    else
    {
        dump_block_np(buffer, len);
    }
    printf("  [%d bytes]\n", len);
}


//=== Callback handlers ===

/** Translates and dislays messages coming from a device.
* @param pGuiStateCallbackCtx A generic pointer to context, NULL in case of PTGrab(Part).
* @param dwGuiState A bitmask indicating the current GUI state plus an indication
*		of what others parameters are available. It can be combined from values
*		PT_SAMPLE_AVAILABLE, PT_MESSAGE_PROVIDED and PT_PROGRESS_PROVIDED.
*		In the current implementation only PT_MESSAGE_PROVIDED is used.
* @param pbyResponse The response from the application back to the PerfectTrust Proxy
*		API on return from the callback. Can be one of values PT_CANCEL or PT_CONTINUE.
*		Other values are reserved for future use.
* @param dwMessage The number of a message to display to the user. For message numbers
*		see PT_GUIMSG_XXXX. GuiState indicates if a Message is provided; if not
*		the parameter is 0.
* @param byProgress A Value that indicates (as a percentage) the amount of progress
*		in the development of a Sample/BIR. The value may be used to display a progress
*		bar. GuiState indicates if a sample Progress value is provided in the call;
*		if not the parameter is 0. This parameter is reserved for future use,
*		currently it is always 0.
* @param pSampleBuffer The current sample buffer for the application to display.
*		GuiState indicates if a sample Buffer is provided; if not the parameter is
*		NULL. This parameter is reserved for future use, currently it is always NULL.
*		The buffer is allocated and controlled by PerfectTrust, it must not be freed.
* @param pData Optional data, which may be available for some GUI message codes.
*		If no data is provided the parameter is NULL. The data is allocated and
*		controlled by PerfectTrust, it must not be freed.
**/
static PT_STATUS StateCallback (
	void *pGuiStateCallbackCtx,
	PT_DWORD dwGuiState,
	 PT_BYTE *pbyResponse,
	PT_DWORD dwMessage,
	PT_BYTE byProgress,
	void *pSampleBuffer,
	PT_DATA *pData
)
{
    char *msg;
    static PT_DWORD lastGuiState = 0;
    static PT_BYTE wheel = 0;

    switch(dwMessage){
        case PT_GUIMSG_GOOD_IMAGE: msg = "Good image"; break;
        case PT_GUIMSG_NO_FINGER: msg = "No finger detected"; break;
        case PT_GUIMSG_TOO_LIGHT: msg = "Finger image is too light/latent image detected"; break;
        case PT_GUIMSG_TOO_DRY: msg = "Finger is too dry"; break;
        case PT_GUIMSG_TOO_DARK: msg = "Finger image is too dark"; break;
        case PT_GUIMSG_TOO_HIGH: msg = "Finger is too high"; break;
        case PT_GUIMSG_TOO_LOW: msg = "Finger is too low"; break;
        case PT_GUIMSG_TOO_LEFT: msg = "Finger is too left"; break;
        case PT_GUIMSG_TOO_RIGHT: msg = "Finger is too right"; break;
        case PT_GUIMSG_TOO_SMALL: msg = "Finger image is too small"; break;
        case PT_GUIMSG_TOO_STRANGE: msg = "Finger image is too strange"; break;
        case PT_GUIMSG_BAD_QUALITY: msg = "Finger iamge has bad quality"; break;
        case PT_GUIMSG_PUT_FINGER: msg = "Swipe/Put finger"; break;
        case PT_GUIMSG_PUT_FINGER2: msg = "Swipe/Put finger 2nd time"; break;
        case PT_GUIMSG_PUT_FINGER3: msg = "Swipe/Put finger 3rd time"; break;
        case PT_GUIMSG_PUT_FINGER4: msg = "Swipe/Put finger 4th time"; break;
        case PT_GUIMSG_PUT_FINGER5: msg = "Swipe/Put finger 5th time"; break;
        case PT_GUIMSG_REMOVE_FINGER: msg = "Remove finger"; break;
        case PT_GUIMSG_CONSOLIDATION_FAIL: msg = "Consolidation failed"; break;
        case PT_GUIMSG_CONSOLIDATION_SUCCEED: msg = "Consolidation succeed"; break;
        case PT_GUIMSG_CLEAN_SENSOR: msg = "Clean the sensor"; break;
        case PT_GUIMSG_KEEP_FINGER: msg = "Keep finger"; break;
        case PT_GUIMSG_START: msg = ""; break;
        case PT_GUIMSG_VERIFY_START: msg = "Verification start"; break;
        case PT_GUIMSG_ENROLL_START: msg = "Enrollment start"; break;
        case PT_GUIMSG_FINGER_DETECT_START: msg = "Finger detection start"; break;
        case PT_GUIMSG_GUI_FINISH: msg = "Operation has finished"; break;
        case PT_GUIMSG_GUI_FINISH_SUCCEED: msg = "Operation has succeeded"; break;
        case PT_GUIMSG_GUI_FINISH_FAIL: msg = "Operation has failed"; break;
        case PT_GUIMSG_CALIB_START: msg = "Calibration start"; break;
        case PT_GUIMSG_TOO_FAST: msg = "Finger was swiped too fast"; break;
        case PT_GUIMSG_TOO_SKEWED: msg = "Finger was too skewed during swipe"; break;
        case PT_GUIMSG_TOO_SHORT: msg = "Finger swipe was too short"; break;
        case PT_GUIMSG_TOUCH_SENSOR: msg = "Touch sensor with finger"; break;
        case PT_GUIMSG_PROCESSING_IMAGE: msg = "Processing an image"; break;
        case PT_GUIMSG_SWIPE_IN_PROGRESS: msg = "Finger swipe is in progress"; break;
        case PT_GUIMSG_BACKWARD_MOVEMENT: msg = "Backward movement detected"; break;
        case PT_GUIMSG_JOINT_DETECTED: msg = "Finger joint detected"; break;
        case PT_GUIMSG_CENTER_AND_PRESS_HARDER: msg = "Center finger and press harder"; break;
        case PT_GUIMSG_IMAGE_PROCESSED: msg = "Image processing has finished"; break;
        case PT_GUIMSG_ENROLL_PROGRESS: msg = "Progress of dynamic enrollment"; break;
        default: msg = "";
            printf("Unknown callback message received: %ld\n", (long)dwMessage);
    }

    //make it look nicer, don't print repeatedly messages
    if(lastGuiState == dwMessage)
    {
        switch(wheel){
            case 0: printf("\b/"); wheel++; break;
            case 1: printf("\b-"); wheel++; break;
            case 2: printf("\b\\"); wheel++; break;
            case 3: printf("\b|"); wheel = 0; break;
        }
    } else {
        printf("\bResponse from a device: %s \n",msg);
    }

    lastGuiState = dwMessage;
    
    *pbyResponse = PT_CONTINUE;

    return PT_STATUS_OK;
}

/** Function called by PTSleep(TfmApi.c) -> HLTransact(commspi.c). It decides, whether a device should be still in sleep or it should be woken up.
* @param pIdleCallbackCtx A generic pointer to context information that
*     was provided when calling PTSleep.
* @param pbyResponse The response from the application back to the PTAPI
*     on return from the callback. Can be one of the values PT_SLEEP_STOP,
*     i.e. wakeup, or PT_SLEEP_CONTINUE, i.e. continue to sleep. Other
*     values are reserved for future use.
**/
PT_STATUS IdleCallback(
		void *pIdleCallbackCtx,
		PT_BYTE *pbyResponse)
{
	PT_BYTE *i = (PT_BYTE *)pIdleCallbackCtx;
    if(*i<20){
        *pbyResponse = PT_SLEEP_CONTINUE;
        printf("PT_SLEEP_CONTINUE\n");
    } else {
        *pbyResponse = PT_SLEEP_STOP;
        printf("PT_SLEEP_STOP\n");
    }

    (*i)++;

	// Let device sleep for a while
	usleep(150000);

    return PT_STATUS_OK;
}


//=== Commands ===

/** Opens device session. */
static void cmd_open(void)
{
    err = PTOpen(DSN, &conn);
    if(check_err(err, "PTOpen()"))
    {
        return;
    }

    // Register GUI callbacks handler.
    err = PTSetGUICallbacks(conn, NULL, NULL, StateCallback, NULL);
    if(check_err(err, "PTSetGUICallbacks()"))
    {
            return;
    }

}
/** Closes device session. */
static void cmd_close(void)
{
    err = PTClose(conn);
    if(check_err(err, "PTClose()"))
    {
        return;
    }
}

/** Prints basic device description. */
static void cmd_info(void)
{
    PT_INFO* info;

    err = PTInfo(conn, &info);
    if(check_err(err, "PTInfo()"))
    {
        return;
    }

    printf("  version:               0x%08x\n", info->FwVersion);
    printf("  min next version:      0x%08x\n", info->FwMinNextVersion);
    printf("  variant:               0x%08x\n", info->FwVariant);
    printf("  functionality:         0x%08x\n", info->FwFunctionality);
    printf("  config:                0x%08x\n", info->FwConfig);
    printf("  id:                    0x%08x\n", info->Id);
    printf("  authentify id:         0x%08x\n", info->AuthentifyId);
    printf("  usage:                 0x%08x\n", info->Usage);
    printf("  sensor type:           0x%08x\n", info->SensorType);
    printf("  image width:           %hd\n", info->ImageWidth);
    printf("  image height:          %hd\n", info->ImageHeight);
    printf("  max grab window:       0x%08x\n", info->MaxGrabWindow);
    printf("  companion vendor code: 0x%08x\n", info->CompanionVendorCode);

    PTFree(info);
}

/** Prints extended device description. */
static void cmd_extended_info(void)
{
    PT_EXTENDED_INFO *info;

    err = PTExtendedInfo(conn, &info);
    if(check_err(err, "PTExtendedInfo()"))
    {
        return;
    }

    printf("  version:     0x%08x\n", info->Version);
    printf("  GUID:        "); dump_bytes(info->GUID, PT_GUID_SIZE); printf("\n");
    printf("  system id:   0x%08x\n", info->SystemId);
    printf("  system name: %s\n", info->SystemName);
}

/** Grabs fingerprint image. */
static void cmd_grab(void)
{
    PT_DATA* data;

    err = PTGrab(conn, PT_GRAB_TYPE_381_381_8,timeout, PT_TRUE, &data, NULL, NULL);
    if(check_err(err, "PTGrab()"))
    {
        return;
    }

    dump_block(data->Data, data->Length);
    PTFree(data);
}

/** Grabs fingerprint image with a device suspension during finger detection. */
static void cmd_sleep_then_grab(void)
{
    PT_DATA* data;
    PT_DWORD cause;
    PT_DWORD gui_msg;
    PT_BYTE callbackCalledTimes = 0;

    err = PTSleepThenGrab(conn, IdleCallback, &callbackCalledTimes,
                          PT_GRAB_TYPE_381_381_8,
                          timeout, PT_FALSE, &cause, &gui_msg, 
                          &data, NULL, NULL);
    if(check_err(err, "PTSleepThenGrab()"))
    {
        return;
    }

    switch(cause)
    {
        case PT_WAKEUP_CAUSE_HOST:
            printf("   Wake-up cause: PT_WAKEUP_CAUSE_HOST\n");
            break;

        case PT_WAKEUP_CAUSE_FINGER:
            printf("   Wake-up cause: PT_WAKEUP_CAUSE_FINGER\n");
            printf("   GUI message code: %ld\n",(long)gui_msg);
            if(gui_msg == PT_GUIMSG_GOOD_IMAGE)
            {
                dump_block(data->Data, data->Length);
                PTFree(data);
            }
            break;

        default:
            printf("   Wake-up cause: unknown\n");
            break;
    }
}

/** Lists templates enrolled in device.*/
static void cmd_list_all_fingers(void)
{
    PT_FINGER_LIST* fingers;
    int i;

    err = PTListAllFingers(conn, &fingers);
    if(check_err(err, "PTListAllFingers()"))
        return;

    for(i = 0; i < fingers->NumFingers; i++)
    {
        printf("  Finger #%ld:\n", (long)i);
        printf("    slot number: %ld\n", (long)fingers->List[i].SlotNr);
        printf("    finger data: "); 
        dump_bytes(fingers->List[i].FingerData, 
                   fingers->List[i].FingerDataLength); 
        printf("\n");
    }
    printf("  [%ld fingers]\n", (long)(fingers->NumFingers));
}

/** Enrolls new template. */
static void cmd_enroll(void)
{
    PT_LONG slot;

    err = PTEnroll(conn, PT_PURPOSE_ENROLL, NULL, NULL, &slot, 
                   NULL, timeout, NULL, NULL, NULL);
    if(check_err(err, "PTEnroll()"))
        return;

    printf("  New template inserted into slot %ld.\n",(long) slot); 
}

/** Verifies life finger against specified template. */
static void cmd_verify(void)
{
    int slot;
    PT_INPUT_BIR input_bir;
    PT_BOOL match;
    PT_DATA* payload;

    printf("  Slot number to match against: ");
    scanf("%d", &slot);

    input_bir.byForm = PT_SLOT_INPUT;
    input_bir.InputBIR.lSlotNr = slot;

    err = PTVerify(conn, NULL, NULL, NULL, &input_bir, NULL, &match,
        NULL, NULL, &payload, timeout, PT_TRUE, NULL, NULL, NULL);
    if(check_err(err, "PTVerify()"))
    {
        return;
    }

    printf("  Match:   %s\n", match ? "yes" : "no");
    if(match) 
    {
        printf("  Payload: ");
        dump_bytes(payload->Data, payload->Length);
        printf("\n");
        PTFree(payload);
    }
}

/** Verifies life finger against all enrolled templates. */
static void cmd_verify_all(void)
{
    PT_LONG slot;
    PT_DATA* payload;

    err = PTVerifyAll(conn, NULL, NULL, NULL, NULL, &slot, 
        NULL, NULL, &payload, timeout, PT_TRUE, NULL, NULL, NULL);
    if(check_err(err, "PTVerofyAll"))
    {
        return;
    }
    
    printf("  Matching slot:  %d%s\n", (int)slot, (slot >= 0 ? "" : " (no match)"));
    if(slot >= 0)
    {
        printf("  Payload: ");
        dump_bytes(payload->Data, payload->Length);
        printf("\n");
        PTFree(payload);
    }
}

/** Deletes single enrolled template. */
static void cmd_delete(void)
{
    PT_LONG slot;    

    printf("  Slot number to delete: ");
    scanf("%d", &slot);

    err = PTDeleteFinger(conn, slot);
    if(check_err(err, "PTDeleteFinger"))
    {
        return;
    }
}

/** Deletes all enrolled templates in a device. */
static void cmd_delete_all(void)
{
    err = PTDeleteAllFingers(conn);
    if(check_err(err, "PTDeleteAllFingers"))
    {
        return;
    }
}

/** Prints string related to enrolled finger. */
static void cmd_get_finger_data(void)
{
    PT_LONG slot;
    PT_DATA* data;

    printf("  Slot number to retrieve data from: ");
    scanf("%d", &slot);

    err = PTGetFingerData(conn, slot, &data);
    if(check_err(err, "PTGetFingerData"))
    {
        return;
    }

    printf("\n  Data in binary form:\n");
    dump_block(data->Data, data->Length);

    // stupid check if we can try interpret the data as a string:
    if(data->Length > 0  &&  data->Data[data->Length - 1] == 0)
        printf("\n  The data in string form: '%s'\n", (char*) data->Data);

    PTFree(data);
}

/** Stores a string to a device and binds it to a slot containing enrolled finger. */
static void cmd_set_finger_data(void)
{
    char buffer[sizeof(PT_DWORD) + PT_MAX_FINGER_DATA_LENGTH] = { 0 };
    PT_LONG slot;
    PT_DATA* data = (PT_DATA*) buffer;
    char* ptr = (char*) data->Data;

    printf("  Slot number to save data into: ");
    scanf("%d", &slot);

    __fpurge(stdin);  // remove end-of-line from reading the slot
    printf("  Enter some string to be used as finger data "
           "(max. %d characters):\n  ", PT_MAX_FINGER_DATA_LENGTH - 1);
    fgets((char*)data->Data, PT_MAX_FINGER_DATA_LENGTH, stdin);

    // compute length of the data:
    while(*ptr != '\0'  &&  *ptr != '\n')
    {
        ptr++;
        data->Length++;
    }
    if(data->Length > 0) 
    {
        *ptr = '\0';     // rewrite end of line
        data->Length++;  // save the terminator as part of the data
    }

    err = PTSetFingerData(conn, slot, data);
    if(check_err(err, "PTSetFingerData"))
        return;
}


/** Convert template to specified format including standard ANSI/ISO formats */
static void cmd_convert_template_ex(void)
{
    PT_DATA *pAnsi = 0;
    PT_DATA *pInputTemplate;
    static int index = 0;
    
    /* convert normal template and large template in turn */
    if (index == 0)
    {
        pInputTemplate = (PT_DATA *)temp_buf_normal;
    }
    else
    {
        pInputTemplate = (PT_DATA *)temp_buf_large;
    }

    index ^= 1;

    printf("The length of original template is %d\n", pInputTemplate->Length);

    // Convert template from UPEK format to ANSI-378 format
    err = PTConvertTemplateEx(conn, 
            PT_TEMPLATE_TYPE_AUTO, PT_TEMPLATE_ENVELOPE_PTBIR, pInputTemplate,
            PT_TEMPLATE_TYPE_ANSI, PT_TEMPLATE_ENVELOPE_NONE, 0, 0, &pAnsi);
    
    check_err(err, "PTConvertTemplateEx");
    if (err == PT_STATUS_OK)
    {
        printf("Convert template successfully!\nRetrieved an ANSI template with length %d bytes.\n", 
            pAnsi->Length);
    }
#if defined (TARGET_STANDARD_API)    
    free(pAnsi);
#endif    
}

/** Quits program. */
static void cmd_quit(void)
{
    quit = 1;
}

static void cmd_help(void);  // forward declaration

static const struct 
{
    char opcode;
    const char* help;
    void (*func)(void);
} cmd_map[] = {
    { 'o', "PTOpen",                    cmd_open },
    { 'c', "PTClose",                   cmd_close },
    { 'i', "PTInfo",                    cmd_info },
    { 'x', "PTExtendedInfo",            cmd_extended_info },
    { 'g', "PTGrab",                    cmd_grab },
    { 'G', "PTSleepThenGrab",           cmd_sleep_then_grab },
    { 'l', "PTListAllFingers",          cmd_list_all_fingers },
    { 'e', "PTEnroll",                  cmd_enroll },
    { 'v', "PTVerify",                  cmd_verify },
    { 'V', "PTVerifyAll",               cmd_verify_all },
    { 'd', "PTDeleteFinger",            cmd_delete },
    { 'D', "PTDeleteAllFingers",        cmd_delete_all },
    { 'r', "PTGetFingerData",           cmd_get_finger_data },
    { 'w', "PTSetFingerData",           cmd_set_finger_data },
    { 'C', "PTConvertTemplateEX",       cmd_convert_template_ex },
    { 'h', "Help",                      cmd_help },
    { 'q', "Quit",                      cmd_quit }
};

/** Prints help. */
static void cmd_help(void)
{
    int i;
    for(i = 0; i < sizeof(cmd_map) / sizeof(cmd_map[0]); i++)
        printf("  %c ... %s\n", cmd_map[i].opcode, cmd_map[i].help);

    printf("\n  Note that every command must be submitted with <enter>.\n");
}

int main(int argc, char** argv)
{
    char opcode = 0;
    int i;
    void (*func)(void);

    // output everything immediately (no buffering):
    setvbuf(stdout, 0, _IONBF, 0);

    err = PTInitialize(NULL);
    if(check_err(err, "PTInitialize()"))
    {
        exit(EXIT_FAILURE);
    }

    printf("Wellcome to simple tty TFM client.\n");
    printf("If you are new here, press <h> and <enter> to show help.\n");

    while(!quit)
    {
        // read command to perform:
        printf("\n>> ");
        do {
            opcode = getchar();
        } while(opcode == '\n');

        // find the function corresponding to the opcode:
        printf("\n");
        func = NULL;
        for(i = 0; i < sizeof(cmd_map) / sizeof(cmd_map[0]); i++) 
        {
            if(cmd_map[i].opcode == opcode) 
            {
                func = cmd_map[i].func;
                break;
            }
        }

        // if found, perform the function:
        if(func)
        {
            printf("%s\n", cmd_map[i].help);
            func();
        } 
        else 
        {
            printf("Unknown command.\n");
            printf("  Press <h> and <enter> to show help.\n");
        }
    }

    PTTerminate();
    return EXIT_SUCCESS;
}
