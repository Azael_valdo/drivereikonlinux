/**
* @file usbl.c
*
* USB layer functions
*
*/
#include "usbl.h"
#include "tfmerror.h"
#include "defines.h"
#include <usb.h>
#include <errno.h>
#include <string.h>

struct 
{
    int miVendorID;
    int miProductID;
}gDeviceTable [] = COMM_USB_VIDS_PIDS;


#define OS_ERROR_DEVICE_NOT_FOUND (-ENODEV)
#define OS_ERROR_TIMEOUT          (-ETIMEDOUT)
#define OS_ERROR_PERM             (-EPERM)

#define USB_VENDOR_REQUEST 0x40
#define USB_VENDOR_REQUEST_READ 0xC0

// bRequest -> 0xC0 (Vendor specific request)
#define USB_REQUEST_WRITE 0x0C

// wValue -> 0x0100
#define USB_VALUE 0x0100

#define USB_CONTROL_TIMEOUT 200

// timeout for bulk transfer
#define USB_BULK_TIMEOUT 5000

// input bulk endpoint
#define USB_BULK_IN 0x81

// output bulk endpoint
#define USB_BULK_OUT 0x02

// interrupt endpoint
#define USB_INT_EP 0x83


typedef struct  
{
    usb_dev_handle *mHandle;
}USB_CONTEXT;

typedef struct usb_bus USB_BUS;
typedef struct usb_device USB_DEVICE;
typedef struct usb_device_descriptor USB_DEVICE_DESCRIPTOR;
//////////////////////////////////////////////////////////////////////////
static void InitLibUsb()
{
    static int iInitDone = 0;
    if(iInitDone == 0)
    {
        usb_init();
        iInitDone = 1;
    }
}
//////////////////////////////////////////////////////////////////////////
static PT_STATUS UsbTranslateStatus(IN int iUsbError)
{
    if(iUsbError >= 0) return PT_STATUS_OK;
    switch(iUsbError)
    {
    case OS_ERROR_TIMEOUT:
        return PT_STATUS_TIMEOUT;
    case OS_ERROR_PERM:
        return PT_STATUS_OS_ACCESS_DENIED;
    default:
        return PT_STATUS_COMM_ERROR;
    }
}



static uint8 UsbIsValidDevice(IN USB_DEVICE *pDevice)
{
    uint8 byResult = 0;
    int i;
    for(i = 0;i < (int)(sizeof(gDeviceTable)/sizeof(gDeviceTable[0]));i++)
    {
        if((pDevice->descriptor.idVendor == gDeviceTable[i].miVendorID) &&
            (pDevice->descriptor.idProduct == gDeviceTable[i].miProductID))
        {
            byResult = 1;
            break;
        }
    }
    return byResult;
}

static USB_BUS *UsbDoOSEnumerateDevices()
{
    USB_BUS *pResult = NULL;
    int status = 0;
    errno=0;
    InitLibUsb();

    // search device
    status = usb_find_busses();
    if(status<0)
    {
        goto lbExit;
    }

    status = usb_find_devices();
    if(status<0)
    {
        goto lbExit;
    }
    pResult = usb_get_busses();
lbExit:
    return pResult;
};


PT_STATUS UsbOpenDevice(OUT USB_HANDLE *pDevice)
{
    PT_STATUS status = PT_STATUS_OK;
    USB_BUS *pBusses, *pBus;
    USB_DEVICE *pFoundDevice = NULL;
    USB_CONTEXT *pDeviceContext = NULL;
    int iUsbError;

    if(pDevice == NULL)
    {
        status = PT_STATUS_INVALID_PARAMETER;
        goto lbExit;
    }

    pBusses = UsbDoOSEnumerateDevices();
    pBus = pBusses;
    while(pBus && !pFoundDevice)
    {
        USB_DEVICE *pEnumDevice = pBus->devices;
        while(pEnumDevice)
        {
            if(UsbIsValidDevice(pEnumDevice))
            {
                pFoundDevice = pEnumDevice;
                break;
            }
            pEnumDevice = pEnumDevice->next;
        }
        pBus = pBus->next;
    }
    if(!pFoundDevice)
    {
        status =  PT_STATUS_DEVICE_NOT_FOUND;
        goto lbExit;
    }


    pDeviceContext = (USB_CONTEXT *)malloc(sizeof(USB_CONTEXT));

    if(!pDeviceContext)
    {
        status = PT_STATUS_NOT_ENOUGH_MEMORY;
        goto lbExit;
    }

    // open device
    pDeviceContext->mHandle = usb_open(pFoundDevice);
    if( !pDeviceContext->mHandle )
    {
        status = PT_STATUS_COMM_ERROR;
        goto lbExit;
    }


    // set configuration
    iUsbError = usb_set_configuration(pDeviceContext->mHandle, 1);


    if(iUsbError == 0)
    {
        // claim interface
        iUsbError = usb_claim_interface(pDeviceContext->mHandle, 0);
    }
    if( iUsbError < 0 )
    {
        usb_close(pDeviceContext->mHandle);
        status = UsbTranslateStatus(iUsbError);
        goto lbExit;
    }

lbExit:
    if(status != PT_STATUS_OK)
    {
        if(pDeviceContext)
        {
            free(pDeviceContext);
        }
    }
    else
    {
        *pDevice = pDeviceContext;
    }
    return status;
}


PT_STATUS UsbCloseDevice(USB_HANDLE device)
{
    PT_STATUS status = 0;

    if(device != NULL)
    {
        USB_CONTEXT *pDeviceContext = (USB_CONTEXT *)device;
        //release interface
        if(pDeviceContext->mHandle)
        {
            usb_release_interface(pDeviceContext->mHandle, 0);
            //close device
            usb_close(pDeviceContext->mHandle);
        }
        free(pDeviceContext);
    }
    else
    {
        status = PT_STATUS_INVALID_PARAMETER;
    }
    return status;
}

//When USB device is removed and then inserted, PTAPI command will fail.
//The application may try to PTClose() and then PTOpen() the device again.
//In this case something very bad happens (on some Linux systems)
//in usblib and usb_find_busses() locks up.
//Dirty solution is not to close() the lost device.
//So invalidate the usb handle just after libusb returns OS_ERROR_DEVICE_NOT_FOUND error
static void InvalidateHandle(USB_CONTEXT *pDeviceContext)
{
    pDeviceContext->mHandle = 0;
}


PT_STATUS UsbReadStatusReg(USB_HANDLE device,uint32 *pData)
{
    PT_STATUS status;
    char aBuffer[8] = {0};
    int iUsbError;
    USB_CONTEXT *pDeviceContext = (USB_CONTEXT *)device;

    if(!pDeviceContext->mHandle)
    {
        //Invalid handle, broken session
        status = PT_STATUS_COMM_ERROR;
        goto lbExit;
    }
    iUsbError = usb_control_msg(pDeviceContext->mHandle,USB_VENDOR_REQUEST_READ,USB_REQUEST_WRITE,0, 0,aBuffer,sizeof(aBuffer),USB_CONTROL_TIMEOUT);

    if(OS_ERROR_DEVICE_NOT_FOUND==iUsbError)
    {
        InvalidateHandle(pDeviceContext);
    }

    if(iUsbError < 0)
    {
        status = PT_STATUS_COMM_ERROR;
    }
    else
    {
        memcpy(pData,aBuffer+4,sizeof(*pData));
        status = PT_STATUS_OK;
    }
lbExit:
    return status;
}



// sends a command via control channel to the device

PT_STATUS UsbSendCommand(USB_HANDLE device, USB_COMMAND command)
{
    PT_STATUS status;
    char aBuffer[8] = {0};
    int iUsbError,i,iReady = 0;
    USB_CONTEXT *pDeviceContext = (USB_CONTEXT *)device;

    for(i=0;i<20;i++)
    {
        uint32 dwRegisters;
        status = UsbReadStatusReg(device,&dwRegisters);
        if(status != PT_STATUS_OK)
        {
            goto lbExit;
        }
        if ((dwRegisters & 0x4000) == 0) 
        {
            // Interrupt pending bit isn't set which means any previous command was already processed
            // and we can safely send the next one
            iReady = 1;
            break;
        }
        usleep(50000);
    }
    if(!iReady)
    {
        //device is not ready after 20 tries, something really bad happened
        status = PT_STATUS_COMM_ERROR;
        goto lbExit;
    }
    if(!pDeviceContext->mHandle)
    {
        //Invalid handle, broken session?
        status = PT_STATUS_COMM_ERROR;
        goto lbExit;
    }
    iUsbError = usb_control_msg(pDeviceContext->mHandle, USB_VENDOR_REQUEST, USB_REQUEST_WRITE, USB_VALUE, command, aBuffer, 1, USB_CONTROL_TIMEOUT);

    if(OS_ERROR_DEVICE_NOT_FOUND == iUsbError)
    {
        InvalidateHandle(pDeviceContext);
    }

    if(iUsbError < 0)
    {
        status = PT_STATUS_COMM_ERROR;
    }
    else
    {
        status = PT_STATUS_OK;
    }
lbExit:
    return status;
}


// reads data from the device
PT_STATUS UsbReadData(USB_HANDLE device, char *buffer, int length)
{
    PT_STATUS status = PT_STATUS_OK;
    USB_CONTEXT *pDeviceContext = (USB_CONTEXT *)device;
    int iUsbError;

    if(!pDeviceContext->mHandle)
    {
        //Invalid handle, broken session?
        status = PT_STATUS_COMM_ERROR;
        goto lbExit;
    }
    iUsbError = usb_bulk_read(pDeviceContext->mHandle, USB_BULK_IN, buffer,length, USB_BULK_TIMEOUT);

    if(OS_ERROR_DEVICE_NOT_FOUND==iUsbError)
    {
        InvalidateHandle(pDeviceContext);
    }

    if(iUsbError < 0 )
    {
        status = UsbTranslateStatus(iUsbError);
    }

lbExit:
    return status;
}

// writes data to the device
PT_STATUS UsbWriteData(USB_HANDLE device, char *buffer, int length)
{
    PT_STATUS status = PT_STATUS_OK;
    USB_CONTEXT *pDeviceContext = (USB_CONTEXT *)device;
    int iUsbError;

    if(!pDeviceContext->mHandle)
    {
        //Invalid handle, broken session?
        status = PT_STATUS_COMM_ERROR;
        goto lbExit;
    }
    iUsbError = usb_bulk_write(pDeviceContext->mHandle, USB_BULK_OUT, buffer,length, USB_BULK_TIMEOUT);

    if(OS_ERROR_DEVICE_NOT_FOUND==iUsbError)
    {
        InvalidateHandle(pDeviceContext);
    }

    if(iUsbError < 0 )
    {
        status = UsbTranslateStatus(iUsbError);
    }

lbExit:
    return status;
}


