/**
 * @file utils.c
 *
 * Utility functions, e.g. things from <string.h>, which are not available in EPOC
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#include "utils.h"
#include <string.h>

//========================================================================
//      Exported frunctions
//========================================================================

/**
 *	Copy data block
 */
void*	PT_memcpy (void *dst, const void *src, uint32 size)
{
    return memcpy (dst, src, size);
}


/**
 *	Move data block - source and destination may overlap
 */
void*	PT_memmove (void *dst, const void *src, uint32 size)
{
/*
    if (src > dst) {
        // Bottom-up
        uint8 *pDst = (uint8 *)dst;
        uint8 *pSrc = (uint8 *)src;
        uint8 *pEnd = (uint8 *)src+size;
        while (pSrc < pEnd) *pDst++ = *pSrc++;
    } else {
        // Top-down
        uint8 *pDst = (uint8 *)dst+size;
        uint8 *pSrc = (uint8 *)src+size;
        uint8 *pEnd = (uint8 *)src;
        while (pSrc > pEnd) *--pDst = *--pSrc;
    }

    return dst;
*/

    return memmove (dst, src, size);
}



/**
 *	Fill in data block
 */
void*	PT_memset (void *dst, uint8 fillByte, uint32 size)
{
    return memset (dst, fillByte, size);
}

/**
 * Compare block of memory
 */
sint32 PT_memcmp(const void *first, const void *second, uint32 size)
{
    return memcmp(first, second, size);
}

/**
 * Compare two zero terminated strings.
 */
sint32 PT_strcmp(const char *first, const char *second)
{
    return strcmp(first, second);
}




