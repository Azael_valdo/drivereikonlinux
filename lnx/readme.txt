***********************************************************************************************************************
*                                      SimpleComLib notes for Unix-based systems                                      *
*                                                                                                                     *
*                                          Copyright (C) 2011 AuthenTec Inc.                                          *
*                                                                                                                     *
***********************************************************************************************************************


Supported Devices
-----------------
SimpleCommLib supports only companion chip (TCD) devices, Sensor-Only devices are not supported. If you need to support
Sensor-Only solutions, contact the AuthenTec Inc. for a PTAPI library. Note that some notebooks have integrated Sensor-Only
devices with the same VID and PID as TCD50 devices. SimpleCommLib cannot distinguish such devices from regular TCD devices
and will not work correctly. Also note that DSN string of PTOpen() is ignored in SimpleCommLib and thus cannot be used for
opened devices specification. Instead, there is set a list of valid VIDs and PIDs in defines.h file. 

Device File Permissions
-----------------------
Most modern Linux distributions use udev system to manage device special files. When supported by Linux kernel and configured
properly, the system then automatically creates ad-hoc new special files for any device, which user plugs into system.
By default udev assigns file permissions which allow only root to use the device properly as a security measure. Usually, this
is not the desired behavior for fingerprint readers. In order to allow other users to use the device, some udev rules should be
added into udev configuration - valid for Linux kernels version 2.6.26 and above.The rules below are an example, allowing users
belonging to the group plugdev to use certain fingerprint devices. You may need to use other group (or create a group of that
name, and add intended users into the group), or adapt the rules in other way in order to fit in your Linux system properly, and
to meet your requirements.

# Rules enabling to use Upek fingerprint devices by members of group 'plugdev':
SYSFS{idVendor}=="0483", SYSFS{idProduct}=="2015",  SYMLINK+="input/touchchip-%k", MODE="0660", GROUP="plugdev"
SYSFS{idVendor}=="0483", SYSFS{idProduct}=="2016",  SYMLINK+="input/touchchip-%k", MODE="0660", GROUP="plugdev"
SYSFS{idVendor}=="147e", SYSFS{idProduct}=="2015",  SYMLINK+="input/touchchip-%k", MODE="0660", GROUP="plugdev"
SYSFS{idVendor}=="147e", SYSFS{idProduct}=="2016",  SYMLINK+="input/touchchip-%k", MODE="0660", GROUP="plugdev"
SYSFS{idVendor}=="147e", SYSFS{idProduct}=="1000",  SYMLINK+="input/touchchip-%k", MODE="0660", GROUP="plugdev"
SYSFS{idVendor}=="147e", SYSFS{idProduct}=="1001",  SYMLINK+="input/touchchip-%k", MODE="0660", GROUP="plugdev"
SYSFS{idVendor}=="147e", SYSFS{idProduct}=="1002",  SYMLINK+="input/touchchip-%k", MODE="0660", GROUP="plugdev"
SYSFS{idVendor}=="147e", SYSFS{idProduct}=="5002",  SYMLINK+="input/touchchip-%k", MODE="0660", GROUP="plugdev"
SYSFS{idVendor}=="147e", SYSFS{idProduct}=="1003",  SYMLINK+="input/touchchip-%k", MODE="0660", GROUP="plugdev"
SYSFS{idVendor}=="147e", SYSFS{idProduct}=="5003",  SYMLINK+="input/touchchip-%k", MODE="0660", GROUP="plugdev"
SYSFS{idVendor}=="147e", SYSFS{idProduct}=="3000",  SYMLINK+="input/touchchip-%k", MODE="0660", GROUP="plugdev"
SYSFS{idVendor}=="147e", SYSFS{idProduct}=="3001",  SYMLINK+="input/touchchip-%k", MODE="0660", GROUP="plugdev"

For Linux kernels older than 2.6.26 it is necessary to setup access permissions for both fingerprint devices, and USB hubs.

BUS=="usb", MODE="0666"

These rules should be added in a file in appropriate place (usually in /etc/udev/rules.d or in /lib/udev/rules.d, depending
on your Linux distribution). You may create a new file there, e.g. 60-touchchip.rules, or add the rules in already existing
file, if appropriate. After changing the rules you may need to run udevadm control --reload-rules and reconnect fingerprint
device or restart udev system in order to apply the new rules.

Development environment
-----------------------
Package "usb-development" must be installed on development workstation, library source code depends on "usb.h" header file
installed with this package.