/**
 * @file timer.c
 *
 * Timer-related support functions, needed by the ESS/TFM comm library.
 * This file has to be replaced by the real implementation for given target HW platform.
 * This one concrete implementation is for Windows.
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#include "comm.h"
#include <unistd.h>
#include <sys/time.h>
#include <time.h>


//========================================================================
//		Exported functions
//========================================================================

/**
 *	Number of milliseconds since startup
 */
uint32  PT_TimerGetMilliseconds (void)
{
    struct timespec ts;    
    clock_gettime(CLOCK_REALTIME, &ts);
    return (uint32)(ts.tv_sec * 1000 + ts.tv_nsec / (1000 * 1000));
}

/**
*	Wait defined number of microseconds
*/

COMM_STATUS PT_TimerDelayMicroseconds(uint32 dwMicroseconds)
{	
    if (dwMicroseconds) 
	{
		usleep (dwMicroseconds);
	}
    return COMM_STATUS_OK;
}

/**
 *	Wait defined number of milliseconds
 */
COMM_STATUS PT_TimerDelayMilliseconds (uint32 dwMilliseconds)
{
	if (dwMilliseconds) 
	{
		usleep (1000 * dwMilliseconds);
	}
    return COMM_STATUS_OK;
}

/**
 * Check timer status
 */
COMM_STATUS PT_TimerGetStatus(void)
{
    return COMM_STATUS_OK;
}




