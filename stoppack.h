/**
 * @file token.c
 * This file turns packing of structures off.  (That is, it enables
 * automatic alignment of structure fields.)  An include file is needed
 * because various compilers do this in different ways.
 *
 * stoppack.h is the complement to strpack?.h.  An inclusion of stoppack.h
 * MUST ALWAYS be preceded by an inclusion of one of strpack?.h, in one-to-one
 * correspondence.
 *
 * @author Petr Kostka <petr.kostka@st.com>
 */

#if ! (defined(RC_INVOKED))
#if !defined(STRPACK_H_ACTIVE)
#error Missing #include "strpack1.h" prior to #include "stoppack.h"
#else
#undef STRPACK_H_ACTIVE
#endif

#if defined(PACKED_PRE) && !defined(__CYGWIN__)
#undef PACKED_PRE
#endif

#if defined(PACKED_POST) && !defined(__CYGWIN__)
#undef PACKED_POST
#endif

#if defined(PACKED) && !defined(__CYGWIN__)
#undef PACKED
#endif

#if defined(__CW_FREESCALE_HC12__)
#pragma align on
#else
#if defined(_MSC_VER)
#if defined(_WIN16) || defined(_DOS)
#pragma pack()
#else
#pragma pack(pop)
#endif
#elif defined(__MWERKS__) // #if defined(_MSC_VER)
#pragma pack(pop)
#elif defined(_ARC) 
#pragma pack();
#elif defined(__arm) // #elif defined(_ARC)
#elif defined(__ARMCC__) 
#endif // #elif defined(__arm)
#endif // #if defined(__CW_FREESCALE_HC12__)
#endif
