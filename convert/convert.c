/**
 * @file convert.c
 *
 * PTConvertTemplateEx PTAPI command.
 *
 * Copyright (C) 2010-2011 AuthenTec Inc.
 */

#include "types.h"
#include "tfmtypes.h"
#include "endian.h"
#include "tfmapi.h"
#include "tfmerror.h"
#include "convert.h"
#include "comm.h"
#include "clcodes.h"
#include "clbio.h"
#include "utils.h"
#include "errcodes.h"
#include "timer.h"
#include "defines.h"

/*------------ Externals ----------------------------------*/

// Macro for aligning value to next nearest DWORD boundary
#define DW_ALIGN(value) ((uint32)(value) + 3 & 0xfffffffc)
#define ALIGN_4(value) (((unsigned long long)(value) + 3) & ~3)
#if defined (TARGET_STANDARD_API)

// Indication of PTInitialize() call
extern PT_BOOL apiInitialized;
//The session context block
extern PT_SESSION gSession;

void* MallocCopy(PT_DWORD dwSize, void *pSrcData);
#endif


PT_STATUS PT_SimpleTransact(
                            IN PT_SESSION *pS,
                            IN PT_DWORD dwControlCode,
                            IN PT_DWORD dwSendSize,
                            OUT PT_DWORD *pdwRecvSize,
                            IN PT_DWORD dwFlags
                            );

PT_STATUS PT_FirstTransact(
    IN PT_SESSION *pS,
    IN PT_DWORD dwControlCode,
    IN PT_DWORD dwSendFragmentSize,
    IN PT_DWORD dwSendTotalSize,
    IN PT_DWORD dwFlags);

PT_STATUS PT_MiddleTransact(
    IN PT_SESSION *pS,
    IN PT_DWORD dwSendFragmentSize,
    IN PT_DWORD dwFlags);

PT_STATUS PT_LastTransact(
    IN PT_SESSION *pS,
    IN PT_DWORD dwSendFragmentSize,
    OUT PT_DWORD *pdwRecvSize,
    IN PT_DWORD dwFlags);
bool8 PT_FitsSentDataToFragment(
    IN uint32 dwSendSize,
    IN uint8 byFragmentType,
    OUT uint32 *pdwMaxTransferSize
);

/*------------ Commands ------------------------------------*/

#if defined(TCD50D_TCS1_TCS2)
#if !defined(TBX)

/* 
The function fills the destination buffer with two possible source buffers.
The function assumes DestLength is calculated by caller carefully.
The caller should make sure source 1 and source 2 are not overlaied.
If pSrc2Buf is NULL, it means there is only 1 source buffer.

Caution: The data in pDestBuf must be in little endian.
*/
static PT_STATUS FillDestBuffer(uint8 *pDestBuf, 
                            uint32 DestLength, 
                            PT_DATA *pSrc1Buf,
                            PT_DATA *pSrc2Buf,
                            uint8 **ppSrcPos)
{
    uint8 *pSrcPos;

    if (ppSrcPos == NULL || pDestBuf == NULL || pSrc1Buf == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    pSrcPos = *ppSrcPos;

    if (pSrcPos == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }
    //If only one source buffer, just copy
    if (pSrc2Buf == NULL || pSrc2Buf->Length == 0)
    {
        if (DestLength > pSrc1Buf->Data + pSrc1Buf->Length - pSrcPos)
        {
            DestLength = pSrc1Buf->Data + pSrc1Buf->Length -pSrcPos;
        }
        PT_memmove(pDestBuf, pSrcPos, DestLength);
        pSrcPos += DestLength;
        *ppSrcPos = pSrcPos;
        return PT_STATUS_OK;

    }

    //Now the position of source pointer is in source 1 buffer
    if (pSrcPos >= (uint8 *)pSrc1Buf && pSrcPos < pSrc1Buf->Data + pSrc1Buf->Length)
    {
        uint32 Src1RemainingLen = pSrc1Buf->Data + pSrc1Buf->Length - pSrcPos;

        if (Src1RemainingLen >= DestLength)
        {
            PT_memmove(pDestBuf, pSrcPos, DestLength);
            pSrcPos += DestLength;
            if (pSrcPos == pSrc1Buf->Data + pSrc1Buf->Length)
            {
                //Adjust the current position of pointer to source 2 buffer
                pSrcPos = (uint8 *)pSrc2Buf;
            }
        }
        else
        {
            uint32 alignBytes;
        
            //We need copy two parts to destination buffer
            PT_memmove(pDestBuf, pSrcPos, Src1RemainingLen);
            //we have to care about the alignment requirement for source buffer 2.
            alignBytes = (0 -  pSrc1Buf->Length) & 0x03;
            PT_memmove(pDestBuf + Src1RemainingLen + alignBytes, (uint8 *)pSrc2Buf, DestLength - alignBytes - Src1RemainingLen);
            pSrcPos = ((uint8 *)pSrc2Buf) + (DestLength - alignBytes - Src1RemainingLen);
        }
    }
    //Now the position of source pointer is in source 2 buffer
    else if (pSrcPos >= (uint8 *)pSrc2Buf && pSrcPos < pSrc2Buf->Data + pSrc2Buf->Length)
    {
        PT_memmove(pDestBuf, pSrcPos, DestLength);
        pSrcPos += DestLength;
    }
    else
    {   //wrong position
        return PT_STATUS_INVALID_PARAMETER;
    }
    //write the position of pointer
    *ppSrcPos = pSrcPos;
    return PT_STATUS_OK;
}

PTAPI_DLL PT_STATUS PTAPI PTConvertTemplateEx(
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwSourceType,
    IN PT_DWORD dwSourceEnvelope,
    IN PT_DATA *pInputData,
    IN PT_DWORD dwTargetType,
    IN PT_DWORD dwTargetEnvelope,
    IN PT_DATA *pSupplementaryData,
    IN PT_DWORD dwFlags,
    OUT PT_DATA **ppOutputData)
{
    typedef struct request {
        CL_TAG tag;
        uint32 sourceType;
        uint32 sourceEnvelope;
        uint32 targetType;
        uint32 targetEnvelope;
        uint32 flags;
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        uint32 dataLen;         // size of returned data
    } Response;

    PT_STATUS status;
    PT_SESSION *pS = &gSession;
    uint32 sendSize, recvSize;
    uint8 *pPos;

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || pInputData == NULL || ppOutputData == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }

    // size of packet
    sendSize = sizeof(Request);
    sendSize += sizeof(PT_DWORD) + pInputData->Length; // add length of input data
    sendSize = DW_ALIGN(sendSize);
    sendSize += sizeof(PT_DWORD); // add length of supplementary data
    if (pSupplementaryData && pSupplementaryData->Length)
    {
        sendSize += pSupplementaryData->Length;
    }


    // prepare Request structure
    ((Request*)pS->pBuf)->sourceType = HTOL32(dwSourceType);
    ((Request*)pS->pBuf)->sourceEnvelope = HTOL32(dwSourceEnvelope);
    ((Request*)pS->pBuf)->targetType = HTOL32(dwTargetType);
    ((Request*)pS->pBuf)->targetEnvelope = HTOL32(dwTargetEnvelope);
    ((Request*)pS->pBuf)->flags = HTOL32(dwFlags);

    if (PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        //send data in a single packet
        *(PT_DWORD *)(pS->pBuf + sizeof(Request)) = HTOL32(pInputData->Length);
        PT_memmove(
            pS->pBuf + sizeof(Request) + sizeof(PT_DWORD), 
            pInputData->Data, 
            pInputData->Length); // copy input data
        pPos = (uint8 *)ALIGN_4(pS->pBuf + sizeof(Request) + sizeof(PT_DWORD) + pInputData->Length);
        if (pSupplementaryData) // copy supplementary data (if any)
        {
            *(PT_DWORD *)pPos = HTOL32(pSupplementaryData->Length);
            PT_memmove(
                pPos + sizeof(PT_DWORD), 
                pSupplementaryData->Data, 
                pSupplementaryData->Length); //copy data           
        }
        else
        {
            PT_memset(pPos, 0, sizeof(PT_DWORD));
        }

    
        // transact data
        status = PT_SimpleTransact(pS, CL_COMMAND_BIO_CONVERT_TEMPLATE_EX, sendSize, &recvSize, 0);
    }
    else
    {
        uint32 nextTransferSize;
        uint8 *pSrcPos;
        
        // Get next transfer size
        PT_FitsSentDataToFragment(sendSize, TLTYPE_FIRST, &nextTransferSize);
        pSrcPos = (uint8 *)pInputData;
        // copy input data into communication buffer
        FillDestBuffer(pS->pBuf + sizeof(Request), 
                   nextTransferSize - sizeof(Request),
                   pInputData,
                   pSupplementaryData,
                   &pSrcPos
                   );

        // transfer header and first data part
        status = PT_FirstTransact(pS, CL_COMMAND_BIO_CONVERT_TEMPLATE_EX, nextTransferSize, sendSize, 0);
        sendSize -= nextTransferSize;

        while(status == PT_STATUS_OK)
        {
            if(!PT_FitsSentDataToFragment(sendSize, TLTYPE_LAST, NULL))
            {
                // Get next transfer size
                PT_FitsSentDataToFragment(sendSize, TLTYPE_MIDDLE, &nextTransferSize);

                // copy input data into communication buffer
                FillDestBuffer(pS->pBuf, 
                           nextTransferSize,
                           pInputData,
                           pSupplementaryData,
                           &pSrcPos
                           );
        
                // transfer
                status = PT_MiddleTransact(pS, nextTransferSize, 0);
                sendSize -= nextTransferSize;
             } else {
                 // copy input data into communication buffer
                 FillDestBuffer(pS->pBuf, 
                               sendSize,
                               pInputData,
                               pSupplementaryData,
                               &pSrcPos
                               );

                 if (pSupplementaryData == NULL || pSupplementaryData->Length == 0)
                 {
                     //set the length of Supplementary Data to 0
                     //the alignment was condidered when we calculated sendSize
                     pPos = &pS->pBuf[sendSize - 4];
                     PT_memset(pPos, 0, sizeof(PT_DWORD));
                 }
             
                 // transfer
                 status = PT_LastTransact(pS, sendSize, (uint32 *)&recvSize, 0);
                 break;
             }
        }
           	
    }

    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response) ||
            recvSize < sizeof(Response) + LTOH32(((Response*)pS->pBuf)->dataLen))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        *ppOutputData = (PT_DATA*)(pS->pBuf + offsetof(Response, dataLen));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
        (*ppOutputData)->Length = LTOH32((*ppOutputData)->Length);
#endif

#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        if ((*ppOutputData = (PT_DATA*)MallocCopy((*ppOutputData)->Length + sizeof(PT_DWORD), *ppOutputData)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
#endif
    }

    return status;
}

#endif // #if !defined(TBX)
#endif // #if defined(TCD50D_TCS1_TCS2)
