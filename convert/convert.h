/**
 * @file convert.h
 *
 * PTConvertTemplateEx PTAPI command.
 *
 * Copyright (C) 2010 UPEK Inc.
 */

#ifndef convert_h
#define convert_h

#include "tfmtypes.h"

/** 
 * Template types
 */
//@{
#define PT_TEMPLATE_TYPE_LEGACY             0       ///< Legacy template type is maintained for the purpose of compatibility with older devices, where it was the only template type supported. This template type should be considered obsolete.
#define PT_TEMPLATE_TYPE_ALPHA              1       ///< The alpha template type contains the minimum sufficient set of biometric information needed for a reasonable biometric performance and represents the best trade-off between template size and biometric performance. Its content is intended as "core" information that would be included in any future template type and every future version of UPEK biometric systems has to be able to process alpha type content.
#define PT_TEMPLATE_TYPE_BETA               2       ///< The beta template type is an extension to the alpha type. Additionally it contains optional block(s) of additional biometric information. Beta templates are in general able to achieve better biometric performance, but at the cost of an increased template size. The full benefit of the beta template can be applied only if it is matched against another beta template.
#define PT_TEMPLATE_TYPE_ALPHA_MULTI        3       ///< This type represents a multi-template extension of alpha template type. It is intended and recommended for use as an enrollment template, where multiple views of the fingerprint (alpha templates), which are captured during the enrollment process, are stored together within a single data structure. This allows the highest possible biometric performance, but at the cost of a higher template size and a higher matching time.
#define PT_TEMPLATE_TYPE_ANSI               16      ///< ANSI_INCITS-378-2004 template (currently can be used only as a parameter for PTConvertTemplateEx).
#define PT_TEMPLATE_TYPE_ISO_FMR            17      ///< ISO/IEC 19794-2 FMR template (currently can be used only as a parameter for PTConvertTemplateEx).
#define PT_TEMPLATE_TYPE_ISO_FMC_NORMAL     18      ///< ISO/IEC 19794-2 normal FMC template (currently can be used only as a parameter for PTConvertTemplateEx).
#define PT_TEMPLATE_TYPE_ISO_FMC_COMPACT    19      ///< ISO/IEC 19794-2 compact FMC template (currently can be used only as a parameter for PTConvertTemplateEx).
#define PT_TEMPLATE_TYPE_AUTO               0xffffffff    ///< Automatic or default template type (can be used only as a parameter for PTConvertTemplateEx).
//@}

/** 
 * Template envelope types
 */
//@{
#define PT_TEMPLATE_ENVELOPE_NONE           0       ///< No template envelope (template in raw format).
#define PT_TEMPLATE_ENVELOPE_PTBIR          1       ///< PT_BIR template envelope.
//@}

/** 
 * Flags for PTConvertTemplateEx
 */
//@{
#define PT_CONVTEMPL_FLAG_KEEP_TEMPLATE_TYPE    0x00000001    ///< If set then output template type is the same as the source template type (parameter dwTargetType is ignored). This flag is currently ignored.
#define PT_CONVTEMPL_FLAG_RETURN_PAYLOAD        0x00000002    ///< If set then output template contains payload copied from the input template, otherwise output template is returned without payload. This flag is currently ignored.
//@}

/** 
 * Convert template to specified format including standard ANSI/ISO formats.
 * 
 * @param hConnection Handle to the connection to FM.
 * @param dwSourceType Source format type (see PT_TEMPLATE_TYPE_xxxx). If PT_TEMPLATE_TYPE_AUTO is used then source template type is detected automatically.
 * @param dwSourceEnvelope Source template envelope (see PT_TEMPLATE_ENVELOPE_xxxx).
 * @param pInputData Source template data.
 * @param dwTargetType Target template type (see PT_TEMPLATE_TYPE_xxxx).If PT_TEMPLATE_TYPE_AUTO is used then target template type is chosen automatically.
 * @param dwTargetEnvelope Target template envelope (see PT_TEMPLATE_ENVELOPE_xxxx).
 * @param pSupplementaryData Reserved for future use, set to NULL.
 * @param dwFlags Additional flags (see PT_CONVTEMPL_FLAG_xxxx).
 * @param ppOutputData Address of the pointer, which will be set to point to the converted template in the target format. The template has to be discarded by a call to PTFree().
 * @return Status code.
 */
PTAPI_DLL PT_STATUS PTAPI PTConvertTemplateEx(
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwSourceType,
    IN PT_DWORD dwSourceEnvelope,
    IN PT_DATA *pInputData,
    IN PT_DWORD dwTargetType,
    IN PT_DWORD dwTargetEnvelope,
    IN PT_DATA *pSupplementaryData,
    IN PT_DWORD dwFlags,
    OUT PT_DATA **ppOutputData
);

#endif /* convert_h */
