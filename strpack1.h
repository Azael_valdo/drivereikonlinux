/**
 * @file strpack.h
 * This file turns 1 byte packing of structures on.  (That is, it disables
 * automatic alignment of structure fields.)  An include file is needed
 * because various compilers do this in different ways.
 *
 * The file stoppack.h is the complement to this file.
 *
 * @author Petr Kostka <petr.kostka@st.com>
 */

#if ! (defined(RC_INVOKED))
#if !defined(STRPACK_H_ACTIVE)
#define STRPACK_H_ACTIVE
#else
#error Cannot nest "#include "strpack1.h""
#endif
#if defined(PACKED) && !defined(__CYGWIN__)
#error Macro PACKED can be defined only inside of strpack1.h
#endif

#if defined(_MSC_VER)
#pragma warning(disable:4103)

#if defined(_WIN16) || defined(_DOS)
#pragma pack(1)
#else
#pragma pack(push, 1)
#endif

#elif defined(__CW_FREESCALE_HC12__)
#pragma align off

#elif defined(__MWERKS__)  // #if defined(_MSC_VER)
#pragma pack(push, 1)

#elif defined(_ARC)        // #elif defined(__MWERKS__)
pragma Pack(1);

#elif defined(__arm)       // #elif defined(_ARC)

#if defined(__GNUC__)
#define PACKED_POST __attribute__((packed)) 
#else
#define PACKED_PRE __packed
#endif

#elif defined(__ARMCC__)   // #elif defined(_arm)

#if defined(__GNUC__)
#define PACKED_POST __attribute__((packed)) 
#else
#define PACKED_PRE __packed
#endif

#elif defined(__GNUC__)
#define PACKED_POST __attribute__((packed)) 
#define PACKED_PRE 

#endif                     

#ifndef PACKED_POST
#define PACKED_POST
#endif

#ifndef PACKED_PRE
#define PACKED_PRE
#endif

#define PACKED PACKED_PRE

#endif // ! (defined(lint) || defined(_lint) || defined(RC_INVOKED))
