/**
 * @file utils.h
 *
 * Utility functions, e.g. things from <string.h>, which are not available in EPOC
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#ifndef __UTILS_H__
#define __UTILS_H__

#include    "types.h"

#define min(a, b)  (((a) < (b)) ? (a) : (b)) 
#define max(a, b)  (((a) > (b)) ? (a) : (b))

//========================================================================
//      Exported functions
//========================================================================

/**
 *	Copy data block
 */
void*	PT_memcpy (void *dst, const void *src, uint32 size);


/**
 *	Move data block - source and destination may overlap
 */
void*	PT_memmove (void *dst, const void *src, uint32 size);


/**
 *	Fill in data block
 */
void*	PT_memset (void *dst, uint8 fillByte, uint32 size);

/**
 * Compare block of memory
 */
sint32  PT_memcmp(const void *first, const void *second, uint32 size);

/**
 * Compare two zero terminated strings.
 */
sint32  PT_strcmp(const char *first, const char *second);


#endif	// __UTILS_H__
