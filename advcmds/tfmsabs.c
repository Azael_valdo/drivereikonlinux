/**
 * @file tfmsabs.c
 *
 * Implementation of SAB commands for simplified ESS/TFM API library
 *
 * Copyright (C) 2001-2011 Authentec Inc.
 */

#include "types.h"
#include "tfmtypes.h"
#include "endian.h"
#include "tfmsabs.h"
#include "tfmerror.h"
#include "errcodes.h"
#include "clcodes.h"
#include "comm.h"
#include "utils.h"
#include <string.h>
#include "Tdbg.h"

TDBG_DEFINE_AREA(tfmsabs)

// Macro for aligning value to next nearest DWORD boundary
#define DW_ALIGN(value) ((uint32)(((value) + 3) & 0xfffffffc))


/*------------ Externals ----------------------------------*/

/**
 * The session context block.
 * In this implementation we allow only one session, so we can allocate it statically
 */
extern PT_SESSION gSession;


#if defined (TARGET_STANDARD_API)

// Indication of PTInitialize() call
extern PT_BOOL apiInitialized;

void* MallocCopy(PT_DWORD dwSize, void *pSrcData);
#endif


PT_STATUS PT_SimpleTransact(
    IN PT_SESSION *pS,
    IN PT_DWORD dwControlCode,
    IN PT_DWORD dwSendSize,
    OUT PT_DWORD *pdwRecvSize,
    IN PT_DWORD dwFlags
);


//----- Local helper functions ---------------------------------

static PT_STATUS TfmCheckConnHandle(IN PT_CONNECTION hConnection)
{
#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
    {
        return PT_STATUS_API_NOT_INIT;
    }
#endif    
    // check validity of input parameters
    if (!hConnection)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }
    return PT_STATUS_OK;
}

/**
 * Return transport length of input SECRET, also validity of input SECRET is checked.
 * pInputSecret can be NULL.
 */
static PT_STATUS GetInputSecretLength(IN PT_INPUT_SECRET *pInputSecret, OUT PT_DWORD *pdwInputSecretLength)
{
    PT_STATUS status = PT_STATUS_OK;

    if (pInputSecret != NULL)
    {
        switch (pInputSecret->dwForm)
        {
        case PT_FULL_SECRET:
            *pdwInputSecretLength = 2*sizeof(uint32) + pInputSecret->InputSecret.pSecret->Length; // length of form + secret
            break;

        case PT_REF_SECRET:
            *pdwInputSecretLength = 2*sizeof(uint32); // length of form + tag
            break;

        default :
            status = PT_STATUS_INVALID_PARAMETER;
        }
    }
    else
    {
        // secret with zero length will be send
        *pdwInputSecretLength = 2*sizeof(uint32);
    }
    return status;
}

/**
 * Copy input SECRET to given location and convert endians from host to little.
 * Returns pointer behind written data.
 * pInputSecret can be NULL.
 */
static PT_BYTE* CopyInputSecret(IN PT_BYTE *pBuffer, IN PT_INPUT_SECRET *pInputSecret)
{
    if (pInputSecret != NULL)
    {
        *(uint32*)pBuffer = HTOL32((uint32)pInputSecret->dwForm); // write form
        pBuffer += sizeof(uint32);
        switch (pInputSecret->dwForm)
        {
        case PT_FULL_SECRET:
            *(uint32*)pBuffer = HTOL32(pInputSecret->InputSecret.pSecret->Length);
            pBuffer += sizeof(uint32);
            PT_memcpy(pBuffer, pInputSecret->InputSecret.pSecret->Data, pInputSecret->InputSecret.pSecret->Length); // write secret
            pBuffer += pInputSecret->InputSecret.pSecret->Length;
            break;

        case PT_REF_SECRET:
            *(sint32*)pBuffer = HTOL32(pInputSecret->InputSecret.dwDataTag); // write tag
            pBuffer += sizeof(uint32);
            break;

        default :
            TASSERTF(); // invalid secret form
        }
    }
    else
    {
        // secret with zero length will be send
        *(uint32*)pBuffer = HTOL32(PT_FULL_SECRET); 
        pBuffer += sizeof(uint32);
        *(uint32*)pBuffer = 0; // zero length
        pBuffer += sizeof(uint32);
    }
    return pBuffer; // return pointer behind written input secret
}




/**
 * Return transport length of input SAB, also validity of input SAB is checked.
 */
static PT_STATUS GetInputSabLength(IN PT_INPUT_SAB *pInputSab, OUT PT_DWORD *pdwInputSabLength)
{
    PT_STATUS status = PT_STATUS_OK;
    TASSERT(pInputSab != NULL);

    switch (pInputSab->dwForm)
    {
    case PT_WRAPPED_SAB:
        *pdwInputSabLength = 2*sizeof(uint32) + pInputSab->InputSab.pWrappedSab->Length; // length of form + SAB
        break;

    case PT_SLOT_SAB:
        *pdwInputSabLength = sizeof(uint32) + sizeof(sint32); // length of form + slot
        break;

    default :
        status = PT_STATUS_INVALID_PARAMETER;
    }
    return status;
}

/**
 * Copy input SAB to given location and converts its endians from host 
 * to little. Returns pointer behind written data.
 */
static PT_BYTE* CopyInputSab(IN PT_BYTE *pBuffer, IN PT_INPUT_SAB *pInputSab)
{
    TASSERT(pInputSab != NULL);

    *(uint32*)pBuffer = HTOL32((uint32)pInputSab->dwForm); // write form
    pBuffer += sizeof(uint32);
    switch (pInputSab->dwForm)
    {
    case PT_WRAPPED_SAB:
        *(uint32*)pBuffer = HTOL32(pInputSab->InputSab.pWrappedSab->Length);
        pBuffer += sizeof(uint32);
        PT_memcpy(pBuffer, pInputSab->InputSab.pWrappedSab->Data, pInputSab->InputSab.pWrappedSab->Length); // write SAB
        pBuffer += pInputSab->InputSab.pWrappedSab->Length;
        break;

    case PT_SLOT_SAB:
        *(sint32*)pBuffer = HTOL32(pInputSab->InputSab.lSlotNr); // write slot
        pBuffer += sizeof(uint32);
        break;

    default :
        TASSERTF(); // invalid SAB form
    }
    return pBuffer; // return pointer behind written input SAB
}


//----- Miscellaneous PTAPI functions -------------------------------------

PTAPI_DLL PT_STATUS PTAPI PTGetSabData(IN PT_CONNECTION hConnection,IN PT_INPUT_SAB *pInputSab,
    IN PT_INPUT_SECRET *pSecret,IN PT_DWORD dwTag,OUT PT_DATA **ppSabData)
{
    PT_STATUS status = PT_STATUS_OK;
    PT_DWORD sendLen, retLen, tmpLen;
    PT_BYTE *pSendData, *pPos;
    PT_SESSION *pS = &gSession;
    TTRACE_ENTER(tfmsabs, TDBG_TRACE, ("PTGetSabData(%ld,%p,%p,%ld,%p)",(long)(hConnection), pInputSab, pSecret, (long)(dwTag), ppSabData));

    // check API initialization and connection handle
    if ((status = TfmCheckConnHandle(hConnection)) != PT_STATUS_OK)
    {
        goto endfunc;
    }
    // check validity of input parameters
    if (pInputSab == NULL || ppSabData == NULL)
    {
        status = PT_STATUS_INVALID_PARAMETER;
        goto endfunc;
    }

    // compute and check length of sending data
    sendLen = 2*sizeof(uint32); // size of flags + tag
    if ((status = GetInputSabLength(pInputSab, &tmpLen)) != PT_STATUS_OK)
    {
        goto endfunc;
    }
    sendLen += tmpLen; // add length of SAB
    sendLen = DW_ALIGN(sendLen); // align length to DWORD boundary
    if ((status = GetInputSecretLength(pSecret, &tmpLen)) != PT_STATUS_OK)
    {
        goto endfunc;
    }
    sendLen += tmpLen; // add length of secret

    // fill in sending data
    pPos = pSendData = pS->pBuf;
    pPos += sizeof(CL_TAG);sendLen += sizeof(CL_TAG);
    *(uint32*)pPos = HTOL32(dwTag); // tag (aligned in the buffer)
    pPos += sizeof(uint32);
    *(uint32*)pPos = HTOL32(0); // flags (aligned in the buffer)
    pPos += sizeof(uint32);
    pPos = CopyInputSab(pPos, pInputSab); // SAB (aligned in the buffer)
    pPos = pSendData + DW_ALIGN(pPos - pSendData); // align to DWORD boundary inside buffer
    pPos = CopyInputSecret(pPos, pSecret); // secret

    
    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_MISC_GET_SAB_DATA, sendLen, &retLen, 0);
    if (status != PT_STATUS_OK)
    {
        goto endfunc;
    }

    // get returned data
    *ppSabData = (PT_DATA*)(pS->pBuf + sizeof(uint32));
    (*ppSabData)->Length = LTOH32((*ppSabData)->Length);

#if defined (TARGET_STANDARD_API)
    // allocate buffer for output parameter and copy data into it
    if ((*ppSabData = (PT_DATA*)MallocCopy((*ppSabData)->Length + sizeof(PT_DATA), *ppSabData)) == NULL)
    {
        status = PT_STATUS_MALLOC_FAILED;
    }
#endif

endfunc:;
    TTRACE_LEAVE(("PTGetSabData() -> %ld", (long)(status)));
    return status;
}


PTAPI_DLL PT_STATUS PTAPI PTUseSabData(IN PT_CONNECTION hConnection,IN PT_INPUT_SAB *pInputSab,IN PT_INPUT_SECRET *pSecret,
    IN PT_DWORD dwTag,IN PT_DWORD dwOperation,IN PT_DATA *pInputData,OUT PT_DATA **ppOutputData)
{
    PT_STATUS status = PT_STATUS_OK;
    PT_DWORD sendLen, retLen, tmpLen;
    PT_BYTE *pSendData, *pPos;
    PT_SESSION *pS = &gSession;
    TTRACE_ENTER(tfmsabs, TDBG_TRACE, ("PTUseSabData(%ld,%p,%p,%lu,%lu,%p,%p)",(long)(hConnection), pInputSab, pSecret, (long)(dwTag), (long)(dwOperation), pInputData, ppOutputData));

    // check API initialization and connection handle
    if ((status = TfmCheckConnHandle(hConnection)) != PT_STATUS_OK)
    {
        goto endfunc;
    }
    // check validity of input parameters
    if (pInputSab == NULL || pInputData == NULL || ppOutputData == NULL)
    {
        status = PT_STATUS_INVALID_PARAMETER;
        goto endfunc;
    }

    // compute length of sending data
    sendLen = 3*sizeof(uint32); // size of Tag + operation + flags
    if ((status = GetInputSabLength(pInputSab, &tmpLen)) != PT_STATUS_OK)
    {
        goto endfunc;
    }
    sendLen += tmpLen; // add length of SAB
    sendLen = DW_ALIGN(sendLen); // align length to DWORD boundary
    if ((status = GetInputSecretLength(pSecret, &tmpLen)) != PT_STATUS_OK)
    {
        goto endfunc;
    }
    sendLen += tmpLen; // add length of secret
    sendLen = DW_ALIGN(sendLen); // align length to DWORD boundary
    sendLen += sizeof(PT_DWORD) + pInputData->Length; // add length of inputData

    // fill in sending data
    pPos = pSendData = pS->pBuf;
    pPos += sizeof(CL_TAG);sendLen += sizeof(CL_TAG);
    *(uint32*)pPos = HTOL32(dwTag); // dwTag (aligned in the buffer)
    pPos += sizeof(uint32);
    *(uint32*)pPos = HTOL32(dwOperation); // dwOperation (aligned in the buffer)
    pPos += sizeof(sint32);
    *(uint32*)pPos = HTOL32(0);     // flags (aligned in the buffer)
    pPos += sizeof(uint32);
    pPos = CopyInputSab(pPos, pInputSab); // add SAB (aligned in the buffer)
    pPos = pSendData + DW_ALIGN(pPos - pSendData); // align to DWORD boundary
    pPos = CopyInputSecret(pPos, pSecret); // add secret
    pPos = pSendData + DW_ALIGN(pPos - pSendData); // align to DWORD boundary
    *(uint32*)pPos =  HTOL32(pInputData->Length);
    pPos += sizeof(uint32);
    PT_memcpy(pPos, pInputData->Data, pInputData->Length);

    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_MISC_USE_SAB_DATA, sendLen, &retLen, 0);
    if (status != PT_STATUS_OK)
    {
        goto endfunc;
    }

    // get returned data
    *ppOutputData = (PT_DATA*)(pS->pBuf + sizeof(uint32));
    (*ppOutputData)->Length = LTOH32((*ppOutputData)->Length);

#if defined (TARGET_STANDARD_API)
    // allocate buffer for output parameter and copy data into it
    if ((*ppOutputData = (PT_DATA*)MallocCopy((*ppOutputData)->Length + sizeof(PT_DATA), *ppOutputData)) == NULL)
    {
        status = PT_STATUS_MALLOC_FAILED;
    }
#endif


endfunc:;
    TTRACE_LEAVE(("PTUseSabData() -> %ld", (long)(status)));
    return status;
}

