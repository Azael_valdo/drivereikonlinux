/**
 * @file tfmsabs.h
 *
 * Declaration of SAB commands for simplified ESS/TFM API library
 *
 * Copyright (C) 2001-2011 Authentec Inc.
 */

#ifndef tfmsabs_h
#define tfmsabs_h

#include "tfmtypes.h"

/** 
 * Attribute data access rights
 */
#define PT_ATRB_GET_ALWAYS    0x00000003    ///< The attribute data can be read out from FM any time
#define PT_ATRB_GET_ON_MATCH    0x00000001    ///< Finger data: Data can be read only from last matched data slot.
#define PT_ATRB_USE_ALWAYS    0x0000000c    ///< Data can be used any time.
#define PT_ATRB_USE_ON_MATCH    0x00000004    ///< Finger data: Data can be used only from last matched data slot.
#define PT_ATRB_COPY_ALWAYS    0x00000030    ///< The attribute data can be copied internally inside FM any time
#define PT_ATRB_COPY_ON_MATCH    0x00000010    ///< Finger data: Data can be copied only from last matched data slot.

/** 
 * Attribute data operation access rights
 */
#define PT_ATRB_USE_TYPE_MASK    0xFF000000    ///< Attribute operation bit mask.
#define PT_ATRB_USE_TYPE_HMAC    0x00000000    ///< HMAC-SHA-1 operations.
#define PT_ATRB_USE_TYPE_RSA    0x01000000    ///< RSA operations.
#define PT_ATRB_USE_TYPE_HOTP    0x02000000    ///< HOTP operations.
#define PT_ATRB_USE_TYPE_SECURID    0x03000000    ///< SecurID operations.
#define PT_ATRB_USE_TYPE_UPEKOTP    0x04000000    ///< UPEK OTP operations.
#define PT_ATRB_USE_TYPE_HMACSHA256    0x05000000    ///< HMAC-SHA-256 operations.
#define PT_ATRB_USE_TYPE_OCRA    0x06000000    ///< OCRA operations.
#define PT_ATRB_USE_TYPE_VIPOTP    0x07000000    ///< VIP OTP operations.
#define PT_ATRB_USE_TYPE_SAB_ACCESS    0x80000000    ///< Data can be used for SAB secret verification.
#define PT_ATRB_USE_TYPE_GENERAL    0xFF000000    ///< General purpose operations.

/** 
 * Attribute data 'use' operations
 */
#define PT_ATRBOP_HMACRETURN    0x00000000    ///< Compute and return HMAC-SHA1.
#define PT_ATRBOP_HMACVERIFY    0x00000001    ///< Compute and verify HMAC-SHA1.
#define PT_ATRBOP_HMACRETURNOSAP    0x00000002    ///< Compute and return HMAC-SHA1 OSAP.
#define PT_ATRBOP_HMACVERIFYOSAP    0x00000003    ///< Compute and verify HMAC-SHA1 OSAP.
#define PT_ATRBOP_RSA_ENCRYPT_CRYPT    0x00000100    ///< RSA encrypt data.
#define PT_ATRBOP_RSA_DECRYPT_CRYPT    0x00000101    ///< RSA decrypt data.
#define PT_ATRBOP_RSA_ENCRYPT_SIGN    0x00000102    ///< RSA sign data.
#define PT_ATRBOP_RSA_DECRYPT_SIGN    0x00000103    ///< RSA decrypt-signed data.
#define PT_ATRBOP_SECURID_GENTOKENCODE    0x00000300    ///< Generate SecurId tokencode.
#define PT_ATRBOP_SECURID_GENPASSCODE    0x00000301    ///< Generate SecurId passcode.
#define PT_ATRBOP_HMACSHA256_RETURN    0x00000500    ///< Compute and return HMAC-SHA256.
#define PT_ATRBOP_VIPOTP_GETAUTH    0x00000700    ///< Generate authentication request for VIP OTP provisioning.
#define PT_ATRBOP_VIPOTP_GENOTP    0x00000701    ///< Generate VIP OTP.

/** 
 * Attribute data 'use-set' operations
 */
#define PT_ATRBOP_RSA_GENKEY    0x00010100    ///< Generate RDA key pair.
#define PT_ATRBOP_RSA_SETKEY    0x00010101    ///< Set RSA key.
#define PT_ATRBOP_HOTP_SETSEED    0x00010200    ///< Set HOTP seed.
#define PT_ATRBOP_HOTP_SECURESETSEED    0x00010201    ///< Set HOTP encrypted seed.
#define PT_ATRBOP_SECURID_SETSEED    0x00010300    ///< Set SecurId seed.
#define PT_ATRBOP_SECURID_SECURESETSEED    0x00010301    ///< Set SecurId encrypted seed.
#define PT_ATRBOP_UPEKOTP_SETSEED    0x00010400    ///< Set UPEK-OTP seed.
#define PT_ATRBOP_UPEKOTP_SECURESETSEED    0x00010401    ///< Set UPEK-OTP encrypted seed.
#define PT_ATRBOP_OCRA_SETSEED    0x00010600    ///< Set OCRA seed.
#define PT_ATRBOP_OCRA_SECURESETSEED    0x00010601    ///< Set OCRA encrypted seed.
#define PT_ATRBOP_VIPOTP_CREATE    0x00010700    ///< Create VIP OTP attribute.
#define PT_ATRBOP_GENERAL_SECURESETDATA    0x0001FF00    ///< Set encrypted data.

/** 
 * Attribute data 'use-update' operations
 */
#define PT_ATRBOP_HOTP_GENPWD    0x00020200    ///< Generate HOTP password.
#define PT_ATRBOP_HOTP_SETSEQNUM    0x00020201    ///< Set HOTP sequence number.
#define PT_ATRBOP_UPEKOTP_GENPWD    0x00020400    ///< Generate UPEK-OTP password.
#define PT_ATRBOP_UPEKOTP_SETSEQNUM    0x00020401    ///< Set UPEK-OTP sequence number.
#define PT_ATRBOP_OCRA_GENTRUNC    0x00020600    ///< Generate truncated OCRA code.
#define PT_ATRBOP_OCRA_GENFULL    0x00020601    ///< Generate full OCRA code.
#define PT_ATRBOP_OCRA_SETSEQNUM    0x00020602    ///< Set OCRA sequence number.
#define PT_ATRBOP_VIPOTP_REENCRYPT    0x00020700    ///< Reencrypt VIP OTP secret returned by provisioning server.

/** 
 * Fingerprint data access rights
 */
#define PT_FPDATA_GET_ALWAYS    PT_ATRB_GET_ALWAYS    ///< Data can be read any time.
#define PT_FPDATA_GET_ON_MATCH    PT_ATRB_GET_ON_MATCH    ///< Data can be read only from last matched data slot.
#define PT_FPDATA_USE_ALWAYS    PT_ATRB_USE_ALWAYS    ///< Data can be used any time.
#define PT_FPDATA_USE_ON_MATCH    PT_ATRB_USE_ON_MATCH    ///< Data can be used only from last matched data slot.
#define PT_FPDATA_COPY_ALWAYS    PT_ATRB_COPY_ALWAYS    ///< Data can be copied any time.
#define PT_FPDATA_COPY_ON_MATCH    PT_ATRB_COPY_ON_MATCH    ///< Data can be copied only from last matched data slot.

/** 
 * Fingerprint data operations
 */
#define PT_FPDATAOP_HMACRETURN    PT_ATRBOP_HMACRETURN    ///< Compute and return HMAC-SHA1.
#define PT_FPDATAOP_HMACVERIFY    PT_ATRBOP_HMACVERIFY    ///< Compute and verify HMAC-SHA1.
#define PT_FPDATAOP_HMACRETURNOSAP    PT_ATRBOP_HMACRETURNOSAP    ///< Compute and return HMAC-SHA1 OSAP.
#define PT_FPDATAOP_HMACVERIFYOSAP    PT_ATRBOP_HMACVERIFYOSAP    ///< Compute and verify HMAC-SHA1 OSAP.

/** 
 * A structure used to input a SAB secret to the API. Such input can be either the
 * secret data itself or reference to a finger data of last matched data.
 */
typedef struct pt_input_secret {
  PT_DWORD dwForm; 	///< Form of the input secret
  union {
        PT_DATA   *pSecret;         // Used when dwForm = PT_FULL_SECRET
        PT_DWORD  dwDataTag;        // Used when dwForm = PT_REF_SECRET
        PT_BYTE   abyReserved[20];  // reserved
    } InputSecret;
  } PT_INPUT_SECRET;


#define PT_FULL_SECRET    1    ///< Input is the secret itself in form of PT_DATA structure.
#define PT_REF_SECRET    2    ///< Input is a number of tag of finger data stored as last matched data.

/** 
 * Wrapped SAB
 */
typedef PT_DATA     PT_SAB;

/** 
 * A structure used to input a SAB to the API. Such input can be either
 * wrapped SAB or slot number of SAB stored in FM's non-volatile memory.
 */
typedef struct pt_input_sab {
  PT_DWORD dwForm; 	///< Form of the input SAB
  union {
        PT_SAB  *pWrappedSab;       // Used when dwForm = PT_WRAPPED_SAB
        PT_LONG lSlotNr;            // Used when dwForm = PT_SLOT_SAB
        PT_BYTE abyReserved[20];    // reserved
    } InputSab;
  } PT_INPUT_SAB;

#define PT_WRAPPED_SAB    1    ///< Input is a wrapped SAB in form of PT_SAB structure.
#define PT_SLOT_SAB    2    ///< Input is a number of SAB slot stored in FM's non-volatile memory.

/** 
 * Read the attribute data associated with given SAB.
 * 
 * @param hConnection Handle to the connection to FM.
 * @param pInputSab SAB which attribute data has to be read. It can be either SAB stored
 * in FM's non-volatile memory (PT_SLOT_SAB form of PT_INPUT_SAB structure) or wrapped
 * SAB (PT_WRAPPED_SAB form of PT_INPUT_SAB structure).
 * @param pSecret Secret protecting data of given SAB. Can be NULL, if requested data
 * are accessible without secret verification.
 * @param dwTag Tag of requested attribute data.
 * @param ppSabData Address of the pointer, which will be set to point to the requested attribute
 * data associated with given SAB. If no data are associated with the SAB, the result will
 * be a data block with zero length. The data has to be freed by a call to PTFree.
 * @return Status code.
 */
PTAPI_DLL PT_STATUS PTAPI PTGetSabData(
    IN PT_CONNECTION hConnection,
    IN PT_INPUT_SAB *pInputSab,
    IN PT_INPUT_SECRET *pSecret,
    IN PT_DWORD dwTag,
    OUT PT_DATA **ppSabData
);


/** 
 * Perform given 'use' operation with attribute data of specified SAB.
 * 
 * @param hConnection Handle to the connection to FM.
 * @param pInputSab SAB which attribute data has to be used. It can be either SAB stored
 * in FM's non-volatile memory (PT_SLOT_SAB form of PT_INPUT_SAB structure) or wrapped
 * SAB (PT_WRAPPED_SAB form of PT_INPUT_SAB structure).
 * @param pSecret Secret protecting data of given SAB. Can be NULL, if used attribute data
 * are accessible without secret verification.
 * @param dwTag Tag of attribute data item which has to be used.
 * @param dwOperation Code of operation to be performed (see values PT_ATRBOP_xxxx).
 * @param pInputData Block of input data used for given operation.
 * @param ppOutputData Address of the pointer, which will be set to point to the output block
 * of data containing result(s) of given operation. The data has to be freed by a call to PTFree().
 * @return Status code.
 */
PTAPI_DLL PT_STATUS PTAPI PTUseSabData(
    IN PT_CONNECTION hConnection,
    IN PT_INPUT_SAB *pInputSab,
    IN PT_INPUT_SECRET *pSecret,
    IN PT_DWORD dwTag,
    IN PT_DWORD dwOperation,
    IN PT_DATA *pInputData,
    OUT PT_DATA **ppOutputData
);

#endif //#ifndef tfmsabs_h
