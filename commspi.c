 /**
 * @file commspi.c
 *
 * General Communication Layer (transport+link layer) interface for ESS/TFM communication protocol
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#include "comm.h"
#include "utils.h"
#include "tfmerror.h"
#include "linkspi.h"
#include "timer.h"

#if defined(ENABLE_PTSECURECHANNEL)
#include "secch.h"
#endif

//========================================================================
//      Constants, macros and types
//========================================================================

/// current protocol version
#define TL_PROTOCOL_VERSION             (1<<5)

// Frame types
/// @name Frame types
//@{
/// normal data frame, to be delivered to the transport layer
#define LL_FRAME_TYPE_DATA              0
/// acknowledgment
#define LL_FRAME_TYPE_ACK               1
/// negative acknowledgment, request to resend
#define LL_FRAME_TYPE_NACK              2
/// answer to reset
#define LL_FRAME_TYPE_ATR               3
/// request to change parameters during connection setup
#define LL_FRAME_TYPE_SET_PARAMS        4
/// confirmation of parameters change during connection setup
#define LL_FRAME_TYPE_CONFIRM_PARAMS    5
/// reset frame when hw reset isn't supported
#define LL_FRAME_TYPE_RESET             6
/// end of connection
#define LL_FRAME_TYPE_CLOSE             7
/// wait time extension
#define LL_FRAME_TYPE_WTX               8
/// continue, response to WTX
#define LL_FRAME_TYPE_CONTINUE          9
/// abort, response to WTX
#define LL_FRAME_TYPE_ABORT             10
/// frame containing raw data used for sending stream of data from TFM to host
#define LL_FRAME_TYPE_RAW_DATA          11
/// frame used by host to request raw data abort
#define LL_FRAME_TYPE_ABORT_RAW_DATA    12
//@}

#define LINK_LAYER_HEADER_SIZE 3

//========================================================================
//		CRC functions
//========================================================================

/**
 *	Computes CRC V41 for given buffer and size
 */
uint16 ComputeCrc(uint8 *pBuffer, uint32 size, uint16 initVal) 
{
    uint16 crc = initVal;

    if (pBuffer != NULL) 
    {
        for( ; size > 0; --size ) 
        {
            crc = (uint16)((uint8)(crc >> 8) | (crc << 8));
            crc ^= (uint8)*pBuffer++;
            crc ^= (uint8)(crc & 0xff) >> 4;
            crc ^= (crc << 8) << 4;
            crc ^= ((crc & 0xff) << 4) << 1;
        }
    }
    return(crc);
}
//========================================================================
//		Sleep in session functions
//========================================================================

/**
 * Wait until AWAKE signal is set to desired state or timeout expires.
 *
 * @param boWaitForAwake If TRUE, wait until AWAKE signal is set, otherwise
 *   wait until it is cleared
 * @param dwTimeout Timeout in miliseconds
 * @return On success returns COMM_STATUS_OK, if timeout expired returns
 *   COMM_STATUS_TIMEOUT, if some other problem occurred returns COMM_STATUS_ERROR
 */
static COMM_STATUS WaitForAwake(BOOL boWaitForAwake, uint32 dwTimeout)
{
    COMM_STATUS status;
    uint32 awake = 0;
    uint32 startTime = PT_TimerGetMilliseconds();

    do {
        if (PT_TimerGetMilliseconds() - startTime > dwTimeout) 
            return COMM_STATUS_TIMEOUT; // timeout

        // Check for asynchronous power down
        status = PT_TimerGetStatus();
        if (COMM_STATUS_OK != status) { 
            // Asynchronous power down
            return status;
        }
        
        awake = LinkSpiIsAwake(); // check AWAKE signal
    } while (awake != (uint32)boWaitForAwake);

    return COMM_STATUS_OK;
}
//========================================================================
//		Link layer functions
//========================================================================

/**
 *	Read and decode one Link Layer frame.
 *  The data part will be stored at gpConn.
 *
 *	@param pS Pointer to session block
 *  @param pbyFrameType Type of the frame (see LL_FRAME_TYPE_xxxx)
 *  @param pbyFrameSeq The frame sequence (used only for data frames)
 *  @param pdwLength Length of the data part of the frame
 *  @return Status code
 */
static COMM_STATUS LlReadFrame (PT_SESSION *pS, uint8 *pbyFrameType, uint8 *pbyFrameSeq, uint32 *pdwLength)
{
    
    COMM_STATUS status = COMM_STATUS_OK;
    uint16 wExpectedCrc, wRecvCrc;
    uint32 pBufSize = MAX_FRAME_DATA_SIZE;

    //read a frame
    status = LinkSpiReceiveFrame(pS->pBuf, &pBufSize, COMM_TIMEOUT);
    if (status != COMM_STATUS_OK)
    {
        goto lbExit;
    }

     // Parse the header
    *pbyFrameType = (uint8) (pS->pBuf[0] & 0x0f);
    *pbyFrameSeq  = (uint8) (pS->pBuf[1] >> 4);
    *pdwLength    = (pS->pBuf[2] + ((pS->pBuf[1] & 0x07) << 8));

    if (*pdwLength > MAX_FRAME_DATA_SIZE) 
    {
        status = COMM_STATUS_ERROR; 
        goto lbExit;
    }

    // check CRC
    wExpectedCrc = ComputeCrc(pS->pBuf, LINK_LAYER_HEADER_SIZE+(*pdwLength), 0);
    wRecvCrc = (uint16)(pS->pBuf[LINK_LAYER_HEADER_SIZE+(*pdwLength)] | (pS->pBuf[LINK_LAYER_HEADER_SIZE+(*pdwLength)+1] << 8));

    if (wExpectedCrc != wRecvCrc) 
    {
        status = COMM_STATUS_ERROR; // Wrong CRC
        goto lbExit;
    }

    //PT_memmove(pS->pBuf, pS->pBuf+3, (uint32) *pdwLength);    - Moved to later processing

lbExit:
    return status;
}


/**
 *	Encode and write one Link Layer frame.
 *  The data part is already prepared at gpConn.
 *
 *	@param pS Pointer to session block
 *  @param byFrameType Type of the frame (see LL_FRAME_TYPE_xxxx)
 *  @param byFrameSeq The frame sequence (used only for data frames)
 *  @param dwLength Length of the data part of the frame
 *  @return Status code
 */
static COMM_STATUS LlWriteFrame (PT_SESSION *pS, uint8 byFrameType, uint8 byFrameSeq, uint32 dwLength)
{
    COMM_STATUS status = COMM_STATUS_OK;
    uint16 crc;

    // Verify the length
    if (dwLength > MAX_FRAME_DATA_SIZE)
    {
        status = COMM_STATUS_ERROR;
        goto lbExit;
    }

    //PT_memmove(pS->pBuf+3, pS->pBuf, dwLength);   - Moved to previous processing
    pS->pBuf[0] = byFrameType;
    pS->pBuf[1] = (uint8)((byFrameSeq<<4) | ((dwLength >> 8) & 0x07));
    pS->pBuf[2] = (uint8)dwLength;

    crc = ComputeCrc(pS->pBuf, LINK_LAYER_HEADER_SIZE+dwLength, 0);
    pS->pBuf[LINK_LAYER_HEADER_SIZE+dwLength] = (uint8)(crc & 0xFF);
    pS->pBuf[LINK_LAYER_HEADER_SIZE+dwLength+1] = (uint8)((crc >> 8) & 0xFF);

    status = LinkSpiSendFrame(pS->pBuf, LINK_LAYER_HEADER_SIZE+dwLength+2, COMM_TIMEOUT);
    
lbExit:;    
    return status;
}


//========================================================================
//		High-level functions
//========================================================================

// 
// These are the same as the exported functions, but without closing the connection on error
//

/**
 *	As CommOpen
 */
static COMM_STATUS HLOpen (PT_SESSION *pS)
{
    COMM_STATUS status;
    uint8       byFrameType=0, byFrameSeq;
    uint32      dwLength=0;
    uint32      i;
    void        *p;

    // Update session structure
    pS->dwLastSentFragmentLength = 0;       // No last fragment is valid
    // Open the USB

    //initialize a link layer
    status = LinkSpiInitialize();
    if(status!=COMM_STATUS_OK)
    {
        goto error;
    }
 

    // reset packet sequence
    pS->bySeqNr = 0;
    
    // wakeup device (even if already HW awake, verify by this that virtual WAKEUP is set)
	
    status = LinkSpiWakeUp(COMM_TIMEOUT);
    if(status!=COMM_STATUS_OK)
    {
        goto error;
    }
	  
    for ( i = 0; i < 3; ++i) 
    { // wait a while before next reset
      // 3 attempts before failure
    
        if(i>0)
        {
            PT_TimerDelayMilliseconds(i * 10);
        }
    
        status = LinkSpiSendCommand(LINK_SPI_CMD_CONN_RESET, NULL, 0, COMM_VIRTUAL_COMMAND_TIMEOUT);

        //wait for device reset
        PT_TimerDelayMicroseconds(COMM_RESET_INTERVAL);

        if (status == COMM_STATUS_OK) 
        {
            // Data in pS->pBuf are unimportant, there is no need to move them 3B back to skip header
            status = LlReadFrame (pS, &byFrameType, &byFrameSeq, &dwLength);
            if (status == COMM_STATUS_OK) 
            {
                if (byFrameType == LL_FRAME_TYPE_ATR) 
                {
                    break;
                }
                else 
                {
                    status = COMM_STATUS_ERROR;
                }
            }
        }
    }
    
    status = PT_TimerDelayMilliseconds (5);
    // Check for potential asynchronous power down
    if (COMM_STATUS_OK != status) {
        goto error;
    }

    // Prepare and send the SEND_PARAMS frame
    p = pS->pBuf + LINK_LAYER_HEADER_SIZE;
    STORE_BYTE (p, LL_PROTOCOL_VERSION);		// Protocol version
    STORE_BYTE (p, 0);	                        // Flags
    STORE_DWORD(p, HTOL32(COMM_WTX_TIMEOUT));   // WTX timeout
    STORE_WORD (p, HTOL32(MAX_FRAME_DATA_SIZE));// Max size of the data part of LL frame
    
    // Try to send it COMM_TRIES times before giving up when getting NACK reply
    for (i = 0; i < COMM_TRIES; ++i)
    {
        // Request the params change
        if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_SET_PARAMS, 0, (uint32)((uint8 *)p - pS->pBuf - LINK_LAYER_HEADER_SIZE))) != COMM_STATUS_OK) 
        {
            // Try it again
            continue;
        }

        // Receive the CONFIRM_PARAMS frame
        if ((status = LlReadFrame (pS, &byFrameType, &byFrameSeq, &dwLength)) != COMM_STATUS_OK) 
        {
            // Try it again
            continue;
        }

        if (byFrameType != LL_FRAME_TYPE_CONFIRM_PARAMS || dwLength != 1 || pS->pBuf[0 + LINK_LAYER_HEADER_SIZE] == FALSE)
        {
            // Try it one more time
            status = COMM_STATUS_ERROR;
        }
        else
        {
            // Parameters confirmed
            break;
        }
    }
    if (COMM_STATUS_OK != status) 
    {
        goto error;
    }

    // Wait for ESS/TFM to change parameters - 100 msec should be enough
    PT_TimerDelayMilliseconds (5);

    // And clear the sequence number counter
    pS->bySeqNr = 0;

    status = COMM_STATUS_OK;

error:

    if (status != COMM_STATUS_OK)
    {
        // Call LinkSpi destructor
        LinkSpiDestroy();
    }

    return status;
}


/**
 *	As CommClose
 */
static COMM_STATUS HLClose (PT_SESSION *pS)
{
    COMM_STATUS status;
    uint8       byFrameType, byFrameSeq;
    uint32      dwLength, i;


    // Estimate failure unless we really succeed
    status = COMM_STATUS_ERROR;

    for (i=0; i<COMM_TRIES; i++) {
        // Send the LL_FRAME_TYPE_CLOSE frame
        pS->pBuf[0 + LINK_LAYER_HEADER_SIZE] = 1;                  // Allow TCD's out-of-session sleep
        if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_CLOSE, 0, 1)) != COMM_STATUS_OK) 
        {
            goto lbExit;
        }

        // Await the acknowledge
        // (Data in pS->pBuf are unimportant, there is no need to move them 3B back to skip header)
        if ((status = LlReadFrame (pS, &byFrameType, &byFrameSeq, &dwLength)) != COMM_STATUS_OK) {
            if((sint32)status == (sint32)PT_STATUS_TIMEOUT)
            {
                status = COMM_STATUS_OK;    //close does not have to be acknowledged, a device might be already sleeping
                goto lbExit;
            } else {
                status = COMM_STATUS_ERROR;
                continue;       // Try again
            }
        }
        
        if(byFrameType == LL_FRAME_TYPE_NACK){
            uint16 errCode = pS->pBuf[4 + LINK_LAYER_HEADER_SIZE];
            errCode = (errCode << 8) + pS->pBuf[3 + LINK_LAYER_HEADER_SIZE];
            continue;
        } else {
            if ((byFrameType != LL_FRAME_TYPE_ACK) || (dwLength != 0)) {
                continue;       // Try again
            }
        }

        //clear virtual WAKEUP to allow TCD's transition to out-of-session sleep
        status = LinkSpiSendCommand(LINK_SPI_CMD_SLEEP, NULL, 0, COMM_VIRTUAL_COMMAND_TIMEOUT);

        // If we are here -> acknowledged, we can exit
        break;
    }

lbExit:

    // Call LinkSpi destructor
    LinkSpiDestroy();

    if (COMM_STATUS_OK != status) 
    {
        status = COMM_STATUS_ERROR;
    }

    return status;
}


/**
 *	As CommTransact
 */
static COMM_STATUS HLTransact (PT_SESSION *pS,
                        IN  uint32   dwSendFragmentSize, IN  uint32   dwSendTotalSize, IN  uint8   bySendFragmentType,
                        OUT uint32 *pdwRecvFragmentSize, OUT uint32 *pdwRecvTotalSize, OUT uint8 *pbyRecvFragmentType,
                        IN  uint32   dwFlags)
{
    COMM_STATUS status;
    bool8        boNavigCanceled = FALSE;
    uint32      dwTriesLeft = COMM_TRIES;
    uint32 awake = FALSE;

    // Create the outbound packet
    // We have to move the data frame to make a space for the header
    //:TODO: Modify LlWriteFrame to be able to handle TL packets directly, without adding the header into the buffer
    switch (bySendFragmentType) {
        case TLTYPE_SINGLE:
        case TLTYPE_FIRST:
            // 3 byte TL header
            if (dwSendFragmentSize+3 > MAX_FRAME_DATA_SIZE || dwSendTotalSize > 0x3FFFF) 
                return COMM_STATUS_ERROR;
            if (dwSendFragmentSize) 
                PT_memmove (pS->pBuf + 3 + LINK_LAYER_HEADER_SIZE, pS->pBuf, dwSendFragmentSize);
            dwSendFragmentSize += 3;
            pS->pBuf[0 + LINK_LAYER_HEADER_SIZE] = (uint8) (bySendFragmentType+(dwSendTotalSize>>16)+TL_PROTOCOL_VERSION);
            pS->pBuf[1 + LINK_LAYER_HEADER_SIZE] = (uint8) (dwSendTotalSize);
            pS->pBuf[2 + LINK_LAYER_HEADER_SIZE] = (uint8) (dwSendTotalSize >> 8);
            break;

        case TLTYPE_MIDDLE:
        case TLTYPE_LAST:
        case TLTYPE_CONTINUE:
            // 1 byte TL header
            if (dwSendFragmentSize+1 > MAX_FRAME_DATA_SIZE) 
                return COMM_STATUS_ERROR;
            if (bySendFragmentType == TLTYPE_CONTINUE && dwSendFragmentSize) 
                return COMM_STATUS_ERROR; // TLSF_CONTINUE packet must have no data!
            if (dwSendFragmentSize) 
                PT_memmove (pS->pBuf + 1 + LINK_LAYER_HEADER_SIZE, pS->pBuf, dwSendFragmentSize);
            dwSendFragmentSize += 1;
            pS->pBuf[0 + LINK_LAYER_HEADER_SIZE] = (uint8) (bySendFragmentType+TL_PROTOCOL_VERSION);
            break;

        default:
            // Unknown type
            return COMM_STATUS_ERROR;
    }

    // And copy the packet for error recovery purposes
    PT_memcpy (pS->pLastSentFragmentBuf, pS->pBuf, dwSendFragmentSize);
    pS->dwLastSentFragmentLength = dwSendFragmentSize;
    
    // Send the packet
    if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_DATA, pS->bySeqNr, dwSendFragmentSize)) != COMM_STATUS_OK) 
    {
        goto error;
    }

    //Let a TCD time to prepare a reply
    switch (bySendFragmentType) {
        case TLTYPE_SINGLE:
        case TLTYPE_FIRST:
            PT_TimerDelayMicroseconds(COMM_FIRST_REQUEST_TO_REPLY_INTERVAL);
            break;

        case TLTYPE_MIDDLE:
        case TLTYPE_LAST:
        case TLTYPE_CONTINUE:
            PT_TimerDelayMicroseconds(COMM_CONTINUE_REQUEST_TO_REPLY_INTERVAL);
            break;

        default:
            // Unknown type
            return COMM_STATUS_ERROR;
    }

    
    
    //----- 
    
    // handle sleep in session
    if (dwFlags & TRANSACT_FLAG_SLEEP)
    {
    
        // wait for AWAKE signal to be set
        status = WaitForAwake(FALSE, COMM_TIMEOUT);

        // if AWAKE wasn't set within timeout, device can report error - try to receive frame
        // else continue in sleep procedure
        if(status != COMM_STATUS_TIMEOUT)
        {
            if (status != COMM_STATUS_OK) 
            {
                goto error; // communication problem
            }
            
            //confirm sleep
            status = LinkSpiSuspend(COMM_TIMEOUT);
            if(status != PT_STATUS_OK)
            {
                goto error;
            }

            // TFM/ESS is sleeping now
            for ( ; ; ) 
            {     
                // Check AWAKE state
                awake = LinkSpiIsAwake();
                if (awake) 
                {
                    // FM woke up by itself (HW finger detect)
                    if ((status = LinkSpiWakeUp(COMM_TIMEOUT)) != COMM_STATUS_OK) 
                    {
                        goto error; 
                    }

                    break;
                } 
                else 
                {
                    if (pS->pfnIdleCallback != NULL)
                    {
                        uint8 response;
                        status = pS->pfnIdleCallback(pS->pIdleCallbackCtx, &response); // call idle callback
                        if (status != COMM_STATUS_OK || response == PT_SLEEP_STOP) 
                        {  
                            // wait until FM awakes
                            if ((status = LinkSpiWakeUp(COMM_TIMEOUT)) != COMM_STATUS_OK) 
                            {
                                goto error; 
                            }
                            break;
                        }

                        // Handle potential asynchronous power down                   
                        if (!pS->boInSession) 
                        {
                            status = COMM_STATUS_ASYNC_POWERDOWN;
                            goto error;
                        }
                    } 
                    else 
                    {
                        status = PT_TimerDelayMilliseconds(50); // no callback, just save CPU power during polling
                        // Handle potential asynchronous power down
                        if (COMM_STATUS_OK != status) {
                            goto error;
                        }
                    }
                }
            }
        }
    }   
     
    // Await the response
    for( ; ; )
    {
        uint8   byFrameType, byFrameSeq;
        uint32  dwLength;
        
        status = LlReadFrame (pS, &byFrameType, &byFrameSeq, &dwLength);
        if (status == COMM_STATUS_TIMEOUT) 
        {
            // Timeout - resend the last data fragment
            if (--dwTriesLeft == 0) 
            {
                goto error;  // Retries exhausted
            }

            PT_memcpy (pS->pBuf, pS->pLastSentFragmentBuf, pS->dwLastSentFragmentLength);
            if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_DATA, pS->bySeqNr, pS->dwLastSentFragmentLength)) != COMM_STATUS_OK) 
            {
                goto error;
            }
            continue;
        } 
        else if (status == COMM_STATUS_ERROR) 
        {
            // Problem receiving a frame. Most probably framing error or bad CRC. Send NACK and retry.
            //:TODO: Improve error codes to distinguish between soft and hard (non-recoverable) errors 
            void *p = pS->pBuf + LINK_LAYER_HEADER_SIZE;
            STORE_WORD (p, HTOL16(0));  // Error reason code - can be zero
            if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_NACK, 0, (uint32)((uint8 *)p - pS->pBuf - LINK_LAYER_HEADER_SIZE))) != COMM_STATUS_OK) 
            {
                goto error;
            }
            continue;
        } 
        else if (status != COMM_STATUS_OK) 
        {
            // Any other error - lblbExit.
            goto error;
        }
        

        // Frame received OK - parse it!
        if (byFrameType == LL_FRAME_TYPE_DATA) 
        {
            // This is what we waited for
            if (byFrameSeq != pS->bySeqNr) 
            {
                status = COMM_STATUS_ERROR;
                goto error;
            }
            *pdwRecvFragmentSize = dwLength;
            break;
        } 
        else if (byFrameType == LL_FRAME_TYPE_RAW_DATA) 
        {
            // Process raw data (navigation data)
            PT_NAVIGATION_DATA navigData;
            uint8 response = PT_CANCEL;
            uint16 *p = (uint16 *)(((uint8*)(pS->pBuf))+ LINK_LAYER_HEADER_SIZE);
            // get navigation data
            LOAD_WORD(p,navigData.signalBits);
            LOAD_WORD(p,navigData.dx);
            LOAD_WORD(p,navigData.dy);
            navigData.signalBits = LTOH16(navigData.signalBits);
            navigData.dx = LTOH16(navigData.dx);
            navigData.dy = LTOH16(navigData.dy);
            if (pS->pfnNavigCallback != NULL && !boNavigCanceled) 
            {
                // call navigation callback
                status = pS->pfnNavigCallback(pS->pNavigCallbackCtx, &navigData, &response);
                if (status != COMM_STATUS_OK) 
                    response = PT_CANCEL;
                // Handle potential asynchronous power down
                status = PT_TimerGetStatus();
                if (COMM_STATUS_OK != status) {
                    goto error;
                }
                if (!pS->boInSession) 
                {
                    status = COMM_STATUS_ASYNC_POWERDOWN;
                    goto error;
                }
                if (response == PT_CANCEL){ 
                    boNavigCanceled = TRUE;
                }
            }
            // send response
            if ((dwFlags & TRANSACT_FLAG_NAV_BIDIR) || response == PT_CANCEL)
            {
                *(uint8*)(pS->pBuf + LINK_LAYER_HEADER_SIZE) = response;
                if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_RAW_DATA, 0, sizeof(uint8))) != COMM_STATUS_OK) 
                {
                    goto error;
                }
            }
        } 
        else if (byFrameType == LL_FRAME_TYPE_WTX) 
        {           
            // WTX - send continue
            // relax before send, TFM calls it from an interrupt and needs time to recover
            status = PT_TimerDelayMicroseconds(COMM_POLLING_INTERVAL);
            // Handle potential asynchronous power down
            if (COMM_STATUS_OK != status) {
                goto error;
            }
            
            if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_CONTINUE, 0, 0)) != COMM_STATUS_OK) 
            {
                goto error;
            }
    
            // If we are in the bidirectional navigation and received WTX, it means that a raw data packet was lost
            // Send a raw data to resume the raw data communication
            if (dwFlags & TRANSACT_FLAG_NAV_BIDIR)
            {
                void *p = pS->pBuf + LINK_LAYER_HEADER_SIZE;
                STORE_BYTE (p, boNavigCanceled ? PT_CANCEL : PT_CONTINUE);
                if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_RAW_DATA, 0, (uint32)((uint8 *)p - pS->pBuf - LINK_LAYER_HEADER_SIZE))) != COMM_STATUS_OK) 
                {
                    goto error;
                }
            }
        } 
        else if (byFrameType == LL_FRAME_TYPE_NACK) 
        {
            // NACK - repeat the last data packet
            PT_memcpy (pS->pBuf, pS->pLastSentFragmentBuf, pS->dwLastSentFragmentLength);
            if ((status = LlWriteFrame (pS, LL_FRAME_TYPE_DATA, pS->bySeqNr, pS->dwLastSentFragmentLength)) != COMM_STATUS_OK) 
            {
                goto error;
            }
        } 
        else 
        {
            // Nothing else is supposed to come -> error
            status = COMM_STATUS_ERROR;
            goto error;
        }
    }

    // We've received the data frame - parse it
    if ((pS->pBuf[0 + LINK_LAYER_HEADER_SIZE] & 0xE0) != TL_PROTOCOL_VERSION) 
    {
        status = COMM_STATUS_ERROR;
        goto error;
    }
    *pbyRecvFragmentType = (uint8) (pS->pBuf[0 + LINK_LAYER_HEADER_SIZE] & 0x1C);

    if (*pbyRecvFragmentType == TLTYPE_SINGLE || *pbyRecvFragmentType == TLTYPE_FIRST) 
    {
        // The TL header is 3 bytes
        if (*pdwRecvFragmentSize < 3) 
        {
            status = COMM_STATUS_ERROR;
            goto error;
        }      
        *pdwRecvFragmentSize -= 3;
        *pdwRecvTotalSize = pS->pBuf[1 + LINK_LAYER_HEADER_SIZE] + (pS->pBuf[2 + LINK_LAYER_HEADER_SIZE] << 8) + ((pS->pBuf[0 + LINK_LAYER_HEADER_SIZE] & 0x03) << 16);
        if (*pdwRecvFragmentSize) 
            PT_memmove (pS->pBuf, pS->pBuf + 3 + LINK_LAYER_HEADER_SIZE, *pdwRecvFragmentSize);
    } 
    else if (*pbyRecvFragmentType == TLTYPE_MIDDLE || *pbyRecvFragmentType == TLTYPE_LAST || *pbyRecvFragmentType == TLTYPE_CONTINUE) 
    {
        // The TL header is 1 byte
        if (*pdwRecvFragmentSize < 1 ||
            (*pbyRecvFragmentType == TLTYPE_CONTINUE && *pdwRecvFragmentSize != 1)) 
        {
            status = COMM_STATUS_ERROR;
            goto error;
        }
        *pdwRecvFragmentSize -= 1;
        *pdwRecvTotalSize = 0;
        if (*pdwRecvFragmentSize) 
        {
            PT_memmove (pS->pBuf, pS->pBuf + 1 + LINK_LAYER_HEADER_SIZE, *pdwRecvFragmentSize);
        } 
    } 
    else 
    {
        // Other possible TL packets we currently do not recognize
        status = COMM_STATUS_ERROR;
        goto error;
    }

    status = COMM_STATUS_OK;
    
    // Success, increment the sequence counter
    pS->bySeqNr = (uint8)((pS->bySeqNr+1) & 0x0f);

error:

    return status;
}



//========================================================================
//		Exported functions
//========================================================================

/**
 * Open a communication session.
 * Must not be called if a session is already opened.
 *
 * @param pS Pointer to session block
 * @return Status code
 */
COMM_STATUS PT_CommOpen (IN PT_SESSION *pS)
{
    COMM_STATUS status;

    if (pS->boInSession) 
    {
        // Already in session
        status = COMM_STATUS_ERROR;
    } 
    else 
    {
        status = HLOpen (pS);
    }

    // Check the result
    if (status != COMM_STATUS_OK) 
    {
        // Close everything
        PT_CommClose(pS);
    } 
    else 
    {
        // The session is open
        pS->boInSession = TRUE;
    }
    return status;
}


/**
 * Close a communication session.
 * If a session is already closed, does nothing.
 *
 * @param pS Pointer to session block
 * @return Status code
 */
COMM_STATUS PT_CommClose (IN PT_SESSION *pS)
{
    COMM_STATUS status = COMM_STATUS_OK;

    if (pS->boInSession) 
    {
        // Session is not yet closed - let's try to close it
        status = HLClose(pS);
    }
    
    pS->boInSession = FALSE;
    return status;
}


/**
 * Perform a transport-layer transaction (send and/or receive a data fragment).
 * A session must be already opened.
 * If CommTransact returns with any other code than COMM_STATUS_OK, a non-recoverable problem
 * has ocurred. The current session is automatically terminated. CommOpen must be called
 * before any communication can continue.
 *
 * The outbound fragment had to be prepared in advance in the pS->pBuff buffer.
 * The inbound fragment will be after the transaction available in the pS->pBuff buffer.
 *
 * @param pS				Pointer to session block
 * @param dwSendFragmentSize    Length of the fragment to be sent
 * @param dwSendTotalSize       Total length of all the outbound fragments. 
 *                              Used only for fragment types TLTYPE_SINGLE and TLTYPE_FIRST.
 * @param bySendFragmentType    Type of the fragment to be sent, see TLTYPE_xxxx
 *
 * @param pdwRecvFragmentSize   Length of the received fragment
 * @param pdwRecvTotalSize      Total length of all the inbound fragments. 
 *                              Valid only when recv. fragment type is TLTYPE_SINGLE or TLTYPE_FIRST.
 * @param pbyRecvFragmentType   Type of the received fragment, see TLTYPE_xxxx
 * @param dwFlags               Flags controlling special behavior (sleep, WAKEUP/AWAKE behavior etc.)
 *                              See TRANSACT_FLAG_xxxx.
 *
 * @return Status code.
 */
COMM_STATUS PT_CommTransact (IN  PT_SESSION *pS,
                             IN  uint32   dwSendFragmentSize, IN  uint32   dwSendTotalSize, IN  uint8   bySendFragmentType,
                             OUT uint32 *pdwRecvFragmentSize, OUT uint32 *pdwRecvTotalSize, OUT uint8 *pbyRecvFragmentType,
                             IN  uint32   dwFlags)
{
    COMM_STATUS status;

    if (! pS->boInSession) 
    {
        // Not in session
        status = COMM_STATUS_SESSION_TERMINATED;
    } 
    else 
    {
#if defined(ENABLE_PTSECURECHANNEL)
        // process data for secure channel
        status = ProcessSendData(pS, bySendFragmentType, dwSendFragmentSize, dwSendTotalSize, &dwSendFragmentSize, &dwSendTotalSize);
        if (status != PT_STATUS_OK) goto end;
#endif // #if defined(ENABLE_PTSECURECHANNEL)

        status = HLTransact (pS,
                             dwSendFragmentSize, dwSendTotalSize, bySendFragmentType,
                             pdwRecvFragmentSize, pdwRecvTotalSize, &pS->byLastRecvFragmentType,
                             dwFlags);

        *pbyRecvFragmentType = pS->byLastRecvFragmentType;

#if defined(ENABLE_PTSECURECHANNEL)
        if (status == PT_STATUS_OK)
        {
            // process received data from secure channel
            status = ProcessReceiveData(pS, *pbyRecvFragmentType, *pdwRecvFragmentSize, pdwRecvFragmentSize);
        }
#endif // #if defined(ENABLE_PTSECURECHANNEL)

    }

#if defined(ENABLE_PTSECURECHANNEL)
end:; 
#endif // #if defined(ENABLE_PTSECURECHANNEL)

    // Check the result
    if (status != COMM_STATUS_OK) 
    {
        // Close USB
        pS->boInSession = FALSE;
        HLClose(pS);
    }
    return status;
}



