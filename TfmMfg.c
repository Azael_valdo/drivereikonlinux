/**
 * @file tfmint.c
 *
 * Implementation of internal commands for simplified ESS/TFM API library
 *
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#include "types.h"
#include "tfmtypes.h"
#include "endian.h"
#include "tfmmfg.h"
#include "tfmerror.h"
#include "errcodes.h"
#include "clcodes.h"
#include "comm.h"
#include "utils.h"

/*------------ Externals ----------------------------------*/

/**
 * The session context block.
 * In this implementation we allow only one session, so we can allocate it statically
 */
extern PT_SESSION gSession;

#if defined (TARGET_STANDARD_API)

// Indication of PTInitialize() call
extern PT_BOOL apiInitialized;
  
void* MallocCopy(PT_DWORD dwSize, void *pSrcData);
#endif

bool8 PT_FitsSentDataToFragment(
    IN uint32 dwSendSize,
    IN uint8 byFragmentType,
    OUT uint32 *pdwMaxTransferSize
);

PT_STATUS PT_SimpleTransact(
    IN PT_SESSION *pS,
    IN PT_DWORD dwControlCode,
    IN PT_DWORD dwSendSize,
    OUT PT_DWORD *pdwRecvSize,
    IN PT_DWORD dwFlags
);

PT_STATUS PT_FirstTransact(
    IN PT_SESSION *pS,
    IN PT_DWORD dwControlCode,
    IN PT_DWORD dwSendFragmentSize,
    IN PT_DWORD dwSendTotalSize,
    IN PT_DWORD dwFlags
);

PT_STATUS PT_MiddleTransact(
    IN PT_SESSION *pS,
    IN PT_DWORD dwSendFragmentSize,
    IN PT_DWORD dwFlags
);

PT_STATUS PT_LastTransact(
    IN PT_SESSION *pS,
    IN PT_DWORD dwSendFragmentSize,
    OUT PT_DWORD *pdwRecvSize,
    IN PT_DWORD dwFlags
);

/*------------ Commands ------------------------------------*/


PTAPI_DLL PT_STATUS PTAPI PTDiagnostics(
    IN PT_CONNECTION hConnection,
    IN PT_DATA *pInData,
    OUT PT_DATA **ppOutData)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 dataLen;     // size of input data
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        uint32 dataLen;     // size of output data
    } Response;

    PT_STATUS status;
    PT_DWORD diagOperation;
    uint32 sendSize, recvSize, flags = 0;
    PT_SESSION *pS = &gSession;
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pInData == NULL || ppOutData == NULL || pInData->Length < sizeof(PT_DWORD))
        return PT_STATUS_INVALID_PARAMETER;
    
    diagOperation = LTOH32(*(PT_DWORD*)(pInData->Data)); // get diagnostic operation
    if (diagOperation == PT_DIAG_AWAKE_WAKEUP)
    {
        if (pInData->Length < sizeof(PT_DIAG_INDATA_AWAKE_WAKEUP))
            return PT_STATUS_INVALID_PARAMETER;
    
        pS->dwDiagTimeout = LTOH32(((PT_DIAG_INDATA_AWAKE_WAKEUP*)(pInData->Data))->DiagTimeout);
        pS->dwDiagLoops = LTOH32(((PT_DIAG_INDATA_AWAKE_WAKEUP*)(pInData->Data))->DiagLoops);
    
        flags |= TRANSACT_FLAG_TEST;
    }

    // check length of input data
    sendSize = sizeof(Request) + pInData->Length;
    
    if (!PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }
    
    // copy input data into communication buffer
    PT_memmove(pS->pBuf + sizeof(Request), pInData->Data, pInData->Length);
    ((Request*)pS->pBuf)->dataLen = HTOL32(pInData->Length);
    
    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_MISC_DIAGNOSTICS, sendSize, &recvSize, flags);
    
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response) ||
            recvSize < sizeof(Response) + LTOH32(((Response*)pS->pBuf)->dataLen))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif

        // check result of AWAKE/WAKEUP test from host
        if (diagOperation == PT_DIAG_AWAKE_WAKEUP)
        {
            PT_DWORD result = LTOH32(((PT_DIAG_OUTDATA_AWAKE_WAKEUP*)(pS->pBuf + sizeof(Response)))->ResultCode);

            if (result != PT_DIAG_AW_RESULT_NOT_SUPPORTED)
            {
                if (pS->dwDiagResult == DIAG_RESULT_TIMEOUT_NAWAKE) 
                {
                    // first possible error in test loop
                    ((PT_DIAG_OUTDATA_AWAKE_WAKEUP*)(pS->pBuf + sizeof(Response)))->ResultCode = HTOL32(PT_DIAG_AW_RESULT_TIMEOUT_NAWAKE);
                }
                else if (pS->dwDiagResult == DIAG_RESULT_TIMEOUT_AWAKE && result != PT_DIAG_AW_RESULT_TIMEOUT_NWAKEUP)
                {
                    // third possible error (after TIMEOUT_NAWAKE and TIMEOUT_NWAKEUP) in test loop
                    ((PT_DIAG_OUTDATA_AWAKE_WAKEUP*)(pS->pBuf + sizeof(Response)))->ResultCode = HTOL32(PT_DIAG_AW_RESULT_TIMEOUT_AWAKE);
                }
            }
        }
        
        *ppOutData = (PT_DATA*)(pS->pBuf + offsetof(Response, dataLen));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
        (*ppOutData)->Length = LTOH32((*ppOutData)->Length);
#endif
#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        if ((*ppOutData = (PT_DATA*)MallocCopy((*ppOutData)->Length + sizeof(PT_DWORD), *ppOutData)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
#endif
    }
    
    return status;
}    


PTAPI_DLL PT_STATUS PTAPI PTCalibrate(
    IN PT_CONNECTION hConnection, 
    IN PT_DWORD dwType)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 type;        // Type of calibration operation
    } Request;
    
    PT_SESSION *pS = &gSession;
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection) return PT_STATUS_INVALID_PARAMETER;

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->type = HTOL32(dwType);

    // transact data
    return PT_SimpleTransact(pS, CL_COMMAND_BIO_CALIBRATE, sizeof(Request), (uint32 *)NULL, 0);
}


PTAPI_DLL PT_STATUS PTAPI PTFirmwareUpdate(
    IN PT_CONNECTION hConnection,
    IN PT_DATA *pUpdateData)
{
    PT_STATUS status;
    uint32 sendSize, remaining;
    uint32 maxLastFragSize;
    uint8 *pPos;
    PT_SESSION *pS = &gSession;
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters
    if (!hConnection || pUpdateData == NULL)
    {
        return PT_STATUS_INVALID_PARAMETER;
    }
        
    sendSize = pUpdateData->Length;
    if (PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {

        // copy input data into comm buffer
        PT_memmove(pS->pBuf + sizeof(CL_TAG), pUpdateData->Data, pUpdateData->Length);

        status = PT_SimpleTransact(pS, CL_COMMAND_FIRMWARE_UPDATE, sendSize, (uint32 *)NULL, 0);
    }
    else
    {
        // transaction must be fragmented
        remaining = pUpdateData->Length + sizeof(CL_TAG);
        pPos = pUpdateData->Data;

        // send first fragment
        PT_FitsSentDataToFragment(remaining, TLTYPE_FIRST, &sendSize);
        
        PT_memmove(pS->pBuf + sizeof(CL_TAG), pUpdateData->Data, sendSize - sizeof(CL_TAG));

        status = PT_FirstTransact(pS, CL_COMMAND_FIRMWARE_UPDATE, sendSize, remaining, 0);

        pPos += sendSize - sizeof(CL_TAG);
        remaining -= sendSize;
        PT_FitsSentDataToFragment(remaining, TLTYPE_LAST, &maxLastFragSize);

        while (status == PT_STATUS_OK && remaining > maxLastFragSize)
        {
            // send middle fragments
            PT_FitsSentDataToFragment(remaining, TLTYPE_MIDDLE, &sendSize);
            PT_memmove(pS->pBuf, pPos, sendSize);

            status = PT_MiddleTransact(pS, sendSize, 0);

            pPos += sendSize;
            remaining -= sendSize;
        }

        // send last fragment
        if (status == PT_STATUS_OK)
        {
            PT_memmove(pS->pBuf, pPos, remaining);

            status = PT_LastTransact(pS, remaining, (uint32 *)NULL, 0);
        }
    }
    
    return status;
}


PTAPI_DLL PT_STATUS PTAPI PTManufacturing(
    IN PT_CONNECTION hConnection,
    OUT PT_DATA **ppOutData)
{
    typedef struct response_tag {
        CL_TAG tag;
        uint32 dataLen;     // size of returned data
    } Response;

    PT_STATUS status;
    uint32 recvSize;
    PT_SESSION *pS = &gSession;
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters   
    if (!hConnection || ppOutData == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    status = PT_SimpleTransact(pS, CL_COMMAND_FIRMWARE_MANUFACTURING, sizeof(CL_TAG), &recvSize, 0);

    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response) ||
            recvSize < sizeof(Response) + LTOH32(((Response*)pS->pBuf)->dataLen))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get returned data
        *ppOutData = (PT_DATA*)(pS->pBuf + offsetof(Response, dataLen));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
        (*ppOutData)->Length = LTOH32((*ppOutData)->Length);
#endif

#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        if ((*ppOutData = (PT_DATA*)MallocCopy((*ppOutData)->Length + sizeof(PT_DATA), *ppOutData)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
#endif
     
    }
    
    return status;
}


PTAPI_DLL PT_STATUS PTAPI PTManufacturingEx(
    IN PT_CONNECTION hConnection,
    IN PT_DATA *pInData,
    OUT PT_DATA **ppOutData)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 dataLen;     // size of input data
    } Request;
    
    typedef struct response_tag {
        CL_TAG tag;
        uint32 dataLen;     // size of returned data
    } Response;
    
    PT_STATUS status;
    uint32 sendSize, recvSize;
    PT_SESSION *pS = &gSession;
    UNREFERENCED_PARAMETER (hConnection);
    
#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    
    
    // check validity of input parameters   
    if (!hConnection || pInData == NULL || ppOutData == NULL)
        return PT_STATUS_INVALID_PARAMETER;
    
    // check length of input data
    sendSize = sizeof(Request) + pInData->Length;
    
    if (!PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }
    
    // copy input data into communication buffer
    PT_memmove(pS->pBuf + sizeof(Request), pInData->Data, pInData->Length);
    ((Request*)pS->pBuf)->dataLen = HTOL32(pInData->Length);
    
    status = PT_SimpleTransact(pS, CL_COMMAND_FIRMWARE_MANUFACTURINGEX, sendSize, &recvSize, 0);
    
    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response) ||
            recvSize < sizeof(Response) + LTOH32(((Response*)pS->pBuf)->dataLen))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        
        // get returned data
        *ppOutData = (PT_DATA*)(pS->pBuf + offsetof(Response, dataLen));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
        (*ppOutData)->Length = LTOH32((*ppOutData)->Length);
#endif
        
#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        if ((*ppOutData = (PT_DATA*)MallocCopy((*ppOutData)->Length + sizeof(PT_DATA), *ppOutData)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
#endif
        
    }
    
    return status;
}


PTAPI_DLL PT_STATUS PTAPI PTFirmwareGetCfg(
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwCfgItemId,
    IN PT_DATA **ppData)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 cfgId;           // Configuration item ID
    } Request;

    typedef struct response_tag {
        CL_TAG tag;
        uint32 dataLen;         // size of returned data
    } Response;

    PT_STATUS status;
    uint32 recvSize;
    PT_SESSION *pS = &gSession;
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || ppData == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->cfgId = HTOL32(dwCfgItemId);

    // transact data
    status = PT_SimpleTransact(pS, CL_COMMAND_FIRMWARE_GET_CFG, sizeof(Request), &recvSize, 0);

    if (status == PT_STATUS_OK)
    {
#ifdef _DEBUG
        // check length of received data
        if (recvSize < sizeof(Response) ||
            recvSize < sizeof(Response) + LTOH32(((Response*)pS->pBuf)->dataLen))
        {
            return PT_STATUS_WRONG_RESPONSE;
        }
#endif
        *ppData = (PT_DATA*)(pS->pBuf + offsetof(Response, dataLen));
#if TFM_ENDIAN != TFM_ENDIAN_LITTLE
        (*ppData)->Length = LTOH32((*ppData)->Length);
#endif

#if defined (TARGET_STANDARD_API)
        // allocate buffer for output parameter and copy data into it
        if ((*ppData = (PT_DATA*)MallocCopy((*ppData)->Length + sizeof(PT_DWORD), *ppData)) == NULL)
            status = PT_STATUS_MALLOC_FAILED;
#endif
    }

    return status;
}


PTAPI_DLL PT_STATUS PTAPI PTFirmwareSetCfg(
    IN PT_CONNECTION hConnection,
    IN PT_DWORD dwCfgItemId,
    IN PT_DATA *pData)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 cfgId;           // Configuration item ID
    } Request;

    uint32 sendSize;
    PT_SESSION *pS = &gSession;
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || pData == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    // check length of input data
    sendSize = sizeof(Request) + sizeof(PT_DWORD) + pData->Length;

    if (!PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->cfgId = HTOL32(dwCfgItemId);
    *(PT_DWORD *)(pS->pBuf + sizeof(Request)) = HTOL32(pData->Length);
    PT_memmove(pS->pBuf + sizeof(Request) + sizeof(PT_DWORD), pData->Data, pData->Length);
    
    // transact data
    return PT_SimpleTransact(pS, CL_COMMAND_FIRMWARE_SET_CFG, sendSize, (uint32*)NULL, 0);
}


PTAPI_DLL PT_STATUS PTAPI PTFirmwareSetCfgEx (
    IN PT_CONNECTION hConnection, 
    IN PT_DATA *pData)
{
    uint32 sendSize;
    PT_SESSION *pS = &gSession;
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection || pData == NULL)
        return PT_STATUS_INVALID_PARAMETER;

    // check length of input data
    sendSize = sizeof(CL_TAG) + sizeof(PT_DWORD) + pData->Length;

    if (!PT_FitsSentDataToFragment(sendSize, TLTYPE_SINGLE, NULL))
    {
        return PT_STATUS_DATA_TOO_LARGE;
    }

    // copy input data into communication buffer
    *(PT_DWORD *)(pS->pBuf + sizeof(CL_TAG)) = HTOL32(pData->Length);
    PT_memmove(pS->pBuf + sizeof(CL_TAG) + sizeof(PT_DWORD), pData->Data, pData->Length);

    // transact data
    return PT_SimpleTransact(pS, CL_COMMAND_FIRMWARE_SET_CFGEX, sendSize, (uint32*)NULL, 0);
}


PTAPI_DLL PT_STATUS PTAPI PTFirmwareRequestReboot(
    IN PT_CONNECTION hConnection,
    IN PT_BOOL boReboot)
{
    typedef struct request_tag {
        CL_TAG tag;
        uint32 reboot;      // Reboot
    } Request;

    PT_SESSION *pS = &gSession;
    UNREFERENCED_PARAMETER (hConnection);

#if defined (TARGET_STANDARD_API)
    // check for initialization
    if (!apiInitialized)
        return PT_STATUS_API_NOT_INIT;
#endif    

    // check validity of input parameters
    if (!hConnection) return PT_STATUS_INVALID_PARAMETER;

    // copy input data into communication buffer
    ((Request*)pS->pBuf)->reboot = HTOL32(boReboot);

    // transact data
    return PT_SimpleTransact(pS, CL_COMMAND_FIRMWARE_REQUEST_REBOOT, sizeof(Request), (uint32 *)NULL, 0);
}


