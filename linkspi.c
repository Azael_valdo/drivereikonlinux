/**
 * @file linkspi.c
 * Link layer interface for ESS/TFM communication protocol. Provides a frame construction / fragmentation to miniframes, CRC control.
 *
 * Copyright (C) 2008 UPEK Inc.
 *
 * @author Martin Krahulik
 */

#include "utils.h"             // memcpy functions
#include "tfmerror.h"           // Error codes
#include "linkspi.h"
#include "spi.h"
#include "defines.h"
#include "errcodes.h"
#include "timer.h"

//#define LINK_SPI_STATISTICS
#ifdef LINK_SPI_STATISTICS
#include "ui.h"
#endif //LINK_SPI_STATISTICS

// === Defines =====
#define LINK_SPI_VIRTUAL_SIGNALS_NOT_SUPPORTED       0   // TCD does not support virtual signals
#define LINK_SPI_VIRTUAL_SIGNALS_REQUIRED            1   // TCD requires a use of virtual signals
#define LINK_SPI_VIRTUAL_SIGNALS_UNKNOWN             2   // Not queried, if virtual signals are required or not

#define LINK_SPI_STATUS_AWAKE_BIT                  0x1   // AWAKE signal value
#define LINK_SPI_STATUS_IN_SLEEP_BIT               0x2   // Bit set when PTSleep handler is running
#define LINK_SPI_STATUS_WAKEUP_BIT                 0x4   // Current state of WAKEUP signal
#define LINK_SPI_STATUS_VIRTUAL_REQUIRED_BIT      0x20   // Virtual WAKEUP signal required

/// bits used to validate packet
#define LINK_SPI_PACKET_VALID_MASK        0xf0
#define LINK_SPI_PACKET_VALID_SHIFT       4
/// 1 - data ready, 0 - empty frame
#define LINK_SPI_DATA_READY_MASK          0x08
#define LINK_SPI_DATA_READY_SHIFT         3
/// 1 - frame start, 0 - in the middle
#define LINK_SPI_START_OF_FRAME_MASK      0x04
#define LINK_SPI_START_OF_FRAME_SHIFT     2
/// 1 - frame end
#define LINK_SPI_END_OF_FRAME_MASK        0x02
#define LINK_SPI_END_OF_FRAME_SHIFT       1
/// 1 - command (control channel), 0 - data frame (data channel)
#define LINK_SPI_CTRL_CHANNEL_MASK        0x01
#define LINK_SPI_CTRL_CHANNEL_SHIFT       0

/// valid transfer starts with this value
#define LINK_SPI_PACKET_VALID_VALUE       0xa

#define LINK_SPI_PACKET_VALID \
    (LINK_SPI_PACKET_VALID_VALUE << LINK_SPI_PACKET_VALID_SHIFT)

#define LINK_SPI_DATA_READY   \
    (1 << LINK_SPI_DATA_READY_SHIFT)

#define LINK_SPI_START_OF_FRAME   \
    (1 << LINK_SPI_START_OF_FRAME_SHIFT)

#define LINK_SPI_END_OF_FRAME \
    (1 << LINK_SPI_END_OF_FRAME_SHIFT)

#define LINK_SPI_COMMAND_PACKET   \
    (1 << LINK_SPI_CTRL_CHANNEL_SHIFT)

#define LINK_SPI_DATA_PACKET  \
    (0 << LINK_SPI_CTRL_CHANNEL_SHIFT)

/// creates data packet header
#define LINK_SPI_DATA_PACKET_HEADER(StartOfFrame, EndOfFrame) \
    ((uint8)(LINK_SPI_PACKET_VALID | LINK_SPI_DATA_READY | (StartOfFrame ? LINK_SPI_START_OF_FRAME : 0) | \
             (EndOfFrame ? LINK_SPI_END_OF_FRAME : 0) | LINK_SPI_DATA_PACKET))

#define LINK_SPI_IS_PACKET_VALID(Header)  \
    (((Header) & LINK_SPI_PACKET_VALID_MASK) == LINK_SPI_PACKET_VALID)
    
#define LINK_SPI_IS_DATA_READY(Header)    \
    (((Header) & LINK_SPI_DATA_READY_MASK) != 0)
    
#define LINK_SPI_IS_DATA_PACKET(Header)    \
    (((Header) & LINK_SPI_CTRL_CHANNEL_MASK) == 0)
    
/// creates command packet header; presumes all commands fit to one packet
#define LINK_SPI_COMMAND_PACKET_HEADER    \
    (LINK_SPI_PACKET_VALID | LINK_SPI_DATA_READY | LINK_SPI_START_OF_FRAME | LINK_SPI_END_OF_FRAME | LINK_SPI_COMMAND_PACKET)    

#define LINK_SPI_IS_FRAME_START(Header)   \
    (((Header) & LINK_SPI_START_OF_FRAME_MASK) != 0)

#define LINK_SPI_IS_FRAME_END(Header)     \
    (((Header) & LINK_SPI_END_OF_FRAME_MASK) != 0)
    
// === Structures ===

//buffer, where are merged minifames during bi-directional transmission
typedef struct _Tag_LinkSpiReceiveBuffer{
    uint8 pBuffer[MAX_FRAME_DATA_SIZE];   // Data
    uint32 bufferOffset;                  // Position in a buffer
    bool8 isFrameValid;                   // Test, if a frame is valid
} LinkSpiReceiveBuffer;

#include "strpack1.h"
typedef PACKED_PRE struct _tag_LINK_SPI_COMMAND {
// command layout
    uint8       spiPacketHeader;    // SPI_COMMAND_PACKET_HEADER
    PACKED_PRE struct {
        uint8   cmdValue : 6;         // command value
        uint8   response : 1;         // 0 for commands, 1 for resposes
        uint8   error : 1;            // an error occured during command processing, examine CmdData.Status
    } PACKED_POST cmdHeader;
    uint8       crc;                // 8-bit CRC which covers whole command
    PACKED_PRE union {
        sint16  status;             // status value for errors
        uint8   params[SPI_PACKET_SIZE-3];
    } PACKED_POST cmdData;
} PACKED_POST LINK_SPI_COMMAND, *PLINK_SPI_COMMAND;
#include "stoppack.h"

// === Globals =====
LinkSpiReceiveBuffer gLinkSpiReceiveBuffer;
uint8 gLinkSpiVirtualSignalsRequired;
bool8 gLinkSpiLastIsAwake = FALSE;		//result of last check, if device is awake

// --- Statistics ---

#ifdef LINK_SPI_STATISTICS
uint32 statReceivedMiniframes = 0;
uint32 statReceivedEmptyMiniframes = 0;
uint32 statReceivedIncorrectMiniframes = 0;
#endif //LINK_SPI_STATISTICS

// === Functions ===

/* Function initializes a link layer.
*/
PT_STATUS LinkSpiInitialize(void)
{
    PT_STATUS status = PT_STATUS_OK;
    status = SpiInitialize();
    
    gLinkSpiReceiveBuffer.bufferOffset = 0;
    gLinkSpiReceiveBuffer.pBuffer[0] = 0;
    gLinkSpiReceiveBuffer.isFrameValid = FALSE;
    
    gLinkSpiVirtualSignalsRequired = LINK_SPI_VIRTUAL_SIGNALS_UNKNOWN; //it will be queried by the first testin of awake signal

#ifdef LINK_SPI_STATISTICS    
    UiPrint("LinkSpi stat: statReceivedMiniframes=%d, statReceivedEmptyMiniframes=%d, statReceivedIncorrectMiniframes=%d\n\r", 
        statReceivedMiniframes, statReceivedEmptyMiniframes, statReceivedIncorrectMiniframes);
#endif //LINK_SPI_STATISTICS

    return status;        
}

/* Link layer destructor.
*/
void LinkSpiDestroy(void)
{
    SpiDestroy();
}

/*Internal function. Stores a miniframe received during a LinkSpiSendFrame command.
* @param pMiniframeBuffer Pointer to a miniframe.
* @param pOutputBuffer	Pointer to a beginning of buffer, where data are stored.
* @return PT_STATUS_NOT_ENOUGH_MEMORY, if there was received more data than a frame, PT_STATUS_OK otherwise.
*/
PT_STATUS LinkSpiSaveReceivedData(uint8 *pMiniframeBuffer, uint8 *pOutputBuffer)
{
    PT_STATUS status = PT_STATUS_OK;
    uint8 frameHeader = pMiniframeBuffer[0];
    bool8 frameStart = FALSE;
        
    //test, if a miniframe is a beginnig of a new frame
    if (LINK_SPI_IS_FRAME_START(frameHeader)) 
    {      
        gLinkSpiReceiveBuffer.isFrameValid = FALSE;
        gLinkSpiReceiveBuffer.bufferOffset = 0;
        frameStart = TRUE;
    }

    //advance in buffer behind up to now received data
    pOutputBuffer += gLinkSpiReceiveBuffer.bufferOffset;
    
    //copy a miniframe to an intermediate buffer
    if (frameStart || (gLinkSpiReceiveBuffer.bufferOffset != 0)) 
    {
        uint32 copyLength = MAX_FRAME_DATA_SIZE - gLinkSpiReceiveBuffer.bufferOffset;
        if(SPI_PACKET_DATA_SIZE <= copyLength)
        {
            copyLength = SPI_PACKET_DATA_SIZE;
        } else {
            //miniframe doesn't fit to an intermediate buffer
            //if there is received whole frame, let decide upper layer from a CRC what to do
            if(!LINK_SPI_IS_FRAME_END(frameHeader))
            {
                status = PT_STATUS_LL_OVERFLOW;
                goto lbExit;
            }
        }

        PT_memcpy(pOutputBuffer, pMiniframeBuffer+SPI_PACKET_HEADER_SIZE, copyLength); //skip header 1B
        gLinkSpiReceiveBuffer.bufferOffset += copyLength;
    }
    
    //test, if a miniframe was a last part of a frame
    if (LINK_SPI_IS_FRAME_END(frameHeader)) 
    {
        gLinkSpiReceiveBuffer.isFrameValid = TRUE;
    }
    
lbExit:;    
    return status;
}


/* Function computes CRC for a command.
* @param command Command.
* @return Crc value;
*/
uint8 LinkSpiComputeCommandCrc(void *command)
{
    uint8 idx;
    uint8 crc = 0;
    uint8 *it = (uint8*)command+1; //CRC is computed without a command header

    for(idx=0; idx<SPI_PACKET_SIZE-1; idx++, it++)
    {
        crc = (uint8)((uint8)(crc >> 4) | (crc << 4));
        crc ^= *it;
        crc ^= (uint8)(crc & 0xf) >> 2;
        crc ^= (crc << 4) << 2;
        crc ^= ((crc & 0xf) << 2) << 1;
    }

    return(crc);
}

/* Splits a frame into a miniframes, adds a miniframe header and CRC, and sendouts at SPI level. 
* @param pBuffer Buffer containing a frame data (input).
* @param bufferSize Size of data (max. MAX_FRAME_DATA_SIZE, input).
* @param timeout Timeout interval (input). 
* @return Status of an operation.
*/
PT_STATUS LinkSpiSendFrame(IN uint8 *pBuffer, IN uint32 bufferSize, IN sint32 timeout)
{
    PT_STATUS status = PT_STATUS_COMM_ERROR;
    uint8 miniframeBuffer[SPI_PACKET_SIZE];
    uint8 receiveMiniframeBuffer[SPI_PACKET_SIZE];
    uint32 remainingSize = bufferSize;
    uint32 copySize;
    uint32 startTime;

   // get a start time
    startTime = PT_TimerGetMilliseconds(); 
    
    if(pBuffer == NULL || bufferSize == 0)
    {
        //Nothing to send, buffer is empty
        goto lbExit;
    }
    
    //erase a miniframe
    PT_memset(receiveMiniframeBuffer, 0, SPI_PACKET_DATA_SIZE);
    PT_memset(miniframeBuffer, 0, SPI_PACKET_DATA_SIZE);
    
    //create a packet header (1B)
    miniframeBuffer[0] = LINK_SPI_DATA_PACKET_HEADER(TRUE, remainingSize <= SPI_PACKET_SIZE);
    
    //copy the first part of data (directly without a second byte of a header and CRC)
    copySize = remainingSize >= SPI_PACKET_DATA_SIZE ? SPI_PACKET_DATA_SIZE : remainingSize;
    PT_memcpy(miniframeBuffer+1, pBuffer, copySize);
    pBuffer += copySize;
    remainingSize -= copySize;
    
    for( ; ; )
    {    
        //timeout condition
        if((PT_TimerGetMilliseconds() - startTime > (uint32)timeout) && (timeout != -1))
        {
            status =  PT_STATUS_TIMEOUT; // timeout
            goto lbExit;
        }
    
        status = SpiSendAndReceiveMiniframe(miniframeBuffer, receiveMiniframeBuffer); //send out a miniframe to SPI layer and receive back data from a TFM/ESS
        if(status != PT_STATUS_OK){
            //Unable to send/receive a miniframe
            goto lbExit;
        }
        
#ifdef LINK_SPI_STATISTICS
        statReceivedMiniframes++;
#endif //LINK_SPI_STATISTICS        
       
        //test, if reply buffer contains valid data 
        if (LINK_SPI_IS_PACKET_VALID(receiveMiniframeBuffer[0])){
            //test, if packet contains some data
            if(LINK_SPI_IS_DATA_READY(receiveMiniframeBuffer[0])){
                if (LINK_SPI_IS_DATA_PACKET(receiveMiniframeBuffer[0])) {
                    status = LinkSpiSaveReceivedData(receiveMiniframeBuffer, gLinkSpiReceiveBuffer.pBuffer);
                    if(status != PT_STATUS_OK)
                    {
                        goto lbExit;
                    }
                    
                    // restart a timer, transfer is still alive and data valid
                    startTime = PT_TimerGetMilliseconds();
                }
            } else {
                //An empty packet received
                //Here should be no waiting for a COMM_POLLING_INTERVAL, an empty packet means a TCD is waiting for a data
                //and has nothing to send back yet. There would be a wait between all outgoing miniframes.
#ifdef LINK_SPI_STATISTICS
                statReceivedEmptyMiniframes++;
#endif //LINK_SPI_STATISTICS                 
            }
        } else {
            //Invalid reply received, retrying transfer
            PT_TimerDelayMicroseconds(COMM_POLLING_INTERVAL);
#ifdef LINK_SPI_STATISTICS
            statReceivedIncorrectMiniframes++;
            UiPrint("SF: Invalid %d#\n\r",statReceivedMiniframes);
#endif //LINK_SPI_STATISTICS            
            continue;
        }

             
        if (remainingSize == 0) {   // all data written, stop transfer
            break;
        }
                        
        // prepare the next frame
        miniframeBuffer[0] = LINK_SPI_DATA_PACKET_HEADER(FALSE, remainingSize <= SPI_PACKET_DATA_SIZE);
        copySize = remainingSize >= SPI_PACKET_DATA_SIZE ? SPI_PACKET_DATA_SIZE : remainingSize;
        PT_memcpy(miniframeBuffer+1, pBuffer, copySize);
        pBuffer += copySize;
        remainingSize -= copySize;  
        
        //let a TCD's handler time to process a previous request
        PT_TimerDelayMicroseconds(COMM_INTERMINIFRAME_INTERVAL);
    }
    
    if (remainingSize == 0){
        status = PT_STATUS_OK;
    } else {
        //timeout
        status = PT_STATUS_LL_TIMEOUT;
   }
 
lbExit:;    
    return status;
}

/* Receives a frame, checks its validity.
* @param pBuffer Output buffer containing received data(output).
* @param bufferSize Size of a buffer at the input, length of a received frame at the output (input).
* @param timeout Timeout interval (input).
* @return Status of an operation.
*/
PT_STATUS LinkSpiReceiveFrame(OUT uint8 *pBuffer, IN OUT uint32 *bufferSize, IN sint32 timeout)
{
    PT_STATUS status = PT_STATUS_OK;
    uint8 miniframeBuffer[SPI_PACKET_SIZE] = {0};
    uint8 emptyMiniframeBuffer[SPI_PACKET_SIZE] = {0};
    uint32 startTime;
    bool8 firstEmptyIncorrectMiniframe = TRUE;
  
    // get a start time
    startTime = PT_TimerGetMilliseconds(); 
    
    if(pBuffer == NULL || bufferSize == 0)
    {
        //There is no space to store a received frame
        status = PT_STATUS_INVALID_PARAMETER;
        goto lbExit;
    }

    //if gLinkSpiReceiveBuffer.pBuffer contains already some data, copy them to the output buffer
    if(gLinkSpiReceiveBuffer.bufferOffset != 0)
    {
        PT_memcpy(pBuffer, gLinkSpiReceiveBuffer.pBuffer, gLinkSpiReceiveBuffer.bufferOffset);   
    }
    
    emptyMiniframeBuffer[0] = LINK_SPI_PACKET_VALID;    //empty packet
    
    //while an intermediate buffer does not contain a valid frame, receive miniframes
    while(!gLinkSpiReceiveBuffer.isFrameValid)
    {    
        //timeout condition
        if((PT_TimerGetMilliseconds() - startTime > (uint32)timeout) && (timeout != -1))
        {
            status =  PT_STATUS_TIMEOUT; // timeout
            goto lbExit;
        }
    
        status = SpiSendAndReceiveMiniframe(emptyMiniframeBuffer, miniframeBuffer);    //read a miniframe from a TFM/ESS
        if(status != PT_STATUS_OK){
            //Unable to send/receive a miniframe
            goto lbExit;
        }

#ifdef LINK_SPI_STATISTICS
        statReceivedMiniframes++;
#endif //LINK_SPI_STATISTICS
        
        if (LINK_SPI_IS_PACKET_VALID(miniframeBuffer[0])){
            //test, if packet contains some data
            if(LINK_SPI_IS_DATA_READY(miniframeBuffer[0])){
                // reply buffer contains valid data
                if (LINK_SPI_IS_DATA_PACKET(miniframeBuffer[0])) {
                    //Data packet received during write
                    status = LinkSpiSaveReceivedData(miniframeBuffer, pBuffer);
                    if(status != PT_STATUS_OK)
                    {
                        goto lbExit;
                    }
                    
                    // restart a timer, transfer is still alive and data valid
                    startTime = PT_TimerGetMilliseconds();

                    firstEmptyIncorrectMiniframe = TRUE;
                }
            } else {
                //An empty packet received

                if(!firstEmptyIncorrectMiniframe)
                {
                    PT_TimerDelayMicroseconds(COMM_POLLING_INTERVAL);
                }
                firstEmptyIncorrectMiniframe = FALSE;
#ifdef LINK_SPI_STATISTICS
                statReceivedEmptyMiniframes++;
#endif //LINK_SPI_STATISTICS                
            }
        } else {
            //Invalid reply received, retrying transfer
            if(!firstEmptyIncorrectMiniframe)
            {
                PT_TimerDelayMicroseconds(COMM_POLLING_INTERVAL);
            }
            firstEmptyIncorrectMiniframe = FALSE;
#ifdef LINK_SPI_STATISTICS
            statReceivedIncorrectMiniframes++;
            UiPrint("RF: Invalid %d#\n\r",statReceivedMiniframes);
#endif //LINK_SPI_STATISTICS            
            continue;
        }
        
        //let a TCD's handler time to process a previous request
        PT_TimerDelayMicroseconds(COMM_INTERMINIFRAME_INTERVAL);
    }

    //test, if an output buffer is big enough to handle a frame
    if(*bufferSize < gLinkSpiReceiveBuffer.bufferOffset)
    {
        //Buffer too small for this frame
        status = PT_STATUS_LL_OVERFLOW;
        goto lbExit;
    }     
    
lbExit:; 
    gLinkSpiReceiveBuffer.bufferOffset = 0;
    gLinkSpiReceiveBuffer.isFrameValid = FALSE;   
    
    if(status != PT_STATUS_OK)
        *bufferSize = 0; //returned no data

    return status;
}

/** Functions sends a command to the device.
* @param command Command number (input).
* @param pReplyData A buffer, where a reply is stored (input, NULL, if no reply is requested).
* @param replySize A size of reply buffer (input).
* @param timeout A timeout interval (input, -1 means inifinity)
* @return Status of an operation.
**/
PT_STATUS LinkSpiSendCommand(IN LinkSpiCommands command, OUT uint8 *pReplyData OPTIONAL, IN uint32 replySize, IN sint32 timeout){
    
    PT_STATUS status = PT_STATUS_OK;
    uint8 miniframeBuffer[SPI_PACKET_SIZE] = {0};
    LINK_SPI_COMMAND cmd;
    bool8 resendCommand = FALSE;
    uint32 startTime;
    
    // get a start time
    startTime = PT_TimerGetMilliseconds(); 
    
    //create a packet content
    PT_memset(&cmd, 0, sizeof(LINK_SPI_COMMAND));
    cmd.spiPacketHeader = LINK_SPI_COMMAND_PACKET_HEADER;
    cmd.cmdHeader.cmdValue = (uint8)command;
    cmd.crc = LinkSpiComputeCommandCrc(&cmd);

    //send out a miniframe to SPI layer and receive back data from a TFM/ESS
    status = SpiSendAndReceiveMiniframe(&cmd, miniframeBuffer);
    if(status != PT_STATUS_OK){
        //Unable to send/receive a miniframe
        goto lbExit;
    }
    
#ifdef LINK_SPI_STATISTICS
    statReceivedMiniframes++;
#endif //LINK_SPI_STATISTICS    
   
    //test, if a received packet is valid
    if(LINK_SPI_IS_PACKET_VALID(miniframeBuffer[0]))
    {
        //test, if packet contains some data
        if(LINK_SPI_IS_DATA_READY(miniframeBuffer[0]))
        {
            //test, if there has come a reply to the command or a some data
            if (LINK_SPI_IS_DATA_PACKET(miniframeBuffer[0])) 
            {
                //"A data packet received during sending a command
                status = LinkSpiSaveReceivedData(miniframeBuffer, gLinkSpiReceiveBuffer.pBuffer);
                if(status != PT_STATUS_OK)
                {
                    goto lbExit;
                }
                
                // no need to restart a timer, a reply to command should be short
            }
        }
    }
    
    //if a reply is requested, get it
    if(pReplyData != NULL)
    {
        status = PT_STATUS_TIMEOUT;

        do {
            PT_TimerDelayMicroseconds(COMM_COMMAND_REQUEST_REPLY_INTERVAL);    //make a break between miniframe transmissions
            
            PT_memset(miniframeBuffer, 0, SPI_PACKET_SIZE);
            miniframeBuffer[0] = LINK_SPI_PACKET_VALID;               //empty packet
           
            if (resendCommand){                
                //send out a command again to SPI layer
                status = SpiSendAndReceiveMiniframe(&cmd, miniframeBuffer);
                if(status != PT_STATUS_OK){
                    //Unable to send/receive a miniframe
                     goto lbExit;
                }
                
#ifdef LINK_SPI_STATISTICS
                statReceivedMiniframes++;
#endif //LINK_SPI_STATISTICS                
                
            } else {
                //send out an empty miniframe to SPI layer
                status = SpiSendAndReceiveMiniframe(miniframeBuffer, miniframeBuffer);
                if(status != PT_STATUS_OK){
                    //Unable to send/receive a miniframe
                    goto lbExit;
                }
                
#ifdef LINK_SPI_STATISTICS
                statReceivedMiniframes++;
#endif //LINK_SPI_STATISTICS  
              
            }
            
            if (LINK_SPI_IS_PACKET_VALID(miniframeBuffer[0])) {
                if (LINK_SPI_IS_DATA_READY(miniframeBuffer[0])) {
                    // reply buffer contains valid data
                    
                    //test, if there has come a reply to the command or a some data
                    if (LINK_SPI_IS_DATA_PACKET(miniframeBuffer[0])) 
                    {
                        //A data packet received as a response to a command
                        status = LinkSpiSaveReceivedData(miniframeBuffer, gLinkSpiReceiveBuffer.pBuffer);
                        if(status != PT_STATUS_OK)
                        {
                            goto lbExit;
                        }
                        
                        // no need to restart a timer, a reply to command should be short
                    } else {
                        PLINK_SPI_COMMAND cmdReply = (PLINK_SPI_COMMAND)miniframeBuffer;
                        uint8 receivedCrc = cmdReply->crc;

                        cmdReply->crc = 0;
                        if (receivedCrc == LinkSpiComputeCommandCrc(cmdReply)){
                            //Check that response corresponds to the sent command, otherwise skip it 
                            if((cmdReply->cmdHeader.response == TRUE) && (cmdReply->cmdHeader.cmdValue == command))
                            {
                                if(!cmdReply->cmdHeader.error) {
                                    // all done
                                    status = PT_STATUS_OK;
                                    if ((pReplyData != NULL) && (replySize > 0)){
                                        // copy received data
                                        uint32 copySize = SPI_PACKET_DATA_SIZE - 2;
    
                                        if (copySize > replySize) {
                                            copySize = replySize;
                                            }
                                        PT_memcpy(pReplyData, &(cmdReply->cmdData.params), copySize);
                                        }
                                } else {
                                    status = cmdReply->cmdData.status;
                                }
                            }
                        } else {
                            //Invalid CRC %x in command reply, %x expected
                            status = PT_STATUS_LL_BAD_CRC;
#ifdef LINK_SPI_STATISTICS
                            statReceivedIncorrectMiniframes++;
                            UiPrint("SC1: Invalid %d#\n\r",statReceivedMiniframes);
#endif //LINK_SPI_STATISTIC                            
                        }
                        break;
                    }
                } else {
                    //Empty packet received, retrying request
                    if(!resendCommand)
                    {
                        PT_TimerDelayMicroseconds(COMM_POLLING_INTERVAL);
                    }
                    
#ifdef LINK_SPI_STATISTICS
                    statReceivedEmptyMiniframes++;
#endif //LINK_SPI_STATISTICS                    
                }

                resendCommand = FALSE;
            } else {
                resendCommand = TRUE;       // invalid packet, expect command wasn't received correctly
                PT_TimerDelayMicroseconds(COMM_POLLING_INTERVAL);
                
#ifdef LINK_SPI_STATISTICS
                statReceivedIncorrectMiniframes++;
                UiPrint("SC2: Invalid %d#\n\r",statReceivedMiniframes);
#endif //LINK_SPI_STATISTICS                
            }
        } while((timeout == -1) || (PT_TimerGetMilliseconds() - startTime < (uint32)timeout)); //timeout condition
    }
      
lbExit:;  
    return status;
}

/* Function tests, if a TCD device is awake.
* @return One if awake, 0 otherwise.
*/
bool8 LinkSpiIsAwake(void)
{
    uint8 statusBits = 0;
    PT_STATUS status = PT_STATUS_OK;

#ifndef NO_HARDWARE_AWAKE   //awake state is tested by a hardware line
    gLinkSpiLastIsAwake = SpiIsAwake();
    
    if(gLinkSpiLastIsAwake && (gLinkSpiVirtualSignalsRequired == LINK_SPI_VIRTUAL_SIGNALS_UNKNOWN)) 
    {   //we should find out, if device requires virtual signals
    
#else   //we should try, if a device responds to our requests and get a current state of virtual signals
    {
        //we can't do HW check, if device is responsive
        //if it was last time sleeping, supposingly it is sleeping now as well => wake it up up HW before asking for its state
        if(!gLinkSpiLastIsAwake)
        {
            //wakeup by hardware
            SpiWakeUp();
        }

#endif //NO_HARDWARE_AWAKE, there must be supported virtual signals
        
        //get a status of a device
        //device might be sleeping, in this case it will not provide valid response
        status = LinkSpiSendCommand(LINK_SPI_CMD_GET_STATUS, (uint8*)(&statusBits), sizeof(statusBits), COMM_VIRTUAL_COMMAND_TIMEOUT);
        if(status == PT_STATUS_OK)
        {			
            gLinkSpiVirtualSignalsRequired = (statusBits & LINK_SPI_STATUS_VIRTUAL_REQUIRED_BIT) != 0;
            
            //if there are supported virtual signals, get a status
            if(gLinkSpiVirtualSignalsRequired)
            {
                gLinkSpiLastIsAwake = (statusBits & LINK_SPI_STATUS_AWAKE_BIT) != 0;
            }
        }
    }
    return gLinkSpiLastIsAwake;
}

/* Function suspends a device.
* @param timeout Timeout interval of an operation.
* @return PT_STATUS_OK, if device was woken up; PT_STATUS_TIMEOUT, if timer expired.
*/
PT_STATUS LinkSpiSuspend(IN sint32 timeout)
{
    PT_STATUS status = PT_STATUS_OK;

    //try to sleep device by a report command, no reply is expected
    if(gLinkSpiVirtualSignalsRequired)
    {
        status = LinkSpiSendCommand(LINK_SPI_CMD_SLEEP,NULL,0, timeout);
    }

    return status;
}

/* Function wakes up a device
* @param timeout Timeout interval of an operation.
* @return PT_STATUS_OK, if device was woken up; PT_STATUS_TIMEOUT, if timer expired.
*/
PT_STATUS LinkSpiWakeUp(sint32 timeout)
{
    PT_STATUS status = PT_STATUS_OK;
    uint32 startTime;
    uint32 isAwakeStartTime;
    bool8 isAwake = FALSE;

     // get a start time
    startTime = PT_TimerGetMilliseconds();    

    //wakeup by hardware
    SpiWakeUp();

    do {
        //Waking up device by report command
        if((PT_TimerGetMilliseconds() - startTime > (uint32)timeout) && (timeout != -1))
        {
            status =  PT_STATUS_TIMEOUT; // timeout
            goto lbExit;
        }

        //try to wake up device by a report command, no reply is expected
        if(gLinkSpiVirtualSignalsRequired != LINK_SPI_VIRTUAL_SIGNALS_NOT_SUPPORTED)
        {
            status = LinkSpiSendCommand(LINK_SPI_CMD_WAKEUP,NULL,0, COMM_VIRTUAL_COMMAND_TIMEOUT);
        } else {
            status = PT_STATUS_OK; //should never happen on a TCD50
        }

        //wait, till TFM/ESS becomes operable, each 300ms resend a wakeup command
        isAwakeStartTime = PT_TimerGetMilliseconds(); 
        while(!isAwake && (PT_TimerGetMilliseconds() - isAwakeStartTime < 300))
        {
            isAwake = LinkSpiIsAwake();
        }
    } while(!isAwake);  //test, if TFM/ESS is still sleeping   
   
lbExit:;
    return status;
}
