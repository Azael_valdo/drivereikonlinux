/**
 * @file linkspi.h
 * Link layer interface for ESS/TFM communication protocol. Provides a frame construction / fragmentation to miniframes, CRC control in case of @c command @c reports.
 *
 * Copyright (C) 2008 UPEK Inc.
 *
 * @author Martin Krahulik
 */


#ifndef _LINK_SPI_H_
#define _LINK_SPI_H_

#ifndef DOXYGEN_PASS //tfmtypes.h makes problems to doxygen parser
    #include "types.h"    //include types definition
    #include "defines.h"
    #include "tfmtypes.h" //PT_STATUS definition
#endif

/**Special commands for a communication with a TFM/ESS.**/
typedef enum _tag_LinkSpiCommands{
    LINK_SPI_CMD_CONN_RESET     = 0x1, ///< Communication reset command.
    LINK_SPI_CMD_WAKEUP         = 0x2, ///< Communication wake up command.
    LINK_SPI_CMD_SLEEP          = 0x3,  ///< Communication sleep command.
    LINK_SPI_CMD_GET_STATUS     = 0x4  ///< Commands gets a status of virtual signals.
} LinkSpiCommands;

/** Function initializes a link layer.
**/
PT_STATUS LinkSpiInitialize(void);

/** Link layer destructor.
**/
void LinkSpiDestroy(void);

/** Function splits a frame into miniframes, adds miniframe headers, and sends out at SPI level.
* @param pBuffer Buffer containing a frame data (input).
* @param bufferSize Size of data (max. MAX_FRAME_DATA_SIZE, input).
* @param timeout Timeout interval (input).
* @return Status of an operation.
**/
PT_STATUS LinkSpiSendFrame(IN uint8 *pBuffer, IN uint32 bufferSize, IN sint32 timeout);

/** Function receives a frame, checks its validity.
* @param pBuffer Output buffer containing received data(output).
* @param bufferSize Size of a buffer at the input, length of a received frame at the output (input).
* @param timeout Timeout interval (input).
* @return Status of an operation.
**/
PT_STATUS LinkSpiReceiveFrame(OUT uint8 *pBuffer, INOUT uint32 *bufferSize, IN OPTIONAL sint32 timeout);

/** Functions sends a command to the device.
* @param command Command number (input).
* @param pReplyData A buffer, where a reply is stored (input, NULL, if no reply is requested).
* @param replySize A size of reply buffer (input).
* @param timeout A timeout interval (input, -1 means inifinity)
* @return Status of an operation.
**/
PT_STATUS LinkSpiSendCommand(IN LinkSpiCommands command, OUT OPTIONAL uint8 *pReplyData, IN OPTIONAL uint32 replySize, IN sint32 timeout);

/** Function tests, if a device is awake.
* @return TRUE, if awake; FALSE otherwise.
**/
bool8 LinkSpiIsAwake(void);

/* Function suspends a device.
* @param dwTimeout Timeout interval of an operation.
* @return PT_STATUS_OK, if device was woken up; PT_STATUS_TIMEOUT, if timer expired.
*/
PT_STATUS LinkSpiSuspend(IN sint32 timeout);

/** Function wakes up a device
* @param dwTimeout Timeout interval of an operation (input, -1 means infinity).
* @return PT_STATUS_OK, if device was woken up; PT_STATUS_TIMEOUT, if timer expired.
**/
PT_STATUS LinkSpiWakeUp(IN sint32 timeout);

#endif //_LINK_SPI_H_
