/**
 * @file defines.h
 *
 * Values, which can be set/changed by the user of the Comm library
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#ifndef __DEFINES_H__
#define __DEFINES_H__

//========================================================================
//      Communication Interface selection
//
//   Uncomment lines below to implement one of the following interfaces
//
//========================================================================

/**
* Definition of a low level communication interface (USE_USB, USE_SPI, USE_UART).
**/
#if !defined(USE_USB) && !defined(USE_SPI) && !defined(USE_UART)
// #define USE_USB
#define USE_SPI
// #define USE_UART
#endif

//Uncomment not to use AWAKE signal hardware detection
//#define NO_HARDWARE_AWAKE

//Uncomment for TCD50D with TCS1/TCS2 sensor (TCD50v3) in order to allow
//508DPI image grabbing with PTGrab(PT_GRAB_TYPE_508_508_8_SCAN508_508_8)
//Keep commented out for all other hardware configurations.
//#define TCD50D_TCS1_TCS2

//========================================================================
//      Constants
//========================================================================

/**
 * Communication bit rate for communication with ESS/TFM.\ Ignored by USB and SPI communication interfaces.
 * Max. 115200 bps, for ESS 2.x even 230400 bps.
 * Has to be one of the LL_SIO_BAUDRATE_xxxx constants
 */
#define COMM_BIT_RATE       LL_SIO_BAUDRATE_115200


/**
 * List of pairs {VendorID, Product ID} for supported USB devices
 */
#define COMM_USB_VIDS_PIDS \
	{\
		{0x0483,0x2016},/* TCD21, TCD41, TCD42*/ \
		{0x147E,0x2016},/* TCD50*/ \
	}



/**
 * AWAKE after WAKEUP timeout in milliseconds.\ Ignored by a SPI communication interface.
 */
#define COMM_AWAKE_TIMEOUT   4000

/**
 * Timeout in ms, in which SPI virtual command should be processed.
 */
#define COMM_VIRTUAL_COMMAND_TIMEOUT 60000

/**
 * Communication timeout in milliseconds. After the timeout the frame will be considered lost and resent.
 * The length of timeout depends on comm bit rate (it has to allow at least enough time
 * to safely send biggest possible frame there and back). And to keep overhead low
 * it should not be less than 1 second.
 */
#define COMM_TIMEOUT        1500


/**
 * WTX (keep-alive) timeout in milliseconds.\ Ignored by a SPI communication interface. 
 * Time, after which ESS/TFM sends WTX packets to avoid timeouts.
 * Recommended length is 70% of COMM_TIMEOUT
 */
#define COMM_WTX_TIMEOUT    1000


/**
 * Number of tries to send a frame before the connection is considered broken.\ Ignored by a SPI communication interface. 
 * Recommended 2-3 tries. Minimum 1. Value 0 is illegal!
 */
#define COMM_TRIES          2


/**
 * Maximal length of the data part of one communication frame.
 * IMPORTANT: 
 *  MAX_FRAME_DATA_SIZE must not be longer than 2047 (1024 for SPI)
 *  MAX_FRAME_DATA_SIZE must not be shorter than the longest expected received data 
 * (with the exception of PTGrab, PTSleepThanGrab, PTCapture, PTSleepThenCapture, PTEnroll, PTLoadFinger, and PTListAllFingers commands)
 */
#if defined(USE_SPI)
    #define MAX_FRAME_DATA_SIZE   1024
#else
    #define MAX_FRAME_DATA_SIZE   2047
#endif //#if !defined(USE_SPI)

/**
 * Size of statically allocated frame buffer holding one or more communication frames exchanged with device.
 * Must be multiple of MAX_FRAME_DATA_SIZE (due to error recovery protocol).
 * Maximal size of multi-alpha template is currently 2286B + 146B payload, signed data are not supported by this library.
 * Functions taking advantage of frame buffer size bigger than MAX_FRAME_DATA_SIZE:
 *      PTCapture, PTSleepThenCapture, PTEnroll, PTLoadFinger, PTListAllFingers.
 */
#if defined(USE_SPI)
    #define FRAME_BUFFER_SIZE     3 * MAX_FRAME_DATA_SIZE
#else
    #define FRAME_BUFFER_SIZE     2 * MAX_FRAME_DATA_SIZE
#endif //#if !defined(USE_SPI)

/**
 * Flag whether active state of AWAKE signal is in logical 0 or 1 (0 is default).
 */
#define COMM_AWAKE_ACTIVE_STATE     0

/**
 * Interval in microseconds in between consecutive miniframes on SPI interface.
 */
#define COMM_INTERMINIFRAME_INTERVAL            5  

/**
 * Interval in microseconds in between command request and reply check on SPI interface.
 */
#define COMM_COMMAND_REQUEST_REPLY_INTERVAL     10  


//=== TCD50 with area sensor recommended values ===
#if defined(TCD50D_TCS1_TCS2)
/** 
 *Add support of asynchronous callbacks transfer of image data.
 */ 
#define ENABLE_ASYNC_IMG_TRANSFER

/**
 * A time in microseconds between a frame sending to a TCD and the first polling for reply.\ Applies to a first frame of a command or a single-frame command.
 * Currently used by a SPI communication interface only.
 */
#define COMM_FIRST_REQUEST_TO_REPLY_INTERVAL 50

/**
 * A time in microseconds between a frame sending to a TCD and the first polling for reply.\ Applies to middle/last/continue frames of a command.
 * Currently used by a SPI communication interface only.
 */
#define COMM_CONTINUE_REQUEST_TO_REPLY_INTERVAL 50

/**
 * A polling interval in microseconds applied, if the first reply on a command is empty or if received data are invalid.
 * Currently used by a SPI communication interface only.
 */
#define COMM_POLLING_INTERVAL  50

/**
 * A delay between communication reset and checking for its result.
  * Currently used by a SPI communication interface only.
 */
#define COMM_RESET_INTERVAL 500

#else //#if defined(TCD50D_TCS1_TCS2)
//=== TCD50 with strip sensor recommended values ===
/**
 * A time in microseconds between a frame sending to a TCD and the first polling for reply.\ Applies to a first frame of a command or a single-frame command.
 * Currently used by a SPI communication interface only.
 */
#define COMM_FIRST_REQUEST_TO_REPLY_INTERVAL 10000

/**
 * A time in microseconds between a frame sending to a TCD and the first polling for reply.\ Applies to middle/last/continue frames of a command.
 * Currently used by a SPI communication interface only.
 */
#define COMM_CONTINUE_REQUEST_TO_REPLY_INTERVAL 3000

/**
 * A polling interval in microseconds applied, if the first reply on a command is empty or if received data are invalid.
 * Currently used by a SPI communication interface only.
 */
#define COMM_POLLING_INTERVAL  20000

/**
 * A delay between communication reset and checking for its result.
 * Currently used by a SPI communication interface only.
 */
#define COMM_RESET_INTERVAL 0

#endif //#if defined(TCD50D_TCS1_TCS2)

#endif // __DEFINES_H__
