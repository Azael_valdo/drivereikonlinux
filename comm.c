 /**
 * @file comm.c
 *
 * General Communication Layer (transport+link layer) interface for ESS/TFM communication protocol
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */
 
#include "defines.h" 

#if defined(USE_USB)
  #include "commusb.c"
#elif defined(USE_SPI)
  #include "commspi.c"
#elif defined(USE_UART)
  #include "commuart.c"
#endif
