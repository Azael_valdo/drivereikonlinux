/**
 * @file types.h
 *
 * General types used internally by the ESS/TFM library
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#ifndef __TYPES_H__
#define __TYPES_H__

#if defined (__SYMBIAN32__)
#include <e32def.h>
#endif

#if defined (WIN32) && !defined (UEFI)
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <stdarg.h>
#include <stddef.h>
#endif


#ifndef offsetof
/**Macro calculating and offset of a variable within a structure.
* @param s_name Structure name.
* @param m_name Variable name.
* @return Offset value.
**/
#define offsetof(s_name,m_name)  (unsigned)&(((s_name *)0)->m_name)
#endif

//========================================================================
//      Types
//========================================================================

typedef signed char     sint8;      ///<Signed integer, 8-bits.
typedef unsigned char   uint8;      ///<Unsigned integer, 8-bits. 
typedef unsigned char   bool8;      ///<Boolean, 8-bits.
typedef signed short    sint16;     ///<Signed integer, 16-bits.
typedef unsigned short  uint16;     ///<Unsigned integer, 16-bits.
#if defined(_DOS)     /* DOS */
typedef signed long     sint32;     ///<Signed integer, 32-bits.
typedef unsigned long   uint32;     ///<Unsigned integer, 32-bits.
#else
typedef signed int      sint32;     ///<Signed integer, 32-bits.
typedef unsigned int    uint32;     ///<Unsigned integer, 32-bits.
#endif 
typedef float           float32;    ///<Floating point number, 32-bits.
typedef double          float64;    ///<Floating point number, 64-bits.


#if !defined (WIN32) || defined (UEFI)
#if !defined (BOOL)
#define BOOL uint32                 ///<Boolean, 32-bits.
#endif
#endif

#ifndef IN
#define IN                          ///<Input parameter of a function.
#endif

#ifndef OUT
#define OUT                         ///<Output parameter of a function.
#endif

#ifndef INOUT
#define INOUT                       ///<Input/output parameter of a function.
#endif

#ifndef FALSE
#define FALSE           (0)         ///<False value.
#endif

#ifndef TRUE
#define TRUE            (1)         ///<True value.
#endif

#ifndef NULL
#define NULL            (void *)(0) ///<Null pointer
#endif

// String literals for tracing purposes
// For ASCII:	x
// For UNICODE: L##x
// For SYMBIAN:	_L(x)	
#ifndef _T
    #if defined (__SYMBIAN32__)
        #define _T(x)	_L(x)
    #elif defined (UNICODE) || defined (_UNICODE)
        #define _T(x)	L##x
    #else
        #define _T(x)	x
    #endif
#endif

#ifndef UNREFERENCED_PARAMETER
// Macro must be empty for ARM compiler, otherwise we get lots of warnings

/**Macro calming down a compiler that a parameter is not referenced.
* @param x Parameter to be ignored.
**/
#define UNREFERENCED_PARAMETER(x)
#endif


#endif // __TYPES_H__



