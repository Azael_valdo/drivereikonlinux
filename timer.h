/**
 * @file timer.h
 *
 * Timer-related support functions, needed by the ESS/TFM comm library
 * 
 * Copyright (C) 2001-2006 UPEK Inc.
 */

#ifndef __TIMER_H__
#define __TIMER_H__

#include "types.h"
#include "comm.h"


//========================================================================
//		Exported functions
//========================================================================

void PT_TimerStart(void);
void PT_TimerTerminate(void);

/**
 *	Number of milliseconds since startup
 */
uint32  PT_TimerGetMilliseconds (void);


/**
 * Wait defined number of milliseconds
 * @return COMM_STATUS_OK if timeout elapsed, COMM_STATUS_ASYNC_POWERDOWN 
 *  in case asynchronous power down happened
 */
COMM_STATUS  PT_TimerDelayMilliseconds(uint32 dwMilliseconds);

/**
 * Wait defined number of microseconds
 * @return COMM_STATUS_OK if timeout elapsed, COMM_STATUS_ASYNC_POWERDOWN 
 *  in case asynchronous power down happened
 */
COMM_STATUS  PT_TimerDelayMicroseconds(uint32 dwMicroseconds);

/**
 * Checks whether timer is still running.
 * @return COMM_STATUS_OK if running, COMM_STATUS_ASYNC_POWERDOWN 
 *  in case asynchronous power down happened
 */
COMM_STATUS PT_TimerGetStatus(void);


#endif // __TIMER_H__



