/**
 * @file endian.h
 * The file was copied from ptintra tree.
 * Macros for converting integer values from little endian to host endian
 * and vice versa.
 *
 * While TFM device alwyas uses little-endian, host can be little endian or
 * big endian. This header provides macros for converting between thw two
 * byte orders.
 *
 * @author Martin Mitas <martin.mitas@upek.com>
 */

#if !defined(TFM_Endian_h)
#define TFM_Endian_h

#include "tfmtypes.h"

/* Constants describing supported byte orders. */
#define TFM_ENDIAN_BIG        4321
#define TFM_ENDIAN_LITTLE     1234

//#define TFM_ENDIAN TFMENDIAN_BIG

#if !defined(TFM_ENDIAN)
/* Detect byte order of the host. */
#if defined(__APPLE__)  &&  defined(__MACH__)
    /* Mac OS X */
    #include <machine/endian.h>
    #ifndef BYTE_ORDER
        #error Header <machine/endian.h> should define macro BYTE_ORDER.
    #endif
    #if BYTE_ORDER == LITTLE_ENDIAN
        #define TFM_ENDIAN     TFM_ENDIAN_LITTLE
    #elif BYTE_ORDER == BIG_ENDIAN
        #define TFM_ENDIAN     TFM_ENDIAN_BIG
    #else
        #error Unsupported byte order.
    #endif
#elif defined(__linux__)
    /* Linux */
    #include <endian.h>
    #ifndef __BYTE_ORDER
        #error Header <endian.h> should define macro __BYTE_ORDER  apestas.
    #endif
    #if __BYTE_ORDER == __LITTLE_ENDIAN
        #define TFM_ENDIAN     TFM_ENDIAN_LITTLE
    #elif __BYTE_ORDER == __BIG_ENDIAN
        #define TFM_ENDIAN     TFM_ENDIAN_BIG
    #else
        #error Unsupported byte order.
    #endif
#else
    /* Unless detected otherwise we will assume little-endian. */
    #define TFM_ENDIAN     TFM_ENDIAN_LITTLE
#endif

#endif /* !defined(TFM_ENDIAN) */

/* Macros which always reverse order of the bytes */
#define TFM_ENDIAN_SWAP16(x)  \
    ((PT_WORD)((((PT_WORD)(x) & 0xff00) >> 8) |  \
               (((PT_WORD)(x) & 0x00ff) << 8)))

#define TFM_ENDIAN_SWAP32(x)  \
    ((PT_DWORD)((((PT_DWORD)(x) & 0xff000000) >> 24) |  \
                (((PT_DWORD)(x) & 0x00ff0000) >>  8) |  \
                (((PT_DWORD)(x) & 0x0000ff00) <<  8) |  \
                (((PT_DWORD)(x) & 0x000000ff) << 24)))


/* Macros for swaping bytes depending on the host byte order. */
/* (e.g. HTOL16 swaps 16 bits, _H_ost endian to '_L_ittle') */
#if TFM_ENDIAN == TFM_ENDIAN_LITTLE
    #define HTOL16(x)    (x)
    #define HTOL32(x)    (x)
    #define LTOH16(x)    (x)
    #define LTOH32(x)    (x)
    #define HTOB16(x)    TFM_ENDIAN_SWAP16(x)
    #define HTOB32(x)    TFM_ENDIAN_SWAP32(x)
    #define BTOH16(x)    TFM_ENDIAN_SWAP16(x)
    #define BTOH32(x)    TFM_ENDIAN_SWAP32(x)
#elif TFM_ENDIAN == TFM_ENDIAN_BIG
    #define HTOL16(x)    TFM_ENDIAN_SWAP16(x)
    #define HTOL32(x)    TFM_ENDIAN_SWAP32(x)
    #define LTOH16(x)    TFM_ENDIAN_SWAP16(x)
    #define LTOH32(x)    TFM_ENDIAN_SWAP32(x)
    #define HTOB16(x)    (x)
    #define HTOB32(x)    (x)
    #define BTOH16(x)    (x)
    #define BTOH32(x)    (x)
#else
    #error Unsupported byte order.
#endif


#endif  /* TFM_Endian_h */

